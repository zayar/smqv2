//
//  VSView.h
//  zBox
//
//  Created by Zayar Cn on 5/11/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOAPRequest.h"
#import "DetailImagePopUpView.h"
@protocol CourseViewDelegate
- (void) courseShowPicPhotoBrowser:(NSString *)picURL andTitle:(NSString *)picTitle;
- (void) onCourseViewCancel;
@end
@interface CourseView : UIView<DetailImagePopUpViewDelegate,UIWebViewDelegate>
{
    //IBOutlet UIWebView * webView;
    id<CourseViewDelegate> owner;
    IBOutlet UIImageView * imgBgView;
    IBOutlet UITableView * tbl;
    DetailImagePopUpView * detailImgPopUpView;
    NSMutableArray * arrCata;
    SOAPRequest * getCatalogRequest;
    SOAPRequest * rateRequest;
    SOAPRequest * trackRequest;
    NSArray *_photos;
    
}
@property (nonatomic, retain) NSArray *photos;
@property (nonatomic, retain) UITableView *tbl;
@property (assign) id<CourseViewDelegate> owner;
- (void) loadWebView:(NSString *) strURL;
- (IBAction)onCancel:(id)sender;
- (void)vsViewDidLoad;
- (void)vsViewWillAppear;
- (void) vsViewWillDisAppear;
@end
