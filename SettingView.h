//
//  SettingView.h
//  zBox
//
//  Created by Zayar Cn on 6/5/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOAPRequest.h"
@protocol SettingViewDelegate 
-(void) onSettingViewCancel;
-(void) onSettingViewSave;
@end

@interface SettingView : UIView<SOAPRequestDelegate>
{
    IBOutlet UIImageView * imgBgView;
    IBOutlet UITextField * txtLoginName;
    IBOutlet UITextField * txtPassword;
    IBOutlet UILabel * lblName;
    IBOutlet UIScrollView * scrollView;
    id<SettingViewDelegate> owner;
    SOAPRequest * submitChangePassRequest;
}
@property (assign) id<SettingViewDelegate> owner;
-(void)settingViewViewDidLoad;
-(IBAction)onBack:(id)sender;
-(IBAction)onSave:(id)sender;
- (void)settingViewWillAppear;
@end
