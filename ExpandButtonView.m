//
//  ExpadnButtonView.m
//  zBox
//
//  Created by Zayar on 5/27/13.
//
//

#import "ExpandButtonView.h"

@implementation ExpandButtonView
@synthesize owner;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (IBAction)onActual:(id)sender{
    [owner onActualSelected:@"a.png"];
}

- (IBAction)onCouldve:(id)sender{
    [owner onCouldveSelected:@"c.png"];
}

- (IBAction)onCouldveActual:(id)sender{
    [owner onActualCouldveSelected:@"ac.png"];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
