//
//  PROthersViewController.m
//  zBox
//
//  Created by Zayar on 5/9/13.
//
//

#import "PROthersViewController.h"
#import "ObjSessionOther.h"
#import "ObjSessionDetail.h"
@interface PROthersViewController ()

@end

@implementation PROthersViewController
@synthesize objSession;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSString * strPath = [NSString stringWithFormat:@"%@/img_vs_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];
    [imgBgView release];
}

- (void)viewWillAppear:(BOOL)animated{
    [self loadTheOtherView:objSession];
}

- (void)loadTheOtherView:(ObjSubSession *)obj{
    ObjSessionDetail * objSecDetail = [obj.arrSessionDetail objectAtIndex:0];
    ObjSessionOther * objSessionOther = objSecDetail.objSessionOther;
    lblNextTopic.text = objSessionOther.strNextTopic;
    lblNextLesson.text = objSessionOther.strNextLesson;
    lblNextMilestone.text = objSessionOther.strNextMilestone;
    lblNextAdmin.text = objSessionOther.strAdmin;
    NSLog(@"others count %d",[obj.arrOthersSession count]);
    
    /*if([obj.arrOthersSession count]>0){
        int y=0;
        int i=0;
        for(ObjSessionOther * objSOther in obj.arrOthersSession){
            UILabel * lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, y, scrollOther.frame.size.width, 60)];
            if(i%2 == 0){
                lbl.backgroundColor = [UIColor lightGrayColor];
            }
            else{
                lbl.backgroundColor = [UIColor whiteColor];
            }
            lbl.text = [NSString stringWithFormat:@"%@:  %@",objSOther.strOtherName,objSOther.strOtherDes];
            lbl.font = [UIFont systemFontOfSize:13];
            lbl.numberOfLines = 0;
            [scrollOther addSubview:lbl];
            y += lbl.frame.size.height;
            i++;
            [lbl release];
        }
    }*/
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBack:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)dealloc{
    [scrollOther release];
     [imgBgView release];
     [lblTitle release];
     [objSession release];
     [lblNextTopic release];
     [lblNextLesson release];
     [lblNextMilestone release];
     [lblNextAdmin release];
    [super dealloc];
}

@end
