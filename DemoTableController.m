//
//  DemoTableControllerViewController.m
//  FPPopoverDemo
//
//  Created by Alvise Susmel on 4/13/12.
//  Copyright (c) 2012 Fifty Pixels Ltd. All rights reserved.
//

#import "DemoTableController.h"
#import "zBoxAppDelegate.h"

@interface DemoTableController ()

@end

@implementation DemoTableController
@synthesize owner;
- (void)viewDidLoad
{
    [super viewDidLoad];
    //self.title = @"Popover Title";
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
    }
    //cell.textLabel.text = [NSString stringWithFormat:@"cell %d",indexPath.row];
   
    if([indexPath row]==0){
        cell.textLabel.text =@"Actual";
    }
    else if([indexPath row]==1){
        cell.textLabel.text =@"Could've";
    }
    else if([indexPath row]==2){
       cell.textLabel.text =@"Could've & Actual"; 
    }
    cell.selectionStyle=UITableViewCellSelectionStyleGray;
    cell.textLabel.textAlignment=UITextAlignmentCenter;
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /* NewsHubAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    if([indexPath row]==0){
        
        [delegate closeFPPopover];
        
    }
    else if([indexPath row]==1){
        
//        SettingViewController *settingViewController=[[SettingViewController alloc]initWithNibName:@"SettingViewController" bundle:[NSBundle mainBundle]];
//        [self.navigationController pushViewController:settingViewController animated:YES];
//        [settingViewController release];
//        settingViewController=nil;
        [delegate showSetting];
    }*/
    if (indexPath.row == 0) {
        [owner onSelectActual];
    }
    else if(indexPath.row == 1){
        [owner onSelectCouldve];
    }
    else if(indexPath.row == 2){
        [owner onSelectActualAndCouldve];
    }
}

@end
