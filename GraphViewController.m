//
//  GraphViewController.m
//  zBox
//
//  Created by Zayar on 5/12/13.
//
//

#import "GraphViewController.h"
#import "CPTScatterPlot.h"
#import "CorePlot-CocoaTouch.h"
#import "EskPlotTheme.h"
#import "DemoTableController.h"
#import "EskLine2TouchPlot.h"

@interface GraphViewController ()

@end

@implementation GraphViewController
@synthesize objSubject;
@synthesize owner;
@synthesize lineHostingView,barHostingView,demoTableViewController;
/*#ifdef IOS_OLDER_THAN_6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    [image_signature setImage:[self resizeImage:image_signature.image]];
    return (toInterfaceOrientation == UIInterfaceOrientationMaskLandscapeRight);
    NSLog(@"did change orientation!!");
}
#endif
#ifdef IOS_NEWER_OR_EQUAL_TO_6
-(BOOL)shouldAutorotate {
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    [image_signature setImage:[self resizeImage:image_signature.image]];
    return UIInterfaceOrientationMaskLandscapeRight;
    NSLog(@"did change orientation!!");
}
#endif*/
#pragma mark - BarPlot delegate
- (void)barPlot:(EskBarPlot *)plot barWasSelectedAtRecordIndex:(NSUInteger)index
{
    /*selectedProductLabel.text = [NSString stringWithFormat:@"Selected Product: %@", [plot.sampleProduct objectAtIndex:index]];
    costPerUnitLabel.text = [NSString stringWithFormat:@"Cost Per Unit: $%@", [plot.sampleData objectAtIndex:index]];*/
}

#pragma mark - LinePlot delegate
- (void)linePlot:(EskLinePlot *)plot indexLocation:(NSUInteger)index
{
    /*selectedYearLabel.text = [NSString stringWithFormat:@"Selected Year: %@", [plot.sampleYears objectAtIndex:index]];
    spendingPerYearLabel.text = [NSString stringWithFormat:@"Spending Per Year: $%@", [plot.sampleData objectAtIndex:index]];*/
    [self showResultLabel:[plot.sampleData objectAtIndex:index] andHide:YES];
    
}

- (void)eskLine2TouchPlot2:(EskLine2TouchPlot *)plot indexLocation:(NSUInteger)index withFirstIndex:(NSUInteger)fristIndex{
    NSLog(@"here call back 2 !!and sec %@ and fir %@",[plot.sampleData objectAtIndex:index],[plot.sampleData objectAtIndex:fristIndex]);
    
    int fristScore=[[plot.sampleData objectAtIndex:fristIndex] intValue];
    int secScore = [[plot.sampleData objectAtIndex:index] intValue];
    int result = fristScore - secScore;
    [self showResultLabel:[NSString stringWithFormat:@"%d",result] andHide:YES];
    
}

- (void)eskLine2TouchPlot:(EskLine2TouchPlot *)plot indexLocation:(NSUInteger)index withSecondIndex:(NSUInteger)secIndex{
     NSLog(@"here call back 1 !!and sec %@ and fir %@",[plot.sampleData objectAtIndex:secIndex],[plot.sampleData objectAtIndex:index]);

    int fristScore=[[plot.sampleData objectAtIndex:index] intValue];
    int secScore = [[plot.sampleData objectAtIndex:secIndex]intValue];
    int result = fristScore - secScore;
    [self showResultLabel:[NSString stringWithFormat:@"%d",result] andHide:YES];
}

- (void)showResultLabel:(NSString *)str andHide:(BOOL)show{
    if (show) {
        lblDiff.hidden = FALSE;
        lblDiff.text = [NSString stringWithFormat:@"Diff is %@",str];
    }
    else{
        lblDiff.hidden = TRUE;
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeLeft animated:YES];
    
   [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    NSLog(@"view did load");
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return NO;
}

- (void)viewWillDisappear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

#pragma mark - UIViewController lifecycle methods
-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self.view bringSubviewToFront:btnClose];
    [self.view bringSubviewToFront:btnSet];
    [self setViewBounds];
    NSLog(@"graph view x:%f y:%f w:%f h:%f",self.view.frame.origin.x,self.view.frame.origin.y,self.view.frame.size.width,self.view.frame.size.height);
    self.view.frame = CGRectMake(0, 0, 320, 460);
}

- (void)setViewBounds{
    self.view.bounds = CGRectMake(0.0f, 0.0f, 480.0f, 320.0f);
}

- (void)viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    self.view.transform = CGAffineTransformIdentity;
    self.view.transform =CGAffineTransformMakeRotation(3.14 * (90) / 180.0);
    
    self.view.center = CGPointMake(160.0f, 240.0f);
    //self.view.frame = CGRectMake(-20, -20, 320, 460);
    
    //[self onSelectActual];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)  name:UIDeviceOrientationDidChangeNotification  object:nil];
}

- (IBAction)onSet:(id)sender{
    [self setDataLine];
}

- (void)setDataLine{
    if (aaplPlot != nil) {
        //aaplPlot.dataSourceLinePlot.opacity = 0.0f;
        //[graph addPlot:dataSourceLinePlot];
        //aaplPlot.
        CPTPlotSymbol *plotSymbol = [CPTPlotSymbol ellipsePlotSymbol];
        plotSymbol.fill = [CPTFill fillWithColor:[CPTColor whiteColor]];
        plotSymbol.size = CGSizeMake(10.0, 10.0);
        aaplPlot.plotSymbol = plotSymbol;
    }
}

/*- (BOOL) pointingDeviceDownEvent:(CPTNativeEvent *)event
                         atPoint:(CGPoint) 	interactionPoint
{
    
}*/



#pragma mark - CPTPlotDataSource methods

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
	return 0;
    //return 10;
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index {
	
	return [NSDecimalNumber zero];
}

-(void)scatterPlot:(CPTScatterPlot *)plot
plotSymbolWasSelectedAtRecordIndex:(NSUInteger)index
{
    //int plotNumber = 0;
    NSLog(@"index of %d",index);
    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    NSLog(@"here is change orientation");
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
	[super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
	
	/*if( isOrientationChanging ){
     if( toInterfaceOrientation ==  UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight){
     imgBg.frame = CGRectMake(0,0,768,1004);
     imgBg.bounds = imgBg.frame;
     [imgBg setLandscape];
     }
     else{
     imgBg.frame = CGRectMake(0,0,1024,748);
     imgBg.bounds = imgBg.frame;
     [imgBg setPortrait];
     }
     }*/
}

-(BOOL)plotSpace:(CPTPlotSpace *)space shouldHandlePointingDeviceDownEvent:(id)event atPoint:(CGPoint)point{
    NSLog(@"plot touch!!!!");
    return YES;
}

/*- (void) scatterPlot:(CPTScatterPlot *)plot
plotSymbolWasSelectedAtRecordIndex:(NSUInteger)idx
           withEvent:(CPTNativeEvent *)event{

}*/

- (IBAction)onClose:(id)sender{
    //[self dismissModalViewControllerAnimated:YES];
    [owner onGraphViewCancel:self];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) onSelectActual{
    [self closeFPPopover];
    if (lineHostingView != nil) {
        for (UIView * v in lineHostingView.subviews) {
            [v removeFromSuperview];
            [v release];
        }
    }
    //barHostingView.hidden = TRUE;
    //lineHostingView.hidden =FALSE;
    
    EskPlotTheme *defaultTheme = [[EskPlotTheme alloc] init];
    //CPStocksTheme *defaultTheme = [[CPStocksTheme alloc] init];
    [self.view bringSubviewToFront:lineHostingView];
    lineHostingView.frame = CGRectMake(0, 0, 480, 340);
    if (linePlot2 == nil) {
        linePlot2 = [[EskLine2TouchPlot alloc] init];
    }
    [linePlot2 loadTheDataWith:objSubject andIsActual:YES];
    linePlot2.delegate = self;
    [linePlot2 renderInLayer:lineHostingView withTheme:defaultTheme];
    
    [defaultTheme release];
    [self showTheLabels:NO];
    [btnSelector setTitle:@"Actual" forState:normal];
    [self bringToFrontView:lineHostingView];
}

- (void) onSelectCouldve{
    [self closeFPPopover];
    if (lineHostingView != nil) {
        for (UIView * v in lineHostingView.subviews) {
            [v removeFromSuperview];
            [v release];
        }
    }
    //barHostingView.hidden = TRUE;
    //lineHostingView.hidden =FALSE;
    
    EskPlotTheme *defaultTheme = [[EskPlotTheme alloc] init];
    //CPStocksTheme *defaultTheme = [[CPStocksTheme alloc] init];
    [self.view bringSubviewToFront:lineHostingView];
    lineHostingView.frame = CGRectMake(0, 0, 480, 340);
    if (linePlot2 == nil) {
        linePlot2 = [[EskLine2TouchPlot alloc] init];
    }
    [self bringToFrontView:lineHostingView];
    [linePlot2 loadTheDataWith:objSubject andIsActual:NO];
    linePlot2.delegate = self;
    [linePlot2 renderInLayer:lineHostingView withTheme:defaultTheme];
    
    [defaultTheme release];
    [self showTheLabels:NO];
    [btnSelector setTitle:@"Could Have" forState:normal];
}

- (void) onSelectActualAndCouldve{
    [self closeFPPopover];
    if (lineHostingView != nil) {
        for (UIView * v in lineHostingView.subviews) {
            [v removeFromSuperview];
            [v release];
        }
    }
    // lineHostingView.hidden =TRUE;
    //barHostingView.hidden = FALSE;
    EskPlotTheme *defaultTheme = [[EskPlotTheme alloc] init];
    //CPStocksTheme *defaultTheme = [[CPStocksTheme alloc] init];
    
    lineHostingView.frame = CGRectMake(0, 0, 480, 340);
    if (linePlot == nil) {
        linePlot = [[EskLinePlot alloc] init];
    }
    [self bringToFrontView:lineHostingView];
    [linePlot loadTheDataWith:objSubject];
    linePlot.delegate = self;
    [linePlot renderInLayer:lineHostingView withTheme:defaultTheme];
    /*barPlot = [[EskBarPlot alloc] init];
     barPlot.delegate = self;
     [barPlot renderInLayer:barHostingView withTheme:defaultTheme];*/
    
    [defaultTheme release];
    [self showTheLabels:YES];
    [btnSelector setTitle:@"Could Have & Actual" forState:normal];
}

- (void) showTheLabels:(BOOL) show{
    if (show) {
        capActual.hidden = FALSE;
        capCouldve.hidden = FALSE;
        capObjective.hidden = FALSE;
        lblActual.hidden = FALSE;
        lblCouldve.hidden = FALSE;
        lblObjective.hidden = FALSE;
        lblDiff.hidden = TRUE;
    }
    else{
        capActual.hidden = TRUE;
        capCouldve.hidden = TRUE;
        capObjective.hidden = TRUE;
        lblActual.hidden = TRUE;
        lblCouldve.hidden = TRUE;
        lblObjective.hidden = TRUE;
        lblDiff.hidden = FALSE;
    }
}

- (void)bringToFrontView:(UIView *)v{
    [self.view bringSubviewToFront:v];
    [self.view bringSubviewToFront:btnClose];
    [self.view bringSubviewToFront:capActual];
    [self.view bringSubviewToFront:capCouldve];
    [self.view bringSubviewToFront:capObjective];
    [self.view bringSubviewToFront:lblActual];
    [self.view bringSubviewToFront:lblObjective];
    [self.view bringSubviewToFront:lblCouldve];
    [self.view bringSubviewToFront:btnSelector];
    [self.view bringSubviewToFront:lblDiff];
}

- (void)dealloc{
    [capObjective release];
    [capCouldve release];
    [capActual release];
    [lblObjective release];
    [lblCouldve release];
    [lblActual release];
    [barHostingView release];
    [lineHostingView release];
    [linePlot release];
    [aaplPlot release];
    [objSubject release];
    [super dealloc];
}

@end
