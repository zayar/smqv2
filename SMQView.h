//
//  SMQView.h
//  zBox
//
//  Created by Zayar Cn on 5/9/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOAPRequest.h"
@protocol SMQViewDelegate 
-(void) onSMQViewCancel;
-(void) onSMQViewSuccessClose:(BOOL)isCamera;
@end

@interface SMQView : UIView<UIActionSheetDelegate,UIPickerViewDelegate>
{
    id<SMQViewDelegate> owner;
    UIImageView * imgView;
    UIImage * selectedImage;
    IBOutlet UIImageView * imgBgView;
    SOAPRequest * subjectsRequest;
    SOAPRequest * submitQuestionRequest;
    SOAPRequest * checkSPassRequest;
    NSMutableArray * arrCurrentSubject;
    
    UIPickerView *uiPickerView;
    UIActionSheet *menu;
    
   IBOutlet UIButton * btnSubject;
   IBOutlet UIButton * btnCategory;
    int intSubjectSelected;
    int intCategorySelected;
    BOOL isCamera;
    UITextField * txtSv;
}
@property BOOL isCamera;
@property (assign) id<SMQViewDelegate> owner;
@property (nonatomic, retain) IBOutlet UIImageView * imgView;
@property (nonatomic, retain) UIImage * selectedImage;
- (IBAction)onCancel:(id)sender;
- (void)smqViewDidLoad;
- (void)smqViewWillAppear;
- (void) textValidateAlertShow:(NSString *) strError;
- (void)syncSubject;
- (void)reloadSubjectView;
- (void)setImage:(UIImage *)img;
@end
