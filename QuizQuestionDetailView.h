//
//  QuizQuestionDetailView.h
//  zBox
//
//  Created by Zayar on 5/9/13.
//
//

#import <UIKit/UIKit.h>
#import "ObjQuizQuestion.h"
#import "RateView.h"
#import "SOAPRequest.h"
@protocol QuizQuestionDetailDelegate
- (void) showQuizQuestionPicPhotoBrowser:(NSString *)picURL andTitle:(NSString *)picTitle;
- (void) showQuizQuestionDetailView;
- (void) showQuizQuestionDetailView:(int)quiz_id;
- (void) onQuizQuestionDetailCancel;
@end
@interface QuizQuestionDetailView : UIView
{
    id<QuizQuestionDetailDelegate> owner;
    IBOutlet UIImageView * imgView;
    IBOutlet UIImageView * imgBgView;
    IBOutlet UIWebView * webView;
    IBOutlet UIActivityIndicatorView * activity;
    ObjQuizQuestion * objQuizQuestionDetail;
    SOAPRequest * rateRequest;
}
@property (assign) id<QuizQuestionDetailDelegate> owner;
@property (nonatomic, retain) IBOutlet RateView *rateView;
- (void)qqDViewDidLoad;
- (void)qqDViewWillAppear:(ObjQuizQuestion *)objQqd;
- (IBAction)onCancel:(id)sender;
- (IBAction)onViewSolution:(id)sender;

@end
