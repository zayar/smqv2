//
//  vsTableViewCell.m
//  zBox
//
//  Created by Zayar Cn on 6/5/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "vsTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation vsTableViewCell
@synthesize imgBgView,imgThumbView,webThumbView,lblTitle,lblText,activity,isWebLoaded, ratingBgView;
@synthesize rateView,owner;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        //isWebLoaded = FALSE;
        

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)cellViewLoad{
    self.rateView.notSelectedImage = [UIImage imageNamed:@"btn_rate_star_normal.png"];
    self.rateView.halfSelectedImage = [UIImage imageNamed:@"btn_rate_star_selected.png"];
    self.rateView.fullSelectedImage = [UIImage imageNamed:@"btn_rate_star_selected.png"];
    
    self.rateView.editable = YES;
    self.rateView.maxRating = 5;
    self.rateView.delegate = self;
}

- (void)rateSelected:(int)rate{
    self.rateView.rating = rate;
}

- (void)drawTextInRect:(CGRect)rect {
    
    
    
}

- (void)drawContent{
    self.lblText.text = @"test";
    UIFont * cfont = [UIFont fontWithName:@"BradyBunchRemastered" size:24.0];
    UIFont * sfont = [UIFont fontWithName:@"BradyBunchRemastered" size:16.0];
    //cell.lblTitle.font = cfont;
    //cell.lblText.font = sfont;
    CGPoint point = CGPointMake(1,1);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 0.7);
    CGContextSetRGBStrokeColor(context, 2, 2, 2, 1.0);
    CGContextSetTextDrawingMode(context, kCGTextFillStroke);
    CGContextSaveGState(context);
    
    // I change it to my outlet
    [self.lblTitle.text drawAtPoint:point withFont:cfont];
    [self.lblText.text drawAtPoint:point withFont:sfont];
    
    CGContextRestoreGState(context);
}

- (void)rateView:(RateView *)rateView ratingDidChange:(float)rating {
    NSLog(@"selected rate star %@",[NSString stringWithFormat:@"Rating: %f", rating]);
    [owner selectedRate:rating andIndex:self.rateView.tag];
}

-(void)dealloc{
    [rateView release];
    [activity release];
    [lblTitle release];
    [lblText release];
    [imgThumbView release];
    [webThumbView release];
    [imgBgView release];
    [super dealloc];
}
@end
