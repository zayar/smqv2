//
//  CustomPhotoGalleryView.h
//  zBox
//
//  Created by Zayar Cn on 6/4/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "CustomThumbIcon.h"
@protocol CustomPhotoGalleryDelegate 
-(void) onCustomPhotoGalleryCancel;
-(void) onCustomPhotoGalleryChoose:(UIImage *)selectedImage;
@end
@interface CustomPhotoGalleryView : UIView<CustomThumbIconDelegate>
{
    IBOutlet UIImageView * imgBgView;
    id<CustomPhotoGalleryDelegate> owner;
    NSMutableArray *_images;
	NSMutableArray *_thumbs;
    NSArray *arrLibraryPhotos;
	UIImage *_selectedImage;
    int previousIndex;
    UIScrollView *view;
    
    //library photo adding///
    ALAssetsGroup *assetGroup;
	
	NSMutableArray *elcAssets;
	int selectedAssets;
	
	id parent;
	
	NSOperationQueue *queue;
    //End library photo adding///
}
@property (nonatomic, assign) id parent;
@property (nonatomic, assign) ALAssetsGroup *assetGroup;
@property (nonatomic, retain) NSMutableArray *elcAssets;
@property (nonatomic, retain) IBOutlet UILabel *selectedAssetsLabel;

@property (nonatomic, retain) NSMutableArray *images;
@property (nonatomic, retain) NSMutableArray *thumbs;
@property (nonatomic, retain) NSMutableArray *assets;
@property (nonatomic, retain) NSArray * arrLibraryPhotos;
@property (nonatomic, retain) UIImage *selectedImage;
@property (assign) id<CustomPhotoGalleryDelegate> owner;
- (void)customPhotoGalleryViewDidLoad;
- (void)customPhotoGalleryViewWillAppear;
- (void)customPhotoGalleryViewWillDisAppear;
- (IBAction)onCancel:(id)sender;
- (IBAction)onChoose:(id)sender;
- (NSMutableArray *)loadNativePhotoLibrary;
@end
