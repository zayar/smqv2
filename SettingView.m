//
//  SettingView.m
//  zBox
//
//  Created by Zayar Cn on 6/5/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "SettingView.h"
#import "zBoxAppDelegate.h"
#import "ObjUser.h"
#import "StringTable.h"
#import "SOAPRequest.h"

@implementation SettingView
@synthesize owner;

//---size of keyboard---
CGRect keyboardBounds;
//---size of application screen---
CGRect applicationFrame;
//---original size of ScrollView---
CGSize scrollViewOriginalSize;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)settingViewViewDidLoad{
    NSString * strPath = [NSString stringWithFormat:@"%@/img_setting_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];
    [imgBgView release];

    UIFont * cfont = [UIFont fontWithName:@"BradyBunchRemastered" size:18.0];
    txtPassword.font = cfont;
    txtLoginName.font = cfont;

    UIFont * nfont = [UIFont fontWithName:@"BradyBunchRemastered" size:22.0];
    lblName.font = nfont;
    lblName.textColor = [UIColor darkGrayColor];

    scrollViewOriginalSize = scrollView.contentSize;
    applicationFrame = [[UIScreen mainScreen] applicationFrame];
}

- (void)settingViewWillAppear{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    ObjUser * objUser = [delegate.db getUserObj];
    lblName.text = objUser.strName;

    //---registers the notifications for keyboard---
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.window];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    txtPassword.text = @"";
    txtLoginName.text = @"";
}

-(void) moveScrollView:(UIView *) theView {
    //---get the y-coordinate of the view---
    CGFloat viewCenterY = theView.center.y + 20;

    //---calculate how much visible space is left---
    CGFloat freeSpaceHeight = applicationFrame.size.height - keyboardBounds.size.height;

    //---calculate how much the scrollview must scroll---
    CGFloat scrollAmount = viewCenterY - freeSpaceHeight / 2.0;
    if (scrollAmount < 0) scrollAmount = 0;

    //---set the new scrollView contentSize---
    scrollView.contentSize = CGSizeMake(applicationFrame.size.width, applicationFrame.size.height +keyboardBounds.size.height);

    //---scroll the ScrollView---
    [scrollView setContentOffset:CGPointMake(0, scrollAmount) animated:YES];
}

-(void) textFieldDidBeginEditing:(UITextField *)textFieldView {
    [self moveScrollView:textFieldView];
}

-(void) textFieldDidEndEditing:(UITextField *) textFieldView {

    [UIView beginAnimations:@"back to original size" context:nil];
    scrollView.contentSize = scrollViewOriginalSize;
    [UIView commitAnimations];
}

-(BOOL) textFieldShouldReturn:(UITextField *) textFieldView {
    if(textFieldView.tag == 1) {
        UIView * nextView = [self viewWithTag:2];
        [nextView becomeFirstResponder];
        return NO;
    }
    [textFieldView resignFirstResponder];
	return YES;
}

//keyboard appear
-(void) keyboardWillShow:(NSNotification *) notification {
    //---gets the size of the keyboard---
    NSDictionary *userInfo = [notification userInfo];
    NSValue *keyboardValue = [userInfo objectForKey:UIKeyboardBoundsUserInfoKey];
    [keyboardValue getValue:&keyboardBounds];

}

-(void) keyboardWillHide:(NSNotification *) notification {
}

-(void) dealloc{
    [submitChangePassRequest release];
    [scrollView release];
    [lblName release];
    [txtPassword release];
    [txtLoginName release];
    [imgBgView release];
    [super dealloc];
}

-(IBAction)onBack:(id)sender{
    [txtLoginName resignFirstResponder];
    [txtPassword resignFirstResponder];
    [owner onSettingViewCancel];
}

-(IBAction)onSave:(id)sender{
    [txtLoginName resignFirstResponder];
    [txtPassword resignFirstResponder];
    if ([[txtLoginName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:@"Password cannot be blank!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];

        [alert release];
        //[self textValidateAlertShow:@"Retry your name and password!"];
        [self textFieldDidEndEditing:txtLoginName];
        return;
    }
    if ([txtPassword.text isEqualToString:txtLoginName.text]) {
        zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
        ObjUser * objUser = [delegate.db getUserObj];
        [delegate showLoadingScreen];
        submitChangePassRequest = [[SOAPRequest alloc] initWithOwner:self];
        submitChangePassRequest.processId = 7;
        objUser.strNewPassword = txtPassword.text;
        [submitChangePassRequest syncNewPasswordWithUser:objUser];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:@"Password does not match" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
    //[owner onSettingViewCancel];
}

- (void) onErrorLoad: (int) processId{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    [delegate hideLoadingScreen];
    NSLog(@"Error loaded %d",processId);
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{

}

- (void) onJsonLoaded:(NSMutableDictionary *)dics withProcessId:(int)processId{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    if (processId == 7) {
        [delegate hideLoadingScreen];
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];

         NSString * strMessage;
        if (status == STATUS_ACTION_SUCCESS) {
         //NSLog(@"session checking is successed %@",strMessage);
        // int intPoint =[[dics objectForKey:@"point"] intValue];
         //NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
         //[prefs setInteger:intPoint forKey:CHECK_USER_POINT_LINK];
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
            [delegate.db updateUserPassword:txtPassword.text];
            [owner onSettingViewCancel];
         }
        else if(status == STATUS_ACTION_FAILED){
         strMessage = [dics objectForKey:@"message"];
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
         [alert show];
         [alert release];
         }
        else if(status == STATUS_SESSION_EXPIRED){
         strMessage = [dics objectForKey:@"message"];
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
         [alert show];
         [alert release];
         }

    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
