//
//  PRDashboardViewController.m
//  zBox
//
//  Created by Zayar on 5/9/13.
//
//

#import "PRDashboardViewController.h"
#import "StringTable.h"
#import "ObjSessionDetail.h"

@interface PRDashboardViewController ()

@end

@implementation PRDashboardViewController
@synthesize objSession;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString * strPath = [NSString stringWithFormat:@"%@/img_vs_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];
    [imgBgView release];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [self loadDashBoard:objSession];
}

- (void)loadDashBoard:(ObjSubSession *)objSession1{
    
    ObjSessionDetail * objSecDetail = [objSession1.arrSessionDetail objectAtIndex:0];
    objDashboard = objSecDetail.objDashBoard;
    NSLog(@"obj session detail count %d and objDashboard lession %@",[objSession1.arrSessionDetail count],objDashboard.strLesson);
    lblLesson1.backgroundColor = [self setProgressColor:(NSString *)objDashboard.strLesson];
    lblLesson2.backgroundColor = [UIColor yellowColor];
    lblAttitude1.backgroundColor = [self setProgressColor:(NSString *)objDashboard.strAttitude];
    lblAttitude2.backgroundColor = [UIColor yellowColor];
    lblCarelessness1.backgroundColor = [self setProgressColor:(NSString *)objDashboard.strForgetfulness];
    lblCarelessness2.backgroundColor = [UIColor yellowColor];
    lblSpeed1.backgroundColor = [self setProgressColor:(NSString *)objDashboard.strStrategy];
    lblSpeed2.backgroundColor = [UIColor yellowColor];
    
    lblMilestone1.backgroundColor = [self setProgressColor:(NSString *)objDashboard.strMilestone];
    lblMilestone2.backgroundColor = [UIColor yellowColor];
}

- (UIColor *)setProgressColor:(NSString *)str{
    if ([str isEqualToString:PROGRESS_STATUS_ALERT]) {
        return [UIColor redColor];
    }
    else if([str isEqualToString:PROGRESS_STATUS_GOOD]) {
        return [UIColor greenColor];
    }
    else if([str isEqualToString:PROGRESS_STATUS_MEDIOCRE]) {
        return [UIColor yellowColor];
    }
    return nil;
}

- (IBAction)onBack:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    [objDashboard release];
    [lblLesson1 release];
    [lblLesson2 release];
    [lblAttitude1 release];
    [lblAttitude2 release];
    [lblCarelessness1 release];
    [lblCarelessness2 release];
    [lblSpeed1 release];
    [lblSpeed2 release];
    [lblMilestone1 release];
    [lblMilestone2 release];
    [imgBgView release];
    [lblTitle release];
    [objSession release];
    [super dealloc];
}

@end
