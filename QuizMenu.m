//
//  SMQMenu.m
//  zBox
//
//  Created by Zayar Cn on 6/1/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "QuizMenu.h"

@implementation QuizMenu
@synthesize owner;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
       
    }
    return self;
}

- (void)SMQViewDidLoad{
    NSString * strPath = [NSString stringWithFormat:@"%@/popup_main_menu_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgMenuBgView setImage:imgBg];
    [imgMenuBgView release];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)onClose:(id)sender{
    [owner onQuizMenuCancel];
}

- (IBAction)onConsult:(id)sender{
    [owner onQuizMenuConsult];
}

- (IBAction)onMate:(id)sender{
    [owner onQuizMenuMaterial];
}
- (void)dealloc{
    [imgMenuBgView release];
    [super dealloc];
}

@end
