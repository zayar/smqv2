//
//  ProgressSubjectTableCell.h
//  zBox
//
//  Created by Zayar on 3/18/13.
//
//

#import <UIKit/UIKit.h>

@interface ProgressSubjectTableCell : UITableViewCell
{
    UILabel * lblName;
    UILabel * lblActualScore;
    UILabel * lblPreviousScore;
    UIImageView * imgBgView;
}
@property (nonatomic,retain) IBOutlet UILabel * lblName;
@property (nonatomic,retain) IBOutlet UILabel * lblActualScore;
@property (nonatomic,retain) IBOutlet UILabel * lblPreviousScore;
@property (nonatomic,retain) IBOutlet UIImageView * imgBgView;
@end
