//
//  PRToDoViewController.h
//  zBox
//
//  Created by Zayar on 5/9/13.
//
//

#import <UIKit/UIKit.h>
#import "ObjSubSession.h"

@interface PRToDoViewController : UIViewController
{
    IBOutlet UIImageView * imgBgView;
    IBOutlet UILabel * lblTitle;
    ObjSubSession * objSession;
    
    IBOutlet UILabel * lblTopicName;
    IBOutlet UIScrollView * scrollTodo;
}
@property(nonatomic, retain) ObjSubSession * objSession;
@end
