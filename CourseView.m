//
//  VSView.m
//  zBox
//
//  Created by Zayar Cn on 5/11/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "CourseView.h"
#import "vsTableViewCell.h"
#import "zBoxAppDelegate.h"
#import "StringTable.h"
#import "ObjAnswer.h"
#import "SOAPRequest.h"
#import "SVModalWebViewController.h"
#import "ObjCatalog.h"
#import <QuartzCore/QuartzCore.h>
#import "CatalogCell.h"
@implementation CourseView
@synthesize owner;
@synthesize tbl;
@synthesize photos = _photos;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)dealloc{
    [rateRequest release];
    [_photos release];
    [detailImgPopUpView release];
    [tbl release];
    [getCatalogRequest release];
    [arrCata release];
    [imgBgView release];
    [super dealloc];
}

- (void)vsViewDidLoad{
    NSString * strPath = [NSString stringWithFormat:@"%@/img_vs_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];
    [imgBgView release];
    
    arrCata = [[NSMutableArray alloc]init];
    
}

- (void)vsViewWillAppear{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate clearTheBudgeIcon];
    [self syncCatalog];
}

- (void)syncCatalog{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    [delegate showLoadingScreen];
    delegate.isFromMain = FALSE;
    getCatalogRequest = [[SOAPRequest alloc] initWithOwner:self];
    getCatalogRequest.processId = 11;
    [getCatalogRequest syncCatalog];
}

- (void) loadWebView:(NSString *) strURL{
    /*NSString *embedHTML = @"\
    <html><head>\
    <style type=\"text/css\">\
    body {\
    background-color: transparent;\
    color: white;\
    }\
    </style>\
    </head><body style=\"margin:0\">\
    <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
    width=\"%0.0f\" height=\"%0.0f\"></embed>\
    </body></html>";

    NSString *html = [NSString stringWithFormat:embedHTML, strURL, self.frame.size.width, self.frame.size.height];
    [webView loadHTMLString:html baseURL:nil];*/
    
}

-(void) onImageClick:(ObjCatalog *)objAns andIsImage:(BOOL)isImage{
    
    // Let's try adding a webview
    NSURL *URL = [NSURL URLWithString:objAns.strCataMedia];
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:URL];
	webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    webViewController.availableActions = SVWebViewControllerAvailableActionsOpenInSafari | SVWebViewControllerAvailableActionsCopyLink | SVWebViewControllerAvailableActionsMailLink;
    
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate.viewController presentModalViewController: webViewController animated:YES];
}

-(void) onBtnCloseClick{
    NSLog(@"popup close!!!");
    
    //    for(UIView * v in detailImgPopUpView.imgScrollView.subviews){
    //        v=nil;
    //        [v removeFromSuperview];
    //    }
    
    detailImgPopUpView.imgDetailView.frame = CGRectMake(6, 104, detailImgPopUpView.imgDetailView.frame.size.width, detailImgPopUpView.imgDetailView.frame.size.height);
    detailImgPopUpView.webView.frame = CGRectMake(6, 104, detailImgPopUpView.webView.frame.size.width, detailImgPopUpView.webView.frame.size.height);
    detailImgPopUpView.imgScrollView.frame = CGRectMake(6, 104, detailImgPopUpView.imgScrollView.frame.size.width, detailImgPopUpView.imgScrollView.frame.size.height);
    //detailImgPopUpView.imgDesBgView.frame = CGRectMake(6, 270,  detailImgPopUpView.imgDesBgView.frame.size.width,  detailImgPopUpView.imgDesBgView.frame.size.height);
    detailImgPopUpView.lblImgDes.frame = CGRectMake(16, 271, detailImgPopUpView.lblImgDes.frame.size.width, detailImgPopUpView.lblImgDes.frame.size.height);
    detailImgPopUpView.btnClose.frame = CGRectMake(293, 85, detailImgPopUpView.btnClose.frame.size.width, detailImgPopUpView.btnClose.frame.size.height);
    detailImgPopUpView.activity.frame = CGRectMake(144, 177, detailImgPopUpView.activity.frame.size.width, detailImgPopUpView.activity.frame.size.height);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.6];
    [UIView setAnimationDelegate: self];
    detailImgPopUpView.webView.frame = CGRectMake(6, -480, detailImgPopUpView.webView.frame.size.width, detailImgPopUpView.webView.frame.size.height);
    detailImgPopUpView.imgDetailView.frame = CGRectMake(6, -480, detailImgPopUpView.imgDetailView.frame.size.width, detailImgPopUpView.imgDetailView.frame.size.height);
    detailImgPopUpView.imgScrollView.frame = CGRectMake(6, -480, detailImgPopUpView.imgScrollView.frame.size.width, detailImgPopUpView.imgScrollView.frame.size.height);
    //detailImgPopUpView.imgDesBgView.frame = CGRectMake(6, -480,  detailImgPopUpView.imgDesBgView.frame.size.width,  detailImgPopUpView.imgDesBgView.frame.size.height);
    detailImgPopUpView.lblImgDes.frame = CGRectMake(16, -480, detailImgPopUpView.lblImgDes.frame.size.width, detailImgPopUpView.lblImgDes.frame.size.height);
    detailImgPopUpView.btnClose.frame = CGRectMake(293, -480, detailImgPopUpView.btnClose.frame.size.width, detailImgPopUpView.btnClose.frame.size.height);
    detailImgPopUpView.activity.frame = CGRectMake(144, -480, detailImgPopUpView.activity.frame.size.width, detailImgPopUpView.activity.frame.size.height);
    [UIView commitAnimations];
    //detailImgPopUpView.imgBgView.hidden = TRUE;
    [self performSelector:(@selector(hideImagePopUp)) withObject:nil afterDelay:0.6];
}

- (void)hideImagePopUp{
    detailImgPopUpView.hidden = TRUE;
}

- (IBAction)onCancel:(id)sender{
    [self vsViewWillDisAppear];
    [owner onCourseViewCancel];
}
////TableView Datasource & Delegate////
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrCata count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    ObjCatalog * objAns = [arrCata objectAtIndex:[indexPath row]];
    static NSString *CellIdentifier = @"CatalogCell";
    CatalogCell *cell = nil;
     UITableViewCell *cellindent = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cellindent == nil) {
        NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CatalogCell" owner:nil options:nil];		
        for(id currentObject in topLevelObjects){
            if([currentObject isKindOfClass:[UITableViewCell class]]){            
                cell = (CatalogCell *) currentObject;            
                cell.accessoryView = nil;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                break;
            }
        }
        //[cell cellViewLoad];
    }
    [cell.imgBgView setImage:[UIImage imageNamed:@"imb_vs_tablecell_bg.png"]];
    
    if (objAns.strCataMediaType != (id)[NSNull null] && objAns.strCataMediaType != NULL) {
        if([objAns.strCataMediaType isEqualToString:ANSWER_TYPE_IMAGE]){
            cell.webThumbView.hidden = TRUE;
            cell.imgView.hidden = FALSE;
            cell.activity.hidden = FALSE;
            NSLog(@"obj Image link: %@",objAns.strCataMedia);
            NSString * adsImgPath = [delegate.db getFilePath: objAns.strCataMedia];
            if( adsImgPath != nil && (NSNull *) adsImgPath != [NSNull null] && ![adsImgPath isEqualToString:@""] ){
                [cell.imgView setImage: [[UIImage alloc] initWithContentsOfFile: adsImgPath]];
            }
            else{
                [cell.activity startAnimating];
                [objAns.thumbnailCallBacks addObject:cell.imgView];
                [objAns downloadImage:0];
            }
        }
        else if([objAns.strCataMediaType isEqualToString:ANSWER_TYPE_VIDEO]){
            cell.webThumbView.hidden = FALSE;
            cell.imgView.hidden = TRUE;
            cell.activity.hidden = TRUE;
            // NSLog(@"web url : %@ to load and %d",objAns.strQuestionMovieFileLink,cell.isWebLoaded);
            
            NSString *embedHTML = @"\
            <html><head>\
            <style type=\"text/css\">\
            body {\
            background-color: transparent;\
            color: white;\
            }\
            </style>\
            </head><body style=\"margin:0\">\
            <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
            width=\"%0.0f\" height=\"%0.0f\"></embed>\
            </body></html>";
            
            NSString *html = [NSString stringWithFormat:embedHTML, objAns.strCataMedia, cell.webThumbView.frame.size.width, cell.webThumbView.frame.size.height];
            [cell.webThumbView loadHTMLString:html baseURL:nil];
            cell.webThumbView.delegate = self;
            [cell.webThumbView reload];
        }
    }
    
    UIFont * cfont = [UIFont fontWithName:@"BradyBunchRemastered" size:24.0];
    UIFont * sfont = [UIFont fontWithName:@"BradyBunchRemastered" size:18.0];
    cell.lblTitle.font = cfont;
    cell.lblTitle.textColor = [UIColor whiteColor];
    cell.lblText.font = sfont;
    cell.lblText.textColor = [UIColor whiteColor];
    cell.lblTitle.shadowColor = [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.5];
    cell.lblTitle.shadowOffset = CGSizeMake(2, 2);
    cell.lblTitle.text = objAns.strCataTitle;
    //cell.lblText.text = objAns.strCataTitle;
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    return cell;
}

- (void) onImageDownloadFinish:(int)proccessId imageType:(int) itemType{
    
    /*UITableViewCell * vCell = [tbl cellForRowAtIndexPath:rowCellIndex];
    if ([vCell isKindOfClass:[vsTableViewCell class]]) {
    }*/
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 88;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ObjCatalog * obj = [arrCata objectAtIndex:[indexPath row]];
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
        [self syncTrack:obj.idx andString:@"o"];
        if ([obj.strCataMediaType isEqualToString: ANSWER_TYPE_IMAGE]) {
            delegate.objSelectedCatalog = obj;
            NSLog(@"selected obj cata link %@",obj.strCataMediaLink);
            [owner courseShowPicPhotoBrowser:obj.strCataMedia andTitle:(NSString *)obj.strCataTitle];
        }
        else{
            [self onImageClick:obj andIsImage:FALSE];
    }
}

-(void) selectedRate:(int)rate andIndex:(int)idx{
    ObjAnswer * objAns = [arrCata objectAtIndex:idx];
    objAns.rate_point = rate;
    //[arrSolution removeObjectAtIndex:idx];
    //[arrSolution insertObject:objAns atIndex:idx];
    [arrCata replaceObjectAtIndex:idx withObject:objAns];
    NSLog(@"rate id %d and index id %d",rate,idx);
    [self syncRate:objAns];
}

- (void) syncRate:(ObjAnswer *)objAns{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    [delegate showLoadingScreen];
    rateRequest = [[SOAPRequest alloc] initWithOwner:self];
    rateRequest.processId = 9;
    [rateRequest syncRateWithAnswer:objAns];
}

- (void) syncTrack:(int)cataId andString:(NSString *)strType{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    [delegate showLoadingScreen];
    if (trackRequest == nil) {
        trackRequest = [[SOAPRequest alloc] initWithOwner:self];
        trackRequest.processId = 12;
    }
    [trackRequest syncCatalogTrackWith:cataId andResponseType:strType];
}

- (void) vsViewWillDisAppear{
    [arrCata removeAllObjects];
    [tbl reloadData];
}

- (void) onErrorLoad: (int) processId{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    [delegate hideLoadingScreen];
    NSLog(@"Error loaded %d",processId);
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{
    
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    if (processId == 11) {
        
        [delegate hideLoadingScreen];
        NSLog(@"arr count %d",[dics count]);
        NSString * strMessage;
        int status = [[dics objectForKey:@"status"]intValue];
        NSLog(@"%i count", status);
        if (status == STATUS_RETURN_RECORD) {
            NSMutableArray * arrSubject = [dics objectForKey:@"catalogs"];
            NSLog(@"count : %d", [arrSubject count]);
            for ( int i =0; i < [arrSubject count]; i++) {
                NSLog(@"%i", i);
                NSDictionary * dicC = [arrSubject objectAtIndex:i];
                /*ObjAnswer * objA = [[ObjAnswer alloc]init];
                objA.question_id = [[dicQ objectForKey:@"question_id"]intValue];
                objA.subject_id = [[dicQ objectForKey:@"subject_id"] intValue];
                objA.category_id = [[dicQ objectForKey:@"category_id"] intValue];
                objA.strQuestionStatus = [dicQ objectForKey:@"question_status"];
                objA.strQuestionTitle = [dicQ objectForKey:@"question_title"];
                objA.strQuestAnsType = [dicQ objectForKey:@"question_answer_type"];


                objA.strQuestionText = [dicQ objectForKey:@"question_text"];
                if (objA.strQuestAnsType != (id)[NSNull null] && objA.strQuestAnsType != NULL) {

                    if ([objA.strQuestAnsType isEqualToString:ANSWER_TYPE_IMAGE]) {
                        objA.strQuestionImageFileLink = [dicQ objectForKey:@"question_file"];
                        //objA.strQuestionImageFileLink = [NSString stringWithFormat:@"%@%@",BASE_LINK,objA.strQuestionImageFileLink];
                        objA.strQuestionImageThumbFileLink = [dicQ objectForKey:@"question_answer_thumbnail"];
                        NSLog(@"Link %@", objA.strQuestionImageFileLink);

                    }
                    else if([objA.strQuestAnsType isEqualToString:ANSWER_TYPE_VIDEO]){
                        objA.strQuestionMovieFileLink = [dicQ objectForKey:@"question_file"];
                    }
                } else {
                    objA.strQuestAnsType = ANSWER_TYPE_NULL;
                }
                
                
                NSLog(@"thumb nail %@",objA.strQuestionImageThumbFileLink);
                
                objA.strQuestAns = [dicQ objectForKey:@"question_answer"];
                objA.rate_point = [[dicQ objectForKey:@"question_rating"] intValue];*/
                /*
                 "catalog_id":"2",
                 "company_name":"SMaths Tuition",
                 "catalog_title":"A Level Cram School",
                 "catalog_media_type":"I",
                 "catalog_media":"http://www.abc.com./def.jpg",
                 "catalog_link":"http://www.balancedconsultancy.com.sg"
                 */
                ObjCatalog * objCata = [[ObjCatalog alloc]init];
                objCata.idx = [[dicC objectForKey:@"catalog_id"]intValue];
                objCata.strCompanyName = [dicC objectForKey:@"company_name"];
                objCata.strCataTitle = [dicC objectForKey:@"catalog_title"];
                objCata.strCataMediaType = [dicC objectForKey:@"catalog_media_type"];
                objCata.strCataMedia = [dicC objectForKey:@"catalog_media"];
                objCata.strCataMediaLink = [dicC objectForKey:@"catalog_link"];
                NSLog(@"%@", objCata.strCataMediaLink);
                [arrCata addObject:objCata];
                [objCata release];
            }
            [tbl reloadData];
        }
        else if(status == STATUS_NO_RECORD_FOUND){
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];	
            [alert release];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
    }
    else if (processId == 9){
        [delegate hideLoadingScreen];
        NSLog(@"arr count %d",[dics count]);
        NSString * strMessage;
        int status = [[dics objectForKey:@"status"]intValue];
        strMessage = [dics objectForKey:@"message"];
        NSLog(@"%i count", status);
        if (status == STATUS_ACTION_SUCCESS) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];	
            [alert release];
        }
        else if(status == STATUS_NO_RECORD_FOUND){
            //strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];	
            [alert release];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            //strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
    }
    else if (processId == 12){
        [delegate hideLoadingScreen];
        NSLog(@"arr count %d",[dics count]);
        NSString * strMessage;
        int status = [[dics objectForKey:@"status"]intValue];
        strMessage = [dics objectForKey:@"message"];
        NSLog(@"%i count", status);
        if (status == STATUS_ACTION_SUCCESS) {
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];*/
            NSLog(@"reponse %@",strMessage);
        }
        else if(status == STATUS_NO_RECORD_FOUND){
            //strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            //strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
    }
}

////End TableView Datasource & Delegate////
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
