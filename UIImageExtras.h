//
//  UIImageExtras.h
//  zBox
//
//  Created by Zayar Cn on 6/5/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

@interface UIImage (Extras)

- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize;

@end
