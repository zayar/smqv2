//
//  ProgressSubjectTableCell.m
//  zBox
//
//  Created by Zayar on 3/18/13.
//
//

#import "ProgressSubjectTableCell.h"

@implementation ProgressSubjectTableCell
@synthesize lblName,lblActualScore,lblPreviousScore,imgBgView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc{
    [imgBgView release];
    [lblName release];
    [lblActualScore release];
    [lblPreviousScore release];
    [super dealloc];
}

@end
