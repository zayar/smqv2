//
//  VSView.h
//  zBox
//
//  Created by Zayar Cn on 5/11/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOAPRequest.h"
#import "DetailImagePopUpView.h"
#import "ObjQuizQuestion.h"
@protocol QuizSecViewDelegate 
- (void) showQuizSecPicPhotoBrowser:(NSString *)picURL andTitle:(NSString *)picTitle;
- (void) showQuizQuestionDetail:(ObjQuizQuestion *)objQqd;
- (void) onQuizSecViewCancel;
@end
@interface QuizSecListView : UIView<DetailImagePopUpViewDelegate,UIWebViewDelegate>
{
    //IBOutlet UIWebView * webView;
    id<QuizSecViewDelegate> owner;
    IBOutlet UIImageView * imgBgView;
    IBOutlet UITableView * tbl;
    DetailImagePopUpView * detailImgPopUpView;
    NSMutableArray * arrSolution;
    SOAPRequest * getQuestionRequest;
    SOAPRequest * rateRequest;
    NSArray *_photos;
    int quiz_id;
}
@property int quiz_id;
@property (nonatomic, retain) NSArray *photos;
@property (nonatomic, retain) UITableView *tbl;
@property (assign) id<QuizSecViewDelegate> owner;
- (void) loadWebView:(NSString *) strURL;
- (IBAction)onCancel:(id)sender;
- (void)vsViewDidLoad;
- (void)vsViewWillAppear;
- (void) vsViewWillDisAppear;
@end
