//
//  SMQMenu.h
//  zBox
//
//  Created by Zayar Cn on 6/1/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SMQMenuDelegate 
-(void) onSMQMenuCancel;
-(void) onSMQMenuPhoto;
-(void) onSMQMenuLibrary;
@end

@interface SMQMenu : UIView
{
    IBOutlet UIImageView * imgMenuBgView;
    id<SMQMenuDelegate> owner;
}
@property(assign) id<SMQMenuDelegate> owner;
- (void)SMQViewDidLoad;
@end
