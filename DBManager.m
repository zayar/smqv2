//
//  DBManager.m
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "DBManager.h"
#import "zBoxAppDelegate.h"
#import "StringTable.h"
#import "ObjGKey.h"
#import "ObjUser.h"

@implementation DBManager

- (void) checkAndCreateDatabase{
    zBoxAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	NSString * databasePath = [documentsDir stringByAppendingPathComponent: DBNAME];
	
	NSLog(@"checking at %@", databasePath);
	BOOL success;
	
	// Create a FileManager object, we will use this to check the status
	// of the database and to copy it over if required
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	// Check if the database has already been created in the users filesystem
	success = [fileManager fileExistsAtPath:databasePath];
	
	// If the database already exists then return without doing anything
	if(success){
		NSLog(@"db found");
		
		delegate.databasePath = databasePath;
		[delegate.databasePath retain];
		return;
	}
	
	// If not then proceed to copy the database from the application to the users filesystem
	
	// Get the path to the database in the application package
	NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DBNAME];
	
	NSLog(@"copying db...");
	// Copy the database from the package to the users filesystem
	[fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];	
	[fileManager release];
	
	delegate.databasePath = databasePath;
	[delegate.databasePath retain];
	
	NSLog(@"db transfered!");
}

/**** Cached Image DbMethod ****/
- (NSInteger) insertCachedImage:(NSString *)url path:(NSString *) filePath{
	zBoxAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	NSInteger result;
	BOOL opSuccessful = FALSE;
	
	sqlite3 *database;
	sqlite3_stmt *insert_statement;
	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		static char *sql = "INSERT INTO cached_images (url,filePath,downloadTime) VALUES(?,?,?)";
		if (sqlite3_prepare_v2(database, sql, -1, &insert_statement, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
		
		sqlite3_bind_double(insert_statement, 3, [[NSDate date] timeIntervalSince1970]);
		sqlite3_bind_text(insert_statement, 2, [filePath UTF8String], -1, SQLITE_TRANSIENT); 
		sqlite3_bind_text(insert_statement, 1, [url UTF8String], -1, SQLITE_TRANSIENT); 
		
		
		int success = sqlite3_step(insert_statement);
		
		sqlite3_reset(insert_statement);
		if (success != SQLITE_ERROR) {
			NSLog(@"image inserted!");
			result = sqlite3_last_insert_rowid(database);
			opSuccessful = TRUE;
		}
		
		sqlite3_finalize(insert_statement);      
	}
	
	if( opSuccessful ){
		sqlite3_close(database);
		
		return result;
	}
	
	NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
	sqlite3_close(database);
	
	return -1;
}

- (NSString *) getFilePath:(NSString *) url{
	zBoxAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	sqlite3 *database;
	NSString * strPath = @"";
	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		const char *sqlStatement = "SELECT filePath FROM cached_images WHERE url = ?";
		sqlite3_stmt *compiledStatement;
		
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			sqlite3_bind_text(compiledStatement, 1, [url UTF8String], -1, SQLITE_TRANSIENT);
			
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				strPath = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
				//NSLog(@"file path %@", strPath);
			}
		}
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
	
	return strPath;
}

- (NSMutableArray *) getExpiredImages:(double) limit{
	zBoxAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	sqlite3 *database;
	NSMutableArray * list = [[NSMutableArray alloc] init];
	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		const char *sqlStatement = "SELECT filePath FROM cached_images WHERE downloadTime < ?";
		sqlite3_stmt *compiledStatement;
		
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			sqlite3_bind_double(compiledStatement, 1, limit);
			
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				NSString *aCat = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
				[list addObject:aCat];
				[aCat release];
			}
		}
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
	
	return list;
}

- (void) removeCahcedImage{
	zBoxAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	double expiredLimit = [[NSDate date] timeIntervalSince1970]- CACHE_DURATION;
	
	NSMutableArray * files = [self getExpiredImages: expiredLimit];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	for(NSString * f in files){
		if( f != nil && (NSNull *) f != [NSNull null] && ![f isEqualToString:@""] )
			//NSLog(@"Removing cached: %@", f);
			[fileManager removeItemAtPath:f error:NULL];
	}
	
	sqlite3 *database;
	sqlite3_stmt * delete_statment;
	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		const char *sql = "DELETE FROM cached_images WHERE downloadTime < ?";
		if (sqlite3_prepare_v2(database, sql, -1, &delete_statment, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
		
		sqlite3_bind_double(delete_statment, 1, expiredLimit);
		int success = sqlite3_step(delete_statment);
		
		if (success != SQLITE_DONE) {
			NSAssert1(0, @"Error: failed to save priority with message '%s'.", sqlite3_errmsg(database));
		}
		
		sqlite3_reset(delete_statment);
	}
	
	sqlite3_close(database);
}

/**** End Cached Image DbMethod ****/

-(NSInteger) checkUserIfActive{
	zBoxAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	sqlite3 *database;
	NSInteger total=0;
	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		const char *sqlStatement = "SELECT COUNT(*) FROM tbl_user WHERE active = 1";
        
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				total = sqlite3_column_int(compiledStatement, 0);
			}
		}
		
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
    
	return total;
}

- (ObjUser *) getUserObj{
	zBoxAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	sqlite3 *database;
	ObjUser * objUser = nil;
	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		
		const char *sqlStatement = "SELECT * FROM tbl_user WHERE idx = 1";
		
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                NSLog(@"found...");
                
				objUser = [[[ObjUser alloc] init] autorelease];
				objUser.idx = sqlite3_column_int(compiledStatement, 0);
                
                objUser.strName = @"";
				if( (char *)sqlite3_column_text(compiledStatement, 1) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 1) != [NSNull null]){
					objUser.strName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
				}
                
				objUser.strPassword = @"";
				if( (char *)sqlite3_column_text(compiledStatement, 2) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 2) != [NSNull null]){
					objUser.strPassword = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
				}
                
                objUser.strSession = @"";
				if( (char *)sqlite3_column_text(compiledStatement, 3) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 3) != [NSNull null]){
					objUser.strSession = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
				}
                
                objUser.intActive = sqlite3_column_int(compiledStatement, 4);
			}
		}
		
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
	
	return objUser;
}

- (void) updateUser:(ObjUser *)objUser{
	zBoxAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    sqlite3 *database;
	sqlite3_stmt * update_statment;
	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		const char *sql = "UPDATE tbl_user SET name = ?,session_key = ?,password = ?,super_password=?,freeze_status=?  WHERE idx = 1";
		if (sqlite3_prepare_v2(database, sql, -1, &update_statment, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
        sqlite3_bind_text(update_statment, 1, [objUser.strName UTF8String], -1, SQLITE_TRANSIENT);
		
		sqlite3_bind_text(update_statment, 2, [objUser.strSession UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(update_statment, 3, [objUser.strPassword UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(update_statment, 4, [objUser.strParentPassword UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(update_statment, 5, [objUser.strFreezeStatus UTF8String], -1, SQLITE_TRANSIENT);
        
        int success = sqlite3_step(update_statment);
		
		if (success != SQLITE_DONE) {
			NSAssert1(0, @"Error: failed to save priority with message '%s'.", sqlite3_errmsg(database));
		}
        //NSLog(@"update statement for this %s ",update_statment);
        sqlite3_reset(update_statment);
	}
	
	sqlite3_close(database);
}

- (void) updateUserPassword:(NSString *)strPass{
	zBoxAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    sqlite3 *database;
	sqlite3_stmt * update_statment;
	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		const char *sql = "UPDATE tbl_user SET password = ? WHERE idx = 1";
		if (sqlite3_prepare_v2(database, sql, -1, &update_statment, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
        sqlite3_bind_text(update_statment, 1, [strPass UTF8String], -1, SQLITE_TRANSIENT);
		
        
        int success = sqlite3_step(update_statment);
		
		if (success != SQLITE_DONE) {
			NSAssert1(0, @"Error: failed to save priority with message '%s'.", sqlite3_errmsg(database));
		}
        //NSLog(@"update statement for this %s ",update_statment);
        sqlite3_reset(update_statment);
	}
	
	sqlite3_close(database);
}

@end