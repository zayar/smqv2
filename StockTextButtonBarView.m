//
//  ExpadnButtonView.m
//  zBox
//
//  Created by Zayar on 5/27/13.
//
//

#import "StockTextButtonBarView.h"
#import <QuartzCore/QuartzCore.h>
@implementation StockTextButtonBarView
@synthesize owner;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (IBAction)onActual:(UIButton *)sender{
    [owner onActualSelected:@"a.png"];
    [self setSelectionStyle:sender];
    NSLog(@"here is actual");
}

- (IBAction)onCouldve:(UIButton *)sender{
    [owner onCouldveSelected:@"c.png"];
    [self setSelectionStyle:sender];
     NSLog(@"here is could");
}

- (IBAction)onCouldveActual:(UIButton *)sender{
    [owner onActualCouldveSelected:@"ac.png"];
    [self setSelectionStyle:sender];
     NSLog(@"here is could actual");
}

- (void) setSelectionStyle:(UIButton *)btn{
    
    [btn.layer setCornerRadius:10.0f];
    [btn.layer setMasksToBounds:YES];
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 4.0;
    [btn setTitleColor:[UIColor colorWithRed:0.21f green:0.27f blue:0.46f alpha:1.00f] forState:normal];
    btn.backgroundColor = [UIColor whiteColor];
    
    for (UIView * v in self.subviews) {
        if ([v isKindOfClass:[UIButton class]]) {
            UIButton * btn1 = (UIButton *)v;
            if (![btn isEqual:btn1]) {
                [btn1.layer setCornerRadius:0.0f];
                [btn1.layer setMasksToBounds:YES];
                btn1.layer.borderColor = [UIColor clearColor].CGColor;
                btn1.layer.borderWidth = 0.0;
                [btn1 setTitleColor:[UIColor whiteColor] forState:normal];
                btn1.backgroundColor = [UIColor clearColor];
            }
        }
    }
}

- (void) setDefaultSelector{
    [self onActual:btnActual];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
