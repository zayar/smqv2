//
//  ExpadnButtonView.h
//  zBox
//
//  Created by Zayar on 5/27/13.
//
//

#import <UIKit/UIKit.h>
@protocol ExpandButtonViewDelegate
- (void) onCouldveSelected:(NSString *)strImage;
- (void) onActualSelected:(NSString *)strImage;
- (void) onActualCouldveSelected:(NSString *)strImage;
@end
@interface ExpandButtonView : UIView
{
    id<ExpandButtonViewDelegate> owner;
}
@property id<ExpandButtonViewDelegate> owner;
@end
