//
//  FreezeView.h
//  zBox
//
//  Created by Zayar on 5/9/13.
//
//

#import <UIKit/UIKit.h>
#import "SOAPRequest.h"
@protocol FreezeViewDelegate
-(void) onFreezeViewCancel;
-(void) onFreezeViewSave;
-(void) onFreezeViewChangePassword;
-(void) onFreezeViewLogout;
-(void) onFreezeViewFreeze;
-(void) onUnFreeze;
@end
@interface FreezeView : UIView
{
    IBOutlet UIImageView * imgBgView;
    IBOutlet UILabel * lblName;
    IBOutlet UIButton * btnFreeze;
    id<FreezeViewDelegate> owner;
    SOAPRequest * submitChangePassRequest;
    BOOL isFreeze;
    UISwitch * switchFreeze;
}
@property (assign) id<FreezeViewDelegate> owner;
-(void)freezeViewViewDidLoad;
-(void)freezeViewWillAppear;
- (IBAction)onChangeClick:(id)sender;
- (IBAction)onFreezeClick:(id)sender;
- (IBAction)onLogoutClick:(id)sender;
-(IBAction)onBack:(id)sender;
- (void)setTittleBtnFreeze;
- (void)backCurrentPosition;
@end
