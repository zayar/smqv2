//
//  CustomPhotoAlbumTableCell.h
//  zBox
//
//  Created by Zayar Cn on 6/11/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPhotoAlbumTableCell : UITableViewCell
{
    UIImageView * imgThumbView;
    UILabel * lblName;
}
@property (nonatomic, retain)IBOutlet UIImageView * imgThumbView;
@property (nonatomic, retain)IBOutlet UILabel * lblName;
@end
