//
//  LoadingScreenView.h
//  zBox
//
//  Created by Zayar Cn on 6/12/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingScreenView : UIView
{
   IBOutlet UIImageView * imgBgView;
    
    UIInterfaceOrientation currentOrientation;
    BOOL useTransparent;

    UILabel * lblMsg;
    UIActivityIndicatorView * activity;
}
@property (nonatomic, retain ) IBOutlet UILabel * lblMsg;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView * activity;

@property BOOL useTransparent;
@property UIInterfaceOrientation currentOrientation;
- (void)loadingScreenViewDidLoad;
- (void) changeToOrientation:(UIInterfaceOrientation) orientation;
@end
