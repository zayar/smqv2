//
//  QuizQuestionDetailView.m
//  zBox
//
//  Created by Zayar on 5/9/13.
//
//

#import "QuizQuestionDetailView.h"
#import "ObjQuizQuestion.h"
#import "StringTable.h"
#import "zBoxAppDelegate.h"
#import "SVModalWebViewController.h"

@implementation QuizQuestionDetailView
@synthesize owner,rateView;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)qqDViewDidLoad{
    NSString * strPath = [NSString stringWithFormat:@"%@/img_vs_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];
    [imgBgView release];
    
    [self loadRateView];
}

- (void)qqDViewWillAppear:(ObjQuizQuestion *)objQqd{
    objQuizQuestionDetail = objQqd;
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    if (objQqd.strQuestType != (id)[NSNull null] && objQqd.strQuestType != NULL) {
        if([objQqd.strQuestType isEqualToString:ANSWER_TYPE_IMAGE]){
            webView.hidden = TRUE;
            imgView.hidden = FALSE;
            activity.hidden = FALSE;
            objQqd.owner = self;
            NSLog(@"obj Image link: %@",objQqd.strQuestText);
            NSString * adsImgPath = [delegate.db getFilePath: objQqd.strQuestText];
            if( adsImgPath != nil && (NSNull *) adsImgPath != [NSNull null] && ![adsImgPath isEqualToString:@""] ){
                [imgView setImage: [[UIImage alloc] initWithContentsOfFile: adsImgPath]];
            }
            else{
                [activity startAnimating];
                [objQqd.detailCallBacks addObject:imgView];
                [objQqd downloadImage:1];
            }
        }
        else if([objQqd.strQuestType isEqualToString:ANSWER_TYPE_VIDEO]){
            webView.hidden = FALSE;
            imgView.hidden = TRUE;
            activity.hidden = TRUE;
            // NSLog(@"web url : %@ to load and %d",objAns.strQuestionMovieFileLink,cell.isWebLoaded);
            
            NSString *embedHTML = @"\
            <html><head>\
            <style type=\"text/css\">\
            body {\
            background-color: transparent;\
            color: white;\
            }\
            </style>\
            </head><body style=\"margin:0\">\
            <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
            width=\"%0.0f\" height=\"%0.0f\"></embed>\
            </body></html>";
            
            NSString *html = [NSString stringWithFormat:embedHTML, objQqd.strQuestText, webView.frame.size.width, webView.frame.size.height];
            [webView loadHTMLString:html baseURL:nil];
            webView.delegate = self;
            [webView reload];
        }
    }
}

- (void)loadRateView{
    self.rateView.notSelectedImage = [UIImage imageNamed:@"btn_rate_star_normal.png"];
    self.rateView.halfSelectedImage = [UIImage imageNamed:@"btn_rate_star_selected.png"];
    self.rateView.fullSelectedImage = [UIImage imageNamed:@"btn_rate_star_selected.png"];
    
    self.rateView.editable = YES;
    self.rateView.maxRating = 5;
    self.rateView.delegate = self;
}


- (void) onImageDownloadFinish:(int)proccessId imageType:(int) itemType{
    [activity stopAnimating];
}

- (IBAction)onCancel:(id)sender{
    //[self vsViewWillDisAppear];
    [owner onQuizQuestionDetailCancel];
}

- (IBAction)onViewSolution:(id)sender{
        if ([objQuizQuestionDetail.strAnsType isEqualToString: ANSWER_TYPE_IMAGE]) {
            [owner showQuizQuestionPicPhotoBrowser:objQuizQuestionDetail.strAnsText andTitle:(NSString *)objQuizQuestionDetail.strAnsText];
        }
        else{
            
            [self onImageClick:objQuizQuestionDetail andIsImage:FALSE];
        }
}

-(void) onImageClick:(ObjQuizQuestion *)objAns andIsImage:(BOOL)isImage{
   
    // Let's try adding a webview
    NSURL *URL = [NSURL URLWithString:objAns.strAnsText];
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:URL];
	webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    webViewController.availableActions = SVWebViewControllerAvailableActionsOpenInSafari | SVWebViewControllerAvailableActionsCopyLink | SVWebViewControllerAvailableActionsMailLink;
    
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate.viewController presentModalViewController: webViewController animated:YES];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (void)rateView:(RateView *)rateView ratingDidChange:(float)rating {
    [self selectedRate:rating];
}

-(void) selectedRate:(int)rate {
    NSLog(@"Quiz id %d and rate %d",objQuizQuestionDetail.idx,rate);
    objQuizQuestionDetail.rate_point = rate;
    [self syncRate:objQuizQuestionDetail];
}

- (void) syncRate:(ObjQuizQuestion *)objAns{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    [delegate showLoadingScreen];
    rateRequest = [[SOAPRequest alloc] initWithOwner:self];
    rateRequest.processId = 9;
    [rateRequest syncRateWithQuiz:objAns];
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    if (processId == 9){
        [delegate hideLoadingScreen];
        NSLog(@"arr count %d",[dics count]);
        NSString * strMessage;
        int status = [[dics objectForKey:@"status"]intValue];
        strMessage = [dics objectForKey:@"message"];
        NSLog(@"%i count", status);
        if (status == STATUS_ACTION_SUCCESS) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
        else if(status == STATUS_NO_RECORD_FOUND){
            //strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            //strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
    }
}

- (void)dealloc{
    [rateView release];
    [objQuizQuestionDetail release];
    [imgView release];
    [imgBgView release];
    [super dealloc];
}

@end
