//
//  VSView.m
//  zBox
//
//  Created by Zayar Cn on 5/11/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "QuizView.h"
#import "quizTableViewCell.h"
#import "zBoxAppDelegate.h"
#import "StringTable.h"
#import "ObjAnswer.h"
#import "SOAPRequest.h"
#import "QuizMenu.h"
#import "SVModalWebViewController.h"
#import "PopUpOverlay.h"
#import <QuartzCore/QuartzCore.h>
#import "ObjQuizz.h"
@implementation QuizView
@synthesize owner;
@synthesize tbl;
@synthesize photos = _photos;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)dealloc{
    [objCurrentQuizz release];
    [popUpOverlayView release];
    [smqMenuView release];
    [rateRequest release];
    [_photos release];
    [detailImgPopUpView release];
    [tbl release];
    [getQuestionRequest release];
    [arrSolution release];
    [imgBgView release];
    [super dealloc];
}

- (void)vsViewDidLoad{
    NSString * strPath = [NSString stringWithFormat:@"%@/img_vs_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];
    [imgBgView release];
    
    arrSolution = [[NSMutableArray alloc]init];
    
}

- (void)vsViewWillAppear{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate clearTheBudgeIcon];
    [self syncQuizz];
}

- (void)syncQuizz{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    [delegate showLoadingScreen];
    delegate.isFromMain = FALSE;
    getQuestionRequest = [[SOAPRequest alloc] initWithOwner:self];
    getQuestionRequest.processId = 12;
    [getQuestionRequest syncQuizz];
}

- (void) loadWebView:(NSString *) strURL{
    /*NSString *embedHTML = @"\
    <html><head>\
    <style type=\"text/css\">\
    body {\
    background-color: transparent;\
    color: white;\
    }\
    </style>\
    </head><body style=\"margin:0\">\
    <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
    width=\"%0.0f\" height=\"%0.0f\"></embed>\
    </body></html>";

    NSString *html = [NSString stringWithFormat:embedHTML, strURL, self.frame.size.width, self.frame.size.height];
    [webView loadHTMLString:html baseURL:nil];*/
    
}

-(void) onImageClick:(ObjAnswer *)objAns andIsImage:(BOOL)isImage{
    // objAns.strQuestionMovieFileLink
    //    if(isOriginalImage)
    //    {
    /*
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    
    if (detailImgPopUpView == nil) {
        NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DetailImagePopUpView" owner:nil options:nil];		
        for(id currentObject in topLevelObjects){
            if([currentObject isKindOfClass:[DetailImagePopUpView class]]){
                detailImgPopUpView = (DetailImagePopUpView *) currentObject;
                detailImgPopUpView.backgroundColor = [UIColor clearColor];
                //detailImgPopUpView.frame = CGRectMake(6,0,detailImgPopUpView.frame.size.width, detailImgPopUpView.frame.size.width);
                [detailImgPopUpView preLoadView];
                [self addSubview: detailImgPopUpView];
                break;
            }
        }
    }
    if (isImage){
        detailImgPopUpView.imgDetailView.hidden = FALSE;
        detailImgPopUpView.webView.hidden = TRUE;
    }
    else {
        detailImgPopUpView.imgDetailView.hidden = TRUE;
        detailImgPopUpView.webView.hidden = FALSE;
    }
    detailImgPopUpView.hidden = FALSE;
    //objAns.owner = self;
    detailImgPopUpView.owner = self;
    detailImgPopUpView.imgDetailView.frame = CGRectMake(6, -480, detailImgPopUpView.imgDetailView.frame.size.width, detailImgPopUpView.imgDetailView.frame.size.height);
    detailImgPopUpView.webView.frame = CGRectMake(6, -480, detailImgPopUpView.webView.frame.size.width, detailImgPopUpView.webView.frame.size.height);
    //detailImgPopUpView.imgDesBgView.frame = CGRectMake(6, -480,  detailImgPopUpView.imgDesBgView.frame.size.width,  detailImgPopUpView.imgDesBgView.frame.size.height);
    detailImgPopUpView.lblImgDes.frame = CGRectMake(16, -480, detailImgPopUpView.lblImgDes.frame.size.width, detailImgPopUpView.lblImgDes.frame.size.height);
    detailImgPopUpView.btnClose.frame = CGRectMake(293, -480, detailImgPopUpView.btnClose.frame.size.width, detailImgPopUpView.btnClose.frame.size.height);
    detailImgPopUpView.activity.frame = CGRectMake(144, -480, detailImgPopUpView.activity.frame.size.width, detailImgPopUpView.activity.frame.size.height);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.6];
    [UIView setAnimationDelegate: self];
    detailImgPopUpView.imgDetailView.frame = CGRectMake(6, 104, detailImgPopUpView.imgDetailView.frame.size.width, detailImgPopUpView.imgDetailView.frame.size.height);
    detailImgPopUpView.webView.frame = CGRectMake(6, 104, detailImgPopUpView.webView.frame.size.width, detailImgPopUpView.webView.frame.size.height);
    //detailImgPopUpView.imgDesBgView.frame = CGRectMake(6, 270,  detailImgPopUpView.imgDesBgView.frame.size.width,  detailImgPopUpView.imgDesBgView.frame.size.height);
    detailImgPopUpView.lblImgDes.frame = CGRectMake(16, 271, detailImgPopUpView.lblImgDes.frame.size.width, detailImgPopUpView.lblImgDes.frame.size.height);
    detailImgPopUpView.btnClose.frame = CGRectMake(293, 85, detailImgPopUpView.btnClose.frame.size.width, detailImgPopUpView.btnClose.frame.size.height);
    detailImgPopUpView.activity.frame = CGRectMake(144, 177, detailImgPopUpView.activity.frame.size.width, detailImgPopUpView.activity.frame.size.height);
    [UIView commitAnimations];
    if (isImage) {
        [self bringSubviewToFront: detailImgPopUpView.imgDetailView];
        detailImgPopUpView.imgDetailView.image = nil;
        
        NSString * imgPath = [delegate.db getFilePath: objAns.strQuestionImageFileLink];
        
        if( imgPath != nil && (NSNull *) imgPath != [NSNull null] && ![imgPath isEqualToString:@""] ){
            UIImage * img = [[UIImage alloc] initWithContentsOfFile: imgPath];
            [detailImgPopUpView.imgDetailView setImage: img];
            [img release];
            [detailImgPopUpView.activity stopAnimating];
        }
        else{
            [objAns.thumbnailCallBacks addObject: detailImgPopUpView.imgDetailView];
            [objAns downloadImage:0];
            [detailImgPopUpView.activity startAnimating];
        }
    }
    else {
        [self bringSubviewToFront: detailImgPopUpView.webView];
        detailImgPopUpView.imgDetailView.image = nil;
        [detailImgPopUpView loadWebView:objAns.strQuestionMovieFileLink];
    }
    
    
    //detailImgPopUpView.lblImgDes.font=[UIFont boldSystemFontOfSize: 10.0];
    //detailImgPopUpView.lblImgDes.font=[UIFont fontWithName:@"Zawgyi-One" size:10.0];
    detailImgPopUpView.lblImgDes.text = objAns.strQuestionText;
    //}
     */
    
    // Let's try adding a webview
    NSURL *URL = [NSURL URLWithString:objAns.strQuestAns];
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:URL];
	webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    webViewController.availableActions = SVWebViewControllerAvailableActionsOpenInSafari | SVWebViewControllerAvailableActionsCopyLink | SVWebViewControllerAvailableActionsMailLink;
    
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate.viewController presentModalViewController: webViewController animated:YES];
}

-(void) onBtnCloseClick{
    NSLog(@"popup close!!!");
    
    //    for(UIView * v in detailImgPopUpView.imgScrollView.subviews){
    //        v=nil;
    //        [v removeFromSuperview];
    //    }
    
    detailImgPopUpView.imgDetailView.frame = CGRectMake(6, 104, detailImgPopUpView.imgDetailView.frame.size.width, detailImgPopUpView.imgDetailView.frame.size.height);
    detailImgPopUpView.webView.frame = CGRectMake(6, 104, detailImgPopUpView.webView.frame.size.width, detailImgPopUpView.webView.frame.size.height);
    detailImgPopUpView.imgScrollView.frame = CGRectMake(6, 104, detailImgPopUpView.imgScrollView.frame.size.width, detailImgPopUpView.imgScrollView.frame.size.height);
    //detailImgPopUpView.imgDesBgView.frame = CGRectMake(6, 270,  detailImgPopUpView.imgDesBgView.frame.size.width,  detailImgPopUpView.imgDesBgView.frame.size.height);
    detailImgPopUpView.lblImgDes.frame = CGRectMake(16, 271, detailImgPopUpView.lblImgDes.frame.size.width, detailImgPopUpView.lblImgDes.frame.size.height);
    detailImgPopUpView.btnClose.frame = CGRectMake(293, 85, detailImgPopUpView.btnClose.frame.size.width, detailImgPopUpView.btnClose.frame.size.height);
    detailImgPopUpView.activity.frame = CGRectMake(144, 177, detailImgPopUpView.activity.frame.size.width, detailImgPopUpView.activity.frame.size.height);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.6];
    [UIView setAnimationDelegate: self];
    detailImgPopUpView.webView.frame = CGRectMake(6, -480, detailImgPopUpView.webView.frame.size.width, detailImgPopUpView.webView.frame.size.height);
    detailImgPopUpView.imgDetailView.frame = CGRectMake(6, -480, detailImgPopUpView.imgDetailView.frame.size.width, detailImgPopUpView.imgDetailView.frame.size.height);
    detailImgPopUpView.imgScrollView.frame = CGRectMake(6, -480, detailImgPopUpView.imgScrollView.frame.size.width, detailImgPopUpView.imgScrollView.frame.size.height);
    //detailImgPopUpView.imgDesBgView.frame = CGRectMake(6, -480,  detailImgPopUpView.imgDesBgView.frame.size.width,  detailImgPopUpView.imgDesBgView.frame.size.height);
    detailImgPopUpView.lblImgDes.frame = CGRectMake(16, -480, detailImgPopUpView.lblImgDes.frame.size.width, detailImgPopUpView.lblImgDes.frame.size.height);
    detailImgPopUpView.btnClose.frame = CGRectMake(293, -480, detailImgPopUpView.btnClose.frame.size.width, detailImgPopUpView.btnClose.frame.size.height);
    detailImgPopUpView.activity.frame = CGRectMake(144, -480, detailImgPopUpView.activity.frame.size.width, detailImgPopUpView.activity.frame.size.height);
    [UIView commitAnimations];
    //detailImgPopUpView.imgBgView.hidden = TRUE;
    [self performSelector:(@selector(hideImagePopUp)) withObject:nil afterDelay:0.6];
}

- (void)hideImagePopUp{
    detailImgPopUpView.hidden = TRUE;
}

- (IBAction)onCancel:(id)sender{
    [self vsViewWillDisAppear];
    [owner onQuizViewCancel];
}
////TableView Datasource & Delegate////
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrSolution count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    ObjQuizz * objAns = [arrSolution objectAtIndex:[indexPath row]];
    static NSString *CellIdentifier = @"quizTableViewCell";
    quizTableViewCell *cell = nil;
     UITableViewCell *cellindent = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cellindent == nil) {
        NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"quizTableViewCell" owner:nil options:nil];		
        for(id currentObject in topLevelObjects){
            if([currentObject isKindOfClass:[UITableViewCell class]]){            
                cell = (quizTableViewCell *) currentObject;            
                cell.accessoryView = nil;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                break;
            }
        }
        [cell cellViewLoad];
        cell.owner = self;
    }
    [cell.imgBgView setImage:[UIImage imageNamed:@"imb_vs_tablecell_bg.png"]];
    UIFont * cfont = [UIFont fontWithName:@"BradyBunchRemastered" size:24.0];
    UIFont * sfont = [UIFont fontWithName:@"BradyBunchRemastered" size:10.0];
    cell.lblTitle.font = cfont;
    cell.lblTitle.textColor = [UIColor whiteColor];
    cell.lblText.font = sfont;
    cell.lblText.textColor = [UIColor whiteColor];
    cell.lblTitle.shadowColor = [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.5];
    //cell.lblTitle.t
    cell.lblTitle.shadowOffset = CGSizeMake(2, 2);
    cell.lblTitle.text = objAns.strQuizName;
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    cell.btnConsult.tag = [indexPath row];
    return cell;
}

-(void) onQuizClick:(int)tag{
    objCurrentQuizz = [arrSolution objectAtIndex:tag];
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:tag inSection:0];
    quizTableViewCell *cell = (quizTableViewCell *)[tbl cellForRowAtIndexPath:myIP];
    btnSelectConsult = cell.btnConsult;
    [self showSMQMenuView];
}

- (void)showPopOverlay{
    if( popUpOverlayView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PopUpOverlay" owner:nil options:nil];
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[PopUpOverlay class]]){
				popUpOverlayView = (PopUpOverlay *) currentObject;
				[popUpOverlayView popupOverlayViewDidLoad];
				[self addSubview: popUpOverlayView];
				break;
			}
		}
	}
    popUpOverlayView.hidden = FALSE;
}

- (void)hidePopOverlay{
    popUpOverlayView.hidden = TRUE;
}

- (void) onImageDownloadFinish:(int)proccessId imageType:(int) itemType{
    
    /*UITableViewCell * vCell = [tbl cellForRowAtIndexPath:rowCellIndex];
    if ([vCell isKindOfClass:[vsTableViewCell class]]) {
    }*/
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ObjAnswer * obj = [arrSolution objectAtIndex:[indexPath row]];
    /*if (![obj.strQuestionStatus isEqualToString:ANSWER_STATUS_PENDING]) {
        if ([obj.strQuestAnsType isEqualToString: ANSWER_TYPE_IMAGE]) {
            [owner showQuizPicPhotoBrowser:obj.strQuestAns andTitle:(NSString *)obj.strQuestionTitle];
        }
        else{
            [self onImageClick:obj andIsImage:FALSE];
        }
    }*/
    [owner showQuizSecondViewWith:obj.idx];
}

- (void)showSMQMenuView{
    if( smqMenuView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"QuizMenu" owner:nil options:nil];
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[QuizMenu class]]){
				smqMenuView = (QuizMenu *) currentObject;
				[smqMenuView SMQViewDidLoad];
				[self addSubview: smqMenuView];
				break;
			}
		}
	}
    [self showPopOverlay];
    smqMenuView.hidden = FALSE;
    smqMenuView.owner = self;
    smqMenuView.frame = CGRectMake(0, 480, smqMenuView.frame.size.width, smqMenuView.frame.size.height);
    [self bringSubviewToFront: smqMenuView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    smqMenuView.frame = CGRectMake(0, 248, smqMenuView.frame.size.width, smqMenuView.frame.size.height);
    [UIView commitAnimations];
}

-(void) onQuizMenuCancel{
    smqMenuView.frame = CGRectMake(0, 264, smqMenuView.frame.size.width, smqMenuView.frame.size.height);
    [self bringSubviewToFront: smqMenuView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    smqMenuView.frame = CGRectMake(0, 480, smqMenuView.frame.size.width, smqMenuView.frame.size.height);
    [UIView commitAnimations];
    [self performSelector:(@selector(closeQuizMenuView)) withObject:nil afterDelay:0.4];
    //smqView.hidden = TRUE;
}

-(void) closeQuizMenuView{
    smqMenuView.hidden = TRUE;
    [self hidePopOverlay];
}

-(void) selectedRate:(int)rate andIndex:(int)idx{
    ObjAnswer * objAns = [arrSolution objectAtIndex:idx];
    objAns.rate_point = rate;
    //[arrSolution removeObjectAtIndex:idx];
    //[arrSolution insertObject:objAns atIndex:idx];
    [arrSolution replaceObjectAtIndex:idx withObject:objAns];
    NSLog(@"rate id %d and index id %d",rate,idx);
    [self syncRate:objAns];
}

- (void) syncRate:(ObjAnswer *)objAns{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    [delegate showLoadingScreen];
    rateRequest = [[SOAPRequest alloc] initWithOwner:self];
    rateRequest.processId = 9;
    [rateRequest syncRateWithAnswer:objAns];
}

- (void) syncCreateConsult:(ObjQuizz *)objQuizz{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    [delegate showLoadingScreen];
    if (createQuizRequest == nil) {
        createQuizRequest = [[SOAPRequest alloc] initWithOwner:self];
    }
    
    createQuizRequest.processId = 21;
    [createQuizRequest syncCreateQuizzConsultWithUser:objQuizz];
}

- (void) vsViewWillDisAppear{
    [arrSolution removeAllObjects];
    [tbl reloadData];
}

- (void) onErrorLoad: (int) processId{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    [delegate hideLoadingScreen];
    NSLog(@"Error loaded %d",processId);
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{
    
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    if (processId == 12) {
        
        [delegate hideLoadingScreen];
        NSLog(@"arr count %d",[dics count]);
        NSString * strMessage;
        int status = [[dics objectForKey:@"status"]intValue];
        NSLog(@"%i count", status);
        if (status == STATUS_RETURN_RECORD) {
            NSMutableArray * arrSubject = [dics objectForKey:@"quizzes"];
            NSLog(@"count : %d", [arrSubject count]);
            if ([arrSubject count]>0) {
                [arrSolution removeAllObjects];
            }
            for ( int i =0; i < [arrSubject count]; i++) {
                NSLog(@"%i", i);
                /*
                 "quiz_id": "1",
                 "quiz_name": "Quiz 123",
                 "quiz_source": "Textbook",
                 "class_id": "1",
                 "class_name": "ABCDE",
                 "subject_id": "18",
                 "subject_name": "MATH-Low Pri",
                 "quiz_question_count": 1
                 */
                NSDictionary * dicQ = [arrSubject objectAtIndex:i];
                ObjQuizz * objQ = [[ObjQuizz alloc]init];
                objQ.idx = [[dicQ objectForKey:@"quiz_id"]intValue];
                objQ.strQuizName = [dicQ objectForKey:@"quiz_name"];
                objQ.strQuizSource = [dicQ objectForKey:@"quiz_source"];
                objQ.class_id = [[dicQ objectForKey:@"class_id"] intValue];
                objQ.strClassName = [dicQ objectForKey:@"class_name"];
                objQ.subject_id = [[dicQ objectForKey:@"subject_id"] intValue];
                objQ.strSubjectName = [dicQ objectForKey:@"subject_id"];
                objQ.quiz_qest_count = [[dicQ objectForKey:@"quiz_question_count"] intValue];
               
                [arrSolution addObject:objQ];
                [objQ release];
            }
            [tbl reloadData];
        }
        else if(status == STATUS_NO_RECORD_FOUND){
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];	
            [alert release];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
    
    }
    else if (processId == 9){
        [delegate hideLoadingScreen];
        NSLog(@"arr count %d",[dics count]);
        NSString * strMessage;
        int status = [[dics objectForKey:@"status"]intValue];
        strMessage = [dics objectForKey:@"message"];
        NSLog(@"%i count", status);
        if (status == STATUS_ACTION_SUCCESS) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];	
            [alert release];
        }
        else if(status == STATUS_NO_RECORD_FOUND){
            //strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];	
            [alert release];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            //strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
    }
    else if (processId == 21){
        [delegate hideLoadingScreen];
        NSLog(@"arr count %d",[dics count]);
        NSString * strMessage;
        int status = [[dics objectForKey:@"status"]intValue];
        strMessage = [dics objectForKey:@"message"];
        NSLog(@"%i count", status);
        if (status == STATUS_ACTION_SUCCESS) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
            btnSelectConsult.enabled = FALSE;
        }
        else if(status == STATUS_NO_RECORD_FOUND){
            //strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            //strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
    }
}

-(void) onQuizMenuConsult{
    if (objCurrentQuizz != nil) {
        objCurrentQuizz.strConsultType = @"C";
        [self syncCreateConsult:objCurrentQuizz];
    }
}

-(void) onQuizMenuMaterial{
    if (objCurrentQuizz != nil) {
        objCurrentQuizz.strConsultType = @"M";
        [self syncCreateConsult:objCurrentQuizz];
    }
    
}

////End TableView Datasource & Delegate////

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
