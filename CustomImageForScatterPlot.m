//
//  CustomImageForScatterPlot.m
//  zBox
//
//  Created by Zayar on 5/26/13.
//
//

#import "CustomImageForScatterPlot.h"

#import "CPTLayer.h"

@implementation CustomImageForScatterPlot

-(void)dealloc{
    
    [_image release];
    [super dealloc];
}
-(id)initWithImage:(UIImage *)image{
    
    CGRect f = CGRectMake(0, 0, image.size.width, image.size.height);
    
    if (self = [super initWithFrame:f]) {
        
        _image = [image retain];
    }
    
    return self;
    
}

-(void)drawInContext:(CGContextRef)ctx{
    
    CGContextDrawImage(ctx, self.bounds, _image.CGImage);
    
}
@end
