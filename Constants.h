//
//  Constants.h
//  zBox
//
//  Created by Zayar on 5/12/13.
//
//

#import <Foundation/Foundation.h>

#define IOS_OLDER_THAN_6 ( [ [ [ UIDevice currentDevice ] systemVersion ] floatValue ] < 6.0 )
#define IOS_NEWER_OR_EQUAL_TO_6 ( [ [ [ UIDevice currentDevice ] systemVersion ] floatValue ] >= 6.0 )
