//
//  SMQView.m
//  zBox
//
//  Created by Zayar Cn on 5/9/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "SMQView.h"
#import "zBoxAppDelegate.h"
#import "StringTable.h"
#import "ObjSubject.h"
#import "ObjCategory.h"
#import "Utility.h"

@implementation SMQView
@synthesize imgView,owner,selectedImage,isCamera;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)smqViewDidLoad{
    NSString * strPath = [NSString stringWithFormat:@"%@/img_smq_selected_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];
    [imgBgView release];
    arrCurrentSubject = [[NSMutableArray alloc]init];
    
    uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,170,0,0)];
    
    uiPickerView.delegate = self;
    uiPickerView.showsSelectionIndicator = YES;// note this is default to NO
    
    
    menu = [[UIActionSheet alloc] initWithTitle:@"Date"
                                       delegate:self
                              cancelButtonTitle:@"Done"
                         destructiveButtonTitle:@"Cancel"
                              otherButtonTitles:nil];
    
    //[btnCategory setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    //[btnSubject setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
}

- (void)smqViewWillAppear{
    NSLog(@"smq view image %@",selectedImage);
    //selectedImage = imgView.image;
    intCategorySelected = 0;
    intSubjectSelected = 0;
    [self syncSubject];
}

- (void)syncSubject{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    [delegate showLoadingScreen];
    subjectsRequest = [[SOAPRequest alloc] initWithOwner:self];
    subjectsRequest.processId = 5;
    [subjectsRequest syncSubjectAndCritea];
}

- (void)setImage:(UIImage *)img{
    UIImage * displayImage = [Utility imageByScalingProportionallyToSize:img newsize:CGSizeMake(278, 225) resizeFrame:YES];
    imgView.image = nil;
    imgView.image = displayImage;
    img = [Utility imageByScalingProportionallyToSize:img newsize:CGSizeMake(480, 640) resizeFrame:YES];
    self.selectedImage = img;
}

- (void) onErrorLoad: (int) processId{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    [delegate hideLoadingScreen];
    NSLog(@"Error loaded %d",processId);
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{
    
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    if (processId == 5) {
        
        [delegate hideLoadingScreen];
        NSLog(@"arr count %d",[dics count]);
        /*int status = [[dics objectForKey:@"status"] intValue];
        
        NSString * strMessage;
        if (status == STATUS_ACTION_SUCCESS) {
            NSLog(@"session checking is successed %@",strMessage);
            int intPoint =[[dics objectForKey:@"point"] intValue];
            NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
            [prefs setInteger:intPoint forKey:CHECK_USER_POINT_LINK];
        }
        else if(status == STATUS_NO_RECORD_FOUND){
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];	
            [alert release];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
        */
        NSMutableArray * arrSubject = [dics objectForKey:@"subjects"];
        for ( int i =0; i < [arrSubject count]; i++) {
            NSDictionary * tempSubjectDic = [arrSubject objectAtIndex:i];
            ObjSubject * objSubj = [[ObjSubject alloc]init];
            objSubj.subject_id = [[tempSubjectDic objectForKey:@"subject_id"]intValue];
            objSubj.strName = [tempSubjectDic objectForKey:@"subject_name"];
            objSubj.strTopUpStatus = [tempSubjectDic objectForKey:@"subject_top_up"];
            NSMutableArray * arrTemp = [tempSubjectDic objectForKey:@"categories"];
            for ( int y =0; y < [arrTemp count]; y++) {
                NSDictionary * tempCateDic = [arrTemp objectAtIndex:y];
                ObjCategory * objCate = [[ObjCategory alloc]init];
                objCate.idx = [[tempCateDic objectForKey:@"topic_id"]intValue];
                objCate.strName = [tempCateDic objectForKey:@"topic_name"];
                if(objSubj.arrCategories == nil) {
                    objSubj.arrCategories = [[NSMutableArray alloc] init];
                }
                
                [objSubj.arrCategories addObject:objCate];
                
                [objCate release];
            }
            [arrCurrentSubject addObject:objSubj];
            [objSubj release];
        }

        [self reloadSubjectView];
    }
    
    else if (processId == 6) {
            
            NSLog(@"arr count %d",[dics count]);
            int status = [[dics objectForKey:@"status"] intValue];
            
            NSString * strMessage = [dics objectForKey:@"message"];;
            if (status == STATUS_ACTION_SUCCESS) {
                NSLog(@"session checking is successed %@",strMessage);
                ///int intPoint =[[dics objectForKey:@"points"] intValue];
                //NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
                //[prefs setInteger:intPoint forKey:CHECK_USER_POINT_LINK];
                //[self reloadLabelPointWith:intPoint];
                [owner onSMQViewSuccessClose:isCamera];
                [self clearSubjectArray];
            }
            else if(status == STATUS_ACTION_FAILED){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];	
                [alert release];
            }
            else if(status == STATUS_SESSION_EXPIRED){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                [alert release];
                [delegate hideLoadingScreen];
                [delegate showLoginFromMain];
            }
            [owner onSMQViewSuccessClose:isCamera];
            [delegate hideLoadingScreen];
    }
    
    else if (processId == 17) {
        
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        
        NSString * strMessage = [dics objectForKey:@"message"];;
        if (status == STATUS_ACTION_SUCCESS) {
            NSLog(@"session checking is successed %@",strMessage);
            ///int intPoint =[[dics objectForKey:@"points"] intValue];
            //NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
            //[prefs setInteger:intPoint forKey:CHECK_USER_POINT_LINK];
            //[self reloadLabelPointWith:intPoint];
            //[owner onSMQViewSuccessClose:isCamera];
            [self syncTheQuestion];
        }
        else if(status == STATUS_ACTION_FAILED){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
            [delegate hideLoadingScreen];
            [delegate showLoginFromMain];
        }
        [owner onSMQViewSuccessClose:isCamera];
        [delegate hideLoadingScreen];
    }
}

- (void) syncTheQuestion{
    ObjSubject * objSub = [arrCurrentSubject objectAtIndex:intSubjectSelected];
    ObjCategory * objCate = [objSub.arrCategories objectAtIndex:intCategorySelected];
    [self syncSubmitQuestion:objSub.subject_id andCateGoryId:objCate.idx andImage:selectedImage andSubName:objSub.strName andCatName:objCate.strName];
}

- (void)reloadSubjectView{
    if ([arrCurrentSubject count]>0) {
        ObjSubject * objSub = [arrCurrentSubject objectAtIndex:intSubjectSelected];
        [btnSubject setTitle:objSub.strName forState:normal];
        
        ObjCategory * objCate = [objSub.arrCategories objectAtIndex:intCategorySelected];
        [btnCategory setTitle:objCate.strName forState:normal];
    }
}

- (void)syncSubmitQuestion:(int)subjectId andCateGoryId:(int)cateId andImage:(UIImage *)image andSubName:(NSString *)subName andCatName:(NSString *) catName{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    int userPoint = [prefs integerForKey:CHECK_USER_POINT_LINK];
    if (userPoint > 0) {

        submitQuestionRequest = [[SOAPRequest alloc] initWithOwner:self];
        submitQuestionRequest.processId = 6;
        NSLog(@"%@", subName);
        NSLog(@"%@", catName);
        [submitQuestionRequest syncSubmitQuestion:image andSubjectId:subjectId andCategoryId:cateId andSubjectName:subName andCategoryName:catName];
    }
    else {
        [delegate hideLoadingScreen];
        [self textValidateAlertShow:@"You have not enough point to submit question"];
    }
    
}

- (void) textValidateAlertShow:(NSString *) strError{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: APP_TITLE
                          message: strError
                          delegate: nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
    [alert release];
    
}

- (IBAction)onSubject:(id)sender{
    [menu setTitle:@"Subject"];
    // Add the picker
    uiPickerView.tag = 0;
    uiPickerView.delegate=self;
    [uiPickerView reloadAllComponents];
    [uiPickerView selectRow:intSubjectSelected inComponent:0 animated:YES];
    [menu addSubview:uiPickerView];
    [menu showInView:self];
    [menu setBounds:CGRectMake(0,0,320,500)];
}

- (IBAction)onCategory:(id)sender{
    [menu setTitle:@"Category"];
    // Add the picker
    uiPickerView.tag = 1;
    uiPickerView.delegate=self;
    [uiPickerView reloadAllComponents];
    [uiPickerView selectRow:intCategorySelected inComponent:0 animated:YES];
    [menu addSubview:uiPickerView];
    [menu showInView:self];
    [menu setBounds:CGRectMake(0,0,320,500)];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    
    if (pickerView.tag == 0) {
        ObjSubject * objSubject = [arrCurrentSubject objectAtIndex:row];
        NSLog(@"status name %@",objSubject.strName);
        return objSubject.strName;
    }
    if (pickerView.tag == 1) {
        ObjSubject * objSubject = [arrCurrentSubject objectAtIndex:intSubjectSelected];
        ObjCategory  * objCate = [objSubject.arrCategories objectAtIndex:row];
        NSLog(@"status name %@",objCate.strName);
        return objCate.strName;
    }
    /*else if (pickerView.tag == 1) {
        ObjectCurrency * objCurrency= [arrCurrencyFrom objectAtIndex:row];
        //ObjectExchangeRate * objExchage = [delegate.db getLastExchangeRateBy:objCurrency.uniqIdx andDate:strSelectedDate];
        ObjectExchangeRate * objExchage = [delegate.db getLastExchangeRateBy:objCurrency.uniqIdx andDate:strSelectedDate andTime:strSelectedTime];
        rate=objExchage.rate;
        amount=[txtAmount.text floatValue];
        return objCurrency.name;
    }
    else if (pickerView.tag == 2) {
        ObjectCurrency * objCurrency = [arrCurrencyTo objectAtIndex:row];
        // ObjectExchangeRate * objExchage = [delegate.db getLastExchangeRateBy:objCurrency.uniqIdx andDate:strSelectedDate];
        ObjectExchangeRate * objExchage = [delegate.db getLastExchangeRateBy:objCurrency.uniqIdx andDate:strSelectedDate andTime:strSelectedTime];
        inverseRate=objExchage.rate;
        amount=[txtAmount.text floatValue];
        return objCurrency.name;
    }
    else if (pickerView.tag == 3) {
        NSString * strTime=[arrTime objectAtIndex:row];
        return strTime;
    }*/
    return @"";
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == 0) {
        return [arrCurrentSubject count];
    }
    if (pickerView.tag == 1) {
        ObjSubject * objSubject = [arrCurrentSubject objectAtIndex:intSubjectSelected];
        return [objSubject.arrCategories count];
    }
    /*else if (pickerView.tag == 1 ) {
        return [arrCurrencyFrom count];
    }
    else if (pickerView.tag == 2 ) {
        return [arrCurrencyTo count];
    }
    else if (pickerView.tag == 3 ) {
        return [arrTime count];
    }*/
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"didSelectRow>>>>didSelectRow");
    if (pickerView.tag == 0) {
        intSubjectSelected = row;
    }
    if (pickerView.tag == 1) {
        intCategorySelected = row;
    }
    /*else if(pickerView.tag == 1){
        intFromIndex = row;
    }
    else if(pickerView.tag == 2){
        intToIndex = row;
    }
    else if(pickerView.tag == 3){
        intTimeIndex = row;
    }*/
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    
    if (buttonIndex == 0) {
        //self.label.text = @"Destructive Button";
        NSLog(@"Cancel Button");
    } 
    
    else if (buttonIndex == 1) {
        NSLog(@"Other Button Done Clicked and selected index %d",intSubjectSelected);
        if (uiPickerView.tag == 0){
            //Date picker click
            
            ObjSubject * objSubject = [arrCurrentSubject objectAtIndex:intSubjectSelected];
            [btnSubject setTitle:objSubject.strName forState:normal];
            
            intCategorySelected = 0;
            ObjCategory * objCate = [objSubject.arrCategories objectAtIndex:intCategorySelected];
            [btnCategory setTitle:objCate.strName forState:normal];
            
        }
        if (uiPickerView.tag == 1){
            //Date picker click
            
            ObjSubject * objSubject = [arrCurrentSubject objectAtIndex:intSubjectSelected];
            ObjCategory * objCate = [objSubject.arrCategories objectAtIndex:intCategorySelected];
            [btnCategory setTitle:objCate.strName forState:normal];
            //strSelectedDate = strDate;
        }
        /*else if(uiPickerView.tag == 1){
            
            //from
            ObjectCurrency * objCurrency = [arrCurrencyFrom objectAtIndex:intFromIndex];
            [btnFrom setTitle:objCurrency.name forState:normal];
            
            //ObjectExchangeRate * objExchage = [delegate.db getLastExchangeRateBy:objCurrency.uniqIdx andDate:strSelectedDate];
            ObjectExchangeRate * objExchage = [delegate.db getLastExchangeRateBy:objCurrency.uniqIdx andDate:strSelectedDate andTime:strSelectedTime];
            
            isDefault=objCurrency.isDefault;
            rate=objExchage.rate;//from rate
            
            if(objCurrency.isDefault==1){
                btnTo.enabled=TRUE;
                arrCurrencyTo = [delegate.db getAllCurrencyBy:0];
                ObjectCurrency * objCurrency = [arrCurrencyTo objectAtIndex:0];
                
                //ObjectExchangeRate * objDefaulSingExchageRate = [delegate.db getLastExchangeRateBy:objCurrency.uniqIdx andDate:strSelectedDate];
                ObjectExchangeRate * objDefaulSingExchageRate = [delegate.db getLastExchangeRateBy:objCurrency.uniqIdx andDate:strSelectedDate andTime:strSelectedTime];
                NSLog(@"objCurrency %@ and objexchange %f and defaulf inverse rate %f",objCurrency.name,objExchage.rate,objDefaulSingExchageRate.rate);
                [btnTo setTitle:objCurrency.name forState:normal];
                rate=objExchage.rate;//to rate
                inverseRate = objDefaulSingExchageRate.rate;
            }
            if(objCurrency.isDefault==0){
                btnTo.enabled=FALSE;
                arrCurrencyTo = [delegate.db getAllCurrencyBy:1];
                ObjectCurrency * objCurrency = [arrCurrencyTo objectAtIndex:0];
                [btnTo setTitle:objCurrency.name forState:normal];
                inverseRate=objExchage.rate;
            }
            if(objCurrency.isDefault==1){
                if(inverseRate==0){
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Exchange Rate(time)" message:@"There is no Exchange Rate at that time!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                }
            }
            else{
                if(rate==0){
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Exchange Rate(time)" message:@"There is no Exchange Rate at that time!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                }
            }
            [self calculation];
        }*/
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissModalViewControllerAnimated:YES];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)clearSubjectArray{
    if (arrCurrentSubject != nil) {
        [arrCurrentSubject removeAllObjects];
    }
}

- (IBAction)onCancel:(id)sender{
    [self clearSubjectArray];
    [owner onSMQViewCancel];
}

- (IBAction)onSend:(id)sender{
    if ([arrCurrentSubject count]>0) {
        
        zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
        
        ObjSubject * objSub = [arrCurrentSubject objectAtIndex:intSubjectSelected];
        ObjCategory * objCate = [objSub.arrCategories objectAtIndex:intCategorySelected];
        if (objSub.strTopUpStatus != (id)[NSNull null]) {
            if ([objSub.strTopUpStatus isEqualToString:STATUS_NOTTOPUP]) {
                [delegate showLoadingScreen];
                [self syncSubmitQuestion:objSub.subject_id andCateGoryId:objCate.idx andImage:selectedImage andSubName:objSub.strName andCatName:objCate.strName];
            }
            else if([objSub.strTopUpStatus isEqualToString:STATUS_TOPUP]){
                [self showDialogForSupervisorPass];
            }
        
        } else {
            [delegate showLoadingScreen];
            [self syncSubmitQuestion:objSub.subject_id andCateGoryId:objCate.idx andImage:selectedImage andSubName:objSub.strName andCatName:objCate.strName];
        }
    }
}

- (void) showDialogForSupervisorPass{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Supervisor's Password" message:@"   " delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    if (txtSv == nil) {
        txtSv = [[UITextField alloc]initWithFrame:CGRectMake(40, 45, 215, 30)];
    }
    txtSv.tag = 0;
    [txtSv setTextAlignment:UITextAlignmentCenter];
    txtSv.placeholder = @"password";
    [txtSv setBorderStyle:UITextBorderStyleBezel];
    txtSv.secureTextEntry = TRUE;
    txtSv.backgroundColor = [UIColor whiteColor];
    
    [alert addSubview:txtSv];
    alert.frame =  CGRectMake(10, 100, 310, 320);
    [alert show];
    [alert release];
}

- (void) alertView: (UIAlertView *)alertView clickedButtonAtIndex:(NSInteger) buttonIndex{
    if( [alertView tag] == 0 ){
        if(buttonIndex == 1) {
            [self syncSPassWith:txtSv.text];
		}
	}
    
}

- (void)syncSPassWith:(NSString *)strPass{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    [delegate showLoadingScreen];
    if (checkSPassRequest == nil ) {
        checkSPassRequest = [[SOAPRequest alloc] initWithOwner:self];
    }
    checkSPassRequest.processId = 17;
    [checkSPassRequest syncSupervieorPassWith:strPass];
}

- (void) dealloc{
    [arrCurrentSubject release];
    [subjectsRequest release];
    [imgBgView release];
    [imgView release];
    [super dealloc];
}

@end
