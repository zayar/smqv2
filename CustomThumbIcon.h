//
//  CustomThumbIcon.h
//  zBox
//
//  Created by Zayar Cn on 6/6/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CustomThumbIconDelegate 
- (void) onThumbIcon_Click:(id) sender;
@end

@interface CustomThumbIcon : UIView<UIGestureRecognizerDelegate>
{
    UIImageView * imgShowdowView;
    UIImageView * imgThumbView;
    UIImageView * imgOverlayTapeView;
    id<CustomThumbIconDelegate> owner;
}
@property (assign) id<CustomThumbIconDelegate> owner;
@property (nonatomic,retain) UIImageView * imgThumbView;
- (void) attachEventHandlers;
-(void)customeSetImage:(UIImage *)img;
- (void)selectedImageStyle;
- (void)deSelectedImageStyle;
@end
