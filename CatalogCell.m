//
//  CatalogCell.m
//  zBox
//
//  Created by Zayar on 5/8/13.
//
//

#import "CatalogCell.h"

@implementation CatalogCell
@synthesize imgBgView,lblTitle,lblText,activity,webThumbView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawContent{
    UIFont * cfont = [UIFont fontWithName:@"BradyBunchRemastered" size:24.0];
    UIFont * sfont = [UIFont fontWithName:@"BradyBunchRemastered" size:18.0];
    //cell.lblTitle.font = cfont;
    //cell.lblText.font = sfont;
    CGPoint point = CGPointMake(1,1);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 0.7);
    CGContextSetRGBStrokeColor(context, 2, 2, 2, 1.0);
    CGContextSetTextDrawingMode(context, kCGTextFillStroke);
    CGContextSaveGState(context);
    
    // I change it to my outlet
    [self.lblTitle.text drawAtPoint:point withFont:cfont];
    [self.lblText.text drawAtPoint:point withFont:sfont];
    
    CGContextRestoreGState(context);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc{
    [webThumbView release];
    [imgView release];
    [lblText release];
    [activity release];
    [lblTitle release];
    [imgBgView release];
    [super dealloc];
}

@end
