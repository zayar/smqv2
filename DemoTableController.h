//
//  DemoTableControllerViewController.h
//  FPPopoverDemo
//
//  Created by Alvise Susmel on 4/13/12.
//  Copyright (c) 2012 Fifty Pixels Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol DemoTableDelegate
- (void) onSelectActual;
- (void) onSelectCouldve;
- (void) onSelectActualAndCouldve;
@end
@interface DemoTableController : UITableViewController{
    id<DemoTableDelegate> owner;
}
@property id<DemoTableDelegate> owner;
@end
