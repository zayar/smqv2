//
//  EskLinePlot.m
//  CorePlotSample
//
//  Created by Ken Wong on 8/9/11.
//  Copyright 2011 Essence Work LLC. All rights reserved.
//

#import "EskLine2TouchPlot.h"
#import "ObjSubject.h"
#import "ObjSubSession.h"
#import "CustomImageForScatterPlot.h"

#define kHighPlot @"HighPlot"
#define kLinePlot @"LinePlot"
#define kLinePlot2 @"LinePlot2"
#define kObjectivePlot @"ObjectivePlot"
#define kSelectedPlot @"SelectedPlot"
#define kNumberOfMarkerPlotSymbols 3

@implementation EskLine2TouchPlot

@synthesize delegate;
@synthesize sampleData, sampleYears,objectiveDatas;

- (id)init
{
    self = [super init];
    if (self)
    {
        arrTouchPoint = [[NSMutableArray alloc] initWithCapacity:2];
    }
    
    return self;
}

- (void)loadTheDataWith:(ObjSubject *)objSubject andIsActual:(BOOL)isActual{
    // setting up the sample data here.
    
    /*sampleYears = [[NSMutableArray alloc] initWithObjects:@"2010/4/3", @"2011/4/3", @"2012/4/3", @"2013/4/3", @"2014/4/3", @"2015/4/3", @"2016/4/3",@"2017/4/3", nil];
    
    sampleData = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInt:60],
                  [NSNumber numberWithInt:30],
                  [NSNumber numberWithInt:20],
                  [NSNumber numberWithInt:50],
                  [NSNumber numberWithInt:70],
                  [NSNumber numberWithInt:85],
                  [NSNumber numberWithInt:65],
                  [NSNumber numberWithInt:90],nil];*/
    //arrSelectedArr = [[NSMutableArray alloc]init];
    NSLog(@"dates count %d and objective %f",[objSubject.arrSession count],objSubject.objectiveMark);
    sampleYears = [[NSMutableArray alloc] initWithCapacity:[objSubject.arrSession count]];
    objectiveDatas = [[NSMutableArray alloc] initWithCapacity:[objSubject.arrSession count]];
    sampleData = [[NSMutableArray alloc] initWithCapacity:[objSubject.arrSession count]];
    NSString * strDate=@"";
    NSArray * allReversed = [[objSubject.arrSession reverseObjectEnumerator] allObjects];

    for (ObjSubSession * objSub in allReversed) {
        NSLog(@"date %@ and actual scores %@ and couldve scores %@ ",objSub.strReportDate,objSub.strProgressActualScores,objSub.strProgressCouldveScores);
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"MMM"];
        strDate = [dateFormatter stringFromDate:objSub.reportDate];
        [sampleYears addObject:strDate];
        [dateFormatter release];
        if (isActual) {
            [sampleData addObject:objSub.strProgressActualScores];
            
        }
        else{
            [sampleData addObject:objSub.strProgressCouldveScores];
        }
        isActualPlot = isActual;
        
        [objectiveDatas addObject:[NSString stringWithFormat:@"%0.2f", objSubject.objectiveMark]];
    }
}

- (void)dealloc
{
    [arrTouchPoint release];
    [objectivePlot release];
    [objectiveDatas release];
    [sampleData release];
    [sampleYears release];
    [linePlot release];
    [touchPlot release];
    [super dealloc];
}

- (void)renderInLayer:(CPTGraphHostingView *)layerHostingView withTheme:(CPTTheme *)theme
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    CGRect bounds = layerHostingView.bounds;
    
    // Create the graph and assign the hosting view.
    graph = [[CPTXYGraph alloc] initWithFrame:bounds];
    layerHostingView.hostedGraph = graph;

    [graph applyTheme:theme];
    
    // Change gradient color here
    graph.fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:0.21f green:0.28f blue:0.45f  alpha:1.0f]];

    graph.plotAreaFrame.masksToBorder = NO;
    
    // chang the chart layer orders so the axis line is on top of the bar in the chart.
    NSArray *chartLayers = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:CPTGraphLayerTypePlots],
                            [NSNumber numberWithInt:CPTGraphLayerTypeMajorGridLines],
                            [NSNumber numberWithInt:CPTGraphLayerTypeMinorGridLines],
                            [NSNumber numberWithInt:CPTGraphLayerTypeAxisLines],
                            [NSNumber numberWithInt:CPTGraphLayerTypeAxisLabels],
                            [NSNumber numberWithInt:CPTGraphLayerTypeAxisTitles],
                            nil];
    graph.topDownLayerOrder = chartLayers;
    [chartLayers release];
    
    
    // Add plot space for horizontal bar charts
    //Default
    /*graph.paddingLeft = 90.0;
     graph.paddingTop = 50.0;
     graph.paddingRight = 20.0;
     graph.paddingBottom = 60.0;*/
    graph.paddingLeft = 50.0;
	graph.paddingTop = 10.0;
	graph.paddingRight = 30.0;
	graph.paddingBottom = 40.0;
    
    // Setup plot space
    plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    plotSpace.allowsUserInteraction = YES;
    plotSpace.delegate = self;
    NSLog(@"data count %d",[sampleData count]);
    if ([sampleData count] > 20) {
        plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(20.0f)];
    }
    else{
        plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat([sampleData count] - 1)];
    }
    
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(100)];
    
    // Setup grid line style
    CPTMutableLineStyle *majorXGridLineStyle = [CPTMutableLineStyle lineStyle];
    majorXGridLineStyle.lineWidth = 1.0f;
    majorXGridLineStyle.lineColor = [[CPTColor grayColor] colorWithAlphaComponent:0.25f];
    
    // Setup x-Axis.
	CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    CPTXYAxis *x = axisSet.xAxis;
    x.labelingPolicy = CPTAxisLabelingPolicyNone;
    x.majorGridLineStyle = majorXGridLineStyle;
    x.majorIntervalLength = CPTDecimalFromString(@"1");
    x.minorTicksPerInterval = 1;
    
    x.orthogonalCoordinateDecimal = CPTDecimalFromString(@"0");
    x.title = @"";
    x.timeOffset = 30.0f;
 	NSArray *exclusionRanges = [NSArray arrayWithObjects:[CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0) length:CPTDecimalFromInt(0)], nil];
	x.labelExclusionRanges = exclusionRanges;
    
    // Use custom x-axis label so it will display year 2010, 2011, 2012, ... instead of 1, 2, 3, 4
    NSMutableArray *labels = [[NSMutableArray alloc] initWithCapacity:[sampleYears count]];
    int idx = 0;
    for (NSString *year in sampleYears)
    {
        if (idx%5 == 0) {
            CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:year textStyle:x.labelTextStyle];
            label.tickLocation = CPTDecimalFromInt(idx);
            label.offset = 5.0f;
            [labels addObject:label];
            [label release];
        }
        idx++;
    }
    x.axisLabels = [NSSet setWithArray:labels];
    [labels release];
    
    // Setup y-Axis.
    CPTMutableLineStyle *majorYGridLineStyle = [CPTMutableLineStyle lineStyle];
    majorYGridLineStyle.lineWidth = 1.0f;
    majorYGridLineStyle.dashPattern =  [NSArray arrayWithObjects:[NSNumber numberWithFloat:5.0f], [NSNumber numberWithFloat:5.0f], nil];
    majorYGridLineStyle.lineColor = [[CPTColor lightGrayColor] colorWithAlphaComponent:0.25];
    
    
    CPTXYAxis *y = axisSet.yAxis;
    y.majorGridLineStyle = majorYGridLineStyle;
    y.majorIntervalLength = CPTDecimalFromString(@"10");
    y.minorTicksPerInterval = 1;
    y.orthogonalCoordinateDecimal = CPTDecimalFromString(@"0");
    NSArray *yExlusionRanges = [NSArray arrayWithObjects:
                                [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0) length:CPTDecimalFromFloat(0.0)],
                                nil];
    y.labelExclusionRanges = yExlusionRanges;
    
    // Create a high plot area
	highPlot = [[[CPTScatterPlot alloc] init] autorelease];
    highPlot.identifier = kHighPlot;
    
    CPTMutableLineStyle *highLineStyle = [[highPlot.dataLineStyle mutableCopy] autorelease];
	highLineStyle.lineWidth = 2.f;
    if (isActualPlot) {
        highLineStyle.lineColor = [CPTColor whiteColor];
    }
    else{
        highLineStyle.lineColor = [CPTColor yellowColor];
    }
    
    highPlot.dataLineStyle = highLineStyle;
    highPlot.dataSource = self;
	
    CPTFill *areaFill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:0.50f green:0.67f blue:0.65f alpha:0.4f]];
    highPlot.areaFill = areaFill;
    highPlot.areaBaseValue = CPTDecimalFromString(@"0");
    [graph addPlot:highPlot];
    
    
    // Create a high plot area
	CPTScatterPlot *objectivePlot = [[[CPTScatterPlot alloc] init] autorelease];
    objectivePlot.identifier = kObjectivePlot;
    
    CPTMutableLineStyle *objectiveStyle = [[objectivePlot.dataLineStyle mutableCopy] autorelease];
	objectiveStyle.lineWidth = 2.f;
    //highLineStyle.lineColor = [CPTColor colorWithComponentRed:0.50f green:0.67f blue:0.65f alpha:1.0f];
    objectiveStyle.lineColor = [CPTColor greenColor];
    objectivePlot.dataLineStyle = objectiveStyle;
    objectivePlot.dataSource = self;
    
    //CPTFill *areaFill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:0.50f green:0.67f blue:0.65f alpha:0.4f]];
    CPTFill *objectiveFill = [CPTFill fillWithColor:[CPTColor clearColor]];
    objectivePlot.areaFill = objectiveFill;
    objectivePlot.areaBaseValue = CPTDecimalFromString(@"0");
    [graph addPlot:objectivePlot];
    
    // Create the Savings Marker Plot
    selectedCoordination = 0;
    selected2Coordination = 0;
    
    touchPlot = [[[CPTScatterPlot alloc] initWithFrame:CGRectNull] autorelease];
    touchPlot.identifier = kLinePlot;
    touchPlot.dataSource = self;
    touchPlot.delegate = self;
    touchPlot.hidden = TRUE;
    [self applyTouchPlotColor];
    [graph addPlot:touchPlot];
    
    touchPlot2 = [[[CPTScatterPlot alloc] initWithFrame:CGRectNull] autorelease];
     touchPlot2.identifier = kLinePlot2;
     touchPlot2.dataSource = self;
     touchPlot2.delegate = self;
    touchPlot2.hidden = TRUE;
     [self applyTouchPlot2Color];
     [graph addPlot:touchPlot2];
    
    /*selectedPlot = [[[CPTScatterPlot alloc] initWithFrame:CGRectNull] autorelease];
    selectedPlot.identifier = kSelectedPlot;
    selectedPlot.dataSource = self;
    selectedPlot.delegate = self;
    //selectedPlot.hidden = TRUE;
    [self applySelectedPlotColor];
    //[graph addPlot:touchPlot2];*/
    
    [pool drain];
}

// Assign different color to the touchable line symbol.
- (void)applyTouchPlotColor
{
    CPTColor *touchPlotColor = [CPTColor yellowColor];
    
    CPTMutableLineStyle *savingsPlotLineStyle = [CPTMutableLineStyle lineStyle];
    savingsPlotLineStyle.lineColor = touchPlotColor;
    
    CPTPlotSymbol *touchPlotSymbol = [CPTPlotSymbol ellipsePlotSymbol];
    //touchPlotSymbol.fill = [CPTFill fillWithGradient:[CPTGradient gradientWithBeginningColor:[CPTColor] endingColor:<#(CPTColor *)#>]];
    
    touchPlotSymbol.fill = [CPTFill fillWithImage:[[CPTImage alloc] initForPNGFile:[[NSBundle mainBundle] pathForResource:@"dot" ofType:@"png"]]];
    
    touchPlotSymbol.lineStyle = savingsPlotLineStyle;
    touchPlotSymbol.size = CGSizeMake(15.0f, 15.0f);
    
    
    touchPlot.plotSymbol = touchPlotSymbol;
    
    CPTMutableLineStyle *touchLineStyle = [CPTMutableLineStyle lineStyle];
    touchLineStyle.lineColor = [CPTMutableLineStyle lineStyle];
    touchLineStyle.lineColor = [CPTColor colorWithComponentRed:207/255.0f green:181/255.0f blue:59/255.0f alpha:1.0f];
    touchLineStyle.lineWidth = 2.0f;
    
    touchPlot.dataLineStyle = touchLineStyle;
    
}

- (void)applyTouchPlot2Color
{
    CPTColor *touchPlotColor = [CPTColor yellowColor];
    
    CPTMutableLineStyle *savingsPlotLineStyle = [CPTMutableLineStyle lineStyle];
    savingsPlotLineStyle.lineColor = touchPlotColor;
    
    CPTPlotSymbol *touchPlotSymbol = [CPTPlotSymbol ellipsePlotSymbol];
    touchPlotSymbol.fill = [CPTFill fillWithImage:[[CPTImage alloc] initForPNGFile:[[NSBundle mainBundle] pathForResource:@"dot" ofType:@"png"]]];
    touchPlotSymbol.lineStyle = savingsPlotLineStyle;
    touchPlotSymbol.size = CGSizeMake(15.0f, 15.0f);
    
    touchPlot2.plotSymbol = touchPlotSymbol;
    
    CPTMutableLineStyle *touchLineStyle = [CPTMutableLineStyle lineStyle];
    touchLineStyle.lineColor = [CPTColor colorWithComponentRed:207/255.0f green:181/255.0f blue:59/255.0f alpha:1.0f];
    //207,181,59
    touchLineStyle.lineWidth = 2.0f;
    
    touchPlot2.dataLineStyle = touchLineStyle;
    
}

- (void)applySelectedPlotColor
{
    CPTColor *touchPlotColor = [CPTColor yellowColor];
    
    CPTMutableLineStyle *savingsPlotLineStyle = [CPTMutableLineStyle lineStyle];
    savingsPlotLineStyle.lineColor = touchPlotColor;
    
    CPTPlotSymbol *touchPlotSymbol = [CPTPlotSymbol ellipsePlotSymbol];
    touchPlotSymbol.fill = [CPTFill fillWithImage:[[CPTImage alloc] initForPNGFile:[[NSBundle mainBundle] pathForResource:@"dot" ofType:@"png"]]];
    touchPlotSymbol.lineStyle = savingsPlotLineStyle;
    touchPlotSymbol.size = CGSizeMake(15.0f, 15.0f);
    
    selectedPlot.plotSymbol = touchPlotSymbol;
    
    CPTMutableLineStyle *touchLineStyle = [CPTMutableLineStyle lineStyle];
    touchLineStyle.lineColor = [CPTColor colorWithComponentRed:207/255.0f green:181/255.0f blue:59/255.0f alpha:1.0f];
    //207,181,59
    touchLineStyle.lineWidth = 2.0f;
    
    selectedPlot.dataLineStyle = touchLineStyle;
    
}

// Highlight the touch plot when the user holding tap on the line symbol.
- (void)applyHighLightPlotColor:(CPTScatterPlot *)plot
{
    CPTColor *selectedPlotColor = [CPTColor redColor];
    
    CPTMutableLineStyle *symbolLineStyle = [CPTMutableLineStyle lineStyle];
    symbolLineStyle.lineColor = selectedPlotColor;
    
    CPTPlotSymbol *plotSymbol = nil;
    plotSymbol = [CPTPlotSymbol ellipsePlotSymbol];
    plotSymbol.fill = [CPTFill fillWithImage:[[CPTImage alloc] initForPNGFile:[[NSBundle mainBundle] pathForResource:@"dot" ofType:@"png"]]];
    plotSymbol.lineStyle = symbolLineStyle;
    plotSymbol.size = CGSizeMake(15.0f, 15.0f);
    //CustomImageForScatterPlot* layer = [[CustomImageForScatterPlot alloc] initWithImage:[UIImage imageNamed:@"dot.png"]];
    //layer.frame = CGRectMake(2, 0, 34, 6);
    
    plot.plotSymbol = plotSymbol;
    
    CPTMutableLineStyle *selectedLineStyle = [CPTMutableLineStyle lineStyle];
    selectedLineStyle.lineColor = [CPTColor colorWithComponentRed:207/255.0f green:181/255.0f blue:59/255.0f alpha:1.0f];
    selectedLineStyle.lineWidth = 3.0f;
    
    plot.dataLineStyle = selectedLineStyle;
    
    //highPlot.areaBaseValue = CPTDecimalFromInteger(firstIndex);
    //plot.dataLineStyle = selectedLineStyle;
    //plot.anchorPoint
    
    
}

//- (CPTLayer *)dataLabelForPlot:(CPTPlot *)plot recordIndex:(NSUInteger)index
//{
//   // CustomImageForScatterPlot* layer = [[CustomImageForScatterPlot alloc] initWithImage:[UIImage imageNamed:@"dot.png"]];
//    //layer.frame = CGRectMake(2, 0, 34, 6);
//    
//    //return layer;
//}

#pragma mark - CPPlotSpace Delegate Methods
// This implementation of this method will put the line graph in a fix position so it won't be scrollable.
-(CPTPlotRange *)plotSpace:(CPTPlotSpace *)space willChangePlotRangeTo:(CPTPlotRange *)newRange forCoordinate:(CPTCoordinate)coordinate
{

    if (coordinate == CPTCoordinateY) {
        return ((CPTXYPlotSpace *)space).yRange;
    }
    else
    {
        return ((CPTXYPlotSpace *)space).xRange;
    }
}

// This method is call when user touch & drag on the plot space.
- (BOOL)plotSpace:(CPTPlotSpace *)space shouldHandlePointingDeviceDraggedEvent:(id)event atPoint:(CGPoint)point
{

    // Convert the touch point to plot area frame location
    CGPoint pointInPlotArea = [graph convertPoint:point toLayer:graph.plotAreaFrame];
    NSLog(@"touch and point %f and %f",pointInPlotArea.x,pointInPlotArea.y);
    //NSArray *sortedTouches = [[event allControlEvents];
    
    NSDecimal newPoint[2];
    [graph.defaultPlotSpace plotPoint:newPoint forPlotAreaViewPoint:pointInPlotArea];
    NSDecimalRound(&newPoint[0], &newPoint[0], 0, NSRoundPlain);
    int x = [[NSDecimalNumber decimalNumberWithDecimal:newPoint[0]] intValue];
    
    if (x < 0)
    {
        x = 0;
    }
    else if (x > [sampleData count])
    {
        x = [sampleData count]-1;
    }
    
   
            selected2Coordination = x;
            touchPlot2.hidden = FALSE;
            if ([delegate respondsToSelector:@selector(eskLine2TouchPlot2:indexLocation:withFirstIndex:)])
                [delegate eskLine2TouchPlot2:self indexLocation:selected2Coordination withFirstIndex:firstIndex];
            secondIndex = selected2Coordination;
            [touchPlot2 reloadData];
            [self fillSelectedAreaWithStart:firstIndex andEndPoint:secondIndex];
    
    return YES;
}

- (BOOL)plotSpace:(CPTPlotSpace *)space shouldHandlePointingDeviceDownEvent:(id)event
          atPoint:(CGPoint)point
{
    CGPoint pointInPlotArea = [graph convertPoint:point toLayer:graph.plotAreaFrame];
    NSDecimal newPoint[2];
    [graph.defaultPlotSpace plotPoint:newPoint forPlotAreaViewPoint:pointInPlotArea];
    NSDecimalRound(&newPoint[0], &newPoint[0], 0, NSRoundPlain);
    int x = [[NSDecimalNumber decimalNumberWithDecimal:newPoint[0]] intValue];
    
    
    if (x < 0)
    {
        x = 0;
    }
    else if (x > [sampleData count])
    {
        x = [sampleData count]-1;
    }
    if (!touchPlotSelected)
    {
        NSLog(@"point in plot");
        if (touchPlot2.hidden) {
            touchPlot.hidden = FALSE;
            selectedCoordination = x;
            
            if ([delegate respondsToSelector:@selector(eskLine2TouchPlot:indexLocation:withSecondIndex:andFirstPoint:andSecondPoint:)])
                [delegate eskLine2TouchPlot:self indexLocation:selectedCoordination withSecondIndex:secondIndex andFirstPoint:firstPoint andSecondPoint:secondPoint];
            [touchPlot reloadData];
            firstIndex = selectedCoordination;
            //[self fillSelectedAreaWithStart:firstIndex andEndPoint:secondIndex];
            touchedInPlot = TRUE;
            firstPoint = point;
        }
       
    }
    
    if (!touchPlot2Selected)
    {
        NSLog(@"point in plot2");
        if (!touchPlot.hidden){
            touchPlot2.hidden = FALSE;
            selected2Coordination = x;
            
            if ([delegate respondsToSelector:@selector(eskLine2TouchPlot2:indexLocation:withFirstIndex:)])
                [delegate eskLine2TouchPlot2:self indexLocation:selected2Coordination withFirstIndex:firstIndex];
            secondIndex = selected2Coordination;
            [touchPlot2 reloadData];
            //[self fillSelectedAreaWithStart:firstIndex andEndPoint:secondIndex];
             touchedInPlot = TRUE;
            secondPoint = point;
        }
        
    }

    //[graph convertPoint:[touch1 locationInView:graph.view] toLayer:graph.plotAreaFrame];
    return YES;
}

- (BOOL)plotSpace:(CPTPlotSpace *)space shouldHandlePointingDeviceUpEvent:(id)event atPoint:(CGPoint)point
{
    // Restore the vertical line plot to its initial color.
    [self applyTouchPlotColor];
    [self applyTouchPlot2Color];
    touchPlotSelected = NO;
    touchPlot2Selected = NO;
    touched2InPlot = YES;
    touchedInPlot = YES;
    return YES;
}

#pragma mark -
#pragma mark Scatter plot delegate methods

- (void)scatterPlot:(CPTScatterPlot *)plot plotSymbolWasSelectedAtRecordIndex:(NSUInteger)index
{
    if ([(NSString *)plot.identifier isEqualToString:kLinePlot])
    {
        touchPlotSelected = YES;
        [self applyHighLightPlotColor:plot];
        if ([delegate respondsToSelector:@selector(eskLine2TouchPlot:indexLocation:withSecondIndex:andFirstPoint:andSecondPoint:)])
            [delegate eskLine2TouchPlot:self indexLocation:index withSecondIndex:secondIndex andFirstPoint:firstPoint andSecondPoint:secondPoint];
        firstIndex = index;
        [self fillSelectedAreaWithStart:firstIndex andEndPoint:secondIndex];
    }
    
    if ([(NSString *)plot.identifier isEqualToString:kLinePlot2])
    {
        touchPlot2Selected = YES;
        [self applyHighLightPlotColor:plot];
        if ([delegate respondsToSelector:@selector(eskLine2TouchPlot2:indexLocation:withFirstIndex:)])
            [delegate eskLine2TouchPlot2:self indexLocation:index withFirstIndex:firstIndex];
        secondIndex = index;
        [self fillSelectedAreaWithStart:firstIndex andEndPoint:secondIndex];
    }
}

- (void)fillSelectedAreaWithStart:(NSInteger)startPoint andEndPoint:(NSInteger)endPoint{
    /*NSLog(@"start point %d and end point %d",startPoint,endPoint);
    if (startPoint==endPoint) {
        NSLog(@"There is one point...");
    }
    else if (startPoint>endPoint) {
        int startIndex= endPoint;
        int endIndex= startPoint;
        NSLog(@"here is end point is greater than start point. S:%d E:%d",startIndex,endIndex);
        [self loadStartIndexAndEndIndexforSelectedPlot:startIndex andEnd:endIndex];
        
    }
    else{
        
        int startIndex= startPoint;
        int endIndex= endPoint;
        NSLog(@"here is start point is greater than end point. S:%d E:%d",startIndex,endIndex);
        [self loadStartIndexAndEndIndexforSelectedPlot:startIndex andEnd:endIndex];
        
    }*/
}

- (void)loadStartIndexAndEndIndexforSelectedPlot:(NSInteger)startIndex andEnd:(NSInteger)endIndex{
    int length = endIndex - startIndex;
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(startIndex, length+1)];
    //NSLog(@"start index %d and length %d",dateSeparate.startIndex,length);
    
    arrSelectedArr = [sampleData objectsAtIndexes:indexSet];
    NSLog(@"selected arr count %d",[arrSelectedArr count]);
    //[selectedPlot reloadData];
}

#pragma mark -
#pragma mark Plot Data Source Methods

- (NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    if ([(NSString *)plot.identifier isEqualToString:kLinePlot] ||[(NSString *)plot.identifier isEqualToString:kLinePlot2])
    {
        return kNumberOfMarkerPlotSymbols;
    }
    else if ([(NSString *)plot.identifier isEqualToString:kObjectivePlot]) {
        return [objectiveDatas count];
    }
    /*else if ([(NSString *)plot.identifier isEqualToString:kSelectedPlot]) {
        return [arrSelectedArr count];
    }*/
    else  {
        return [sampleData count];
    }
}

- (NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    NSNumber *num = nil;
    if ( [(NSString *)plot.identifier isEqualToString:kHighPlot] )
    {
        if ( fieldEnum == CPTScatterPlotFieldY )
        {
            num = [sampleData objectAtIndex:index];
        }
        else if (fieldEnum == CPTScatterPlotFieldX)
        {
            num = [NSNumber numberWithInt:index];
            
        }
    }
    if ( [(NSString *)plot.identifier isEqualToString:kObjectivePlot] )
    {
        if ( fieldEnum == CPTScatterPlotFieldY )
        {
            num = [objectiveDatas objectAtIndex:index];
            
        }
        else if (fieldEnum == CPTScatterPlotFieldX)
        {
            num = [NSNumber numberWithInt:index];
            
        }
    }
    else if ([(NSString *)plot.identifier isEqualToString:kLinePlot])
    {
        if ( fieldEnum == CPTScatterPlotFieldY )
        {
            switch (index) {
                case 0:
                    num = [NSNumber numberWithInt:-10];
                    break;
                case 2:
                    num = [NSNumber numberWithInt:105];
                    break;
                default:
                    if (selectedCoordination >[sampleData count])
                    {
                        break;
                    }
                    else{
                        firstIndex = selectedCoordination;
                        NSLog(@"sample data count %d",[sampleData count]);
                        NSLog(@"first index %d",firstIndex);
                        num = [sampleData objectAtIndex:selectedCoordination];
                        break;
                    }
                    
            }
        }
        else if (fieldEnum == CPTScatterPlotFieldX)
        {
            num = [NSNumber numberWithInt:selectedCoordination];
        }
    }
    else if ([(NSString *)plot.identifier isEqualToString:kLinePlot2])
    {
        if ( fieldEnum == CPTScatterPlotFieldY )
        {
            switch (index) {
                case 0:
                    num = [NSNumber numberWithInt:-10];
                    break;
                case 2:
                    num = [NSNumber numberWithInt:105];
                    break;
                default:
                    if (selected2Coordination >[sampleData count]) break;
                    else{
                        NSLog(@"sample data count %d",[sampleData count]);
                        secondIndex = selected2Coordination;
                        NSLog(@"Second index %d",firstIndex);
                        num = [sampleData objectAtIndex:selected2Coordination];
                        break;
                    }
            }
        }
        else if (fieldEnum == CPTScatterPlotFieldX)
        {
            num = [NSNumber numberWithInt:selected2Coordination];
        }
    }
    /*if ( [(NSString *)plot.identifier isEqualToString:kSelectedPlot] )
    {
        if ( fieldEnum == CPTScatterPlotFieldY )
        {
            num = [arrSelectedArr objectAtIndex:index];
            
        }
        else if (fieldEnum == CPTScatterPlotFieldX)
        {
            num = [NSNumber numberWithInt:selected2Coordination];
            
        }
    }*/
    return num;
}


///call from touch layer
- (void)moveFromTouchLayerEvent:(id)event
                        atPoint:(CGPoint)point{
    [self plotSpace:plotSpace shouldHandlePointingDeviceDownEvent:event atPoint:point];
}

- (void)moveDraggFromTouchLayerEvent:(id)event
                        atPoint:(CGPoint)point{
    [self plotSpace:plotSpace shouldHandlePointingDeviceDraggedEvent:event atPoint:point];
    firstPoint = point;
}

- (void)moveDraggForSecondFromTouchLayerEvent:(id)event
                                      atPoint:(CGPoint)point{
    NSLog(@"second point is moving x:%0.1f",point.x);
    CGPoint pointInPlotArea = [graph convertPoint:point toLayer:graph.plotAreaFrame];
    NSLog(@"touch and point %f and %f",pointInPlotArea.x,pointInPlotArea.y);
    //NSArray *sortedTouches = [[event allControlEvents];
    secondPoint = point;
    
    NSDecimal newPoint[2];
    [graph.defaultPlotSpace plotPoint:newPoint forPlotAreaViewPoint:pointInPlotArea];
    NSDecimalRound(&newPoint[0], &newPoint[0], 0, NSRoundPlain);
    int x = [[NSDecimalNumber decimalNumberWithDecimal:newPoint[0]] intValue];
    

        if (x < 0)
        {
            x = 0;
        }
        else if (x > [sampleData count])
        {
            x = [sampleData count]-1;
        }
        
        selectedCoordination = x;
        touchPlot.hidden = FALSE;
        if ([delegate respondsToSelector:@selector(eskLine2TouchPlot:indexLocation:withSecondIndex:andFirstPoint:andSecondPoint:)])
            [delegate eskLine2TouchPlot:self indexLocation:selectedCoordination withSecondIndex:secondIndex andFirstPoint:firstPoint andSecondPoint:secondPoint];
        [touchPlot reloadData];
        firstIndex = selectedCoordination;
        [self fillSelectedAreaWithStart:firstIndex andEndPoint:secondIndex];

}

- (void)moveUpFromTouchLayerEvent{
    touchPlot.hidden = TRUE;
    touchPlot2.hidden = TRUE;
    touchPlot2Selected = FALSE;
    touchPlotSelected = FALSE;
}

@end
