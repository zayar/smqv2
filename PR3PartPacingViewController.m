//
//  PR3PartPacingViewController.m
//  zBox
//
//  Created by Zayar on 5/9/13.
//
//

#import "PR3PartPacingViewController.h"
#import "ObjSubject.h"
#import "ObjPacing.h"
#import "StringTable.h"
#import "ObjSessionDetail.h"
#import "ObjSubSession.h"
#import "ObjThreePartPacing.h"
#import "zBoxAppDelegate.h"
#import "SOAPRequest.h"

//---size of keyboard---
CGRect keyboardBounds;
//---size of application screen---
CGRect applicationFrame;
//---original size of ScrollView---
CGSize scrollViewOriginalSize;
@interface PR3PartPacingViewController ()

@end

@implementation PR3PartPacingViewController
@synthesize objSession,objSubject;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSString * strPath = [NSString stringWithFormat:@"%@/img_vs_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];

    [imgBgView release];
}

- (void)viewWillAppear:(BOOL)animated{
    [self loadThe3PartView:objSession];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

//keyboard appear
-(void) keyboardWillShow:(NSNotification *) notification {
    //---gets the size of the keyboard---
    NSDictionary *userInfo = [notification userInfo];
    NSValue *keyboardValue = [userInfo objectForKey:UIKeyboardBoundsUserInfoKey];
    [keyboardValue getValue:&keyboardBounds];
    
}

-(void) keyboardWillHide:(NSNotification *) notification {
}

-(void) textFieldDidBeginEditing:(UITextField *)textFieldView {
    [self moveScrollView:textFieldView];
}

-(void) textFieldDidEndEditing:(UITextField *) textFieldView {
    
    [UIView beginAnimations:@"back to original size" context:nil];
    scrollView.contentSize = scrollViewOriginalSize;
    [UIView commitAnimations];
}

-(void) moveScrollView:(UIView *) theView {
    //---scroll the ScrollView---
    [scrollView setContentOffset:CGPointMake(0, 216) animated:YES];
}

- (void) moveScrollViewDown:(UIView*) view {
    [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (IBAction)onBack:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)loadThe3PartView:(ObjSubSession *)obj{
    NSLog(@"arr pacing count %d",[objSubject.arrPacing count]);
    ObjSessionDetail * objSecDetail = [obj.arrSessionDetail objectAtIndex:0];
    ObjThreePartPacing * objThreeP = objSecDetail.objThreePartPacing;
    /*ObjSessionOther * objSessionOther = objSecDetail.objSessionOther;
    if ([objSubject.arrPacing count]>0) {
        NSMutableArray * arrAlertTopic=nil;
        NSMutableArray * arrGoodTopic =nil;
        NSMutableArray * arrMedioTopic=nil;
        for (ObjPacing * objPac in obj.arrPacing) {
            if ([objPac.strPacingValue isEqualToString:PROGRESS_STATUS_GOOD]) {
                if (arrGoodTopic == nil) {
                    arrGoodTopic = [[NSMutableArray alloc]init];
                }
                [arrGoodTopic addObject:objPac];
            }
            else if ([objPac.strPacingValue isEqualToString:PROGRESS_STATUS_ALERT]) {
                if (arrAlertTopic == nil) {
                    arrAlertTopic = [[NSMutableArray alloc]init];
                }
                [arrAlertTopic addObject:objPac];
            }
            else if ([objPac.strPacingValue isEqualToString:PROGRESS_STATUS_MEDIOCRE]) {
                if (arrMedioTopic == nil) {
                    arrMedioTopic = [[NSMutableArray alloc]init];
                }
                [arrMedioTopic addObject:objPac];
            }
        }
        
        if([arrAlertTopic count]>0){
            int y=0;
            int i=0;
            for(ObjPacing * objPac in arrAlertTopic){
                UILabel * lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, y, scrollAlert.frame.size.width, 30)];
                if(i%2 == 0){
                    lbl.backgroundColor = [UIColor lightGrayColor];
                }
                else{
                    lbl.backgroundColor = [UIColor whiteColor];
                }
                lbl.text = objPac.strTopicName;
                lbl.font = [UIFont systemFontOfSize:13];
                [scrollAlert addSubview:lbl];
                y += lbl.frame.size.height;
                i++;
                [lbl release];
                
            }
        }

        if([arrGoodTopic count]>0){
            int y=0;
            int i=0;
            for(ObjPacing * objPac in arrGoodTopic){
                UILabel * lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, y, scrollGood.frame.size.width, 30)];
                if(i%2 == 0){
                    lbl.backgroundColor = [UIColor lightGrayColor];
                }
                else{
                    lbl.backgroundColor = [UIColor whiteColor];
                }
                lbl.text = objPac.strTopicName;
                lbl.font = [UIFont systemFontOfSize:13];
                [scrollGood addSubview:lbl];
                y += lbl.frame.size.height;
                i++;
                [lbl release];
                
            }
        }
        if([arrMedioTopic count]>0){
            int y=0;
            int i=0;
            for(ObjPacing * objPac in arrMedioTopic){
                UILabel * lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, y, scrollMediocre.frame.size.width, 30)];
                if(i%2 == 0){
                    lbl.backgroundColor = [UIColor lightGrayColor];
                }
                else{
                    lbl.backgroundColor = [UIColor whiteColor];
                }
                lbl.text = objPac.strTopicName;
                lbl.font = [UIFont systemFontOfSize:13];
                [scrollMediocre addSubview:lbl];
                y += lbl.frame.size.height;
                i++;
                [lbl release];
                
            }
        }
        
    }*/
    
    if (objThreeP.strObjective == nil) {
        lblObjectiveScore.text = [NSString stringWithFormat:@"%s", "70"];
    } else {
        lblObjectiveScore.text = [NSString stringWithFormat:@"%@", objThreeP.strObjective];
    }

    lblEastimateScore.text = [NSString stringWithFormat:@"%@",objThreeP.strEstimatedScore];

    NSString *goodString = objThreeP.strGood;
    goodString = [goodString stringByReplacingOccurrencesOfString:@","
                                                               withString:@"\n\n"];
    lblGood.text = goodString;
    [lblGood sizeToFit];
    
    NSString *mediocreString = objThreeP.strMediocre;
    mediocreString = [mediocreString stringByReplacingOccurrencesOfString:@","
                                         withString:@"\n\n"];
    lblMedicro.text = mediocreString;
    [lblMedicro sizeToFit];
    
    NSString *alertString = objThreeP.strAlert;
    alertString = [alertString stringByReplacingOccurrencesOfString:@"," withString:@"\n\n"];
    lblAlert.text = alertString;
    [lblAlert sizeToFit];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)updateScore:(id)sender {
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate showLoadingScreen];
    SOAPRequest* objScoreRequest = [[SOAPRequest alloc] initWithOwner:self];
    objScoreRequest.processId = 4;
    
    NSString * score = lblObjectiveScore.text;
    score = [score stringByReplacingOccurrencesOfString:@"%" withString:@""];
    [objScoreRequest syncSubmitObjectiveScore:score withProgressReportId:@"1"];
    [lblObjectiveScore resignFirstResponder];
    [self moveScrollViewDown:self.view];
}

- (void) onJsonLoaded:(NSMutableDictionary *)dics withProcessId:(int)processId{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    if (processId == 4) {
        [delegate hideLoadingScreen];
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        
        NSString * strMessage;
        if (status == STATUS_ACTION_SUCCESS) {
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
        else if(status == STATUS_ACTION_FAILED){
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
        
    }
}

- (void)dealloc{
    [lblGood release];
    [lblMedicro release];
    [lblAlert release];
    [lblObjectiveScore release];
    [imgBgView release];
    [lblTitle release];
    [objSession release];
    [objSubject release];
    [super dealloc];
}

@end
