//
//  vsTableViewCell.h
//  zBox
//
//  Created by Zayar Cn on 6/5/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateView.h"
@protocol vsTableViewDelegate 
-(void) selectedRate:(int)rate andIndex:(int)idx;
@end
@interface vsTableViewCell : UITableViewCell<RateViewDelegate>
{
    UIImageView * imgBgView;
    UILabel * lblTitle;
    UILabel * lblText;
    BOOL isWebLoaded;
    UIImageView * ratingBgView;
    id<vsTableViewDelegate> owner;
}
@property BOOL isWebLoaded;
@property (assign) id<vsTableViewDelegate> owner;
@property (nonatomic, retain) IBOutlet UIImageView * imgBgView;
@property (nonatomic, retain) IBOutlet UIImageView * imgThumbView;
@property (nonatomic, retain) IBOutlet UIImageView * ratingBgView;
@property (nonatomic, retain) IBOutlet UIWebView * webThumbView;
@property (nonatomic, retain) IBOutlet UILabel * lblTitle;
@property (nonatomic, retain) IBOutlet UILabel * lblText;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView * activity;
@property (nonatomic, retain) IBOutlet RateView *rateView;
- (void)cellViewLoad;
- (void)rateSelected:(int)rate;
@end
