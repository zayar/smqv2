//
//  EskLinePlot.m
//  CorePlotSample
//
//  Created by Ken Wong on 8/9/11.
//  Copyright 2011 Essence Work LLC. All rights reserved.
//

#import "EskLinePlot.h"
#import "ObjSubject.h"
#import "ObjSubSession.h"
#import "ObjSessionDetail.h"

#define kHighPlot @"HighPlot"
#define kObjectivePlot @"ObjectivePlot"
#define kActualPlot @"ActualPlot"
#define kCouldvePlot @"CouldvePlot"
#define kLinePlot @"LinePlot"

#define kNumberOfMarkerPlotSymbols 3

@implementation EskLinePlot

@synthesize delegate;
@synthesize sampleData, sampleYears,objectiveDatas,couldveDatas;

- (id)init
{
    self = [super init];
    if (self) 
    {
        //firstLineDic = [NSDictionary dictionaryWithObjectsAndKeys:sampleData, kObjectivePlot, sampleYears, @"Dates", nil];
        
        //secondLineDic = [NSDictionary dictionaryWithObjectsAndKeys:@"secondLine", PLOT_IDENTIFIER, secondLineData, PLOT_DATA, nil];
        
    }
    
    return self;
}

- (void)loadTheDataWith:(ObjSubject *)objSubject{
    // setting up the sample data here.
    NSLog(@"dates count %d and objective %f",[objSubject.arrSession count],objSubject.objectiveMark);
    sampleYears = [[NSMutableArray alloc] initWithCapacity:[objSubject.arrSession count]];
    objectiveDatas = [[NSMutableArray alloc] initWithCapacity:[objSubject.arrSession count]];
    couldveDatas = [[NSMutableArray alloc] initWithCapacity:[objSubject.arrSession count]];
    sampleData = [[NSMutableArray alloc] initWithCapacity:[objSubject.arrSession count]];
    NSString * strDate=@"";
    NSArray * allReversed = [[objSubject.arrSession reverseObjectEnumerator] allObjects];
    
    for (ObjSubSession * objSub in allReversed) {
        NSLog(@"date %@ and actual scores %@ and couldve scores %@ ",objSub.strReportDate,objSub.strProgressActualScores,objSub.strProgressCouldveScores);
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"MMM"];
        strDate = [dateFormatter stringFromDate:objSub.reportDate];
        [sampleYears addObject:strDate];
        [dateFormatter release];
        [sampleData addObject:objSub.strProgressActualScores];
        [objectiveDatas addObject:[NSString stringWithFormat:@"%0.2f", objSubject.objectiveMark]];
        [couldveDatas addObject:objSub.strProgressCouldveScores];
        
    }
    /*sampleYears = [[NSMutableArray alloc] initWithObjects:@"2010/4/3", @"2011/4/3", @"2012/4/3", @"2013/4/3", @"2014/4/3", @"2015/4/3", @"2016/4/3",@"2017/4/3", ynil];
    
    sampleData = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInt:60],
                  [NSNumber numberWithInt:30],
                  [NSNumber numberWithInt:20],
                  [NSNumber numberWithInt:50],
                  [NSNumber numberWithInt:70],
                  [NSNumber numberWithInt:85],
                  [NSNumber numberWithInt:65],
                  [NSNumber numberWithInt:90],nil];
    
    objectiveDatas = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInt:70],
                      [NSNumber numberWithInt:70],
                      [NSNumber numberWithInt:70],
                      [NSNumber numberWithInt:70],
                      [NSNumber numberWithInt:70],
                      [NSNumber numberWithInt:70],
                      [NSNumber numberWithInt:70],
                      [NSNumber numberWithInt:70],nil];
    
    couldveDatas =[[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInt:70],
                   [NSNumber numberWithInt:60],
                   [NSNumber numberWithInt:60],
                   [NSNumber numberWithInt:70],
                   [NSNumber numberWithInt:80],
                   [NSNumber numberWithInt:95],
                   [NSNumber numberWithInt:75],
                   [NSNumber numberWithInt:95],nil];*/
}

- (void)dealloc
{
    [objectiveDatas release];
    [couldveDatas release];
    [sampleData release];
    [sampleYears release];
    [linePlot release];
    [super dealloc];
}

- (void)renderInLayer:(CPTGraphHostingView *)layerHostingView withTheme:(CPTTheme *)theme
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    CGRect bounds = layerHostingView.bounds;
    
    // Create the graph and assign the hosting view.
    graph = [[CPTXYGraph alloc] initWithFrame:bounds];
    layerHostingView.hostedGraph = graph;
    [graph applyTheme:theme];
    
    // Change gradient color here
    graph.fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:0.21f green:0.28f blue:0.45f  alpha:1.0f]];
    graph.plotAreaFrame.masksToBorder = NO;
    
    // chang the chart layer orders so the axis line is on top of the bar in the chart.
    NSArray *chartLayers = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:CPTGraphLayerTypePlots],
                                                            [NSNumber numberWithInt:CPTGraphLayerTypeMajorGridLines], 
                                                            [NSNumber numberWithInt:CPTGraphLayerTypeMinorGridLines],  
                                                            [NSNumber numberWithInt:CPTGraphLayerTypeAxisLines], 
                                                            [NSNumber numberWithInt:CPTGraphLayerTypeAxisLabels], 
                                                            [NSNumber numberWithInt:CPTGraphLayerTypeAxisTitles], 
                                                            nil];
    graph.topDownLayerOrder = chartLayers;    
    [chartLayers release];
    
    
    // Add plot space for horizontal bar charts
    graph.paddingLeft = 50.0;
	graph.paddingTop = 10.0;
	graph.paddingRight = 30.0;
	graph.paddingBottom = 40.0;
    
    // Setup plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    plotSpace.allowsUserInteraction = YES;
    plotSpace.delegate = self;
    if ([sampleData count] > 20) {
        plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(20.0f)];
    }
    else{
        plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat([sampleData count]-1)];
    }
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(100)];
    
    // Setup grid line style
    CPTMutableLineStyle *majorXGridLineStyle = [CPTMutableLineStyle lineStyle];
    majorXGridLineStyle.lineWidth = 1.0f;
    majorXGridLineStyle.lineColor = [[CPTColor grayColor] colorWithAlphaComponent:0.25f];
    
    // Setup x-Axis.
	CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    CPTXYAxis *x = axisSet.xAxis;
    x.labelingPolicy = CPTAxisLabelingPolicyNone;
    x.majorGridLineStyle = majorXGridLineStyle;
    x.majorIntervalLength = CPTDecimalFromString(@"1");
    x.minorTicksPerInterval = 1;

    x.orthogonalCoordinateDecimal = CPTDecimalFromString(@"0");
    x.title = @"";
    x.timeOffset = 30.0f;
 	NSArray *exclusionRanges = [NSArray arrayWithObjects:[CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0) length:CPTDecimalFromInt(0)], nil];
	x.labelExclusionRanges = exclusionRanges;
    
    // Use custom x-axis label so it will display year 2010, 2011, 2012, ... instead of 1, 2, 3, 4
    NSMutableArray *labels = [[NSMutableArray alloc] initWithCapacity:[sampleYears count]];
    int idx = 0;
    
    for (NSString *year in sampleYears)
    {
        if (idx%5 == 0) {
            CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:year textStyle:x.labelTextStyle];
            label.tickLocation = CPTDecimalFromInt(idx);
            label.offset = 2.0f;            
            [labels addObject:label];
            [label release];
        }
        idx++;
    }
    x.axisLabels = [NSSet setWithArray:labels];
    [labels release];
    
    // Setup y-Axis.
    CPTMutableLineStyle *majorYGridLineStyle = [CPTMutableLineStyle lineStyle];
    majorYGridLineStyle.lineWidth = 1.0f;
    majorYGridLineStyle.dashPattern =  [NSArray arrayWithObjects:[NSNumber numberWithFloat:5.0f], [NSNumber numberWithFloat:5.0f], nil];
    majorYGridLineStyle.lineColor = [[CPTColor lightGrayColor] colorWithAlphaComponent:0.25];
    
    
    CPTXYAxis *y = axisSet.yAxis;
    y.majorGridLineStyle = majorYGridLineStyle;
    y.majorIntervalLength = CPTDecimalFromString(@"10");
    y.minorTicksPerInterval = 1;
    y.orthogonalCoordinateDecimal = CPTDecimalFromString(@"0");
    y.title = @"Marks";
    NSArray *yExlusionRanges = [NSArray arrayWithObjects:
                                [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0) length:CPTDecimalFromFloat(0.0)],
                                nil];
    y.labelExclusionRanges = yExlusionRanges;
    
    // Create a high plot area
	CPTScatterPlot *highPlot = [[[CPTScatterPlot alloc] init] autorelease];
    highPlot.identifier = kHighPlot;
    
    CPTMutableLineStyle *highLineStyle = [[highPlot.dataLineStyle mutableCopy] autorelease];
	highLineStyle.lineWidth = 2.f;
    //highLineStyle.lineColor = [CPTColor colorWithComponentRed:0.50f green:0.67f blue:0.65f alpha:1.0f];
    highLineStyle.lineColor = [CPTColor whiteColor];
    highPlot.dataLineStyle = highLineStyle;
    highPlot.dataSource = self;
	
    //CPTFill *areaFill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:0.50f green:0.67f blue:0.65f alpha:0.4f]];
    CPTFill *areaFill = [CPTFill fillWithColor:[CPTColor clearColor]];
    highPlot.areaFill = areaFill;
    highPlot.areaBaseValue = CPTDecimalFromString(@"0");
    [graph addPlot:highPlot];
    
    // Create a high plot area
	CPTScatterPlot *objectivePlot = [[[CPTScatterPlot alloc] init] autorelease];
    objectivePlot.identifier = kObjectivePlot;
    
    CPTMutableLineStyle *objectiveStyle = [[objectivePlot.dataLineStyle mutableCopy] autorelease];
	objectiveStyle.lineWidth = 2.f;
    //highLineStyle.lineColor = [CPTColor colorWithComponentRed:0.50f green:0.67f blue:0.65f alpha:1.0f];
    objectiveStyle.lineColor = [CPTColor greenColor];
    objectivePlot.dataLineStyle = objectiveStyle;
    objectivePlot.dataSource = self;
    
    
	
    //CPTFill *areaFill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:0.50f green:0.67f blue:0.65f alpha:0.4f]];
    CPTFill *objectiveFill = [CPTFill fillWithColor:[CPTColor clearColor]];
    objectivePlot.areaFill = objectiveFill;
    objectivePlot.areaBaseValue = CPTDecimalFromString(@"0");
    [graph addPlot:objectivePlot];
    
    
    // Create a high plot area
	CPTScatterPlot *couldvePlot = [[[CPTScatterPlot alloc] init] autorelease];
    couldvePlot.identifier = kCouldvePlot;
    
    CPTMutableLineStyle *couldveStyle = [[couldvePlot.dataLineStyle mutableCopy] autorelease];
	couldveStyle.lineWidth = 2.f;
    //highLineStyle.lineColor = [CPTColor colorWithComponentRed:0.50f green:0.67f blue:0.65f alpha:1.0f];
    couldveStyle.lineColor = [CPTColor yellowColor];
    couldvePlot.dataLineStyle = couldveStyle;
    couldvePlot.dataSource = self;
    
    //CPTFill *areaFill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:0.50f green:0.67f blue:0.65f alpha:0.4f]];
    CPTFill *couldveFill = [CPTFill fillWithColor:[CPTColor clearColor]];
    couldvePlot.areaFill = couldveFill;
    couldvePlot.areaBaseValue = CPTDecimalFromString(@"0");
    [graph addPlot:couldvePlot];
    
    /*
     CPTColor *firstStripesColor = [CPTColor colorWithComponentRed:1.0 green:1.0 blue:1.0 alpha:0.0];
     CPTColor *secondStripesColor = [CPTColor colorWithComponentRed:1.0 green:1.0 blue:1.0 alpha:0.6];
     CPTFill *areaStripesFill = [CPTFill fillWithFirstColor:firstStripesColor secondColor:secondStripesColor stripeWidth:2];
     scatterPlot.areaFill = areaStripesFill;
     */

    
    // Create the Savings Marker Plot
    selectedCoordination = 2;
    
    [pool drain];

}

// Highlight the touch plot when the user holding tap on the line symbol.
- (void)applyHighLightPlotColor:(CPTScatterPlot *)plot
{
    CPTColor *selectedPlotColor = [CPTColor redColor];
    
    CPTMutableLineStyle *symbolLineStyle = [CPTMutableLineStyle lineStyle];
    symbolLineStyle.lineColor = selectedPlotColor;
    
    CPTPlotSymbol *plotSymbol = nil;
    plotSymbol = [CPTPlotSymbol ellipsePlotSymbol];
    plotSymbol.fill = [CPTFill fillWithColor:selectedPlotColor];
    plotSymbol.lineStyle = symbolLineStyle;
    plotSymbol.size = CGSizeMake(15.0f, 15.0f); 
    
    plot.plotSymbol = plotSymbol;
    
    CPTMutableLineStyle *selectedLineStyle = [CPTMutableLineStyle lineStyle];
    selectedLineStyle.lineColor = [CPTColor yellowColor];
    selectedLineStyle.lineWidth = 5.0f;
    
    plot.dataLineStyle = selectedLineStyle;
}

#pragma mark - CPPlotSpace Delegate Methods
// This implementation of this method will put the line graph in a fix position so it won't be scrollable.
-(CPTPlotRange *)plotSpace:(CPTPlotSpace *)space willChangePlotRangeTo:(CPTPlotRange *)newRange forCoordinate:(CPTCoordinate)coordinate 
{
    
    if (coordinate == CPTCoordinateY) {
        return ((CPTXYPlotSpace *)space).yRange;
    }
    else
    {
        return ((CPTXYPlotSpace *)space).xRange;
    }
}

// This method is call when user touch & drag on the plot space.
/*- (BOOL)plotSpace:(CPTPlotSpace *)space shouldHandlePointingDeviceDraggedEvent:(id)event atPoint:(CGPoint)point
{
    // Convert the touch point to plot area frame location
    CGPoint pointInPlotArea = [graph convertPoint:point toLayer:graph.plotAreaFrame];
    
    NSDecimal newPoint[2];
    [graph.defaultPlotSpace plotPoint:newPoint forPlotAreaViewPoint:pointInPlotArea];
    NSDecimalRound(&newPoint[0], &newPoint[0], 0, NSRoundPlain);
    int x = [[NSDecimalNumber decimalNumberWithDecimal:newPoint[0]] intValue];
    
    if (x < 0)
    {
        x = 0;
    }
    else if (x > [sampleData count])
    {
        x = [sampleData count];
    }
    
    return YES;
}

- (BOOL)plotSpace:(CPTPlotSpace *)space shouldHandlePointingDeviceDownEvent:(id)event 
          atPoint:(CGPoint)point
{
    return YES;
}

- (BOOL)plotSpace:(CPTPlotSpace *)space shouldHandlePointingDeviceUpEvent:(id)event atPoint:(CGPoint)point
{
    // Restore the vertical line plot to its initial color.
    return YES;
}*/

#pragma mark - 
#pragma mark Scatter plot delegate methods

/*- (void)scatterPlot:(CPTScatterPlot *)plot plotSymbolWasSelectedAtRecordIndex:(NSUInteger)index
{
    if ([(NSString *)plot.identifier isEqualToString:kLinePlot]) 
    {
        touchPlotSelected = YES;
        [self applyHighLightPlotColor:plot];
        if ([delegate respondsToSelector:@selector(linePlot:indexLocation:)])
            [delegate linePlot:self indexLocation:index];
    } 
}*/

#pragma mark -
#pragma mark Plot Data Source Methods

- (NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot 
{
    if ([(NSString *)plot.identifier isEqualToString:kLinePlot]) 
    {
        return kNumberOfMarkerPlotSymbols;
    }
    else if ([(NSString *)plot.identifier isEqualToString:kHighPlot] ){
        return [sampleData count];
    }
    else if ([(NSString *)plot.identifier isEqualToString:kObjectivePlot]) {
        return [objectiveDatas count];
    }
    else if ([(NSString *)plot.identifier isEqualToString:kCouldvePlot]) {
        return [couldveDatas count];
    }
}

- (NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index 
{
    NSNumber *num = nil;
    if ( [(NSString *)plot.identifier isEqualToString:kHighPlot] )
    {
        if ( fieldEnum == CPTScatterPlotFieldY ) 
        {
            num = [sampleData objectAtIndex:index];
        } 
        else if (fieldEnum == CPTScatterPlotFieldX) 
        {
            num = [NSNumber numberWithInt:index];
        }
    }
    if ( [(NSString *)plot.identifier isEqualToString:kObjectivePlot] )
    {
        if ( fieldEnum == CPTScatterPlotFieldY )
        {
            num = [objectiveDatas objectAtIndex:index];
        }
        else if (fieldEnum == CPTScatterPlotFieldX)
        {
            num = [NSNumber numberWithInt:index];
        }
    }
    if ( [(NSString *)plot.identifier isEqualToString:kCouldvePlot] )
    {
        if ( fieldEnum == CPTScatterPlotFieldY )
        {
            num = [couldveDatas objectAtIndex:index];
        }
        else if (fieldEnum == CPTScatterPlotFieldX)
        {
            num = [NSNumber numberWithInt:index];
        }
    }
    
    else if ([(NSString *)plot.identifier isEqualToString:kLinePlot]) 
    {
        if ( fieldEnum == CPTScatterPlotFieldY ) 
        {
            switch (index) {
                case 0:
                    num = [NSNumber numberWithInt:0];
                    break;
                case 2:
                    num = [NSNumber numberWithInt:9700];
                    break;
                default:
                    num = [sampleData objectAtIndex:selectedCoordination];
                    break;
            }
        } 
        else if (fieldEnum == CPTScatterPlotFieldX) 
        {
            num = [NSNumber numberWithInt:selectedCoordination];
        }
    }
    return num;
}


@end
