//
//  ExpadnButtonView.h
//  zBox
//
//  Created by Zayar on 5/27/13.
//
//

#import <UIKit/UIKit.h>
@protocol StockTextButtonBarViewDelegate
- (void) onCouldveSelected:(NSString *)strImage;
- (void) onActualSelected:(NSString *)strImage;
- (void) onActualCouldveSelected:(NSString *)strImage;
@end
@interface StockTextButtonBarView : UIView
{
    id<StockTextButtonBarViewDelegate> owner;
    IBOutlet UIButton * btnActual;
}
@property id<StockTextButtonBarViewDelegate> owner;
- (void) setDefaultSelector;
@end
