//
//  StringTable.h
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface StringTable : NSObject {
    
}

extern NSString * const DBNAME;
extern NSString * const APP_TITLE;
extern NSString * const APP_ID;

extern NSString * const CONFIG_LINK;
extern NSString * const BASE_LINK;
extern NSString * const GENR_KEY_LINK;
extern NSString * const LOGIN_LINK;
extern NSString * const SUBJECT_CRITERIA_LINK;
extern NSString * const CHECK_LOGIN_SESSION_LINK;
extern NSString * const CHECK_USER_POINT_LINK;
extern NSString * const SUBMIT_QUESTION_LINK;
extern NSString * const USER_QUESTION_DETAIL_LINK;
extern NSString * const CHANGE_PASSWORD_LINK;
extern NSString * const RATING_LINK;
extern NSString * const CATALOG_LINK;
extern NSString * const CATALOG_TRACK_LINK;
extern NSString * const QUIZZ_LINK;
extern NSString * const QUIZZ_QUESTION_LINK;
extern NSString * const PROGRESS_REPORT_LINK;
extern NSString * const ADS_LINK;
extern NSString * const UPDATE_OBJECTIVE_SCORE_LINK;
extern NSString * const FREEZE_LINK;
extern NSString * const CHECK_SUPERVISOR_LINK;
extern NSString * const UNFREEZE_LINK;
extern NSString * const CATALOG_REGISTER_LINK;
extern NSString * const RESET_PASS_LINK;
extern NSString * const QUIZ_CONSULT_LINK;
extern NSString * const QUIZ_RATE_LINK;

extern NSString * const ANSWER_TYPE_IMAGE;
extern NSString * const ANSWER_TYPE_VIDEO;
extern NSString * const ANSWER_TYPE_TEXT;
extern NSString * const ANSWER_TYPE_NULL;

extern NSString * const FREEZE_TYPE;
extern NSString * const UNFREEZE_TYPE;

extern NSString * const ANSWER_STATUS_PENDING;
extern NSString * const ANSWER_STATUS_ANSWERED;
extern NSString * const ANSWER_STATUS_REJECTED;

extern NSString * const APN_SERVER_PATH;

extern int const STATUS_ACTION_SUCCESS;
extern int const STATUS_RETURN_RECORD;
extern int const STATUS_ACTION_FAILED;
extern int const STATUS_SESSION_EXPIRED;
extern int const STATUS_NO_RECORD_FOUND;

extern NSString * const PROGRESS_STATUS_GOOD;
extern NSString * const PROGRESS_STATUS_ALERT;
extern NSString * const PROGRESS_STATUS_MEDIOCRE;
extern NSString * const PROGRESS_LOCAL_REPORT_LINK;

extern NSString * const STATUS_TOPUP;
extern NSString * const STATUS_NOTTOPUP;

extern double const CACHE_DURATION;

@end
