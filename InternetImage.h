//
//  InternetImage.h
//  DBSIndulge
//
//  Created by System Administrator on 28/11/2009.
//  Copyright 2009 Cellcity Pte Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface InternetImage : NSObject {
    
@private NSMutableData *m_ImageRequestData;
@private id m_Delegate;
@private NSURLConnection *imageConnection;
@public NSString* ImageUrl;
@public UIImage* Image;
	bool workInProgress;
    int processId;
}

@property int processId;
@property (nonatomic, retain) NSString* ImageUrl;
@property (nonatomic, retain) UIImage* Image;
@property (nonatomic, assign) bool workInProgress;

-(void)setDelegate:(id)new_delegate;
-(id)initWithUrl:(NSString*)urlToImage;
-(void)downloadImage:(id)delegate;
-(void)abortDownload;

@end


@interface NSObject (InternetImageDelegate)
- (void)internetImageReady:(InternetImage*)internetImage;
@end
