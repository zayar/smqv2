//
//  CustomPhotoGalleryView.m
//  zBox
//
//  Created by Zayar Cn on 6/4/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "CustomPhotoGalleryView.h"
#import "UIImageExtras.h"
#import "CustomThumbIcon.h"
#import "zBoxAppDelegate.h"
#import <AssetsLibrary/AssetsLibrary.h>

@implementation CustomPhotoGalleryView
@synthesize owner;
@synthesize images = _images;
@synthesize thumbs = _thumbs;
@synthesize selectedImage = _selectedImage;
@synthesize assets;

@synthesize parent;
@synthesize selectedAssetsLabel;
@synthesize assetGroup, elcAssets,arrLibraryPhotos;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)customPhotoGalleryViewDidLoad{
    NSString * strPath = [NSString stringWithFormat:@"%@/img_smq_photog_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];
    [imgBgView release];
    
    //arrLibraryPhotos = [[NSMutableArray alloc]init];
    
    _images =  [[NSMutableArray alloc] init];
    _thumbs =  [[NSMutableArray alloc] init];
    
    view = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 10, 320, 390)];
    
    /*for (int i = 1; i<=16; i++) {
        [self addImage:[UIImage imageNamed:[NSString stringWithFormat:@"logo%d.png",i]]];
    }*/
    
    
    
    /*NSMutableArray * arrPic = [self loadNativePhotoLibrary3];
    NSLog(@"asset count %d",[arrPic count]);
    for (int i = 0; i > [arrPic count]; i++) {
        ALAsset *asset = [assets objectAtIndex:i];
        [self addImage:[UIImage imageWithCGImage:[asset thumbnail]]];
    }*/
}

-(void)preparePhotos{
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
    //NSLog(@"enumerating photos and assetCount %d",[assetGroup numberOfAssets]);
    NSMutableArray * returnArray = [[[NSMutableArray alloc]init] autorelease];
    [self.assetGroup enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) 
     {         
         if(result == nil) 
         {
             return;
         }
         
         //ELCAsset *elcAsset = [[[ELCAsset alloc] initWithAsset:result] autorelease];
         //[elcAsset setParent:self];
         [returnArray addObject:result];
         NSLog(@"done enumerating photos and arrCount %d",[returnArray count]);
     }]; 
    
    //NSLog(@"done enumerating photos and arrCount %d",[arrLibraryPhotos count]);
    self.arrLibraryPhotos = [NSArray arrayWithArray:returnArray];
    [self reloadAssetToImage];
    [pool release];
}

- (void)reloadAssetToImage{
    for (ALAsset * alAsset in self.arrLibraryPhotos) {
        
        [self addImageWithAlAsset:alAsset];
    }
    
    [self loadPhotoGalleryView];
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    //[delegate hideLoadingScreen];
}

- (void)loadPhotoGalleryView{
    NSLog(@"thumb count %d",[_thumbs count]);
    // Create view
	previousIndex = -1;
	int row = 0;
	int column = 0;
	for(int i = 0; i < _thumbs.count; ++i) {
		
		UIImage *thumb = [_thumbs objectAtIndex:i];
        CustomThumbIcon * bIcon = [[CustomThumbIcon alloc] initWithFrame: CGRectMake(column*95+24, row*110+10, 90, 100)];
        
        bIcon.owner = self;
		bIcon.tag = i;
		[bIcon.imgThumbView setImage:thumb];
        [bIcon customeSetImage:thumb];
		[bIcon attachEventHandlers];
        [view addSubview:bIcon];
        [bIcon release];
		
		if (column == 2) {
			column = 0;
			row++;
		} else {
			column++;
		}
		
	}
	
	[view setContentSize:CGSizeMake(320, (row+1) * 110 + 10)];
	
	[self addSubview:view];
    
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate hideLoadingScreen];
	//[view release];
}

- (void)addImage:(UIImage *)image {
	[_images addObject:image];
	[_thumbs addObject:[image imageByScalingAndCroppingForSize:CGSizeMake(87, 87)]];
}

- (void)addImageWithAlAsset:(ALAsset *)asset {
    UIImage * detailImg = [UIImage imageWithCGImage:[asset thumbnail]];
    //NSLog(@"img %@",detailImg);
    //UIImage * detailImg = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]];
	//[_images addObject:[detailImg imageByScalingAndCroppingForSize:CGSizeMake(278, 255)]];
	[_thumbs addObject:detailImg];
}

- (void)customPhotoGalleryViewWillAppear{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate showLoadingScreen];
    [self performSelectorInBackground:@selector(preparePhotos) withObject:nil];
}

- (void)customPhotoGalleryViewWillDisAppear{
    for (UIView * v in view.subviews) {
        if ([v isKindOfClass:[CustomThumbIcon class]]) {
            v.hidden = TRUE;
            [v removeFromSuperview];
            v = nil;
        }
    }
    if ([_thumbs count]>0) {
        [_thumbs removeAllObjects];
    }
    if ([_images count]>0) {
        [_images removeAllObjects];
    }
}

/*- (NSMutableArray *)loadNativePhotoLibrary{
    
    void (^assetGroupEnumerator)(ALAssetsGroup *, BOOL *) = ^(ALAssetsGroup *group, BOOL *stop){
        if(group != nil) { 
            [assetGroups addObject:group];
            NSLog(@"Number of assets in group: %d",[group numberOfAssets]);
        }};
    
    assetGroups = [[NSMutableArray alloc] init];
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    NSUInteger groupTypes = ALAssetsGroupSavedPhotos; 
    
    [library enumerateGroupsWithTypes:groupTypes
                           usingBlock:assetGroupEnumerator
                         failureBlock:^(NSError *error) {NSLog(@"A problem occurred");}];
    
    NSLog(@"Asset groups: %d ", [assetGroups count]);
    [library release];
    
    void (^assetEnumerator)(ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if(result != nil) {
            if(![assetURLDictionaries containsObject:[result valueForProperty:ALAssetPropertyURLs]]) {
                if(![[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypeVideo]) {
                    [assetURLDictionaries addObject:[result valueForProperty:ALAssetPropertyURLs]];
                    [assets addObject:result];
                }
            }
        }
    };
    
    assets = [[NSMutableArray alloc] init];
    assetURLDictionaries = [[NSMutableArray alloc] init];
    
    NSInteger indexOfExampleGallery = NSNotFound;
    
    for (ALAssetsGroup  * group in assetGroups) {
            indexOfExampleGallery = [assetGroups indexOfObject:group];
        NSLog(@"here here");
    }
    
    if (indexOfExampleGallery != NSNotFound) {
        [[assetGroups objectAtIndex:indexOfExampleGallery]
         enumerateAssetsUsingBlock:assetEnumerator];
        NSLog(@"Assets %@", assets);
    }
    else
        NSLog(@"Gallery 'Saved Photos' not found on device.");
    
    return assetGroups;
}

- (NSMutableArray *)loadNativePhotoLibrary2{
    
    void (^assetEnumerator)(ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if(result != NULL) {
            NSLog(@"See Asset: %@", result);
            [self.assets addObject:result];
            
        }
    };

    void (^assetGroupEnumerator)(ALAssetsGroup *, BOOL *) =  ^(ALAssetsGroup *group, BOOL *stop) {
        if(group != nil) {
            [group enumerateAssetsUsingBlock:assetEnumerator];
        }
        
    };
    assets = [[NSMutableArray alloc] init];
    ALAssetsLibrary * library = [[ALAssetsLibrary alloc] init];
    [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos
                           usingBlock:assetGroupEnumerator
                         failureBlock: ^(NSError *error) {
                             NSLog(@"Failure");
                         }];
    NSLog(@"asset count %d",[assets count]);
    return assets;
}

- (NSMutableArray *)loadNativePhotoLibrary3{
    
    NSMutableArray * assets3 = [[NSMutableArray array] retain]; // Prepare array to have retrieved images by Assets Library.
    
    void (^assetEnumerator)(ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *asset, NSUInteger index, BOOL *stop) {
        if(asset != NULL) {
            [assets3 addObject:asset]; 
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // show number of retrieved images saved in the Camera role.
                // The [assets count] returns always 0 when I run this program on iPhone device although it worked OK on the simulator.
                NSLog(@"%i", [assets count]);
            });
        }
    };
    
    void (^assetGroupEnumerator)( ALAssetsGroup *, BOOL *) =  ^(ALAssetsGroup *group, BOOL *stop) {
        if(group != nil) {
            [group enumerateAssetsUsingBlock:assetEnumerator];
        }
    };
    
    // Create instance of the Assets Library.
    ALAssetsLibrary* library = [[ALAssetsLibrary alloc] init];
    
    [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos // Retrieve the images saved in the Camera role.
                           usingBlock:assetGroupEnumerator
                         failureBlock: ^(NSError *error) {
                             NSLog(@"Failed.");
                         }];
    return assets3;
}*/

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)buttonClicked:(id)sender{
    _selectedImage = [_images objectAtIndex:[sender tag]];
}

- (void)onThumbIcon_Click:(id)sender{
    if (previousIndex != -1) {
        for (UIView * v in view.subviews) {
            if ([v isKindOfClass:[CustomThumbIcon class]]) {
                CustomThumbIcon * previousSelected = (CustomThumbIcon *)v;
                if (previousSelected.tag == previousIndex) {
                    [previousSelected deSelectedImageStyle];
                }
            }
        }
    }
    CustomThumbIcon * selectedBtn = (CustomThumbIcon *)sender;
    [selectedBtn selectedImageStyle];
    ALAsset * alAsset = [self.arrLibraryPhotos objectAtIndex:[sender tag]];
    //[alAsset retain];
    //_selectedImage = [_images objectAtIndex:[sender tag]];
    UIImage * detailImg = [UIImage imageWithCGImage:[[alAsset defaultRepresentation] fullScreenImage]];
    [detailImg retain];
    [detailImg imageByScalingAndCroppingForSize:CGSizeMake(480, 640)];
    _selectedImage = detailImg;
    previousIndex = [sender tag];
}

- (IBAction)onCancel:(id)sender{
    [owner onCustomPhotoGalleryCancel];
}

- (IBAction)onChoose:(id)sender{
    if (_selectedImage == nil) {
        NSLog(@"you didn't select yep");
    }
    else [owner onCustomPhotoGalleryChoose:_selectedImage];
}

- (void)dealloc{
    [arrLibraryPhotos release];
    [view release];
    [imgBgView release];
    [super dealloc];
}

@end
