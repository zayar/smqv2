//
//  FreezeView.m
//  zBox
//
//  Created by Zayar on 5/9/13.
//
//

#import "FreezeView.h"
#import "zBoxAppDelegate.h"
#import "StringTable.h"

@implementation FreezeView
@synthesize owner;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)freezeViewViewDidLoad{
    NSString * strPath = [NSString stringWithFormat:@"%@/img_setting_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];
    [imgBgView release];
    UIFont * nfont = [UIFont fontWithName:@"BradyBunchRemastered" size:22.0];
    lblName.font = nfont;
    lblName.textColor = [UIColor darkGrayColor];
}

-(void)freezeViewWillAppear{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    ObjUser * objUser = [delegate.db getUserObj];
    lblName.text = objUser.strName;
    [self setTittleBtnFreeze];
}

- (IBAction)onChangeClick:(id)sender{
    NSLog(@"here change pass");
    [owner onFreezeViewChangePassword];
}

- (IBAction)onFreezeClick:(id)sender{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * str = [prefs objectForKey:FREEZE_TYPE];
    if ([str isEqualToString:@"F"]) {
        [switchFreeze setOn:YES];
    }
    else{
        [switchFreeze setOn:NO];
    }
    
}

- (IBAction)onLogoutClick:(id)sender{
    [owner onFreezeViewLogout];
    [owner onFreezeViewCancel];
}

- (void)setTittleBtnFreeze{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * str = [prefs objectForKey:FREEZE_TYPE];
    if ([str isEqualToString:@"F"]) {
        //[btnFreeze setTitle:@"Unfreeze Account" forState:normal];
        [switchFreeze setOn:YES];
    }
    else{
        //[btnFreeze setTitle:@"Freeze Account" forState:normal];
        [switchFreeze setOn:NO];
    }
}

- (void)backCurrentPosition{
    if (switchFreeze.isOn) {
        [switchFreeze setOn:FALSE animated:YES];
        NSLog(@"should off!!");
    }
    else {
        NSLog(@"should on!!!");
        [switchFreeze setOn:TRUE animated:YES];
    }
}

-(IBAction)onBack:(id)sender{
    [owner onFreezeViewCancel];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
    }
    //cell.textLabel.text = [NSString stringWithFormat:@"cell %d",indexPath.row];
    
    if([indexPath row]==0){
        cell.textLabel.text =@"Settings";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    }
    else if([indexPath row]==1){
        
        cell.textLabel.text =@"Freeze Account";
        cell.accessoryType = UITableViewCellAccessoryNone;
        switchFreeze = [[UISwitch alloc]initWithFrame:CGRectMake(155, 10, 30, 30)];
        [switchFreeze addTarget:self action:@selector(changeSwitchValue:) forControlEvents:UIControlEventValueChanged];
        [self setTittleBtnFreeze];
        [cell addSubview:switchFreeze];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //cell.textLabel.textAlignment=UITextAlignmentCenter;
    
    return cell;
}

- (IBAction)changeSwitchValue:(UISwitch *)sender{
    if (sender.isOn) {
        NSLog(@"is on");
        [owner onFreezeViewFreeze];
    }
    else{
        NSLog(@"is off");

                [owner onUnFreeze];
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /* NewsHubAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
     if([indexPath row]==0){
     
     [delegate closeFPPopover];
     
     }
     else if([indexPath row]==1){
     
     //        SettingViewController *settingViewController=[[SettingViewController alloc]initWithNibName:@"SettingViewController" bundle:[NSBundle mainBundle]];
     //        [self.navigationController pushViewController:settingViewController animated:YES];
     //        [settingViewController release];
     //        settingViewController=nil;
     [delegate showSetting];
     }*/
    if (indexPath.row == 0) {
        
        [owner onFreezeViewChangePassword];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
