//
//  ClassFocusViewController.h
//  zBox
//
//  Created by Zayar on 5/9/13.
//
//

#import <UIKit/UIKit.h>
#import "ObjSubSession.h"
#import "ObjSubject.h"
@interface ClassFocusViewController : UIViewController
{
    IBOutlet UIImageView * imgBgView;
    IBOutlet UILabel * lblTitle;
    ObjSubSession * objSession;
    ObjSubject * objSubject;
    
    IBOutlet UIScrollView * scrollSesson;
    IBOutlet UIScrollView * scrollTopic;
    IBOutlet UILabel * lblName;
}
@property (nonatomic,retain) ObjSubSession * objSession;
@property (nonatomic,retain) ObjSubject * objSubject;
- (IBAction)onBack:(id)sender;
@end
