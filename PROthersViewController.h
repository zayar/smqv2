//
//  PROthersViewController.h
//  zBox
//
//  Created by Zayar on 5/9/13.
//
//

#import <UIKit/UIKit.h>
#import "ObjSubSession.h"
@interface PROthersViewController : UIViewController
{
    IBOutlet UIImageView * imgBgView;
    IBOutlet UILabel * lblTitle;
    ObjSubSession * objSession;
    
    IBOutlet UILabel * lblNextTopic;
    IBOutlet UILabel * lblNextLesson;
    IBOutlet UILabel * lblNextMilestone;
    IBOutlet UILabel * lblNextAdmin;
    
    IBOutlet UIScrollView * scrollOther;
}
@property (nonatomic, retain) ObjSubSession * objSession;
@end
