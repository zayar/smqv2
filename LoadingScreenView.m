//
//  LoadingScreenView.m
//  zBox
//
//  Created by Zayar Cn on 6/12/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "LoadingScreenView.h"

@implementation LoadingScreenView
@synthesize currentOrientation, useTransparent;
@synthesize lblMsg, activity;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)loadingScreenViewDidLoad{
    NSString * strPath = [NSString stringWithFormat:@"%@/popupbg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];
    [imgBgView release];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (void) changeToOrientation:(UIInterfaceOrientation) orientation{
	currentOrientation = orientation;
	[imgBgView.image release];
	imgBgView.image = nil;
	
	if (currentOrientation == UIInterfaceOrientationLandscapeLeft || currentOrientation == UIInterfaceOrientationLandscapeRight){
		imgBgView.bounds = CGRectMake(0, 0, 480, 320);
		imgBgView.frame = imgBgView.bounds;
		
		[imgBgView setImage: [[UIImage alloc] initWithContentsOfFile: [[NSString alloc] initWithFormat:@"%@/popupbg_landscape.png", [[NSBundle mainBundle] resourcePath]]]];
	}
	else{
		imgBgView.bounds =CGRectMake(0, 0, 320, 480);
		imgBgView.frame = imgBgView.bounds;
		
		[imgBgView setImage: [[UIImage alloc] initWithContentsOfFile: [[NSString alloc] initWithFormat:@"%@/popupbg.png", [[NSBundle mainBundle] resourcePath]]]];
	}
}

- (void)dealloc{
    [lblMsg release];
    [activity release];
    [imgBgView release];
    [super dealloc];
}

@end
