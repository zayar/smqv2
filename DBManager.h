//
//  DBManager.h
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "ObjUser.h"

@interface DBManager : NSObject{
    
}

- (void) checkAndCreateDatabase;
- (ObjUser *) getUserObj;
- (void) updateUser:(ObjUser *)objUser;

- (NSInteger) insertCachedImage:(NSString *)url path:(NSString *) filePath;
- (NSMutableArray *) getExpiredImages:(double) limit;
- (NSString *) getFilePath:(NSString *) url;
- (void) removeCahcedImage;
- (void) showPreviewWithImage:(UIImage *)img andIsCamera:(BOOL)isCam;
- (void) updateUserPassword:(NSString *)strPass;
@end
