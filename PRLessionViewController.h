//
//  PRLessionViewController.h
//  zBox
//
//  Created by Zayar on 5/9/13.
//
//

#import <UIKit/UIKit.h>
#import "ObjSubSession.h"
#import "ObjLesson.h"

@interface PRLessionViewController : UIViewController
{
    IBOutlet UIImageView * imgBgView;
    IBOutlet UILabel * lblTitle;
    ObjSubSession * objSession;
    ObjLesson * objLesson;
    
    IBOutlet UILabel * lblTopicName;
    
    IBOutlet UILabel * lblMarks;
    IBOutlet UILabel * lblCareless;
    IBOutlet UILabel * lblCouldhave;
    IBOutlet UIScrollView * scrollTopic;
}
@property (nonatomic, retain) ObjSubSession * objSession;
@end
