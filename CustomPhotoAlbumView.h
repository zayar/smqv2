//
//  CustomPhotoAlbumView.h
//  zBox
//
//  Created by Zayar Cn on 6/11/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
@protocol CustomPhotoAlbumViewDelegate 
-(void) onCustomPhotoAlbumViewCancel;
- (void)onCustomPhotoAlbumViewSelectedWithAssetGroup:(ALAssetsGroup *)assetGroup;
//-(void) onCustomPhotoAlbumViewChoose:(UIImage *)selectedImage;
@end
@interface CustomPhotoAlbumView : UIView
{
   IBOutlet UIImageView * imgBgView;
   id<CustomPhotoAlbumViewDelegate> owner;
    NSMutableArray *assetGroups;
	NSOperationQueue *queue;
	id parent;
    
    ALAssetsLibrary *library;
    IBOutlet UITableView * tbl;
}

@property (nonatomic, assign) id parent;
@property (nonatomic, retain) NSMutableArray *assetGroups;

-(void)selectedAssets:(NSArray*)_assets;
@property (assign) id<CustomPhotoAlbumViewDelegate> owner;
- (void)customPhotoAlbumViewViewDidLoad;
@end
