//
//  ClassFocusViewController.m
//  zBox
//
//  Created by Zayar on 5/9/13.
//
//

#import "ClassFocusViewController.h"
#import "ObjClassFocus.h"
#import "ObjSessionDetail.h"

@interface ClassFocusViewController ()

@end

@implementation ClassFocusViewController
@synthesize objSession,objSubject;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [self loadClassFocus:objSession];
}

- (void)loadClassFocus:(ObjSubSession *) obj{
    ObjSessionDetail * objSecDetail = [obj.arrSessionDetail objectAtIndex:0];
    ObjClassFocus * objClassFocus = objSecDetail.objClassFocus;
    
    /*if([obj.arrClassFocus count]>0){
        int y=0;
        int i=0;
        for(ObjClassFocus * objCF in obj.arrClassFocus){
            UILabel * lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, y, scrollSesson.frame.size.width, 30)];
            UILabel * lbl2 = [[UILabel alloc]initWithFrame:CGRectMake(0, y, scrollTopic.frame.size.width, 30)];
            
            if(i%2 == 0){
                lbl.backgroundColor = [UIColor lightGrayColor];
                lbl2.backgroundColor = [UIColor lightGrayColor];
            }
            else{
                lbl.backgroundColor = [UIColor whiteColor];
                lbl2.backgroundColor = [UIColor whiteColor];
            }
            lbl.text = objCF.strSessionName;
            lbl.font = [UIFont systemFontOfSize:13];
            lbl.numberOfLines = 0;
            
            
            lbl2.text = objCF.strTopicName;
            lbl2.font = [UIFont systemFontOfSize:13];
            lbl2.numberOfLines = 0;
            [scrollSesson addSubview:lbl];
            [scrollTopic addSubview:lbl2];
            y += lbl.frame.size.height;
            i++;
            [lbl release];
            [lbl2 release];
            
        }
    }*/
    lblName.text = objClassFocus.strSessionName;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSString * strPath = [NSString stringWithFormat:@"%@/img_vs_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];
    [imgBgView release];
}

- (IBAction)onBack:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    [scrollSesson release];
    [scrollTopic release];
    [objSession release];
    [objSubject release];
    [super dealloc];
}

@end
