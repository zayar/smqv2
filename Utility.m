//
//  Utility.m
//  PropertyGuru
//
//  Created by Tonytoons on 4/19/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Utility.h"

#define NUMBERS	@"0123456789"
#define NUMBERSPERIOD @"0123456789."

@implementation Utility

+ (BOOL) isNumber:(NSString *)input{
   NSCharacterSet *cs;
   NSString *filtered;
   
   // Check for period
   if ([input rangeOfString:@"."].location == NSNotFound)
   {
      cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS] invertedSet];
      filtered = [[input componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
      return [input isEqualToString:filtered];
   }
   
   // Period is in use
   cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERSPERIOD] invertedSet];
   filtered = [[input componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
   NSLog(@"filter: %@", filtered);
   return [input isEqualToString:filtered];   
}

+ (UIImage*)imageByCropping:(UIImage *)imageToCrop toRect:(CGRect)rect{
	//create a context to do our clipping in
	UIGraphicsBeginImageContext(rect.size);
	CGContextRef currentContext = UIGraphicsGetCurrentContext();
	
	//create a rect with the size we want to crop the image to
	//the X and Y here are zero so we start at the beginning of our
	//newly created context
	CGRect clippedRect = CGRectMake(0, 0, rect.size.width, rect.size.height);
	CGContextClipToRect( currentContext, clippedRect);
	
	//create a rect equivalent to the full size of the image
	//offset the rect by the X and Y we want to start the crop
	//from in order to cut off anything before them
	CGRect drawRect = CGRectMake(rect.origin.x * -1,
								 rect.origin.y * -1,
								 imageToCrop.size.width,
								 imageToCrop.size.height);
	
	//draw the image to our clipped context using our offset rect
	CGContextDrawImage(currentContext, drawRect, imageToCrop.CGImage);
	
	//pull the image from our cropped context
	UIImage *cropped = UIGraphicsGetImageFromCurrentImageContext();
	
	NSLog(@"rect width: %0.0f, cropped width: %0.0f", rect.size.width, cropped.size.width);
	
	//pop the context to get back to the default
	UIGraphicsEndImageContext();
	
	//Note: this is autoreleased
	return cropped;
}

+ (UIImage *)forceImageResize:(UIImage *) sourceImage newsize:(CGSize) targetSize{
	UIImage *newImage = nil;
	UIGraphicsBeginImageContext(targetSize);
	
	CGRect thumbnailRect = CGRectZero;   
	thumbnailRect.origin = CGPointMake(0.0,0.0);
	
	thumbnailRect.size.width  = targetSize.width;
	thumbnailRect.size.height = targetSize.height;
	
	[sourceImage drawInRect:thumbnailRect];
	
	newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	if(newImage == nil) NSLog(@"could not scale image");   
	
	return newImage;
}

+ (UIImage *) imageByScalingProportionallyToSize:(UIImage *) sourceImage newsize:(CGSize)targetSize resizeFrame:(BOOL) resize{
   UIImage *newImage = nil;
   
   CGSize imageSize = sourceImage.size;
   CGFloat width = imageSize.width;
   CGFloat height = imageSize.height;
   
   CGFloat targetWidth = targetSize.width;
   CGFloat targetHeight = targetSize.height;
   
   CGFloat scaleFactor = 0.0;
   CGFloat scaledWidth = targetWidth;
   CGFloat scaledHeight = targetHeight;
   
   CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
   
   if (CGSizeEqualToSize(imageSize, targetSize) == NO) {
      
      CGFloat widthFactor = targetWidth / width;
      CGFloat heightFactor = targetHeight / height;
      
      if (widthFactor < heightFactor) 
         scaleFactor = widthFactor;
      else
         scaleFactor = heightFactor;
      
      scaledWidth  = width * scaleFactor;
      scaledHeight = height * scaleFactor;
      
      // center the image
      
      if (widthFactor < heightFactor) {
         thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5; 
      } else if (widthFactor > heightFactor) {
         thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
      }
   }   
   
   // this is actually the interesting part:
   
   if( resize ){
      CGSize newSize = CGSizeMake(scaledWidth,scaledHeight);
      UIGraphicsBeginImageContext(newSize);
   }
   else UIGraphicsBeginImageContext(targetSize);
   
   CGRect thumbnailRect = CGRectZero;   
   if(!resize) thumbnailRect.origin = thumbnailPoint;
   
   thumbnailRect.size.width  = scaledWidth;
   thumbnailRect.size.height = scaledHeight;
   
   [sourceImage drawInRect:thumbnailRect];
   
   newImage = UIGraphicsGetImageFromCurrentImageContext();
   UIGraphicsEndImageContext();
   
   if(newImage == nil){
	   NSLog(@"could not scale image");
   }
   
   return newImage;
}

+ (UIImage *) resizedImage:(UIImage *) inImage newsize:(CGRect) thumbRect{
	CGImageRef			imageRef = [inImage CGImage];
	CGImageAlphaInfo	alphaInfo = CGImageGetAlphaInfo(imageRef);
	
	// There's a wierdness with kCGImageAlphaNone and CGBitmapContextCreate
	// see Supported Pixel Formats in the Quartz 2D Programming Guide
	// Creating a Bitmap Graphics Context section
	// only RGB 8 bit images with alpha of kCGImageAlphaNoneSkipFirst, kCGImageAlphaNoneSkipLast, kCGImageAlphaPremultipliedFirst,
	// and kCGImageAlphaPremultipliedLast, with a few other oddball image kinds are supported
	// The images on input here are likely to be png or jpeg files
	if (alphaInfo == kCGImageAlphaNone)
		alphaInfo = kCGImageAlphaNoneSkipLast;
		
	// Build a bitmap context that's the size of the thumbRect
	CGContextRef bitmap = CGBitmapContextCreate(
												NULL,
												thumbRect.size.width,		// width
												thumbRect.size.height,		// height
												CGImageGetBitsPerComponent(imageRef),	// really needs to always be 8
												4 * thumbRect.size.width,	// rowbytes
												CGImageGetColorSpace(imageRef),
												alphaInfo
												);
	
	// Draw into the context, this scales the image
	CGContextDrawImage(bitmap, thumbRect, imageRef);
	
	// Get an image from the context and a UIImage
	CGImageRef	ref = CGBitmapContextCreateImage(bitmap);
	UIImage*	result = [UIImage imageWithCGImage:ref];
	
	CGContextRelease(bitmap);	// ok if NULL
	CGImageRelease(ref);
	
	return result;
}

+ (NSString *) transformHTML:(NSString *) input{   
	input = [input stringByReplacingOccurrencesOfString:@"<p>" withString:@"\n\n"];
   input = [input stringByReplacingOccurrencesOfString:@"</p>" withString:@""];
   input = [input stringByReplacingOccurrencesOfString:@"<br><br/>" withString:@"\n"];
   input = [input stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
   input = [input stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
   input = [input stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
   input = [input stringByReplacingOccurrencesOfString:@"<b>" withString:@""];
   input = [input stringByReplacingOccurrencesOfString:@"</b>" withString:@""];
   input = [input stringByReplacingOccurrencesOfString:@"&ldquo;" withString:@"\""];
   input = [input stringByReplacingOccurrencesOfString:@"&rdquo;" withString:@"\""];
   input = [input stringByReplacingOccurrencesOfString:@"&lquo;" withString:@"'"];
   input = [input stringByReplacingOccurrencesOfString:@"&rquo;" withString:@"'"];
   input = [input stringByReplacingOccurrencesOfString:@"&ndash;" withString:@"-"];
   
   return input;
}

+ (UIImage*)imageWithBorderFromImage:(UIImage*)source{
   CGSize size = [source size];
   UIGraphicsBeginImageContext(size);
   CGRect rect = CGRectMake(0, 0, size.width, size.height);
   [source drawInRect:rect blendMode:kCGBlendModeNormal alpha:1.0];
   
   CGContextRef context = UIGraphicsGetCurrentContext();
   CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0); 
   CGContextStrokeRect(context, rect);
   
   UIImage *testImg =  UIGraphicsGetImageFromCurrentImageContext();
   
   UIGraphicsEndImageContext();   
   //[source release];
   //CGContextRelease(context);
   
   return testImg;
}

@end
