//
//  DetailImagePopUpView.h
//  NewsHub
//
//  Created by Zayar Cn on 5/9/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol DetailImagePopUpViewDelegate 
-(void) onBtnCloseClick;
@end
@interface DetailImagePopUpView : UIView
{
    UIImageView * imgBgView;
    UIImageView * imgDetailView;
    //UIImageView * imgDesBgView;
    UILabel * lblImgDes;
    UIWebView * webView;
    UIActivityIndicatorView * activity;
    UIButton * btnClose;
    id<DetailImagePopUpViewDelegate> owner;
    NSMutableArray * imageList;
    
    UIScrollView * imgScrollView;
}

@property (assign) id<DetailImagePopUpViewDelegate> owner;
@property (nonatomic, retain) IBOutlet UIButton * btnClose;
@property (nonatomic, retain) IBOutlet UIImageView * imgBgView;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView * activity;
@property (nonatomic, retain) IBOutlet UILabel * lblImgDes;
@property (nonatomic, retain) IBOutlet UIImageView * imgDetailView;
@property (nonatomic, retain) IBOutlet UIWebView * webView;
//@property (nonatomic, retain) IBOutlet UIImageView * imgDesBgView;
@property (nonatomic, retain) IBOutlet UIScrollView * imgScrollView;

@property (nonatomic, retain) NSMutableArray * imageList;

- (void) preLoadView;
- (void) loadWebView:(NSString *) strURL;
@end
