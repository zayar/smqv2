//
//  CatalogCell.h
//  zBox
//
//  Created by Zayar on 5/8/13.
//
//

#import <UIKit/UIKit.h>

@interface CatalogCell : UITableViewCell
{
    UIImageView * imgBgView;
    UILabel * lblTitle;
    UILabel * lblText;
    UIActivityIndicatorView * activity;
    UIImageView * imgView;
}
@property (nonatomic, retain) IBOutlet UIImageView * imgBgView;
@property (nonatomic, retain) IBOutlet UILabel * lblTitle;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView * activity;
@property (nonatomic, retain) IBOutlet UILabel * lblText;
@property (nonatomic, retain) IBOutlet UIWebView * webThumbView;
@property (nonatomic, retain) IBOutlet UIImageView * imgView;
@end
