//
//  VSView.m
//  zBox
//
//  Created by Zayar Cn on 5/11/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "VSView.h"
#import "vsTableViewCell.h"
#import "zBoxAppDelegate.h"
#import "StringTable.h"
#import "ObjAnswer.h"
#import "SOAPRequest.h"
#import "SVModalWebViewController.h"
#import <QuartzCore/QuartzCore.h>
@implementation VSView
@synthesize owner;
@synthesize tbl;
@synthesize photos = _photos;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)dealloc{
    [rateRequest release];
    [_photos release];
    [detailImgPopUpView release];
    [tbl release];
    [getQuestionRequest release];
    [arrSolution release];
    [imgBgView release];
    [super dealloc];
}

- (void)vsViewDidLoad{
    NSString * strPath = [NSString stringWithFormat:@"%@/img_vs_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];
    [imgBgView release];
    
    arrSolution = [[NSMutableArray alloc]init];
    
    /*for (int i = 0; i<10; i++) {
        ObjAnswer * objAns = [[ObjAnswer alloc]init];
        objAns.idx = i;
        objAns.subject_id = i;
        objAns.category_id = i;
        objAns.question_id = i;
        objAns.strQuestionTitle = [NSString stringWithFormat:@"Title %d",i];
        if (i % 2 == 0)
        {
            objAns.strQuestAnsType = ANSWER_TYPE_IMAGE;
            objAns.strQuestionImageFileLink = @"http://askatechteacher.files.wordpress.com/2011/04/math.jpg";
            objAns.strQuestionImageThumbFileLink = @"http://farm4.static.flickr.com/3567/3523321514_371d9ac42f_b.jpg";
        }
        else 
        {
            objAns.strQuestAnsType = ANSWER_TYPE_VIDEO;
            objAns.strQuestionMovieFileLink = @"http://www.youtube.com/watch?v=Iclfjpcjr8E";
        }
        
        objAns.strQuestionText = [NSString stringWithFormat:@"Answer %d",i];
        [arrSolution addObject:objAns];
        [objAns release];
    }*/
    
}

- (void)vsViewWillAppear{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate clearTheBudgeIcon];
    [self syncGetQuestion];
}

- (void)syncGetQuestion{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    [delegate showLoadingScreen];
    delegate.isFromMain = FALSE;
    getQuestionRequest = [[SOAPRequest alloc] initWithOwner:self];
    getQuestionRequest.processId = 7;
    [getQuestionRequest syncGetUserQA];
}

- (void) loadWebView:(NSString *) strURL{
    /*NSString *embedHTML = @"\
    <html><head>\
    <style type=\"text/css\">\
    body {\
    background-color: transparent;\
    color: white;\
    }\
    </style>\
    </head><body style=\"margin:0\">\
    <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
    width=\"%0.0f\" height=\"%0.0f\"></embed>\
    </body></html>";

    NSString *html = [NSString stringWithFormat:embedHTML, strURL, self.frame.size.width, self.frame.size.height];
    [webView loadHTMLString:html baseURL:nil];*/
    
}

-(void) onImageClick:(ObjAnswer *)objAns andIsImage:(BOOL)isImage{
    // objAns.strQuestionMovieFileLink
    //    if(isOriginalImage)
    //    {
    /*
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    
    if (detailImgPopUpView == nil) {
        NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DetailImagePopUpView" owner:nil options:nil];		
        for(id currentObject in topLevelObjects){
            if([currentObject isKindOfClass:[DetailImagePopUpView class]]){
                detailImgPopUpView = (DetailImagePopUpView *) currentObject;
                detailImgPopUpView.backgroundColor = [UIColor clearColor];
                //detailImgPopUpView.frame = CGRectMake(6,0,detailImgPopUpView.frame.size.width, detailImgPopUpView.frame.size.width);
                [detailImgPopUpView preLoadView];
                [self addSubview: detailImgPopUpView];
                break;
            }
        }
    }
    if (isImage){
        detailImgPopUpView.imgDetailView.hidden = FALSE;
        detailImgPopUpView.webView.hidden = TRUE;
    }
    else {
        detailImgPopUpView.imgDetailView.hidden = TRUE;
        detailImgPopUpView.webView.hidden = FALSE;
    }
    detailImgPopUpView.hidden = FALSE;
    //objAns.owner = self;
    detailImgPopUpView.owner = self;
    detailImgPopUpView.imgDetailView.frame = CGRectMake(6, -480, detailImgPopUpView.imgDetailView.frame.size.width, detailImgPopUpView.imgDetailView.frame.size.height);
    detailImgPopUpView.webView.frame = CGRectMake(6, -480, detailImgPopUpView.webView.frame.size.width, detailImgPopUpView.webView.frame.size.height);
    //detailImgPopUpView.imgDesBgView.frame = CGRectMake(6, -480,  detailImgPopUpView.imgDesBgView.frame.size.width,  detailImgPopUpView.imgDesBgView.frame.size.height);
    detailImgPopUpView.lblImgDes.frame = CGRectMake(16, -480, detailImgPopUpView.lblImgDes.frame.size.width, detailImgPopUpView.lblImgDes.frame.size.height);
    detailImgPopUpView.btnClose.frame = CGRectMake(293, -480, detailImgPopUpView.btnClose.frame.size.width, detailImgPopUpView.btnClose.frame.size.height);
    detailImgPopUpView.activity.frame = CGRectMake(144, -480, detailImgPopUpView.activity.frame.size.width, detailImgPopUpView.activity.frame.size.height);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.6];
    [UIView setAnimationDelegate: self];
    detailImgPopUpView.imgDetailView.frame = CGRectMake(6, 104, detailImgPopUpView.imgDetailView.frame.size.width, detailImgPopUpView.imgDetailView.frame.size.height);
    detailImgPopUpView.webView.frame = CGRectMake(6, 104, detailImgPopUpView.webView.frame.size.width, detailImgPopUpView.webView.frame.size.height);
    //detailImgPopUpView.imgDesBgView.frame = CGRectMake(6, 270,  detailImgPopUpView.imgDesBgView.frame.size.width,  detailImgPopUpView.imgDesBgView.frame.size.height);
    detailImgPopUpView.lblImgDes.frame = CGRectMake(16, 271, detailImgPopUpView.lblImgDes.frame.size.width, detailImgPopUpView.lblImgDes.frame.size.height);
    detailImgPopUpView.btnClose.frame = CGRectMake(293, 85, detailImgPopUpView.btnClose.frame.size.width, detailImgPopUpView.btnClose.frame.size.height);
    detailImgPopUpView.activity.frame = CGRectMake(144, 177, detailImgPopUpView.activity.frame.size.width, detailImgPopUpView.activity.frame.size.height);
    [UIView commitAnimations];
    if (isImage) {
        [self bringSubviewToFront: detailImgPopUpView.imgDetailView];
        detailImgPopUpView.imgDetailView.image = nil;
        
        NSString * imgPath = [delegate.db getFilePath: objAns.strQuestionImageFileLink];
        
        if( imgPath != nil && (NSNull *) imgPath != [NSNull null] && ![imgPath isEqualToString:@""] ){
            UIImage * img = [[UIImage alloc] initWithContentsOfFile: imgPath];
            [detailImgPopUpView.imgDetailView setImage: img];
            [img release];
            [detailImgPopUpView.activity stopAnimating];
        }
        else{
            [objAns.thumbnailCallBacks addObject: detailImgPopUpView.imgDetailView];
            [objAns downloadImage:0];
            [detailImgPopUpView.activity startAnimating];
        }
    }
    else {
        [self bringSubviewToFront: detailImgPopUpView.webView];
        detailImgPopUpView.imgDetailView.image = nil;
        [detailImgPopUpView loadWebView:objAns.strQuestionMovieFileLink];
    }
    
    
    //detailImgPopUpView.lblImgDes.font=[UIFont boldSystemFontOfSize: 10.0];
    //detailImgPopUpView.lblImgDes.font=[UIFont fontWithName:@"Zawgyi-One" size:10.0];
    detailImgPopUpView.lblImgDes.text = objAns.strQuestionText;
    //}
     */
    
    // Let's try adding a webview
    NSURL *URL = [NSURL URLWithString:objAns.strQuestAns];
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:URL];
	webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    webViewController.availableActions = SVWebViewControllerAvailableActionsOpenInSafari | SVWebViewControllerAvailableActionsCopyLink | SVWebViewControllerAvailableActionsMailLink;
    
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate.viewController presentModalViewController: webViewController animated:YES];
}

-(void) onBtnCloseClick{
    NSLog(@"popup close!!!");
    
    //    for(UIView * v in detailImgPopUpView.imgScrollView.subviews){
    //        v=nil;
    //        [v removeFromSuperview];
    //    }
    
    detailImgPopUpView.imgDetailView.frame = CGRectMake(6, 104, detailImgPopUpView.imgDetailView.frame.size.width, detailImgPopUpView.imgDetailView.frame.size.height);
    detailImgPopUpView.webView.frame = CGRectMake(6, 104, detailImgPopUpView.webView.frame.size.width, detailImgPopUpView.webView.frame.size.height);
    detailImgPopUpView.imgScrollView.frame = CGRectMake(6, 104, detailImgPopUpView.imgScrollView.frame.size.width, detailImgPopUpView.imgScrollView.frame.size.height);
    //detailImgPopUpView.imgDesBgView.frame = CGRectMake(6, 270,  detailImgPopUpView.imgDesBgView.frame.size.width,  detailImgPopUpView.imgDesBgView.frame.size.height);
    detailImgPopUpView.lblImgDes.frame = CGRectMake(16, 271, detailImgPopUpView.lblImgDes.frame.size.width, detailImgPopUpView.lblImgDes.frame.size.height);
    detailImgPopUpView.btnClose.frame = CGRectMake(293, 85, detailImgPopUpView.btnClose.frame.size.width, detailImgPopUpView.btnClose.frame.size.height);
    detailImgPopUpView.activity.frame = CGRectMake(144, 177, detailImgPopUpView.activity.frame.size.width, detailImgPopUpView.activity.frame.size.height);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.6];
    [UIView setAnimationDelegate: self];
    detailImgPopUpView.webView.frame = CGRectMake(6, -480, detailImgPopUpView.webView.frame.size.width, detailImgPopUpView.webView.frame.size.height);
    detailImgPopUpView.imgDetailView.frame = CGRectMake(6, -480, detailImgPopUpView.imgDetailView.frame.size.width, detailImgPopUpView.imgDetailView.frame.size.height);
    detailImgPopUpView.imgScrollView.frame = CGRectMake(6, -480, detailImgPopUpView.imgScrollView.frame.size.width, detailImgPopUpView.imgScrollView.frame.size.height);
    //detailImgPopUpView.imgDesBgView.frame = CGRectMake(6, -480,  detailImgPopUpView.imgDesBgView.frame.size.width,  detailImgPopUpView.imgDesBgView.frame.size.height);
    detailImgPopUpView.lblImgDes.frame = CGRectMake(16, -480, detailImgPopUpView.lblImgDes.frame.size.width, detailImgPopUpView.lblImgDes.frame.size.height);
    detailImgPopUpView.btnClose.frame = CGRectMake(293, -480, detailImgPopUpView.btnClose.frame.size.width, detailImgPopUpView.btnClose.frame.size.height);
    detailImgPopUpView.activity.frame = CGRectMake(144, -480, detailImgPopUpView.activity.frame.size.width, detailImgPopUpView.activity.frame.size.height);
    [UIView commitAnimations];
    //detailImgPopUpView.imgBgView.hidden = TRUE;
    [self performSelector:(@selector(hideImagePopUp)) withObject:nil afterDelay:0.6];
}

- (void)hideImagePopUp{
    detailImgPopUpView.hidden = TRUE;
}

- (IBAction)onCancel:(id)sender{
    [self vsViewWillDisAppear];
    [owner onVSViewCancel];
}
////TableView Datasource & Delegate////
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrSolution count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    ObjAnswer * objAns = [arrSolution objectAtIndex:[indexPath row]];
    static NSString *CellIdentifier = @"vsTableViewCell";
    vsTableViewCell *cell = nil;
     UITableViewCell *cellindent = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cellindent == nil) {
        NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"vsTableViewCell" owner:nil options:nil];		
        for(id currentObject in topLevelObjects){
            if([currentObject isKindOfClass:[UITableViewCell class]]){            
                cell = (vsTableViewCell *) currentObject;            
                cell.accessoryView = nil;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                break;
            }
        }
        [cell cellViewLoad];
        [cell rateSelected:objAns.rate_point];
        cell.owner = self;
    }
    [cell.imgBgView setImage:[UIImage imageNamed:@"imb_vs_tablecell_bg.png"]];
    if ([objAns.strQuestionStatus isEqualToString: ANSWER_STATUS_PENDING]) {
        cell.webThumbView.hidden = TRUE;
        cell.imgThumbView.hidden = FALSE;
        cell.imgThumbView.image = nil;
        cell.imgThumbView.image = [UIImage imageNamed:@"img_vs_pending.png"];
        cell.rateView.hidden = TRUE;
        cell.ratingBgView.hidden = TRUE;
    }
    else {
        if (objAns.strQuestAnsType != (id)[NSNull null] && objAns.strQuestAnsType != NULL) {
            if([objAns.strQuestAnsType isEqualToString:ANSWER_TYPE_IMAGE]){
                cell.webThumbView.hidden = TRUE;
                cell.imgThumbView.hidden = FALSE;
                cell.activity.hidden = FALSE;
                
                NSLog(@"obj Image link: %@",objAns.strQuestionImageThumbFileLink);
                NSString * adsImgPath = [delegate.db getFilePath: objAns.strQuestionImageThumbFileLink];
                if( adsImgPath != nil && (NSNull *) adsImgPath != [NSNull null] && ![adsImgPath isEqualToString:@""] ){
                    [cell.imgThumbView setImage: [[UIImage alloc] initWithContentsOfFile: adsImgPath]];
                }
                else{
                    [cell.activity startAnimating];
                    [objAns.thumbnailCallBacks addObject:cell.imgThumbView];
                    [objAns downloadImage:0];
                }
            }
            else if([objAns.strQuestAnsType isEqualToString:ANSWER_TYPE_VIDEO]){
                cell.webThumbView.hidden = TRUE;
                cell.imgThumbView.hidden = FALSE;
                cell.activity.hidden = FALSE;
                
                // NSLog(@"web url : %@ to load and %d",objAns.strQuestionMovieFileLink,cell.isWebLoaded);
                /*NSString *embedHTML = @"\
                <html><head>\
                <style type=\"text/css\">\
                body {\
                background-color: transparent;\
                color: white;\
                }\
                </style>\
                </head><body style=\"margin:0\">\
                <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
                width=\"%0.0f\" height=\"%0.0f\"></embed>\
                </body></html>";
                
                NSString *html = [NSString stringWithFormat:embedHTML, objAns.strQuestAns, cell.webThumbView.frame.size.width, cell.webThumbView.frame.size.height];
                [cell.webThumbView loadHTMLString:html baseURL:nil];
                cell.webThumbView.delegate = self;
                [cell.webThumbView reload];*/
                NSString * videoID = [self extractYoutubeID:objAns.strQuestAns];
                NSString * strImgLinkFormat = @"http://img.youtube.com/vi/%@/1.jpg";
                NSString * strUTubeLink = [NSString stringWithFormat:strImgLinkFormat,videoID];

                NSLog(@"youtube thumb link %@ and original link %@ and id %@",strUTubeLink,objAns.strQuestAns,videoID);
                objAns.strQuestionImageThumbFileLink = strUTubeLink;
                NSString * adsImgPath = [delegate.db getFilePath: strUTubeLink];
                if( adsImgPath != nil && (NSNull *) adsImgPath != [NSNull null] && ![adsImgPath isEqualToString:@""] ){
                    [cell.imgThumbView setImage: [[UIImage alloc] initWithContentsOfFile: adsImgPath]];
                }
                else{
                    [cell.activity startAnimating];
                    [objAns.thumbnailCallBacks addObject:cell.imgThumbView];
                    [objAns downloadImage:0];
                }
            }
             cell.rateView.hidden = FALSE;
        }
    }
    UIFont * cfont = [UIFont fontWithName:@"BradyBunchRemastered" size:24.0];
    UIFont * sfont = [UIFont fontWithName:@"BradyBunchRemastered" size:10.0];
    cell.lblTitle.font = cfont;
    cell.lblTitle.textColor = [UIColor whiteColor];
    cell.lblText.font = sfont;
    cell.lblText.textColor = [UIColor whiteColor];
    if (objAns.strQuestionTitle != (id)[NSNull null] && objAns.strQuestionTitle != NULL) {
        cell.lblTitle.text = objAns.strQuestionTitle;
    }
    cell.lblTitle.shadowColor = [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.5];
    //cell.lblTitle.t
    cell.lblTitle.shadowOffset = CGSizeMake(2, 2);
    if (objAns.strQuestionText != (id)[NSNull null] && objAns.strQuestionText != NULL) {
        cell.lblText.text = objAns.strQuestionText;
        cell.lblText.text = objAns.strQuestionText;
    }
    cell.rateView.tag = [indexPath row];
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    return cell;
}

- (void) onImageDownloadFinish:(int)proccessId imageType:(int) itemType{
    
    /*UITableViewCell * vCell = [tbl cellForRowAtIndexPath:rowCellIndex];
    if ([vCell isKindOfClass:[vsTableViewCell class]]) {
    }*/
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ObjAnswer * obj = [arrSolution objectAtIndex:[indexPath row]];
    if (![obj.strQuestionStatus isEqualToString:ANSWER_STATUS_PENDING]) {
        if ([obj.strQuestAnsType isEqualToString: ANSWER_TYPE_IMAGE]) {
            [owner showPicPhotoBrowser:obj.strQuestAns andTitle:(NSString *)obj.strQuestionTitle];
        }
        else{
            /* NSURL *url = [NSURL URLWithString:obj.strQuestionMovieFileLink];
             UIApplication * app = [UIApplication sharedApplication];
             [app openURL:url];*/
            [self onImageClick:obj andIsImage:FALSE];
        }
    }
    
}

- (NSString *)extractYoutubeID:(NSString *)youtubeURL
{

    NSError *error = nil;
    NSString *regexString = @"(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)";
    NSRegularExpression *regex =
    [NSRegularExpression regularExpressionWithPattern:regexString
                                              options:NSRegularExpressionCaseInsensitive
                                                error:&error];
    NSTextCheckingResult *match = [regex firstMatchInString:youtubeURL
                                                    options:0
                                                      range:NSMakeRange(0, [youtubeURL length])];
    if (match) {
        NSRange videoIDRange = [match rangeAtIndex:0];
        NSString *substringForFirstMatch = [youtubeURL substringWithRange:videoIDRange];
        return substringForFirstMatch;
    }
    return nil;
}

-(void) selectedRate:(int)rate andIndex:(int)idx{
    ObjAnswer * objAns = [arrSolution objectAtIndex:idx];
    objAns.rate_point = rate;
    //[arrSolution removeObjectAtIndex:idx];
    //[arrSolution insertObject:objAns atIndex:idx];
    [arrSolution replaceObjectAtIndex:idx withObject:objAns];
    NSLog(@"rate id %d and index id %d",rate,idx);
    [self syncRate:objAns];
}

- (void) syncRate:(ObjAnswer *)objAns{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    [delegate showLoadingScreen];
    rateRequest = [[SOAPRequest alloc] initWithOwner:self];
    rateRequest.processId = 9;
    [rateRequest syncRateWithAnswer:objAns];
}

- (void) vsViewWillDisAppear{
    [arrSolution removeAllObjects];
    [tbl reloadData];
}

- (void) onErrorLoad: (int) processId{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    [delegate hideLoadingScreen];
    NSLog(@"Error loaded %d",processId);
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{
    
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    if (processId == 7) {
        
        [delegate hideLoadingScreen];
        NSLog(@"arr count %d",[dics count]);
        NSString * strMessage;
        int status = [[dics objectForKey:@"status"]intValue];
        NSLog(@"%i count", status);
        if (status == STATUS_RETURN_RECORD) {
            NSMutableArray * arrSubject = [dics objectForKey:@"items"];
            NSLog(@"count : %d", [arrSubject count]);
            for ( int i =0; i < [arrSubject count]; i++) {
                NSLog(@"%i", i);
                NSDictionary * dicQ = [arrSubject objectAtIndex:i];
                ObjAnswer * objA = [[ObjAnswer alloc]init];
                objA.question_id = [[dicQ objectForKey:@"question_id"]intValue];
                objA.subject_id = [[dicQ objectForKey:@"subject_id"] intValue];
                objA.category_id = [[dicQ objectForKey:@"category_id"] intValue];
                objA.strQuestionStatus = [dicQ objectForKey:@"question_status"];
                objA.strQuestionTitle = [dicQ objectForKey:@"question_title"];
                objA.strQuestAnsType = [dicQ objectForKey:@"question_answer_type"];


                objA.strQuestionText = [dicQ objectForKey:@"question_text"];
                if (objA.strQuestAnsType != (id)[NSNull null] && objA.strQuestAnsType != NULL) {

                    if ([objA.strQuestAnsType isEqualToString:ANSWER_TYPE_IMAGE]) {
                        objA.strQuestionImageFileLink = [dicQ objectForKey:@"question_file"];
                        //objA.strQuestionImageFileLink = [NSString stringWithFormat:@"%@%@",BASE_LINK,objA.strQuestionImageFileLink];
                        objA.strQuestionImageThumbFileLink = [dicQ objectForKey:@"question_answer_thumbnail"];
                        NSLog(@"Link %@", objA.strQuestionImageFileLink);

                    }
                    else if([objA.strQuestAnsType isEqualToString:ANSWER_TYPE_VIDEO]){
                        objA.strQuestionMovieFileLink = [dicQ objectForKey:@"question_file"];
                    }
                } else {
                    objA.strQuestAnsType = ANSWER_TYPE_NULL;
                }
                
                
                NSLog(@"thumb nail %@",objA.strQuestionImageThumbFileLink);
                
                objA.strQuestAns = [dicQ objectForKey:@"question_answer"];
                objA.rate_point = [[dicQ objectForKey:@"question_rating"] intValue];
                
                [arrSolution addObject:objA];
                [objA release];
            }
            [tbl reloadData];
        }
        else if(status == STATUS_NO_RECORD_FOUND){
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];	
            [alert release];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
    
    }
    else if (processId == 9){
        [delegate hideLoadingScreen];
        NSLog(@"arr count %d",[dics count]);
        NSString * strMessage;
        int status = [[dics objectForKey:@"status"]intValue];
        strMessage = [dics objectForKey:@"message"];
        NSLog(@"%i count", status);
        if (status == STATUS_ACTION_SUCCESS) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];	
            [alert release];
        }
        else if(status == STATUS_NO_RECORD_FOUND){
            //strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];	
            [alert release];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            //strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
    }
}

////End TableView Datasource & Delegate////

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
