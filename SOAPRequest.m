//
//  SOAPRequest.m
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//
#import <CommonCrypto/CommonDigest.h>
#import "SOAPRequest.h"
#import "StringTable.h"
#import "zBoxAppDelegate.h"
#import "JSONKit.h"
#import "ObjUser.h"
#import "NSData+Base64.h"
#import "ObjAnswer.h"
#import "ObjCatalog.h"
#import "ObjQuizz.h"
@implementation SOAPRequest

@synthesize responseData;
@synthesize owner, processId, rType;

- (id)initWithOwner:(id)del {
	if (self = [super init]) {
        self.owner = del;
    }
	return self;
}

- (NSString *) urlencode: (NSString *) url{
    NSArray *escapeChars = [NSArray arrayWithObjects:@";" , @"/" , @"?" , @":" ,
                            @"@" , @"&" , @"=" , @"+" ,
                            @"$" , @"," , @"[" , @"]",
                            @"#", @"!", @"'", @"(", 
                            @")", @"*", nil];
    
    NSArray *replaceChars = [NSArray arrayWithObjects:@"%3B" , @"%2F" , @"%3F" ,
                             @"%3A" , @"%40" , @"%26" ,
                             @"%3D" , @"%2B" , @"%24" ,
                             @"%2C" , @"%5B" , @"%5D", 
                             @"%23", @"%21", @"%27",
                             @"%28", @"%29", @"%2A", nil];
    
    int len = [escapeChars count];
    
    NSMutableString *temp = [url mutableCopy];
    
    int i;
    for(i = 0; i < len; i++)
    {
        
        [temp replaceOccurrencesOfString: [escapeChars objectAtIndex:i]
                              withString:[replaceChars objectAtIndex:i]
                                 options:NSLiteralSearch
                                   range:NSMakeRange(0, [temp length])];
    }
    
    NSString *out = [NSString stringWithString: temp];
    
    return out;
}

- (void) syncGenerateKey{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    rType = REQUEST_TYPE_GENR_KEY;
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,GENR_KEY_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"client_platform=I&device_token=%@",delegate.strUdid];
    responseData = [[NSMutableData alloc] init];
    NSURL * url = [NSURL URLWithString: fullURL];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
        NSLog(@"loading The Key: %@ param %@", fullURL,strParamPlatform);
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncGenerateKeyWithToken:(NSString *) token{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    rType = REQUEST_TYPE_GENR_KEY;
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,GENR_KEY_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"client_platform=I&device_token=%@",token];
    responseData = [[NSMutableData alloc] init];
    NSURL * url = [NSURL URLWithString: fullURL];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    NSLog(@"loading The Key: %@ and param %@", fullURL,strParamPlatform);
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncSupervieorPassWith:(NSString *) strSPass{
    rType = REQUeST_TYPE_CHECK_SUPERVISOR;
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,CHECK_SUPERVISOR_LINK];
    NSString * encodedPass = [self encodePasswordWithSHA1:strSPass];
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"session_id=%@&password=%@&key=%@",strSession,encodedPass,strKey];
    responseData = [[NSMutableData alloc] init];
    NSURL * url = [NSURL URLWithString: fullURL];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    NSLog(@"loading The Key: %@", fullURL);
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncResetPassWith:(NSString *) strUName{
    rType = REQUEST_TYPE_RESET_PASS;
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,RESET_PASS_LINK];
    /*NSString * encodedPass = [self encodePasswordWithSHA1:strSPass];
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];*/
    NSString * strParamPlatform = [NSString stringWithFormat:@"user_name=%@",strUName];
    responseData = [[NSMutableData alloc] init];
    NSURL * url = [NSURL URLWithString: fullURL];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    NSLog(@"loading The Key: %@", fullURL);
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncLoginWithUser:(ObjUser *)objUser{
    rType = REQUEST_TYPE_LOGIN;
     NSLog(@"user :%@ and pass : %@ in soqr equest",objUser.strName,objUser.strPassword);
    NSString * encodedPass = [self encodePasswordWithSHA1:objUser.strPassword];
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    //NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"username=%@&password=%@&key=%@",objUser.strName,encodedPass,strKey];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,LOGIN_LINK];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    NSLog(@"loading The Key: %@", fullURL);
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncFreezeWithUser:(ObjUser *)objUser{
    rType = REQUEST_TYPE_FREEZE;
    NSLog(@"user :%@ and pass : %@ in soqr equest",objUser.strName,objUser.strParentPassword);
    NSString * encodedPass = [self encodePasswordWithSHA1:objUser.strParentPassword];
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    //NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"session_id=%@&password=%@&key=%@&freeze=Y",objUser.strSession,encodedPass,strKey];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,FREEZE_LINK];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    NSLog(@"loading The Key: %@ and param %@", fullURL,strParamPlatform);
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncRegisterWithCata:(ObjCatalog *)objCata{
    rType = REQUEST_TYPE_CATALOG_REGISTER;
    
    NSString * encodedPass = [self encodePasswordWithSHA1:objCata.strSPass];
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"session_id=%@&password=%@&key=%@&catalog_response_value=%@&catalog_id=%d",strSession,encodedPass,strKey,objCata.strConsentValue,objCata.idx];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,CATALOG_REGISTER_LINK];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    NSLog(@"loading The Key: %@ and param %@", fullURL,strParamPlatform);
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncUnFreezeWithUser:(ObjUser *)objUser{
    rType = REQUEST_TYPE_UNFREEZE;
    NSLog(@"user :%@ and pass : %@ in soqr equest",objUser.strName,objUser.strParentPassword);
    NSString * encodedPass = [self encodePasswordWithSHA1:objUser.strParentPassword];
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    //NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"session_id=%@&password=%@&key=%@&freeze=N",objUser.strSession,encodedPass,strKey];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,UNFREEZE_LINK];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    NSLog(@"loading The Key: %@ and param %@", fullURL,strParamPlatform);
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncCreateQuizzConsultWithUser:(ObjQuizz *)objQuizz{
    rType = REQUEST_TYPE_QUIZZ_CREAT_CONSULT;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"session_id=%@&key=%@&quiz_id=%d&quiz_consultation_type=%@",strSession,strKey,objQuizz.idx,objQuizz.strConsultType];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,QUIZ_CONSULT_LINK];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    NSLog(@"loading The Key: %@ and param %@", fullURL,strParamPlatform);
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncAndCheckTheSessionKey{
    rType = REQUEST_TYPE_CHECK_SESSION;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"session_id=%@&key=%@",strSession,strKey];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,CHECK_LOGIN_SESSION_LINK];
    
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    NSLog(@"loading The Key: %@ and para: %@", fullURL,strParamPlatform);
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (NSString *)encodePasswordWithSHA1:(NSString *)str{
    NSString *hashkey = str;
    // PHP uses ASCII encoding, not UTF
    const char *s = [hashkey cStringUsingEncoding:NSASCIIStringEncoding];
    NSData *keyData = [NSData dataWithBytes:s length:strlen(s)];
    
    // This is the destination
    uint8_t digest[CC_SHA1_DIGEST_LENGTH] = {0};
    // This one function does an unkeyed SHA1 hash of your hash data
    CC_SHA1(keyData.bytes, keyData.length, digest);
    
    // Now convert to NSData structure to make it usable again
    NSData *out = [NSData dataWithBytes:digest length:CC_SHA1_DIGEST_LENGTH];
    // description converts to hex but puts <> around it and spaces every 4 bytes
    NSString *hash = [out description];
    hash = [hash stringByReplacingOccurrencesOfString:@" " withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@"<" withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    return hash;
}

- (void) syncUserPoint{
    rType = REQUEST_TYPE_GET_POINT;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,CHECK_USER_POINT_LINK,strKey,strSession];
    NSString * strParamPlatform = [NSString stringWithFormat:@"key=%@&session_id=%@",strKey,strSession];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"loading The Key: %@ and para = %@", fullURL,strParamPlatform);
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncSubjectAndCritea{
    rType = REQUEST_TYPE_SUBJECT_CRITEA;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"key=%@&session_id=%@",strKey,strSession];
    responseData = [[NSMutableData alloc] init];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,SUBJECT_CRITERIA_LINK];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"loading The Key: %@ and para = %@", fullURL,strParamPlatform);
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncProgressReport{
    rType = REQUEST_TYPE_PROGRESS_REPORT;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"key=%@&session_id=%@",strKey,strSession];
    responseData = [[NSMutableData alloc] init];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK, PROGRESS_REPORT_LINK];
    //NSString * fullURL = [NSString stringWithFormat:@"%@",PROGRESS_LOCAL_REPORT_LINK];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"loading The Key: %@ and para = %@", fullURL,strParamPlatform);
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncCatalog{
    rType = REQUEST_TYPE_CATALOG;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"key=%@&session_id=%@",strKey,strSession];
    responseData = [[NSMutableData alloc] init];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,CATALOG_LINK];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"loading The Key: %@ and para = %@", fullURL,strParamPlatform);
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncAds{
    rType = REQUEST_TYPE_ADS;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"key=%@&session_id=%@",strKey,strSession];
    responseData = [[NSMutableData alloc] init];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,ADS_LINK];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"loading The Key: %@ and para = %@", fullURL,strParamPlatform);
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncCatalogTrackWith:(int)cataId{
    rType = REQUEST_TYPE_CATALOG_TRACK;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"key=%@&session_id=%@&catalog_id=%d",strKey,strSession,cataId];
    responseData = [[NSMutableData alloc] init];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,CATALOG_TRACK_LINK];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"loading The Key: %@ and para = %@", fullURL,strParamPlatform);
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncCatalogTrackWith:(int)cataId andResponseType:(NSString *)strType{
    rType = REQUEST_TYPE_CATALOG_TRACK;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"key=%@&session_id=%@&catalog_id=%d&catalog_response_type=%@",strKey,strSession,cataId,strType];
    responseData = [[NSMutableData alloc] init];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,CATALOG_TRACK_LINK];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"loading The Key: %@ and para = %@", fullURL,strParamPlatform);
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncQuizz{
    rType = REQUEST_TYPE_QUIZZ;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"key=%@&session_id=%@",strKey,strSession];
    responseData = [[NSMutableData alloc] init];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,QUIZZ_LINK];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"loading The Key: %@ and para = %@", fullURL,strParamPlatform);
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncQuizzQuestionWith:(int)quizId{
    rType = REQUEST_TYPE_QUIZZ_QUESTION;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"key=%@&session_id=%@&quiz_id=%d",strKey,strSession,quizId];
    responseData = [[NSMutableData alloc] init];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,QUIZZ_QUESTION_LINK];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"loading The Key: %@ and para = %@", fullURL,strParamPlatform);
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncSubmitQuestion:(UIImage *)img andSubjectId:(int)subj_id andCategoryId:(int)cate_id andSubjectName:(NSString *)subName andCategoryName: (NSString *) categoryName {
    rType = REQUeST_TYPE_SUBMIT_Q;
    NSLog(@"img data %@",img);
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    responseData = [[NSMutableData alloc] init];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,SUBMIT_QUESTION_LINK];
    NSURL *url = [NSURL URLWithString: fullURL];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    // NSString *msgLength = [NSString stringWithFormat:@"%d", [soapMessage length]];
    /*NSString *boundary = [NSString stringWithString:@"---------------------------14737809831466499882746641449"];
	NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
	[request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    NSData *imageData = UIImageJPEGRepresentation(img,90);
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithString:@"Content-Disposition: attachment; name=\"userfile\"; filename=\"iPhoneUpload.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithString:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];
    [body appendData:[[NSString stringWithString:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];*/
    UIImage *newImage = [self imageWithImage:img scaledToSize:CGSizeMake(640, 640)];
    NSData *imageData = UIImageJPEGRepresentation(newImage,0.4);
    NSString *base64image = [self base64forData:imageData];

    NSString *questionTitle = [NSString stringWithFormat:@"%@ - %@", subName, categoryName];
    NSDate* date = [NSDate date];
        
    NSDateFormatter* formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateFormat:@"dd MMM yyyy HH:mm"];
    NSString* curDate = [formatter stringFromDate:date];
    NSString * strCurrDate = [NSString stringWithFormat:@"Submitted by %@",curDate];
    //[formatter release];
    
    NSString * strParamPlatform = [NSString stringWithFormat:@"key=%@&session_id=%@&subject_id=%d&topic_id=%d&question_title=%@&question_file=%@&question_text=%@",strKey,strSession,subj_id,cate_id,questionTitle, base64image, strCurrDate];
    NSLog(@"%@", strParamPlatform);
	// setting the body of the post to the reqeust
    [request setHTTPMethod:@"POST"];
	[request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];

	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();    
    UIGraphicsEndImageContext();
    return newImage;
}

- (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] autorelease];
}

- (void) syncGetUserQA{
    rType = REQUEST_TYPE_GET_QUESTION;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"key=%@&session_id=%@",strKey,strSession];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,USER_QUESTION_DETAIL_LINK];
    
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"loading The Key: %@ and para = %@", fullURL,strParamPlatform);
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncNewPasswordWithUser:(ObjUser *)objUser{
    rType = REQUEST_TYPE_PASSWORD_SUBMIT;
    NSLog(@"user :%@ and pass : %@ in soqr equest",objUser.strName,objUser.strPassword);
    NSString * encodedPrePass = [self encodePasswordWithSHA1:objUser.strPassword];
    NSString * encodedNewPass = [self encodePasswordWithSHA1:objUser.strNewPassword];
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"key=%@&session_id=%@&prev_password=%@&new_password=%@",strKey,strSession,encodedPrePass,encodedNewPass];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,CHANGE_PASSWORD_LINK];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    NSLog(@"loading The Key: %@ and para = %@", fullURL,strParamPlatform);
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncSubmitObjectiveScore:(NSString *) score withProgressReportId:(NSString *) progressReportId {
    rType = REQUEST_TYPE_RATE_SUBMIT;
    
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"key=%@&session_id=%@&progress_report_id=%@&progress_report_objective=%@",strKey,strSession,progressReportId,score];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,UPDATE_OBJECTIVE_SCORE_LINK];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    NSLog(@"loading The Key: %@ and para = %@", fullURL,strParamPlatform);
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncRateWithAnswer:(ObjAnswer *)objAns{
    rType = REQUEST_TYPE_RATE_SUBMIT;

    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"key=%@&session_id=%@&question_id=%d&question_rating=%d",strKey,strSession,objAns.question_id,objAns.rate_point];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,RATING_LINK];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    NSLog(@"loading The Key: %@ and para = %@", fullURL,strParamPlatform);
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncRateWithQuiz:(ObjQuizz *)objQuizz{
    rType = REQUEST_TYPE_QUIZZ_RATE;
    
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"key=%@&session_id=%@&quiz_question_id=%d&quiz_question_rating=%d",strKey,strSession,objQuizz.idx,objQuizz.rate_point];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,RATING_LINK];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    NSLog(@"loading The Key: %@ and para = %@", fullURL,strParamPlatform);
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[responseData setLength:0];
    intResponseStatusCode = 0;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	/*UIAlertView * alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message: [NSString stringWithFormat:@"Error: %@", [error localizedDescription]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
     [alert show];
     [alert release];*/
	/*NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	[connection release];
	responseData = nil;
	[responseData release];
    NSLog(@"error  %@",error);
    [owner onErrorLoad: processId];
    
    NSLog(@"responseString: %@", responseString);*/
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	[connection release];
	//zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
	NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"response %@",responseString);
	responseData = nil;
	[responseData release];
    
   if( rType == REQUEST_TYPE_GENR_KEY ){
        NSLog(@"responseString: %@", responseString);
        NSLog(@"status code %d",intResponseStatusCode);
        NSError *error = nil;
        
        NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        if (soapData == nil)
        {
            NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
            abort();
        }
        
        NSDictionary *dictionary;
        JSONDecoder* decoder = [[JSONDecoder alloc]
                                initWithParseOptions:JKParseOptionNone];
        dictionary = [decoder objectWithData:soapData];
       [decoder release];
        NSLog(@"Feed dictionary count %d",[dictionary count]);
        [owner onJsonLoaded:dictionary withProcessId:processId];
        
    }
   else if( rType == REQUEST_TYPE_CHECK_SESSION ){
        NSLog(@"responseString: %@", responseString);
        NSLog(@"status code %d",intResponseStatusCode);
        NSError *error = nil;
        
        NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        if (soapData == nil)
        {
            NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
            abort();
        }
        
        NSDictionary *dictionary;
        JSONDecoder* decoder = [[JSONDecoder alloc]
                                initWithParseOptions:JKParseOptionNone];
        dictionary = [decoder objectWithData:soapData];
        [decoder release];
        NSLog(@"Feed dictionary count %d",[dictionary count]);
        [owner onJsonLoaded:dictionary withProcessId:processId];
        
    }
   else if( rType == REQUEST_TYPE_LOGIN ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
       
   }
   else if( rType == REQUEST_TYPE_GET_POINT ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
       
   }
   else if( rType == REQUEST_TYPE_SUBJECT_CRITEA ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
       
   }
   else if( rType == REQUeST_TYPE_SUBMIT_Q ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
       
   }
   else if( rType == REQUEST_TYPE_GET_QUESTION ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
       
   }
   else if( rType == REQUEST_TYPE_PASSWORD_SUBMIT ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_RATE_SUBMIT ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_PROGRESS_REPORT ){
       NSLog(@"responseString progress report: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_CATALOG ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_CATALOG_TRACK ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_QUIZZ ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_QUIZZ_QUESTION ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_ADS ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_FREEZE ){
       NSLog(@"REQUEST_TYPE_FREEZE responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUeST_TYPE_CHECK_SUPERVISOR ){
       NSLog(@"REQUEST_TYPE_FREEZE responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_UNFREEZE ){
       NSLog(@"REQUEST_TYPE_FREEZE responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_CATALOG_REGISTER ){
       NSLog(@"REQUEST_TYPE_FREEZE responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_RESET_PASS ){
       NSLog(@"REQUEST_TYPE_FREEZE responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_QUIZZ_CREAT_CONSULT ){
       NSLog(@"REQUEST_TYPE_FREEZE responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_QUIZZ_RATE ){
       NSLog(@"REQUEST_TYPE_FREEZE responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;
       
       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }
       
       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       [decoder release];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
}

- (void) readResults:(NSMutableDictionary*)dics processId:(NSString *) pid{
	//int proId = [pid intValue];
}

- (void) failedParsing:(NSError*)error processId:(NSString *) pid{
	//[owner onErrorLoad: self.processId];
}

- (void)dealloc {
    [responseData release];
    [super dealloc];
}

@end
