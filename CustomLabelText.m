//
//  CustomLabel.m
//  CustomView
//
//  Created by Zayar Cn on 6/25/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "CustomLabelText.h"

@implementation CustomLabelText

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)drawTextInRect:(CGRect)rect {
    
    //CGSize shadowOffset = self.shadowOffset;
    UIColor *textColor = self.textColor;
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(c,1.5);
    CGContextSetLineJoin(c, kCGLineJoinRound);
    
    CGContextSetTextDrawingMode(c, kCGTextStroke);
    self.textColor = [UIColor colorWithRed:34.0f/255.0f green:61.0f/255.0f blue:88.0f/255.0f alpha:1];
    [super drawTextInRect:rect];
    
    CGContextSetTextDrawingMode(c, kCGTextFill);
    self.textColor = textColor;
    //self.shadowOffset = CGSizeMake(0, 0);
    [super drawTextInRect:rect];
    //self.shadowOffset = shadowOffset;
}

- (void)drawContent{
   //UIFont * cfont = [UIFont fontWithName:@"BradyBunchRemastered" size:20.0];
    UIFont * sfont = [UIFont fontWithName:@"Helvetica" size:16.0];
    //cell.lblTitle.font = cfont;
    //cell.lblText.font = sfont;
    CGPoint point = CGPointMake(1,1);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 0.7);
    CGContextSetRGBStrokeColor(context, 2, 2, 2, 1.0);
    CGContextSetTextDrawingMode(context, kCGTextFillStroke);
    CGContextSaveGState(context);
    
    // I change it to my outlet
    [self.text drawAtPoint:point withFont:sfont];
    
    CGContextRestoreGState(context);
}

@end
