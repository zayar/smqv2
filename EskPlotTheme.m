//
//  EskPlotTheme.m
//  CorePlotSample
//
//  Created by Ken Wong on 8/7/11.
//  Copyright 2011 Essence Work LLC. All rights reserved.
//

#import "EskPlotTheme.h"

@implementation EskPlotTheme

+(NSString *)defaultName 
{
	return @"EskBlogSampleTheme";
}

-(void)applyThemeToBackground:(CPTXYGraph *)graph
{
    graph.fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:0.21f green:0.28f blue:0.45f  alpha:1.0f]];
}

/*-(void)applyThemeToAxisSet:(CPTXYAxisSet *)axisSet
{
    CPTMutableTextStyle *titleText = [CPTMutableTextStyle textStyle];
    titleText.color = [CPTColor blackColor];
    titleText.fontSize = 18;
    titleText.fontName = @"Ingleby-BoldItalic";
    
    CPTMutableLineStyle *majorLineStyle = [CPTMutableLineStyle lineStyle];
    majorLineStyle.lineCap = kCGLineCapRound;
    majorLineStyle.lineColor = [CPTColor blackColor];
    majorLineStyle.lineWidth = 3.0;
    
    CPTMutableLineStyle *minorLineStyle = [CPTMutableLineStyle lineStyle];
    minorLineStyle.lineColor = [CPTColor blackColor];
    minorLineStyle.lineWidth = 3.0;
    
    CPTMutableLineStyle *majorTickLineStyle = [CPTMutableLineStyle lineStyle];
    majorTickLineStyle.lineWidth = 3.0f;
    majorTickLineStyle.lineColor = [CPTColor blackColor];
    
    CPTMutableLineStyle *minorTickLineStyle = [CPTMutableLineStyle lineStyle];
    minorTickLineStyle.lineWidth = 3.0f;
    minorTickLineStyle.lineColor = [CPTColor blackColor];
    
    // Create grid line styles
    CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
    majorGridLineStyle.lineWidth = 1.0f;
    majorGridLineStyle.lineColor = [[CPTColor blackColor] colorWithAlphaComponent:0.25];
    
    CPTMutableLineStyle *minorGridLineStyle = [CPTMutableLineStyle lineStyle];
    minorGridLineStyle.lineWidth = 1.0f;
    minorGridLineStyle.lineColor = [[CPTColor blackColor] colorWithAlphaComponent:0.15];    
    //minorGridLineStyle.dashPattern = [NSArray arrayWithObjects:[NSNumber numberWithFloat:5.0f], [NSNumber numberWithFloat:5.0f], nil];
    
	
    CPTMutableTextStyle *xLabelTextStyle = [CPTMutableTextStyle textStyle];
    xLabelTextStyle.color = [CPTColor blackColor];
    xLabelTextStyle.fontSize = 16;
    xLabelTextStyle.fontName = @"Ingleby";
    
    CPTMutableTextStyle *yLabelTextStyle = [CPTMutableTextStyle textStyle];
    yLabelTextStyle.color = [CPTColor blackColor];
    yLabelTextStyle.fontSize = 16;
    yLabelTextStyle.fontName = @"Ingleby";
    
    CPTXYAxis *x = axisSet.xAxis;
	CPTMutableTextStyle *whiteTextStyle = [[[CPTMutableTextStyle alloc] init] autorelease];
	whiteTextStyle.color = [CPTColor blackColor];
	whiteTextStyle.fontSize = 14.0;
	CPTMutableTextStyle *minorTickWhiteTextStyle = [[[CPTMutableTextStyle alloc] init] autorelease];
	minorTickWhiteTextStyle.color = [CPTColor blackColor];
	minorTickWhiteTextStyle.fontSize = 12.0;
	
    x.labelingPolicy = CPTAxisLabelingPolicyFixedInterval;
    x.majorIntervalLength = CPTDecimalFromDouble(0.5);
    x.orthogonalCoordinateDecimal = CPTDecimalFromDouble(0.0);
	x.tickDirection = CPTSignNone;
    x.minorTicksPerInterval = 4;
    x.majorTickLineStyle = majorTickLineStyle;
    x.minorTickLineStyle = minorTickLineStyle;
    x.axisLineStyle = majorLineStyle;
    x.majorTickLength = 7.0;
    x.minorTickLength = 5.0;
	x.labelTextStyle = xLabelTextStyle; 
	x.minorTickLabelTextStyle = whiteTextStyle; 
	x.titleTextStyle = titleText;
	
    CPTXYAxis *y = axisSet.yAxis;
    y.labelingPolicy = CPTAxisLabelingPolicyFixedInterval;
    y.majorIntervalLength = CPTDecimalFromDouble(0.5);
    y.minorTicksPerInterval = 4;
    y.orthogonalCoordinateDecimal = CPTDecimalFromDouble(0.0);
	y.tickDirection = CPTSignNone;
    y.majorTickLineStyle = majorTickLineStyle;
    y.minorTickLineStyle = minorTickLineStyle;
    y.axisLineStyle = majorLineStyle;
    y.majorTickLength = 15.0;
    y.minorTickLength = 5.0;
    y.majorGridLineStyle = majorGridLineStyle;
    y.minorGridLineStyle = minorGridLineStyle;
	y.labelTextStyle = yLabelTextStyle;
	y.minorTickLabelTextStyle = minorTickWhiteTextStyle; 
	y.titleTextStyle = titleText;
    y.titleOffset = 58.0f;
    
    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFormatter setGroupingSeparator:@","];
    [currencyFormatter setGroupingSize:3];
    [currencyFormatter setMaximumFractionDigits:0];

    y.labelFormatter = currencyFormatter;
    
    [currencyFormatter release];
}*/
-(void)applyThemeToAxisSet:(CPTXYAxisSet *)axisSet
{
    CPTMutableLineStyle *majorLineStyle = [CPTMutableLineStyle lineStyle];
    majorLineStyle.lineCap = kCGLineCapRound;
    majorLineStyle.lineColor = [CPTColor whiteColor];
    majorLineStyle.lineWidth = 3.0;
    
    CPTMutableLineStyle *minorLineStyle = [CPTMutableLineStyle lineStyle];
    minorLineStyle.lineColor = [CPTColor whiteColor];
    minorLineStyle.lineWidth = 3.0;
	
    CPTXYAxis *x = axisSet.xAxis;
	CPTMutableTextStyle *whiteTextStyle = [[[CPTMutableTextStyle alloc] init] autorelease];
	whiteTextStyle.color = [CPTColor whiteColor];
	whiteTextStyle.fontSize = 14.0;
	CPTMutableTextStyle *minorTickWhiteTextStyle = [[[CPTMutableTextStyle alloc] init] autorelease];
	minorTickWhiteTextStyle.color = [CPTColor whiteColor];
	minorTickWhiteTextStyle.fontSize = 12.0;
    x.labelingPolicy = CPTAxisLabelingPolicyFixedInterval;
    x.majorIntervalLength = CPTDecimalFromDouble(0.5);
    x.orthogonalCoordinateDecimal = CPTDecimalFromDouble(0.0);
	x.tickDirection = CPTSignNone;
    x.minorTicksPerInterval = 4;
    x.majorTickLineStyle = majorLineStyle;
    x.minorTickLineStyle = minorLineStyle;
    x.axisLineStyle = majorLineStyle;
    x.majorTickLength = 7.0;
    x.minorTickLength = 5.0;
	x.labelTextStyle = whiteTextStyle;
	x.minorTickLabelTextStyle = minorTickWhiteTextStyle;
	x.titleTextStyle = whiteTextStyle;
	
    CPTXYAxis *y = axisSet.yAxis;
    y.labelingPolicy = CPTAxisLabelingPolicyFixedInterval;
    y.majorIntervalLength = CPTDecimalFromDouble(0.5);
    y.minorTicksPerInterval = 4;
    y.orthogonalCoordinateDecimal = CPTDecimalFromDouble(0.0);
	y.tickDirection = CPTSignNone;
    y.majorTickLineStyle = majorLineStyle;
    y.minorTickLineStyle = minorLineStyle;
    y.axisLineStyle = majorLineStyle;
    y.majorTickLength = 7.0;
    y.minorTickLength = 5.0;
	y.labelTextStyle = whiteTextStyle;
	y.minorTickLabelTextStyle = minorTickWhiteTextStyle;
	y.titleTextStyle = whiteTextStyle;
}

-(void)applyThemeToPlotArea:(CPTPlotAreaFrame *)plotAreaFrame
{
    CPTGradient *stocksBackgroundGradient = [[[CPTGradient alloc] init] autorelease];
    stocksBackgroundGradient = [stocksBackgroundGradient addColorStop:[CPTColor colorWithComponentRed:0.21569 green:0.28627 blue:0.44706 alpha:1.0] atPosition:0.0];
	stocksBackgroundGradient = [stocksBackgroundGradient addColorStop:[CPTColor colorWithComponentRed:0.09412 green:0.17255 blue:0.36078 alpha:1.0] atPosition:0.5];
	stocksBackgroundGradient = [stocksBackgroundGradient addColorStop:[CPTColor colorWithComponentRed:0.05882 green:0.13333 blue:0.33333 alpha:1.0] atPosition:0.5];
	stocksBackgroundGradient = [stocksBackgroundGradient addColorStop:[CPTColor colorWithComponentRed:0.05882 green:0.13333 blue:0.33333 alpha:1.0] atPosition:1.0];
    stocksBackgroundGradient.angle = 270.0;
	plotAreaFrame.fill = [CPTFill fillWithGradient:stocksBackgroundGradient];
    
	CPTMutableLineStyle *borderLineStyle = [CPTMutableLineStyle lineStyle];
	borderLineStyle.lineColor = [CPTColor colorWithGenericGray:0.2];
	borderLineStyle.lineWidth = 0.0;
	
	plotAreaFrame.borderLineStyle = borderLineStyle;
	plotAreaFrame.cornerRadius = 0.0;
}

@end
