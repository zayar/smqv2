//
//  TestView.h
//  zBox
//
//  Created by Zayar on 5/29/13.
//
//

#import <UIKit/UIKit.h>
@protocol TestViewDelegate <NSObject>
- (void)myTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)myTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)myTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;

@end

@interface TestView : UIView
@property (nonatomic, assign) id <TestViewDelegate>delegate;
@end
