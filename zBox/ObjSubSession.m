//
//  ObjSubSession.m
//  zBox
//
//  Created by Zayar on 3/17/13.
//
//

#import "ObjSubSession.h"

@implementation ObjSubSession
@synthesize idx,strName,strDueDate,estimatePotential;
@synthesize progressId,classSessionId,strTopicName,strClassFocus,strTopicDesc,strTodoDesc,strNextLessonDesc,strMilestoneDesc,strAdminDesc,strProgressActualScores,strProgressCarelessScores,strProgressLesson,strProgressAttitude,strProgressForgetFulness,strProgressSpeed,strProgressNextMilestone,paper_1_due_date,paper_2_due_date,paper_3_due_date,estimate_potential,arrLesson,arrOthersSession,arrClassFocus,objDashboard,objLesson,strReportDate,strProgressCouldveScores,arrSessionDetail,reportDate;

- (id) init{
	if( self = [super init] ){
		//self.arrCategories = [[NSMutableArray alloc] init];
        //self.arrLesson = [[NSMutableArray alloc]init];
        //self.arrOthersSession = [[NSMutableArray alloc]init];
        //self.arrClassFocus = [[NSMutableArray alloc]init];
        //self.arrExam = [[NSMutableArray alloc]init];
        //self.arrPacing = [[NSMutableArray alloc]init];
        self.objDashboard = [[ObjDashboard alloc]init];
        self.objLesson = [[ObjLesson alloc]init];
	}
	return self;
}

- (void)dealloc{
    [reportDate release];
    [arrSessionDetail release];
    [strProgressCouldveScores release];
    [strReportDate release];
    [objLesson release];
    [objDashboard release];
    [arrClassFocus release];
    [arrOthersSession release];
    [arrLesson release];
    [strTopicName release];
    [strClassFocus release];
    [strTopicDesc release];
    [strTodoDesc release];
    [strNextLessonDesc release];
    [strMilestoneDesc release];
    [strAdminDesc release];
    [strProgressActualScores release];
    [strProgressCarelessScores release];
    [strProgressLesson release];
    [strProgressAttitude release];
    [strProgressForgetFulness release];
    [strProgressSpeed release];
    [strProgressNextMilestone release];
    [strName release];
    [strDueDate release];
    [super dealloc];
}

@end
