//
//  ObjSessionOther.m
//  zBox
//
//  Created by Zayar on 5/12/13.
//
//

#import "ObjSessionOther.h"

@implementation ObjSessionOther
@synthesize strOtherName,strOtherDes,strNextLesson,strNextMilestone,strNextTopic,strAdmin;

- (void)dealloc{
    [strOtherDes release];
    [strNextLesson release];
    [strNextMilestone release];
    [strNextTopic release];
    [strAdmin release];
    [strOtherName release];
    [strOtherDes release];
    [super dealloc];
}
@end
