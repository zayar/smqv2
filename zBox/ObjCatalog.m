//
//  ObjCatalog.m
//  zBox
//
//  Created by Zayar on 5/8/13.
//
//

#import "ObjCatalog.h"
#import "zBoxAppDelegate.h"

/* 
 {
 "status":"2",
 "subjects":[
 {
 "subject_id":"3",
 "subject_name":"Math",
 "sessions":[
 {
 "progress_id":"1",
 "class_session_id":"5",
 "topic_name":"Trigonometry",
 "class_session_id":"5",
 "class_focus ":"Learn about sine and cosine",
 "topic_desc":"To get students to familiarize themselves with trigonometry",
 "todo_desc":"To complete exercise 5 before next class",
 "next_lesson_desc":"Learning calculus",
 "next_milestone_desc":"Complete the calculus past year paper",
 "admin_desc":"Remarks comment",
 "progress_actual_score":â€70",
 "progress_careless_score":"20",
 "progress_lesson":"G",
 "progress_attitude":"G",
 "progress_forgetfulness":"M",
 "progress_speed":"A",
 "progress_next_milestone":"M"
 }],
 "exams":[
 {
 "exam_id":"1",
 "exam_name":"Mid Term Paper",
 "exam_date":"2013-04-19"
 }].
 "pacings":[
 "topic_id":"2",
 "topic_name":"Calculus",
 "pacing_value":"M"
 }]
 }]
 }

 */

@implementation ObjCatalog
@synthesize idx,strCompanyName,strCataTitle,strCataMediaType,strCataMedia,strCataMediaLink,thumbnailCallBacks,detailCallBacks,owner,strReponseType,strConsentValue,strSPass;

- (id) init{
	if( self = [super init] ){
		self.thumbnailCallBacks = [[NSMutableArray alloc] init];
        self.detailCallBacks = [[NSMutableArray alloc]init];
	}
	
	return self;
}

- (void) downloadImage:(int) processId{
    if (processId == 0) {
        if( thumbnailDownloader != nil ){
            [thumbnailDownloader abortDownload];
            [thumbnailDownloader release];
        }
        
        if( strCataMedia != nil && (NSNull *) strCataMedia != [NSNull null] ){
            thumbnailDownloader = [[InternetImage alloc] initWithUrl: strCataMedia];
            thumbnailDownloader.processId = 0;
            NSLog(@"Ads download link: %@", strCataMedia);
        }
        [thumbnailDownloader downloadImage: self];
        thumbnailDownloader.processId = processId;
    }
    if (processId == 1) {
        if( detailDownloader != nil ){
            [detailDownloader abortDownload];
            [detailDownloader release];
        }
        
        if( strCataMediaLink != nil && (NSNull *) strCataMediaLink != [NSNull null] ){
            detailDownloader = [[InternetImage alloc] initWithUrl: strCataMediaLink];
            detailDownloader.processId = 1;
            NSLog(@"Ads Detail download link: %@", strCataMediaLink);
        }
        [detailDownloader downloadImage: self];
        detailDownloader.processId = processId;
    }
    obj_proccessID = processId;
}

- (void)cancelAllDownloads{
	if( thumbnailDownloader != nil ){
		[thumbnailDownloader abortDownload];
	}
}

-(void) internetImageReady:(InternetImage*)downloadedImage{
    NSLog(@"download id %d",downloadedImage.processId);
    if( downloadedImage.Image != nil ){
        zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *tempPath = [paths objectAtIndex:0];
        
        if( downloadedImage.processId == 0 ){
            NSString * imgPath = [tempPath stringByAppendingPathComponent: [NSString stringWithFormat: @"%.0f_%d_news_ads.png", [NSDate timeIntervalSinceReferenceDate] * 1000.0]];
            strThumbnailPath = imgPath;
            [strThumbnailPath retain];
            
            //UIImage * imgDetail = [Utility imageByScalingProportionallyToSize: downloadedImage.Image newsize:CGSizeMake(78,63) resizeFrame:FALSE];
            //UIImage * imgDetail = [Utility forceImageResize:downloadedImage.Image newsize:CGSizeMake(320,60)];
            
            NSData *imageDetailData = [NSData dataWithData:UIImagePNGRepresentation(downloadedImage.Image)];
            [imageDetailData writeToFile:imgPath atomically:YES];
            
            NSLog(@"img thumb Path %@",imgPath);
            
            [delegate.db insertCachedImage:strCataMedia  path:imgPath];
            
            if( self.thumbnailCallBacks != nil ){
                NSLog(@"Ads thumbnail call backs count %d",[self.thumbnailCallBacks count]);
                for(int i=0; i<[self.thumbnailCallBacks count]; i++){
                    NSLog(@"Ads image create........");
                    UIImageView * imgView = [self.thumbnailCallBacks objectAtIndex: i];
                    
                    if( imgView != nil && (NSNull *) imgView != [NSNull null] ){
                        if( imgView.image != nil && (NSNull *) imgView.image != [NSNull null] ){
                            [imgView.image release];
                            imgView.image = nil;
                        }
                        imgView.hidden = FALSE;
                        [imgView setImage: [[UIImage alloc] initWithContentsOfFile: imgPath]];
                    }
                }
            }
            [thumbnailDownloader release];
            thumbnailDownloader = nil;
            //[owner onImageDownloadFinish:obj_CellIndex imageType:itemType];
            [owner onImageDownloadFinish:obj_proccessID imageType:itemType];
        }
        if( downloadedImage.processId == 1 ){
            NSString * imgPath = [tempPath stringByAppendingPathComponent: [NSString stringWithFormat: @"%.0f_%d_detail.png", [NSDate timeIntervalSinceReferenceDate] * 1000.0]];
            strDetailPath = imgPath;
            [strDetailPath retain];
            
            //UIImage * imgDetail = [Utility imageByScalingProportionallyToSize: downloadedImage.Image newsize:CGSizeMake(78,63) resizeFrame:FALSE];
            //UIImage * imgDetail = [Utility forceImageResize:downloadedImage.Image newsize:CGSizeMake(320,60)];
            
            NSData *imageDetailData = [NSData dataWithData:UIImagePNGRepresentation(downloadedImage.Image)];
            [imageDetailData writeToFile:imgPath atomically:YES];
            
            NSLog(@"img detail Path %@",imgPath);
            
            [delegate.db insertCachedImage:strCataMediaLink  path:imgPath];
            
            if( self.detailCallBacks != nil ){
                NSLog(@"Ads detail call backs count %d",[self.detailCallBacks count]);
                for(int i=0; i<[self.detailCallBacks count]; i++){
                    NSLog(@"Ads image create........");
                    UIImageView * imgView = [self.detailCallBacks objectAtIndex: i];
                    
                    if( imgView != nil && (NSNull *) imgView != [NSNull null] ){
                        if( imgView.image != nil && (NSNull *) imgView.image != [NSNull null] ){
                            [imgView.image release];
                            imgView.image = nil;
                        }
                        imgView.hidden = FALSE;
                        [imgView setImage: [[UIImage alloc] initWithContentsOfFile: imgPath]];
                    }
                }
            }
            [detailCallBacks release];
            detailCallBacks = nil;
            //[owner onImageDownloadFinish:obj_CellIndex imageType:itemType];
            [owner onImageDownloadFinish:obj_proccessID imageType:itemType];
        }
    }
}

- (void)dealloc{
    [strSPass release];
    [strConsentValue release];
    [strReponseType release];
    [strThumbnailPath release];
	[thumbnailCallBacks release];
	[thumbnailDownloader release];
    [strCompanyName release];
    [strCataTitle release];
    [strCataMediaType release];
    [strCataMedia release];
    [strCataMediaLink release];
    [super dealloc];
}

@end
