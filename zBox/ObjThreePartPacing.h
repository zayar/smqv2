//
//  ObjThreePartPacing.h
//  zBox
//
//  Created by Zayar on 5/25/13.
//
//

#import <Foundation/Foundation.h>

@interface ObjThreePartPacing : NSObject
{
    NSString * strAlert;
    NSString * strMediocre;
    NSString * strGood;
    NSString * strEstimatedScore;
    NSString * strObjective;
}
@property (nonatomic,retain)NSString * strAlert;
@property (nonatomic,retain)NSString * strMediocre;
@property (nonatomic,retain)NSString * strGood;
@property (nonatomic,retain)NSString * strEstimatedScore;
@property (nonatomic,retain)NSString * strObjective;
@end
