//
//  ObjCategory.h
//  zBox
//
//  Created by Zayar Cn on 6/13/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjCategory : NSObject
{
    int idx;
    NSString * strName;
}
@property int idx;
@property (nonatomic, retain)NSString * strName;
@end
