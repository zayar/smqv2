//
//  ObjExam.m
//  zBox
//
//  Created by Zayar on 5/6/13.
//
//

#import "ObjExam.h"

@implementation ObjExam
@synthesize strName,strDate;

- (void)dealloc{
    [strName release];
    [strDate release];
    [super dealloc];
}

@end
