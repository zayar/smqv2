//
//  ObjSubject.m
//  zBox
//
//  Created by Zayar Cn on 6/13/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "ObjSubject.h"

@implementation ObjSubject
@synthesize subject_id,strName,arrCategories,previousMark,currentMark,arrSession,arrExam,arrPacing,couldveMark,differentMark,examp_p1,examp_p2,examp_p3,objectiveMark,estimated_potential,strTopUpStatus;
- (id) init{
	if( self = [super init] ){
		//self.arrCategories = [[NSMutableArray alloc] init];
        self.arrSession = [[NSMutableArray alloc]init];
        //self.arrExam = [[NSMutableArray alloc]init];
        ///self.arrPacing = [[NSMutableArray alloc]init];
	}
	return self;
}
-(void)dealloc{
    [strTopUpStatus release];
    [arrPacing release];
    [arrExam release];
    [arrSession release];
    [strName release];
    [arrCategories release];
    [super dealloc];
}
@end
