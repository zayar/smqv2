//
//  ObjAds.h
//  zBox
//
//  Created by Zayar on 5/11/13.
//
//

#import <Foundation/Foundation.h>
#import "InternetImage.h"
@protocol ObjAdsDelegate
- (void) onImageDownloadFinish:(int)proccessId imageType:(int) itemType;
- (void) onImageDownloadFinish:(int)proccessId imageType:(int) itemType tblCell:(NSIndexPath *)rowCellIndex;
@end
@interface ObjAds : NSObject
{
    int idx;
    NSString * strName;
    NSString * strLink;

    float adv_duration;
    
    
    NSString * strThumbnailPath;
	NSMutableArray * thumbnailCallBacks;
	InternetImage * thumbnailDownloader;
    
    NSString * strDetailPath;
	NSMutableArray * detailCallBacks;
	InternetImage * detailDownloader;
    
    int obj_proccessID;
    id <ObjAdsDelegate> owner;
    int itemType;
    
    NSMutableArray * arrCatalog;
}
@property int idx;
@property float adv_duration;
@property (nonatomic, retain) NSString * strName;
@property (nonatomic, retain) NSString * strLink;

@property (nonatomic, retain) NSMutableArray * thumbnailCallBacks;
@property (nonatomic, retain) NSMutableArray * detailCallBacks;
- (void) downloadImage:(int) processId;
@property id <ObjAdsDelegate> owner;
@property (nonatomic, retain) NSMutableArray * arrCatalog;
@end
