//
//  ObjExam.h
//  zBox
//
//  Created by Zayar on 5/6/13.
//
//

#import <Foundation/Foundation.h>

@interface ObjExam : NSObject
{
    int idx;
    NSString * strName;
    NSString * strDate;
}
@property int idx;
@property (nonatomic, retain) NSString * strName;
@property (nonatomic, retain) NSString * strDate;
@end
