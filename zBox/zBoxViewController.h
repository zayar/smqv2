//
//  zBoxViewController.h
//  zBox
//
//  Created by Winner Computer Group on 1/31/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioServices.h>
#import "SMQView.h"
#import "VSView.h"
#import "CourseView.h"
#import "QuizView.h"
#import "QuizSecListView.h"
#import "SMQMenu.h"
#import "OverlayViewController.h"
#import "PopUpOverlay.h"
#import "CustomPhotoGalleryView.h"
#import "CustomPhotoAlbumView.h"
#import "SettingView.h"
#import "SOAPRequest.h"
#import "FGalleryViewController.h"
#import "ProgressReport.h"
#import "SessionMenuView.h"
#import "QuizQuestionDetailView.h"
#import "FreezeView.h"
#import "GraphViewController.h"
#import "AdsViewController.h"

@interface zBoxViewController : UIViewController<UIImagePickerControllerDelegate,OverlayViewControllerDelegate,SMQMenuDelegate,VSViewDelegate,SettingViewDelegate,SOAPRequestDelegate,SMQViewDelegate,CustomPhotoAlbumViewDelegate,CustomPhotoGalleryDelegate,UIActionSheetDelegate,FGalleryViewControllerDelegate,FreezeViewDelegate,SessionMenuViewDelegate> {
    UIActionSheet *menu;

    UIImagePickerController *imagePicker;
    OverlayViewController *overlayViewController;
    IBOutlet UIImageView * imgBgView;
    SMQView * smqView;
    VSView * vsView;
    CourseView * csView;
    QuizView * qView;
    QuizQuestionDetailView * qqDView;
    SMQMenu * smqMenuView;
    QuizSecListView * qSecView;
    ProgressReport * progRepoView;
    PopUpOverlay * popUpOverlayView;
    CustomPhotoGalleryView * customPhotoGalleryView;
    CustomPhotoAlbumView * customPhotoAlbumView;
    SettingView * settingView;
    FreezeView * freezeView;
    SOAPRequest * userPointRequest;
    
    IBOutlet UIImageView *imgBanner;
    
    int userPoint;
    IBOutlet UILabel * lblPoint;
    NSArray *_photos;
    FGalleryViewController *networkGallery;
    NSArray *networkImages;
    NSArray *networkCaptions;
    GraphViewController * graphViewController;
    SessionMenuView * sessionMenuView;
    AdsViewController * adsViewController;
    
    IBOutlet UIButton * btnSMQ;
    UITextField * txtParentPassword;
    SOAPRequest * unfreezeRequest;
}
@property (nonatomic, retain) NSArray *photos;

- (IBAction)onClick:(id)sender;
- (void)showPreviewWithImage:(UIImage *)img;
- (void)showSMQMenuView;
- (void)showCustomPhotoAlbum;
@property (nonatomic, retain) OverlayViewController *overlayViewController;
- (void) reloadLabelPointWith:(int)point;
- (void)synUserPoint;
- (void) showQuizQuestionDetail:(ObjQuizQuestion *)objQqd;
- (void)disableSMQ;
- (void)showResetView;
- (void) freezeViewSwitchViewToCurrent;
- (void) bringProgRepoViewUp;

- (void) setImage:(NSString *) urlString;

@end
