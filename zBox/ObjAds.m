//
//  ObjAds.m
//  zBox
//
//  Created by Zayar on 5/11/13.
//
//

#import "ObjAds.h"
#import "zBoxAppDelegate.h"

@implementation ObjAds
@synthesize idx,strName,strLink,thumbnailCallBacks,detailCallBacks,owner,arrCatalog,adv_duration;

- (id) init{
	if( self = [super init] ){
		self.thumbnailCallBacks = [[NSMutableArray alloc] init];
        self.detailCallBacks = [[NSMutableArray alloc]init];
	}
	
	return self;
}

- (void) downloadImage:(int) processId{
    if (processId == 0) {
        if( thumbnailDownloader != nil ){
            [thumbnailDownloader abortDownload];
            [thumbnailDownloader release];
        }
        
        if( strLink != nil && (NSNull *) strLink != [NSNull null] ){
            thumbnailDownloader = [[InternetImage alloc] initWithUrl: strLink];
            thumbnailDownloader.processId = 0;
            NSLog(@"Ads download link: %@", strLink);
        }
        [thumbnailDownloader downloadImage: self];
        thumbnailDownloader.processId = processId;
    }
    if (processId == 1) {
        if( detailDownloader != nil ){
            [detailDownloader abortDownload];
            [detailDownloader release];
        }
        
        if( strLink != nil && (NSNull *) strLink != [NSNull null] ){
            detailDownloader = [[InternetImage alloc] initWithUrl: strLink];
            detailDownloader.processId = 1;
            NSLog(@"Ads download link: %@", strLink);
        }
        [detailDownloader downloadImage: self];
        detailDownloader.processId = processId;
    }
    obj_proccessID = processId;
}

- (void)cancelAllDownloads{
	if( thumbnailDownloader != nil ){
		[thumbnailDownloader abortDownload];
	}
}

-(void) internetImageReady:(InternetImage*)downloadedImage{
    NSLog(@"download id %d",downloadedImage.processId);
    if( downloadedImage.Image != nil ){
        zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *tempPath = [paths objectAtIndex:0];
        
        if( downloadedImage.processId == 0 ){
            NSString * imgPath = [tempPath stringByAppendingPathComponent: [NSString stringWithFormat: @"%.0f_%d_news_ads.png", [NSDate timeIntervalSinceReferenceDate] * 1000.0]];
            strThumbnailPath = imgPath;
            [strThumbnailPath retain];
            
            //UIImage * imgDetail = [Utility imageByScalingProportionallyToSize: downloadedImage.Image newsize:CGSizeMake(78,63) resizeFrame:FALSE];
            //UIImage * imgDetail = [Utility forceImageResize:downloadedImage.Image newsize:CGSizeMake(320,60)];
            
            NSData *imageDetailData = [NSData dataWithData:UIImagePNGRepresentation(downloadedImage.Image)];
            [imageDetailData writeToFile:imgPath atomically:YES];
            
            NSLog(@"img thumb Path %@",imgPath);
            
            [delegate.db insertCachedImage:strLink  path:imgPath];
            
            if( self.thumbnailCallBacks != nil ){
                NSLog(@"Ads thumbnail call backs count %d",[self.thumbnailCallBacks count]);
                for(int i=0; i<[self.thumbnailCallBacks count]; i++){
                    NSLog(@"Ads image create........");
                    UIImageView * imgView = [self.thumbnailCallBacks objectAtIndex: i];
                    
                    if( imgView != nil && (NSNull *) imgView != [NSNull null] ){
                        if( imgView.image != nil && (NSNull *) imgView.image != [NSNull null] ){
                            [imgView.image release];
                            imgView.image = nil;
                        }
                        imgView.hidden = FALSE;
                        [imgView setImage: [[UIImage alloc] initWithContentsOfFile: imgPath]];
                    }
                }
            }
            [thumbnailDownloader release];
            thumbnailDownloader = nil;
            //[owner onImageDownloadFinish:obj_CellIndex imageType:itemType];
            [owner onImageDownloadFinish:obj_proccessID imageType:itemType];
        }
        if( downloadedImage.processId == 1 ){
            NSString * imgPath = [tempPath stringByAppendingPathComponent: [NSString stringWithFormat: @"%.0f_%d_detail.png", [NSDate timeIntervalSinceReferenceDate] * 1000.0]];
            strDetailPath = imgPath;
            [strDetailPath retain];
            
            //UIImage * imgDetail = [Utility imageByScalingProportionallyToSize: downloadedImage.Image newsize:CGSizeMake(78,63) resizeFrame:FALSE];
            //UIImage * imgDetail = [Utility forceImageResize:downloadedImage.Image newsize:CGSizeMake(320,60)];
            
            NSData *imageDetailData = [NSData dataWithData:UIImagePNGRepresentation(downloadedImage.Image)];
            [imageDetailData writeToFile:imgPath atomically:YES];
            
            NSLog(@"img thumb Path %@",imgPath);
            
            [delegate.db insertCachedImage:strLink  path:imgPath];
            
            if( self.detailCallBacks != nil ){
                NSLog(@"Ads thumbnail call backs count %d",[self.detailCallBacks count]);
                for(int i=0; i<[self.detailCallBacks count]; i++){
                    NSLog(@"Ads image create........");
                    UIImageView * imgView = [self.detailCallBacks objectAtIndex: i];
                    
                    if( imgView != nil && (NSNull *) imgView != [NSNull null] ){
                        if( imgView.image != nil && (NSNull *) imgView.image != [NSNull null] ){
                            [imgView.image release];
                            imgView.image = nil;
                        }
                        imgView.hidden = FALSE;
                        [imgView setImage: [[UIImage alloc] initWithContentsOfFile: imgPath]];
                    }
                }
            }
            [detailCallBacks release];
            detailCallBacks = nil;
            //[owner onImageDownloadFinish:obj_CellIndex imageType:itemType];
            [owner onImageDownloadFinish:obj_proccessID imageType:itemType];
        }
    }
}

- (void)dealloc{
    [arrCatalog release];
    [strThumbnailPath release];
	[thumbnailCallBacks release];
	[thumbnailDownloader release];
    
    [strDetailPath release];
	[detailCallBacks release];
	[detailDownloader release];
    [strName release];
    [strLink release];
    [super dealloc];
}

@end
