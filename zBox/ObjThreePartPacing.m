//
//  ObjThreePartPacing.m
//  zBox
//
//  Created by Zayar on 5/25/13.
//
//

#import "ObjThreePartPacing.h"

@implementation ObjThreePartPacing
@synthesize strAlert,strMediocre,strGood,strEstimatedScore;
- (void)dealloc{
    [strAlert release];
    [strMediocre release];
    [strGood release];
    [strEstimatedScore release];
    [strObjective release];
    [super dealloc];
}
@end
