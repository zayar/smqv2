//
//  zBoxViewController.m
//  zBox
//
//  Created by Winner Computer Group on 1/31/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "zBoxViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "SMQView.h"
#import "VSView.h"
#import "SMQMenu.h"
#import "PopUpOverlay.h"
#import "OverlayViewController.h"
#import "CustomPhotoGalleryView.h"
#import "CustomPhotoAlbumView.h"
#import "SettingView.h"
#import "StringTable.h"
#import "zBoxAppDelegate.h"
#import "SOAPRequest.h"
#import "CourseView.h"
#import "QuizView.h"
#import "QuizSecListView.h"
#import "ProgressReport.h"
#import "ObjQuizQuestion.h"
#import "QuizQuestionDetailView.h"
#import "PRDashboardViewController.h"
#import "PRLessionViewController.h"
#import "PRToDoViewController.h"
#import "PROthersViewController.h"
#import "PR3PartPacingViewController.h"
#import "ClassFocusViewController.h"
#import "FreezeView.h"
#import "GraphViewController.h"
#import "ObjGKey.h"
#import "ResetView.h"

@implementation zBoxViewController
@synthesize photos = _photos;
@synthesize overlayViewController;

- (void)dealloc
{
    [txtParentPassword release];
    [qqDView release];
    [sessionMenuView release];
    [progRepoView release];
    [qSecView release];
    [qView release];
    [_photos release];
    [userPointRequest release];
    [lblPoint release];
    [customPhotoAlbumView release];
    [settingView release];
    [customPhotoGalleryView release];
    [popUpOverlayView release];
    [smqMenuView release];
    [imgBgView release];
    [vsView release];
    [menu release];
    [imagePicker release];
    [smqView release];
    [adsViewController release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    menu = [[UIActionSheet alloc] initWithTitle:@"Choose"
                                       delegate:self
                             cancelButtonTitle:@"Cancel"
                         destructiveButtonTitle:nil
                              otherButtonTitles:@"Photo Library",@"Take Photo",nil];
    imagePicker = [[UIImagePickerController alloc] init];
    
    self.overlayViewController = [[[OverlayViewController alloc] initWithNibName:@"OverlayViewController" bundle:nil] autorelease];
    
    // as a delegate we will be notified when pictures are taken and when to dismiss the image picker
    self.overlayViewController.delegate = self;
    
    NSString * strPath = [NSString stringWithFormat:@"%@/img_main_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];
    [imgBgView release];
}

- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"main will appear");
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    int point = [prefs integerForKey:CHECK_USER_POINT_LINK];
    [self reloadLabelPointWith:point];
    if (delegate.isFromMain) {
        [self synUserPoint];
        delegate.isFromMain = FALSE;
        NSLog(@"here is sync point");
    }
}

- (void)disableSMQ{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * f_status = [prefs objectForKey:FREEZE_TYPE];
    NSLog(@"is it freeZZZZZ! f_status %@",f_status);
    if ([f_status isEqualToString:@"F"]) {
        NSLog(@"is it freeZZZZZ!");
        btnSMQ.enabled = FALSE;
    }
    else{
        btnSMQ.enabled = TRUE;
    }
}

- (void)viewDidAppear:(BOOL)animated{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];

    if (delegate.isShowAds) {
        [self performSelector:@selector(showAds) withObject:nil afterDelay:0.8];
        NSLog(@"Here ads is showing!");
    }
}

- (void)showAds{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    if (adsViewController == nil) {
        adsViewController = [[AdsViewController alloc]initWithNibName:@"AdsViewController" bundle:nil];
    }
    [self presentModalViewController:adsViewController animated:YES];
    delegate.isShowAds = FALSE;
    NSLog(@"here is show up!!");
}

- (void)hideAds{
    if (adsViewController != nil) {
        [adsViewController dismissModalViewControllerAnimated:NO];
    }
}

- (void)synUserPoint{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate showLoadingScreen];
    userPointRequest = [[SOAPRequest alloc] initWithOwner:self];
    userPointRequest.processId = 4;
    [userPointRequest syncUserPoint];
}

- (void) reloadLabelPointWith:(int)point{
    lblPoint.text = @"";
    lblPoint.textColor = [UIColor colorWithRed:255.0f/255.0f green:253.0f/255.0f blue:224.0f/255.0f alpha:1];
    
    if (point == 1 || point == 0) {
        lblPoint.text = [NSString stringWithFormat:@"%d Point",point];
    } else {
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
        NSString *formatted = [formatter stringFromNumber:[NSNumber numberWithInteger:point]];
        lblPoint.text = [NSString stringWithFormat:@"%@ Points",formatted];
    }
}

- (void) onErrorLoad: (int) processId{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate hideLoadingScreen];
    NSLog(@"Error loaded %d",processId);
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{
    
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    if (processId == 4) {
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        
        NSString * strMessage;
        if (status == STATUS_RETURN_RECORD) {
            int intPoint =[[dics objectForKey:@"points"] intValue];
            NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
            [prefs setInteger:intPoint forKey:CHECK_USER_POINT_LINK];
            [self reloadLabelPointWith:intPoint];
            
            ObjUser * objUser = [delegate.db getUserObj];
        }
        else if(status == STATUS_NO_RECORD_FOUND){
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];	
            [alert release];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
        
        [delegate hideLoadingScreen];
    }
    if (processId == 18) {
        NSLog(@"arr count 18 %d",[dics count]);
        ObjGKey * objKey = [[ObjGKey alloc]init];
        objKey.status = [[dics objectForKey:@"status"] intValue];
        objKey.strMessage = [dics objectForKey:@"message"];
        
        if (objKey.status == 1) {
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:objKey.strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
            NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
            [prefs setObject:@"A" forKey:FREEZE_TYPE];
            [self disableSMQ];
            if (freezeView != nil) {
                [freezeView setTittleBtnFreeze];
            }*/
            [delegate syncAndCheckTheSession];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:objKey.strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
            [self freezeViewSwitchViewToCurrent];
        }
        [objKey release];
        [delegate hideLoadingScreen];
        
        /*for(NSInteger i=0;i<[arrFeedNews count];i++){
         NSMutableDictionary * dicNews = [arrFeedNews objectAtIndex:i];
         // NSMutableDictionary * dicNews2 = [dicNews objectForKey:@"node"];
         NSMutableDictionary * dicNews2 = dicNews;
         ObjectETNews * objNews = [[ObjectETNews alloc]init];
         objNews.nid = [[dicNews2 objectForKey:@"nid"] intValue];
         objNews.strTitle = [dicNews2 objectForKey:@"title"];
         [objNews.strTitle UTF8String];
         objNews.strBody = [dicNews2 objectForKey:@"body"];
         [objNews.strBody UTF8String];
         objNews.strPhotoLink = [dicNews2 objectForKey:@"photo"];
         [objNews.strPhotoLink UTF8String];
         objNews.intTimetick = [[dicNews2 objectForKey:@"updatetime"] intValue];
         NSLog(@"news title %@",objNews.strTitle);
         }*/
    }
}

- (void) textValidateAlertShow:(NSString *) strError{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: APP_TITLE
                          message: strError
                          delegate: nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
    [alert release];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"btn %d from action menu",buttonIndex);
     
    if (buttonIndex == 0) {
       
        
        // Set source to the camera
        @try {
            imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
            
            // Delegate is self
            imagePicker.delegate = self;
            // Allow editing of image ?
            //imagePicker.allowsImageEditing = YES;
            
            // Show image picker
            [self presentModalViewController:imagePicker animated:YES];
            
        }
        @catch (NSException *exception) {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" 
                                                             message:@"Camera Fuction is not suported for simulation and iPad1." 
                                                            delegate:self cancelButtonTitle:@"Ok" 
                                                   otherButtonTitles:nil];
            [alert show];
            [alert release];
            NSLog(@"exception %@",exception);
        }
        @finally {
            
        }
    } 
    else if (buttonIndex == 1) {
        
        // Set source to the camera
        @try {
            /*imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
            
            // Delegate is self
            imagePicker.delegate = self;
            
            // Allow editing of image ?
            imagePicker.allowsImageEditing = YES;
            
            // Show image picker
            [self presentModalViewController:imagePicker animated:YES];*/
            [self.overlayViewController setupImagePicker:UIImagePickerControllerSourceTypeCamera];
            [self presentModalViewController:self.overlayViewController.imagePickerController animated:YES];
        }
        @catch (NSException *exception) {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" 
                                                             message:@"Camera Fuction is not suported for simulation and iPad1." 
                                                            delegate:self cancelButtonTitle:@"Ok" 
                                                   otherButtonTitles:nil];
            [alert show];
            [alert release];
            NSLog(@"exception %@",exception);
        }
        @finally {
            
        }
    }
    else if (buttonIndex == 2) {
        
    }
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
     UIImage * editedImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    NSLog(@"here is edited image");
    [self imagePickerControllerDidCancel:picker];
    [self showPreviewWithImage:editedImage andIsCamera:YES];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    UIAlertView *alert;
    // Unable to save the image  
    if (error){
        
        alert = [[UIAlertView alloc] initWithTitle:@"Error" 
                                           message:@"Unable to save image to Photo Album." 
                                          delegate:self cancelButtonTitle:@"Ok" 
                                 otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissModalViewControllerAnimated:YES];
}

- (void)didTakePicture:(UIImage *)picture
{
    //[self.capturedImages addObject:picture];
    [self showPreviewWithImage:picture andIsCamera:YES];
    NSLog(@"picture size width %f and height %f",picture.size.width,picture.size.height);
}

// as a delegate we are told to finished with the camera
- (void)didFinishWithCamera
{
    [self dismissModalViewControllerAnimated:YES];
    
    /*if ([self.capturedImages count] > 0)
    {
        if ([self.capturedImages count] == 1)
        {
            // we took a single shot
            [self.imageView setImage:[self.capturedImages objectAtIndex:0]];
        }
        else
        {
            // we took multiple shots, use the list of images for animation
            self.imageView.animationImages = self.capturedImages;
            
            if (self.capturedImages.count > 0)
                // we are done with the image list until next time
                [self.capturedImages removeAllObjects];  
            
            self.imageView.animationDuration = 5.0;    // show each captured photo for 5 seconds
            self.imageView.animationRepeatCount = 0;   // animate forever (show all photos)
            self.imageView.startAnimating;
        }
    }*/
}

- (void)showPopOverlay{
    if( popUpOverlayView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PopUpOverlay" owner:nil options:nil];		
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[PopUpOverlay class]]){
				popUpOverlayView = (PopUpOverlay *) currentObject;
				[popUpOverlayView popupOverlayViewDidLoad];
				[self.view addSubview: popUpOverlayView];
				break;
			}
		}
	}
    popUpOverlayView.hidden = FALSE;
}

- (void)hidePopOverlay{
    popUpOverlayView.hidden = TRUE;
}

//**** SMQ ****//
- (void)showSMQMenuView{
    NSLog(@"Showing Menu");
    if( smqMenuView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SMQMenu" owner:nil options:nil];		
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[SMQMenu class]]){
				smqMenuView = (SMQMenu *) currentObject;
				[smqMenuView SMQViewDidLoad];
				[self.view addSubview: smqMenuView];
				break;
			}
		}
	}
    [self showPopOverlay];
    smqMenuView.hidden = FALSE;
    smqMenuView.owner = self;
    smqMenuView.frame = CGRectMake(0, 480, smqMenuView.frame.size.width, smqMenuView.frame.size.height);
    [self.view bringSubviewToFront: smqMenuView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    smqMenuView.frame = CGRectMake(0, 248, smqMenuView.frame.size.width, smqMenuView.frame.size.height);
    [UIView commitAnimations];

}

-(void) onSMQMenuCancel{
    smqMenuView.frame = CGRectMake(0, 264, smqMenuView.frame.size.width, smqMenuView.frame.size.height);
    [self.view bringSubviewToFront: smqMenuView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    smqMenuView.frame = CGRectMake(0, 480, smqMenuView.frame.size.width, smqMenuView.frame.size.height);
    [UIView commitAnimations];
    [self performSelector:(@selector(closeSMQMenuView)) withObject:nil afterDelay:0.4];
    //smqView.hidden = TRUE;
}

-(void) closeSMQMenuView{
    smqMenuView.hidden = TRUE;
    [self hidePopOverlay];
}

-(void)onSMQMenuPhoto{
    @try {
        /*imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
         
         // Delegate is self
         imagePicker.delegate = self;
         
         // Allow editing of image ?
         imagePicker.allowsImageEditing = YES;
         
         // Show image picker
         [self presentModalViewController:imagePicker animated:YES];*/
       /*if (self.overlayViewController.imagePickerController != nil) {
            [self.overlayViewController setupImagePicker:UIImagePickerControllerSourceTypeCamera];
            [self presentModalViewController:self.overlayViewController.imagePickerController animated:YES];
        }
        else {*/
            imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
             
             // Delegate is self
             imagePicker.delegate = self;
             
             // Allow editing of image ?
             imagePicker.allowsImageEditing = NO;
             
             // Show image picker
             [self presentModalViewController:imagePicker animated:YES];
        //}
        
    }
    @catch (NSException *exception) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" 
                                                         message:@"Camera Function is not supported for simulation and iPad1." 
                                                        delegate:self cancelButtonTitle:@"Ok" 
                                               otherButtonTitles:nil];
        [alert show];
        [alert release];
        NSLog(@"exception %@",exception);
    }
    @finally {
        
    }
}

- (void)onSMQMenuLibrary{
    [self showCustomPhotoAlbum];
}

- (void)showCustomPhotoGallery{
    if( customPhotoGalleryView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomPhotoGalleryView" owner:nil options:nil];		
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[CustomPhotoGalleryView class]]){
				customPhotoGalleryView = (CustomPhotoGalleryView *) currentObject;
				[customPhotoGalleryView customPhotoGalleryViewDidLoad];
				[self.view addSubview: customPhotoGalleryView];
				break;
			}
		}
	}
    [customPhotoGalleryView customPhotoGalleryViewWillAppear];
    customPhotoGalleryView.hidden = FALSE;
    customPhotoGalleryView.owner = self;

    customPhotoGalleryView.frame = CGRectMake(-320, 0, customPhotoGalleryView.frame.size.width, customPhotoGalleryView.frame.size.height);
    [self.view bringSubviewToFront: customPhotoGalleryView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    customPhotoGalleryView.frame = CGRectMake(0, 0, customPhotoGalleryView.frame.size.width, customPhotoGalleryView.frame.size.height);
    [UIView commitAnimations];
	
}

- (void)showCustomPhotoGalleryWithAssetGroup:(ALAssetsGroup *)assetGroup{
    if( customPhotoGalleryView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomPhotoGalleryView" owner:nil options:nil];		
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[CustomPhotoGalleryView class]]){
				customPhotoGalleryView = (CustomPhotoGalleryView *) currentObject;
				[customPhotoGalleryView customPhotoGalleryViewDidLoad];
				[self.view addSubview: customPhotoGalleryView];
				break;
			}
		}
	}
    customPhotoGalleryView.assetGroup = assetGroup;
    [customPhotoGalleryView.assetGroup setAssetsFilter:[ALAssetsFilter allPhotos]];
    [customPhotoGalleryView customPhotoGalleryViewWillAppear];
    customPhotoGalleryView.hidden = FALSE;
    customPhotoGalleryView.owner = self;
    NSLog(@"in CALL CUSTOM PHOTO LIb assetCount %d",[assetGroup numberOfAssets]);
    
    
    customPhotoGalleryView.frame = CGRectMake(320, 0, customPhotoGalleryView.frame.size.width, customPhotoGalleryView.frame.size.height);
    [self.view bringSubviewToFront: customPhotoGalleryView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    customPhotoGalleryView.frame = CGRectMake(0, 0, customPhotoGalleryView.frame.size.width, customPhotoGalleryView.frame.size.height);
    [UIView commitAnimations];
	
}

- (void) onCustomPhotoGalleryCancel{
    customPhotoGalleryView.frame = CGRectMake(0, 0, customPhotoGalleryView.frame.size.width, customPhotoGalleryView.frame.size.height);
    [self.view bringSubviewToFront: customPhotoGalleryView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    customPhotoGalleryView.frame = CGRectMake(320, 0, customPhotoGalleryView.frame.size.width, customPhotoGalleryView.frame.size.height);
    [UIView commitAnimations];
    [customPhotoGalleryView customPhotoGalleryViewWillDisAppear];
    [self performSelector:(@selector(closeCustomPhotoGalleryView)) withObject:nil afterDelay:0.4];
}

- (void)viewWillDisappear:(BOOL)animated{
    [self hideAds];
}

- (void)closeCustomPhotoGalleryView{
    customPhotoGalleryView.hidden = TRUE;
}

-(void)onCustomPhotoGalleryChoose:(UIImage *)selectedImage{
    [self showPreviewWithImage:selectedImage andIsCamera:FALSE];
}

- (void)showCustomPhotoAlbum{
    if( customPhotoAlbumView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomPhotoAlbumView" owner:nil options:nil];		
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[CustomPhotoAlbumView class]]){
				customPhotoAlbumView = (CustomPhotoAlbumView *)currentObject;
				[customPhotoAlbumView customPhotoAlbumViewViewDidLoad];
				[self.view addSubview: customPhotoAlbumView];
				break;
			}
		}
	}
    //[customPhotoAlbumView customPhotoGalleryViewWillAppear];
    customPhotoAlbumView.hidden = FALSE;
    customPhotoAlbumView.owner = self;
    
    customPhotoAlbumView.frame = CGRectMake(320, 0, customPhotoAlbumView.frame.size.width, customPhotoAlbumView.frame.size.height);
    [self.view bringSubviewToFront: customPhotoAlbumView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    customPhotoAlbumView.frame = CGRectMake(0, 0, customPhotoAlbumView.frame.size.width, customPhotoAlbumView.frame.size.height);
    [UIView commitAnimations];
}

-(void) onCustomPhotoAlbumViewCancel{
    customPhotoAlbumView.frame = CGRectMake(0, 0, customPhotoAlbumView.frame.size.width, customPhotoAlbumView.frame.size.height);
    [self.view bringSubviewToFront: customPhotoAlbumView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    customPhotoAlbumView.frame = CGRectMake(320, 0, customPhotoAlbumView.frame.size.width, customPhotoAlbumView.frame.size.height);
    [UIView commitAnimations];
    //[customPhotoAlbumView customPhotoGalleryViewWillDisAppear];
    [self performSelector:(@selector(closeCustomPhotoAlbumView)) withObject:nil afterDelay:0.4];
}

- (void)closeCustomPhotoAlbumView{
    customPhotoAlbumView.hidden = TRUE;
}

- (void)onCustomPhotoAlbumViewSelectedWithAssetGroup:(ALAssetsGroup *)assetGroup{
     NSLog(@"in MAIN asset group count %d",[assetGroup numberOfAssets]);
    [self showCustomPhotoGalleryWithAssetGroup:assetGroup];
}

-(void)onSMQViewCancel{
    smqView.frame = CGRectMake(0, 0, smqView.frame.size.width, smqView.frame.size.height);
    [self.view bringSubviewToFront: smqView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    smqView.frame = CGRectMake(320, 0,smqView.frame.size.width, smqView.frame.size.height);
    [UIView commitAnimations];
    [self performSelector:(@selector(closeSMQView)) withObject:nil afterDelay:0.4];
    //smqView.hidden = TRUE;
}

- (void)showPreviewWithImage:(UIImage *)img andIsCamera:(BOOL)isCam{
    if( smqView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SMQView" owner:nil options:nil];		
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[SMQView class]]){
				smqView = (SMQView *) currentObject;
				[smqView smqViewDidLoad];
				[self.view addSubview: smqView];
				break;
			}
		}
	}
    smqView.isCamera = isCam;
    //smqView.selectedImage = img;
    [smqView setImage:img];
    [smqView smqViewWillAppear];
    smqView.hidden = FALSE;
    smqView.owner = self;
    smqView.frame = CGRectMake(320, 0, smqView.frame.size.width, smqView.frame.size.height);
    [self.view bringSubviewToFront: smqView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    [smqView setImage:img];
    
    smqView.frame = CGRectMake(0, 0,smqView.frame.size.width, smqView.frame.size.height);
    [UIView commitAnimations];
	
}

-(void)closeSMQView{
    smqView.hidden = TRUE;
}

-(void) onSMQViewSuccessClose:(BOOL)isCamera{
    NSLog(@"is default camera %d",isCamera);
    [self onSMQViewCancel];
    if (!isCamera) {
        [self onCustomPhotoGalleryCancel];
        [self onCustomPhotoAlbumViewCancel];
    }
    //[self onSMQMenuCancel];
    [self synUserPoint];
}
//**** END SMQ ****//

//**** VS ****//
- (void)showYouTubeView{
    //NSString * strURL = @"http://www.youtube.com/watch?v=Vetg7vWitTU&feature=related";
    //http://youtu.be/lgT1AidzRWM
    if( vsView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"VSView" owner:nil options:nil];		
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[VSView class]]){
				vsView = (VSView *) currentObject;
				[vsView vsViewDidLoad];
				[self.view addSubview: vsView];
				break;
			}
		}
	}
    [vsView vsViewWillAppear];
    vsView.hidden = FALSE;
    vsView.owner = self;
    vsView.frame = CGRectMake(320, 0, vsView.frame.size.width, vsView.frame.size.height);
    [self.view bringSubviewToFront: vsView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    vsView.frame = CGRectMake(0, 0, vsView.frame.size.width, vsView.frame.size.height);
    [UIView commitAnimations];
	
}

-(void) onVSViewCancel{
    vsView.frame = CGRectMake(0, 0, vsView.frame.size.width, vsView.frame.size.height);
    [self.view bringSubviewToFront: vsView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    vsView.frame = CGRectMake(320, 0, vsView.frame.size.width, vsView.frame.size.height);
    [UIView commitAnimations];
    [self performSelector:(@selector(closeVSView)) withObject:nil afterDelay:0.4];
    //smqView.hidden = TRUE;
}

-(void) closeVSView{
    vsView.hidden = TRUE;
}

- (void) showPicPhotoBrowser:(NSString *)picURL andTitle:(NSString *)picTitle {
    NSLog(@"question image url %@",picURL);
    networkCaptions = [[NSArray alloc] initWithObjects:picTitle,nil];
    networkImages = [[NSArray alloc] initWithObjects:picURL,nil];
    networkGallery = [[FGalleryViewController alloc] initWithPhotoSource:self];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:networkGallery];
    nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:nc animated:YES];
    //[self presentModalViewController:networkGallery animated:YES];
    [networkGallery release];
}

//**** COURSE ****//
- (void)showCourseView{
    //NSString * strURL = @"http://www.youtube.com/watch?v=Vetg7vWitTU&feature=related";
    //http://youtu.be/lgT1AidzRWM
    if( csView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CourseView" owner:nil options:nil];
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[CourseView class]]){
				csView = (CourseView *) currentObject;
				[csView vsViewDidLoad];
				[self.view addSubview: csView];
				break;
			}
		}
	}
    [csView vsViewWillAppear];
    csView.hidden = FALSE;
    csView.owner = self;
    csView.frame = CGRectMake(320, 0, csView.frame.size.width, csView.frame.size.height);
    [self.view bringSubviewToFront: csView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    csView.frame = CGRectMake(0, 0, csView.frame.size.width, csView.frame.size.height);
    [UIView commitAnimations];
	
}

-(void) onCourseViewCancel{
    csView.frame = CGRectMake(0, 0, csView.frame.size.width, csView.frame.size.height);
    [self.view bringSubviewToFront: csView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    csView.frame = CGRectMake(320, 0, csView.frame.size.width, csView.frame.size.height);
    [UIView commitAnimations];
    [self performSelector:(@selector(closeCSView)) withObject:nil afterDelay:0.4];
    //smqView.hidden = TRUE;
}

-(void) closeCSView{
    csView.hidden = TRUE;
}

- (void) courseShowPicPhotoBrowser:(NSString *)picURL andTitle:(NSString *)picTitle {
    NSLog(@"question image url %@",picURL);
    networkCaptions = [[NSArray alloc] initWithObjects:picTitle,nil];
    networkImages = [[NSArray alloc] initWithObjects:picURL,nil];
    networkGallery = [[FGalleryViewController alloc] initWithPhotoSource:self];
    networkGallery.isCatelogView = TRUE;
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:networkGallery];
    nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:nc animated:YES];
    //[self presentModalViewController:networkGallery animated:YES];
    [networkGallery release];
}

//**** QUIZ ****//
- (void)showQuizView{
    //NSString * strURL = @"http://www.youtube.com/watch?v=Vetg7vWitTU&feature=related";
    //http://youtu.be/lgT1AidzRWM
    if( qView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"QuizView" owner:nil options:nil];
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[QuizView class]]){
				qView = (QuizView *) currentObject;
				[qView vsViewDidLoad];
				[self.view addSubview: qView];
				break;
			}
		}
	}
    [qView vsViewWillAppear];
    qView.hidden = FALSE;
    qView.owner = self;
    qView.frame = CGRectMake(320, 0, qView.frame.size.width, qView.frame.size.height);
    [self.view bringSubviewToFront: qView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    qView.frame = CGRectMake(0, 0, qView.frame.size.width, qView.frame.size.height);
    [UIView commitAnimations];
	
}

-(void) onQuizViewCancel{
    qView.frame = CGRectMake(0, 0, qView.frame.size.width, qView.frame.size.height);
    [self.view bringSubviewToFront: qView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    qView.frame = CGRectMake(320, 0, qView.frame.size.width, qView.frame.size.height);
    [UIView commitAnimations];
    [self performSelector:(@selector(closeQuizView)) withObject:nil afterDelay:0.4];
    //smqView.hidden = TRUE;
}

-(void) closeQuizView{
    qView.hidden = TRUE;
}

- (void) showQuizPicPhotoBrowser:(NSString *)picURL andTitle:(NSString *)picTitle {
    NSLog(@"question image url %@",picURL);
    networkCaptions = [[NSArray alloc] initWithObjects:picTitle,nil];
    networkImages = [[NSArray alloc] initWithObjects:picURL,nil];
    networkGallery = [[FGalleryViewController alloc] initWithPhotoSource:self];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:networkGallery];
    nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:nc animated:YES];
    //[self presentModalViewController:networkGallery animated:YES];
    [networkGallery release];
}

- (void) showQuizSecondView{
    //NSString * strURL = @"http://www.youtube.com/watch?v=Vetg7vWitTU&feature=related";
    //http://youtu.be/lgT1AidzRWM
    if( qSecView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"QuizSecListView" owner:nil options:nil];
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[QuizSecListView class]]){
				qSecView = (QuizSecListView *) currentObject;
				[qSecView vsViewDidLoad];
				[self.view addSubview: qSecView];
				break;
			}
		}
	}
    [qSecView vsViewWillAppear];
    qSecView.hidden = FALSE;
    qSecView.owner = self;
    qSecView.frame = CGRectMake(320, 0, qSecView.frame.size.width, qSecView.frame.size.height);
    [self.view bringSubviewToFront: qSecView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    qSecView.frame = CGRectMake(0, 0, qSecView.frame.size.width, qSecView.frame.size.height);
    [UIView commitAnimations];
}

- (void) showQuizSecondViewWith:(int)quiz_id{
    if( qSecView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"QuizSecListView" owner:nil options:nil];
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[QuizSecListView class]]){
				qSecView = (QuizSecListView *) currentObject;
				[qSecView vsViewDidLoad];
				[self.view addSubview: qSecView];
				break;
			}
		}
	}
    qSecView.quiz_id = quiz_id;
    [qSecView vsViewWillAppear];
    qSecView.hidden = FALSE;
    qSecView.owner = self;
    qSecView.frame = CGRectMake(320, 0, qSecView.frame.size.width, qSecView.frame.size.height);
    [self.view bringSubviewToFront: qSecView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    qSecView.frame = CGRectMake(0, 0, qSecView.frame.size.width, qSecView.frame.size.height);
    [UIView commitAnimations];
}

- (void) showQuizQuestionDetail:(ObjQuizQuestion *)objQqd{
    if( qqDView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"QuizQuestionDetailView" owner:nil options:nil];
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[QuizQuestionDetailView class]]){
				qqDView = (QuizQuestionDetailView *) currentObject;
				[qqDView qqDViewDidLoad];
				[self.view addSubview: qqDView];
				break;
			}
		}
	}
    
    [qqDView qqDViewWillAppear:objQqd];
    qqDView.hidden = FALSE;
    qqDView.owner = self;
    qqDView.frame = CGRectMake(320, 0, qqDView.frame.size.width, qqDView.frame.size.height);
    [self.view bringSubviewToFront: qqDView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    qqDView.frame = CGRectMake(0, 0, qqDView.frame.size.width, qqDView.frame.size.height);
    [UIView commitAnimations];
}

-(void) onQuizQuestionDetailCancel{
    qqDView.frame = CGRectMake(0, 0, qqDView.frame.size.width, qqDView.frame.size.height);
    [self.view bringSubviewToFront: qqDView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    qqDView.frame = CGRectMake(320, 0, qqDView.frame.size.width, qqDView.frame.size.height);
    [UIView commitAnimations];
    [self performSelector:(@selector(closeQuizQuestionDetailView)) withObject:nil afterDelay:0.4];
    //smqView.hidden = TRUE;
}

-(void) closeQuizQuestionDetailView{
    qqDView.hidden = TRUE;
}

-(void) onQuizSecViewCancel{
    qSecView.frame = CGRectMake(0, 0, qSecView.frame.size.width, qSecView.frame.size.height);
    [self.view bringSubviewToFront: qSecView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    qSecView.frame = CGRectMake(320, 0, qSecView.frame.size.width, qSecView.frame.size.height);
    [UIView commitAnimations];
    [self performSelector:(@selector(closeQuizSecListView)) withObject:nil afterDelay:0.4];
    //smqView.hidden = TRUE;
}

-(void) closeQuizSecListView{
    qSecView.hidden = TRUE;
}

- (void) showQuizSecPicPhotoBrowser:(NSString *)picURL andTitle:(NSString *)picTitle {
    NSLog(@"question image url %@",picURL);
    networkCaptions = [[NSArray alloc] initWithObjects:picTitle,nil];
    networkImages = [[NSArray alloc] initWithObjects:picURL,nil];
    networkGallery = [[FGalleryViewController alloc] initWithPhotoSource:self];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:networkGallery];
    nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:nc animated:YES];
    //[self presentModalViewController:networkGallery animated:YES];
    [networkGallery release];
}

- (void) showQuizQuestionPicPhotoBrowser:(NSString *)picURL andTitle:(NSString *)picTitle {
    NSLog(@"question image url %@",picURL);
    networkCaptions = [[NSArray alloc] initWithObjects:picTitle,nil];
    networkImages = [[NSArray alloc] initWithObjects:picURL,nil];
    networkGallery = [[FGalleryViewController alloc] initWithPhotoSource:self];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:networkGallery];
    nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:nc animated:YES];
    //[self presentModalViewController:networkGallery animated:YES];
    [networkGallery release];
}

//**** Progress Report ****//
- (void)showProgReportView{
    //NSString * strURL = @"http://www.youtube.com/watch?v=Vetg7vWitTU&feature=related";
    //http://youtu.be/lgT1AidzRWM
    if( progRepoView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ProgressReport" owner:nil options:nil];
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[ProgressReport class]]){
				progRepoView = (ProgressReport *) currentObject;
				[progRepoView progRepoViewDidLoad];
				[self.view addSubview: progRepoView];
				break;
			}
		}
	}
    
    [progRepoView progRepoViewWillAppear];
    progRepoView.hidden = FALSE;
    progRepoView.owner = self;
    progRepoView.frame = CGRectMake(320, 0, progRepoView.frame.size.width, progRepoView.frame.size.height);
    [self.view bringSubviewToFront: progRepoView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    progRepoView.frame = CGRectMake(0, 0, progRepoView.frame.size.width, progRepoView.frame.size.height);
    [UIView commitAnimations];
	
}

-(void) onProgressReportViewCancel{
    progRepoView.frame = CGRectMake(0, 0, progRepoView.frame.size.width, progRepoView.frame.size.height);
    [self.view bringSubviewToFront: progRepoView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    progRepoView.frame = CGRectMake(320, 0, progRepoView.frame.size.width, progRepoView.frame.size.height);
    [progRepoView progRepoViewWillDisAppear];
    [UIView commitAnimations];
    [self performSelector:(@selector(closeProgReportView)) withObject:nil afterDelay:0.4];
    //smqView.hidden = TRUE;
}

-(void) closeProgReportView{
    progRepoView.hidden = TRUE;
}

- (void) onSelectSession:(ObjSubSession *)objSe withSubject:(ObjSubject *)objSubject{
    [self showSessionMenuView:objSe withSubject:objSubject];
}

- (void) onGraph:(ObjSubject *)objSubject{
    if (graphViewController == nil) {
        graphViewController= [[GraphViewController alloc] initWithNibName:@"GraphViewController" bundle:nil];
    }
    [graphViewController viewWillAppear:YES];
    graphViewController.owner = self;
    graphViewController.objSubject = objSubject;
    [self presentModalViewController:graphViewController animated:YES];
    
}

- (void) onGraphViewCancel:(GraphViewController *)viewController{
    NSLog(@"close the view");
    [viewController dismissModalViewControllerAnimated:YES];
    //viewController.view.hidden = TRUE;
    //[viewController.view removeFromSuperview];
    //[viewController release];
    viewController = nil;
}

//**** Session Menu ****//
- (void)showSessionMenuView:(ObjSubSession *)obj withSubject:(ObjSubject *)objSubject{
    //NSString * strURL = @"http://www.youtube.com/watch?v=Vetg7vWitTU&feature=related";
    //http://youtu.be/lgT1AidzRWM
    if( sessionMenuView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SessionMenuView" owner:nil options:nil];
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[SessionMenuView class]]){
				sessionMenuView = (SessionMenuView *) currentObject;
				[sessionMenuView sessionMenuViewDidLoad];
				[self.view addSubview: sessionMenuView];
				break;
			}
		}
	}
    [sessionMenuView sessionMenuViewWillAppear:obj andSubject:objSubject];
    sessionMenuView.hidden = FALSE;
    sessionMenuView.owner = self;
    sessionMenuView.frame = CGRectMake(320, 0, sessionMenuView.frame.size.width, sessionMenuView.frame.size.height);
    [self.view bringSubviewToFront: sessionMenuView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    sessionMenuView.frame = CGRectMake(0, 0, sessionMenuView.frame.size.width, sessionMenuView.frame.size.height);
    [UIView commitAnimations];
}

- (void) showDashboardWith:(ObjSubSession *)objSession{
    PRDashboardViewController * detailViewController = [[PRDashboardViewController alloc]initWithNibName:@"PRDashboardViewController" bundle:nil] ;
    detailViewController.objSession = objSession;
    [self presentModalViewController:detailViewController animated:YES];
    //[detailViewController release];
}

- (void) showLessonWith:(ObjSubSession *)objSession{
    PRLessionViewController * detailViewController = [[PRLessionViewController alloc]initWithNibName:@"PRLessionViewController" bundle:nil];
    detailViewController.objSession = objSession;
    [self presentModalViewController:detailViewController animated:YES];
    //[detailViewController release];
}

- (void) showTodoWith:(ObjSubSession *)objSession{
    PRToDoViewController * detailViewController = [[PRToDoViewController alloc]initWithNibName:@"PRToDoViewController" bundle:nil];
    detailViewController.objSession = objSession;
    [self presentModalViewController:detailViewController animated:YES];
    //[detailViewController release];
}

- (void) showOthersWith:(ObjSubSession *)objSession{
    PROthersViewController * detailViewController = [[PROthersViewController alloc]initWithNibName:@"PROthersViewController" bundle:nil];
    detailViewController.objSession = objSession;
    [self presentModalViewController:detailViewController animated:YES];
}

- (void) show3PartPacingWith:(ObjSubSession *)objSession andSubject:(ObjSubject *)objSub{
    PR3PartPacingViewController * detailViewController = [[PR3PartPacingViewController alloc]initWithNibName:@"PR3PartPacingViewController" bundle:nil];
    detailViewController.objSession = objSession;
    detailViewController.objSubject = objSub;
    [self presentModalViewController:detailViewController animated:YES];
}

- (void) showClassFocusWith:(ObjSubSession *)objSession andSubject:(ObjSubject *)objSub{
    ClassFocusViewController * detailViewController = [[ClassFocusViewController alloc]initWithNibName:@"ClassFocusViewController" bundle:nil];
    detailViewController.objSession = objSession;
    detailViewController.objSubject = objSub;
    [self presentModalViewController:detailViewController animated:YES];
}

-(void) onSessionMenuViewCancel{
    sessionMenuView.frame = CGRectMake(0, 0, sessionMenuView.frame.size.width, sessionMenuView.frame.size.height);
    [self.view bringSubviewToFront: sessionMenuView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    sessionMenuView.frame = CGRectMake(320, 0, sessionMenuView.frame.size.width, sessionMenuView.frame.size.height);
    [UIView commitAnimations];
    [self performSelector:(@selector(closeSessionMenuView)) withObject:nil afterDelay:0.4];
    //smqView.hidden = TRUE;
}

-(void) closeSessionMenuView{
    sessionMenuView.hidden = TRUE;
}

#pragma mark - FGalleryViewControllerDelegate Methods
- (int)numberOfPhotosForPhotoGallery:(FGalleryViewController *)gallery
{
    int num;
    if( gallery == networkGallery ) {
        num = [networkImages count];
    }
	return num;
}

- (FGalleryPhotoSourceType)photoGallery:(FGalleryViewController *)gallery sourceTypeForPhotoAtIndex:(NSUInteger)index
{
	 return FGalleryPhotoSourceTypeNetwork;
}

- (NSString*)photoGallery:(FGalleryViewController *)gallery captionForPhotoAtIndex:(NSUInteger)index
{
    NSString *caption;
     if( gallery == networkGallery ) {
        caption = [networkCaptions objectAtIndex:index];
    }
	return caption;
}

- (NSString*)photoGallery:(FGalleryViewController*)gallery filePathForPhotoSize:(FGalleryPhotoSize)size atIndex:(NSUInteger)index {
    return 0;
}

- (NSString*)photoGallery:(FGalleryViewController *)gallery urlForPhotoSize:(FGalleryPhotoSize)size atIndex:(NSUInteger)index {
    return [networkImages objectAtIndex:index];
}

- (void)handleTrashButtonTouch:(id)sender {
    // here we could remove images from our local array storage and tell the gallery to remove that image
    // ex:
    //[localGallery removeImageAtIndex:[localGallery currentIndex]];
}

- (void)handleEditCaptionButtonTouch:(id)sender {
    // here we could implement some code to change the caption for a stored image
}

//**** END VS ****//

//**** SETTING ****//
- (void)showSettingView{
    if( settingView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SettingView" owner:nil options:nil];		
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[SettingView class]]){
				settingView = (SettingView *) currentObject;
				[settingView settingViewViewDidLoad];
				[self.view addSubview: settingView];
				break;
			}
		}
	}
    [settingView settingViewWillAppear];
    settingView.hidden = FALSE;
    settingView.owner = self;
    
    settingView.frame = CGRectMake(320, 0, settingView.frame.size.width, settingView.frame.size.height);
    [self.view bringSubviewToFront: settingView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    settingView.frame = CGRectMake(0, 0, settingView.frame.size.width, settingView.frame.size.height);
    [UIView commitAnimations];
	
}

- (void)onSettingViewCancel{
     settingView.frame = CGRectMake(0, 0, settingView.frame.size.width, settingView.frame.size.height);
    [self.view bringSubviewToFront: settingView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    settingView.frame = CGRectMake(320, 0, settingView.frame.size.width, settingView.frame.size.height);
    [UIView commitAnimations];
    [self performSelector:(@selector(closeSettingView)) withObject:nil afterDelay:0.4];
}

- (void)closeSettingView{
    settingView.hidden = TRUE;
}

- (void)showFreezeView{
    if( freezeView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FreezeView" owner:nil options:nil];
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[FreezeView class]]){
				freezeView = (FreezeView *) currentObject;
				[freezeView freezeViewViewDidLoad];
				[self.view addSubview: freezeView];
				break;
			}
		}
	}
    [freezeView freezeViewWillAppear];
    freezeView.hidden = FALSE;
    freezeView.owner = self;
    
    freezeView.frame = CGRectMake(320, 0, freezeView.frame.size.width, freezeView.frame.size.height);
    [self.view bringSubviewToFront: freezeView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    freezeView.frame = CGRectMake(0, 0, freezeView.frame.size.width, freezeView.frame.size.height);
    [UIView commitAnimations];
	
}

- (void)onFreezeViewCancel{
    freezeView.frame = CGRectMake(0, 0, freezeView.frame.size.width, freezeView.frame.size.height);
    [self.view bringSubviewToFront: freezeView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    freezeView.frame = CGRectMake(320, 0, freezeView.frame.size.width, freezeView.frame.size.height);
    [UIView commitAnimations];
    [self performSelector:(@selector(closeFreezeView)) withObject:nil afterDelay:0.4];
}

- (void)closeFreezeView{
    freezeView.hidden = TRUE;
}

-(void)onFreezeViewChangePassword{
    NSLog(@"show setting !");
    [self showSettingView];
}

-(void) onFreezeViewLogout{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    ObjUser * objUser = [delegate.db getUserObj];
    objUser.strPassword = @"";
    objUser.strSession = @"";
    objUser.strName = @"";
    objUser.strNewPassword = @"";
    [delegate.db updateUser:objUser];
    
    [delegate showLoginFromMain];    
}

-(void) onFreezeViewFreeze{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate dialogForFreezeAccount];
}

- (void) freezeViewSwitchViewToCurrent{
    if (freezeView != nil) {
        [freezeView backCurrentPosition];
    }
}

-(void) onUnFreezeViewFreeze{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate dialogForFreezeAccount];
}

-(void) onUnFreeze{
    [self dialogForUnFreezeAccount];
}

- (void)dialogForUnFreezeAccount{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Supervisor's Password" message:@"   " delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok",nil];
    if (txtParentPassword == nil) {
        txtParentPassword = [[UITextField alloc]initWithFrame:CGRectMake(40, 45, 215, 30)];
    }
    txtParentPassword.text = @"";
    txtParentPassword.textAlignment = UITextAlignmentCenter;
    alert.tag = 2;
    [txtParentPassword becomeFirstResponder];
    txtParentPassword.placeholder = @"Password";
    txtParentPassword.secureTextEntry = TRUE;
    txtParentPassword.backgroundColor = [UIColor whiteColor];
    [txtParentPassword setBorderStyle:UITextBorderStyleBezel];
    
    [alert addSubview:txtParentPassword];
    alert.frame =  CGRectMake(10, 100, 310, 320);
    alert.delegate = self;
    [alert show];
    [alert release];
}

- (void) alertView: (UIAlertView *)alertView clickedButtonAtIndex:(NSInteger) buttonIndex{
    if( [alertView tag] == 1 ){
        if(buttonIndex == 1) {
            //NSLog(@"here is custom internal");
            //[self showAndLoadCustomWebView:strMessageURL];
		}
	}
    if( [alertView tag] == 2 ){
        if(buttonIndex == 1) {
            zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            //NSLog(@"here is custom internal");
            //[self showAndLoadCustomWebView:strMessageURL];
            ObjUser * objUser = [delegate.db getUserObj];
            objUser.strParentPassword = txtParentPassword.text;
            NSLog(@"txt parent pass %@",objUser.strParentPassword);
            [self syncUnFreezeWith:objUser];
		}
        else if(buttonIndex == 0) {
            [self freezeViewSwitchViewToCurrent];
        }
	}
    
}

- (void) syncUnFreezeWith:(ObjUser *)objUser{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate showLoadingScreen];
    if (unfreezeRequest == nil) {
        unfreezeRequest = [[SOAPRequest alloc] initWithOwner:self];
        unfreezeRequest.processId = 18;
    }
    [unfreezeRequest syncUnFreezeWithUser:objUser];
}
//**** END SETTING ****//

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return NO;
}

- (IBAction)onClick:(id)sender {
    if ([sender tag]==0) {
        //[menu setTitle:@"Choose"];
        //[menu showInView:self.view];
        //[menu setBounds:CGRectMake(0,0,320,200)];
        [self showSMQMenuView];
    }
    else if ([sender tag]==1) {
        [self showYouTubeView];
    }
    else if([sender tag]==2){
        //[self showSettingView];
        [self showFreezeView];
    }
    else if([sender tag]==3){
        [self showQuizView];
    }
    else if([sender tag]==4){
        [self showCourseView];
    }
    else if([sender tag]==6){
        [self showProgReportView];
    }
}

- (void) setImage:(NSString *)urlString {
    
    
        NSURL *bgImageURL = [NSURL URLWithString:urlString];
        NSData *bgImageData = [NSData dataWithContentsOfURL:bgImageURL];
        UIImage *img = [UIImage imageWithData:bgImageData];
        [imgBanner setImage:img];
    

}


@end
