//
//  ObjDashboard.h
//  zBox
//
//  Created by Zayar on 5/12/13.
//
//

#import <Foundation/Foundation.h>
/*lesson": "A",
"attitude": "M",
"forgetfulness": "A",
"strategy": "A",
"milestone": "G"
 */
@interface ObjDashboard : NSObject
{
    NSString * strLesson;
    NSString * strAttitude;
    NSString * strForgetfulness;
    NSString * strStrategy;
    NSString * strMilestone;
}
@property (nonatomic, retain) NSString * strLesson;
@property (nonatomic, retain)NSString * strAttitude;
@property (nonatomic, retain)NSString * strForgetfulness;
@property (nonatomic, retain)NSString * strStrategy;
@property (nonatomic, retain)NSString * strMilestone;
@end
