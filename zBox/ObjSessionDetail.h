//
//  ObjSessionDetail.h
//  zBox
//
//  Created by Zayar on 5/25/13.
//
//

#import <Foundation/Foundation.h>
#import "ObjDashboard.h"
#import "ObjLesson.h"
#import "ObjSessionOther.h"
#import "ObjThreePartPacing.h"
#import "ObjClassFocus.h"

@interface ObjSessionDetail : NSObject
{
    ObjDashboard * objDashBoard;
    ObjLesson * objLesson;
    ObjSessionOther * objSessionOther;
    ObjThreePartPacing * objThreePartPacing;
    ObjClassFocus * objClassFocus;
    NSString * strTodos;
}
@property (nonatomic, retain) ObjDashboard * objDashBoard;
@property (nonatomic, retain) ObjLesson * objLesson;
@property (nonatomic, retain) ObjSessionOther * objSessionOther;
@property (nonatomic, retain) ObjThreePartPacing * objThreePartPacing;
@property (nonatomic, retain) ObjClassFocus * objClassFocus;
@property (nonatomic, retain) NSString * strTodos;
@end
