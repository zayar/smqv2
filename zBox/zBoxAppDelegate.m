//
//  zBoxAppDelegate.m
//  zBox
//
//  Created by Winner Computer Group on 1/31/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "zBoxAppDelegate.h"
#import "DBManager.h"
#import "zBoxViewController.h"
#import "LoginViewController.h"
#import "Reachability.h"
#import "SOAPRequest.h"
#import "ObjGKey.h"
#import "ObjUser.h"
#import "StringTable.h"
#import "LoadingScreenView.h"

@implementation zBoxAppDelegate

@synthesize window=_window;

@synthesize viewController=_viewController;

@synthesize databasePath,db;

@synthesize loginViewController;

@synthesize remoteHostStatus;
@synthesize internetConnectionStatus;
@synthesize localWiFiConnectionStatus;

@synthesize getConnection,strGenKey,strUdid,isFromMain,objSelectedCatalog,isShowAds;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

    //[self getDeviceid];
    // Add registration for remote notifications
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];

    // Clear application badge when app launches
    application.applicationIconBadgeNumber = 0;

    //[[UIApplication sharedApplication] unregisterForRemoteNotifications];
    // Override point for customization after application launch.

    self.db = [[DBManager alloc] init];
	[self.db retain];
	[self.db checkAndCreateDatabase];

    ///self.window.rootViewController = self.viewController;

    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleBlackOpaque;

    [[Reachability sharedReachability] setHostName:@"www.iwh.com.sg"];
	[self updateStatus];

    if( loadingView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"LoadingScreenView" owner:nil options:nil];
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[LoadingScreenView class]]){
				loadingView = (LoadingScreenView *) currentObject;
				loadingView.bounds = self.window.bounds;
				loadingView.frame = loadingView.bounds;
				loadingView.hidden = TRUE;
				[loadingView changeToOrientation: UIInterfaceOrientationPortrait];
				[self.window addSubview: loadingView];
				break;
			}
		}
	}
	loadingView.hidden = TRUE;

    UIDevice *device = [UIDevice currentDevice];
	self.strUdid = [device uniqueIdentifier];
    self.isFromMain = TRUE;

    return YES;
}

- (void)updateStatus
{
	// Query the SystemConfiguration framework for the state of the device's network connections.
	self.remoteHostStatus           = [[Reachability sharedReachability] remoteHostStatus];
	self.internetConnectionStatus	= [[Reachability sharedReachability] internetConnectionStatus];
	self.localWiFiConnectionStatus	= [[Reachability sharedReachability] localWiFiConnectionStatus];
	[self updateCarrierDataNetworkWarning];
}

- (void)updateCarrierDataNetworkWarning
{
	NSString *msg;

	if (self.internetConnectionStatus == NotReachable) {
		msg = @"Sorry, SMATHS is unable to connect to the server. Please check your internet connection or try later";
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		getConnection = FALSE;
	}
    else getConnection = TRUE;
}

- (void)reachabilityChanged:(NSNotification *)note
{
    [self updateStatus];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{

    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    //[prefs setObject:@"" forKey:GENR_KEY_LINK];
    ObjUser * obj= [self.db getUserObj];
    NSLog(@"obj.strSession %@",obj.strSession);
    if ([obj.strSession isEqualToString:@""] || obj.strSession == nil) {
        NSLog(@"login here");
        self.window.rootViewController = self.loginViewController;
        isNowInLogin = TRUE;
        isShowAds = FALSE;
    }
    else {
        NSLog(@"view here");
        self.window.rootViewController = self.viewController;
        [self syncAndCheckTheSession];
        isNowInLogin = FALSE;
        isShowAds = FALSE;
        //[self showAds];
    }

    //self.window.rootViewController = self.viewController;

    [self.window makeKeyAndVisible];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    if ([strKey isEqualToString:@""] || strKey == nil) {
        [self performSelector:@selector(syncGenerateKey) withObject:nil afterDelay:0.1];
        [self showLoadingScreen];
    }
    self.isFromMain = TRUE;
    //[self showOfflineHome];
}

- (void)showOfflineHome{
    self.window.rootViewController = self.viewController;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (void) clearTheBudgeIcon{
    [UIApplication sharedApplication].applicationIconBadgeNumber= 0;
}

////// SYNC FUNCTION //////////
- (void)syncGenerateKey{
    generateKeyRequest = [[SOAPRequest alloc] initWithOwner:self];
    generateKeyRequest.processId = 1;
    [generateKeyRequest syncGenerateKey];
}

- (void)syncAndCheckTheSession{
    checkSessionRequest = [[SOAPRequest alloc] initWithOwner:self];
    checkSessionRequest.processId = 2;
    [checkSessionRequest syncAndCheckTheSessionKey];
}

- (void)synUserPoint{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate showLoadingScreen];
    userPointRequest = [[SOAPRequest alloc] initWithOwner:self];
    userPointRequest.processId = 4;
    [userPointRequest syncUserPoint];
}

- (void) onErrorLoad: (int) processId{
    [self hideLoadingScreen];
    NSLog(@"Error loaded %d",processId);
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{

}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    if (processId == 1) {
        NSLog(@"arr count %d",[dics count]);
        ObjGKey * objKey = [[ObjGKey alloc]init];
        objKey.status = [[dics objectForKey:@"status"] intValue];
        objKey.strKey = [dics objectForKey:@"key"];
        objKey.strMessage = [dics objectForKey:@"message"];

        if (objKey.status == 1) {
            NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
            [prefs setObject:objKey.strKey forKey:GENR_KEY_LINK];

            ObjUser * obj= [self.db getUserObj];
            obj.strFreezeStatus = [dics objectForKey:@"student_status"];
            NSLog(@"freez status %@",obj.strFreezeStatus);
            if (![obj.strSession isEqualToString:@""]) {
                [prefs setObject:obj.strSession forKey:LOGIN_LINK];
                [prefs setObject:obj.strFreezeStatus forKey:FREEZE_TYPE];
                self.strGenKey = obj.strSession;
            }
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:@"Field required." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
        [objKey release];
        [self hideLoadingScreen];
        /*for(NSInteger i=0;i<[arrFeedNews count];i++){
         NSMutableDictionary * dicNews = [arrFeedNews objectAtIndex:i];
         // NSMutableDictionary * dicNews2 = [dicNews objectForKey:@"node"];
         NSMutableDictionary * dicNews2 = dicNews;
         ObjectETNews * objNews = [[ObjectETNews alloc]init];
         objNews.nid = [[dicNews2 objectForKey:@"nid"] intValue];
         objNews.strTitle = [dicNews2 objectForKey:@"title"];
         [objNews.strTitle UTF8String];
         objNews.strBody = [dicNews2 objectForKey:@"body"];
         [objNews.strBody UTF8String];
         objNews.strPhotoLink = [dicNews2 objectForKey:@"photo"];
         [objNews.strPhotoLink UTF8String];
         objNews.intTimetick = [[dicNews2 objectForKey:@"updatetime"] intValue];
         NSLog(@"news title %@",objNews.strTitle);
         }*/
    }
    if (processId == 4) {
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];

        NSString * strMessage;
        if (status == STATUS_ACTION_SUCCESS) {
            NSLog(@"session checking is successed %@",strMessage);
            int intPoint =[[dics objectForKey:@"points"] intValue];
            NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
            [prefs setInteger:intPoint forKey:CHECK_USER_POINT_LINK];
            //[self reloadLabelPointWith:intPoint];
        }
        else if(status == STATUS_NO_RECORD_FOUND){
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
            [self hideAds];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
            [self hideAds];
        }

        [self hideLoadingScreen];
    }
    if (processId == 2) {
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMessage = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            NSLog(@"session checking is successed %@",strMessage);
            NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
            ObjUser * objUser = [self.db getUserObj];
            NSString * strFreeze = [dics objectForKey:@"student_status"];
            NSLog(@"freez status %@",strFreeze);

            [prefs setObject:strFreeze forKey:FREEZE_TYPE];
            [self.db updateUser:objUser];
            NSString *urlString = [dics objectForKey:@"account_logo"];
            NSLog(@"Account Logo - %@", urlString);
            if(self.viewController != nil) {
                [self.viewController disableSMQ];
                if (urlString != (id)[NSNull null]) {
                    NSLog(@"Account Logo - %@", urlString);
                    [self.viewController setImage:urlString];
                }
                NSString * strHasAdv = [[dics objectForKey:@"has_advertisement"] intValue];
                if ([strHasAdv isEqualToString:@"Y"]) {
                    self.isShowAds = TRUE;
                }
                else{
                    self.isShowAds = FALSE;
                }
            }
            //[self synUserPoint];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
            [self hideAds];
            [self showLoginFromMain];
        }
    }
    if (processId == 12){
        [self hideLoadingScreen];
        NSLog(@"arr count %d",[dics count]);
        NSString * strMessage;
        int status = [[dics objectForKey:@"status"]intValue];
        strMessage = [dics objectForKey:@"message"];
        NSLog(@"%i count", status);
        if (status == STATUS_ACTION_SUCCESS) {
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];*/
            NSLog(@"Return %@", strMessage);
        }
        else if(status == STATUS_NO_RECORD_FOUND){
            //strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            //strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
    }
    if (processId == 16) {
        NSLog(@"arr count 16%d",[dics count]);
        ObjGKey * objKey = [[ObjGKey alloc]init];
        objKey.status = [[dics objectForKey:@"status"] intValue];
        objKey.strMessage = [dics objectForKey:@"message"];

        if (objKey.status == 1) {
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:objKey.strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];*/
            //[self showLoginFromMain];
            [self syncAndCheckTheSession];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:objKey.strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
            if (self.viewController != nil) {
                [self.viewController freezeViewSwitchViewToCurrent];
                NSLog(@"button index tag2");
            }
        }

        [objKey release];
        [self hideLoadingScreen];
        /*for(NSInteger i=0;i<[arrFeedNews count];i++){
         NSMutableDictionary * dicNews = [arrFeedNews objectAtIndex:i];
         // NSMutableDictionary * dicNews2 = [dicNews objectForKey:@"node"];
         NSMutableDictionary * dicNews2 = dicNews;
         ObjectETNews * objNews = [[ObjectETNews alloc]init];
         objNews.nid = [[dicNews2 objectForKey:@"nid"] intValue];
         objNews.strTitle = [dicNews2 objectForKey:@"title"];
         [objNews.strTitle UTF8String];
         objNews.strBody = [dicNews2 objectForKey:@"body"];
         [objNews.strBody UTF8String];
         objNews.strPhotoLink = [dicNews2 objectForKey:@"photo"];
         [objNews.strPhotoLink UTF8String];
         objNews.intTimetick = [[dicNews2 objectForKey:@"updatetime"] intValue];
         NSLog(@"news title %@",objNews.strTitle);
         }*/
    }
    if (processId == 18) {
        NSLog(@"arr count 18%d",[dics count]);
        ObjGKey * objKey = [[ObjGKey alloc]init];
        objKey.status = [[dics objectForKey:@"status"] intValue];
        objKey.strMessage = [dics objectForKey:@"message"];

        if (objKey.status == 1) {
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:objKey.strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
            NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
            [prefs setObject:@"U" forKey:FREEZE_TYPE];
            if (self.viewController != nil) {
                [self.viewController disableSMQ];
            }*/
            [self syncAndCheckTheSession];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:objKey.strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
            if (self.viewController != nil) {
                [self.viewController freezeViewSwitchViewToCurrent];
                NSLog(@"button index tag2");
            }
        }

        [objKey release];
        [self hideLoadingScreen];
        /*for(NSInteger i=0;i<[arrFeedNews count];i++){
         NSMutableDictionary * dicNews = [arrFeedNews objectAtIndex:i];
         // NSMutableDictionary * dicNews2 = [dicNews objectForKey:@"node"];
         NSMutableDictionary * dicNews2 = dicNews;
         ObjectETNews * objNews = [[ObjectETNews alloc]init];
         objNews.nid = [[dicNews2 objectForKey:@"nid"] intValue];
         objNews.strTitle = [dicNews2 objectForKey:@"title"];
         [objNews.strTitle UTF8String];
         objNews.strBody = [dicNews2 objectForKey:@"body"];
         [objNews.strBody UTF8String];
         objNews.strPhotoLink = [dicNews2 objectForKey:@"photo"];
         [objNews.strPhotoLink UTF8String];
         objNews.intTimetick = [[dicNews2 objectForKey:@"updatetime"] intValue];
         NSLog(@"news title %@",objNews.strTitle);
         }*/
    }
    else if (processId == 17) {

        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];

        NSString * strMessage = [dics objectForKey:@"message"];;
        if (status == STATUS_ACTION_SUCCESS) {
            NSLog(@"session checking is successed %@",strMessage);
            [self syncCataRegistrationWithCata:objSelectedCatalog];
        }
        else if(status == STATUS_ACTION_FAILED){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
            [self hideLoadingScreen];
            [self showLoginFromMain];
        }

        [self hideLoadingScreen];
    }

    else if (processId == 19) {

        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];

        NSString * strMessage = [dics objectForKey:@"message"];;
        if (status == STATUS_ACTION_SUCCESS) {
            NSLog(@"session checking is successed %@",strMessage);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
        else if(status == STATUS_ACTION_FAILED){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
            [self hideLoadingScreen];
            [self showLoginFromMain];
        }

        [self hideLoadingScreen];
    }
}
////// END SYNC FUNCTION //////////

///// VIEW CONTROLLING ///////////
- (void)showLoginFromMain{
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration: 0.25];
	[UIView setAnimationTransition: UIViewAnimationTransitionFlipFromLeft forView:self.window cache:YES];
	[self.viewController.view removeFromSuperview];
	[self.window addSubview: loginViewController.view];
    [UIView commitAnimations];
    self.isFromMain = TRUE;
}

- (void)showMainFromLogin{

    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration: 0.25];
	[UIView setAnimationTransition: UIViewAnimationTransitionFlipFromLeft forView:self.window cache:YES];
	[self.loginViewController.view removeFromSuperview];
	[self.window addSubview: self.viewController.view];
    isFromMain = TRUE;
    [self.viewController disableSMQ];
    [UIView commitAnimations];
}

- (void)showAds{
    if (adsViewController == nil) {
        adsViewController = [[AdsViewController alloc]initWithNibName:@"AdsViewController" bundle:nil];
    }
    [self.window.rootViewController presentModalViewController:adsViewController animated:YES];

    NSLog(@"here is show up!!");
}

- (void)hideAds{
    if (adsViewController != nil) {
        [adsViewController dismissModalViewControllerAnimated:YES];
    }
}

- (void)showLoadingScreen{
    loadingView.hidden = FALSE;
    loadingView.activity.hidden = FALSE;
    [loadingView.activity startAnimating];
    loadingView.lblMsg.hidden = TRUE;
    [self.window bringSubviewToFront: loadingView];
}

- (void)hideLoadingScreen{
    loadingView.hidden = TRUE;
    loadingView.lblMsg.hidden = TRUE;

    [loadingView.activity stopAnimating];
    loadingView.activity.hidden = TRUE;
}
///// END VIEW CONTROLLING ///////////
/*
 * ------------------------------------------------------------------------------------------
 *  BEGIN APNS CODE
 * ------------------------------------------------------------------------------------------
 */

/**
 * Fetch and Format Device Token and Register Important Information to Remote Server
 */
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    NSString *deviceToken = [[devToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""];
    deviceToken = [deviceToken stringByReplacingOccurrencesOfString: @">" withString: @""] ;
    deviceToken = [deviceToken stringByReplacingOccurrencesOfString: @" " withString: @""];

    NSLog(@"Device Token : %@", deviceToken);

    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    if ([strKey isEqualToString:@""] || strKey == nil) {
        NSLog(@"Device Token : %@", deviceToken);

        [self showLoadingScreen];
        generateKeyRequest = [[SOAPRequest alloc] initWithOwner:self];
        generateKeyRequest.processId = 1;
        [generateKeyRequest syncGenerateKeyWithToken: deviceToken];
    }
    //#endif
}

/**
 * Failed to Register for Remote Notifications
 */
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {

    //#if !TARGET_IPHONE_SIMULATOR

	NSLog(@"Error in registration. Error: %@", error);

    //#endif
}

/**
 * Remote Notification Received while application was open.
 */
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {

#if !TARGET_IPHONE_SIMULATOR

	NSLog(@"remote notification: %@",[userInfo description]);
	NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];

	NSString *alert = [apsInfo objectForKey:@"alert"];
	NSLog(@"Received Push Alert: %@", alert);

	NSString *sound = [apsInfo objectForKey:@"sound"];
	NSLog(@"Received Push Sound: %@", sound);
	AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);

	NSString *badge = [apsInfo objectForKey:@"badge"];
	NSLog(@"Received Push Badge: %@", badge);

	application.applicationIconBadgeNumber = [[apsInfo objectForKey:@"badge"] integerValue];

#endif
}

/*
 * ------------------------------------------------------------------------------------------
 *  END APNS CODE
 * ------------------------------------------------------------------------------------------
 */

- (void)dialogForCatalogRegister{

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Consent Form" message:@"Give consent?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag = 3;
    [alert show];
    [alert release];
}

- (void)showSupervisorPrompt{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Supervisor's Password" message:@"   " delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok" ,nil];
    if (txtParentPassword == nil) {
        txtParentPassword = [[UITextField alloc]initWithFrame:CGRectMake(40, 45, 215, 30)];
    }
     alert.tag = 4;
    txtParentPassword.text = @"";
     txtParentPassword.placeholder = @"password";
     txtParentPassword.secureTextEntry = TRUE;
     txtParentPassword.backgroundColor = [UIColor whiteColor];
    txtParentPassword.textAlignment = UITextAlignmentCenter;
     [txtParentPassword becomeFirstResponder];
     [alert addSubview:txtParentPassword];
     alert.frame =  CGRectMake(10, 100, 310, 320);
     [alert show];
     [alert release];
}

- (void)dialogForOpenCatalog{
    /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Supervisor's Password" message:@"   " delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
     UITextField * txt = [[UITextField alloc]initWithFrame:CGRectMake(60, 45, 150, 25)];

     txt.placeholder = @"password";
     txt.secureTextEntry = TRUE;
     txt.backgroundColor = [UIColor whiteColor];

     [alert addSubview:txt];
     alert.frame =  CGRectMake(10, 100, 310, 320);
     [alert show];
     [alert release];*/
    [self syncTrack:objSelectedCatalog.idx andString:@"c"];
    NSLog(@"catalog link %@",objSelectedCatalog.strCataMediaLink);
    NSString* launchUrl = objSelectedCatalog.strCataMediaLink;
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: launchUrl]];
}

- (void) syncTrack:(int)cataId andString:(NSString *)strType{
    [self showLoadingScreen];
    if (trackRequest == nil) {
        trackRequest = [[SOAPRequest alloc] initWithOwner:self];
        trackRequest.processId = 12;
    }
    [trackRequest syncCatalogTrackWith:cataId andResponseType:strType];
}

- (void) syncFreezeWith:(ObjUser *)objUser{
    [self showLoadingScreen];
    if (freezeRequest == nil) {
        freezeRequest = [[SOAPRequest alloc] initWithOwner:self];
        freezeRequest.processId = 16;
    }
    [freezeRequest syncFreezeWithUser:objUser];
}

- (void) syncUnFreezeWith:(ObjUser *)objUser{
    [self showLoadingScreen];
    if (unfreezeRequest == nil) {
        unfreezeRequest = [[SOAPRequest alloc] initWithOwner:self];
        unfreezeRequest.processId = 18;
    }
    [unfreezeRequest syncUnFreezeWithUser:objUser];
}

- (void) alertView: (UIAlertView *)alertView clickedButtonAtIndex:(NSInteger) buttonIndex{
    if( [alertView tag] == 1 ){
		 if(buttonIndex == 1) {
            //NSLog(@"here is custom internal");
            //[self showAndLoadCustomWebView:strMessageURL];
             ObjUser * objUser = [self.db getUserObj];
             objUser.strParentPassword = txtParentPassword.text;
             [self syncFreezeWith:objUser];
		}else if (buttonIndex == 0){
             if (self.viewController != nil) {
                 [self.viewController freezeViewSwitchViewToCurrent];
                 NSLog(@"button index tag1");
             }
            NSLog(@"button index tag1");
         }
	}
    if( [alertView tag] == 2 ){
        if(buttonIndex == 1) {
            //NSLog(@"here is custom internal");
            //[self showAndLoadCustomWebView:strMessageURL];
            ObjUser * objUser = [self.db getUserObj];
            objUser.strParentPassword = txtParentPassword.text;
            [self syncUnFreezeWith:objUser];
		}
        else if (buttonIndex == 0){
            if (self.viewController != nil) {
                [self.viewController freezeViewSwitchViewToCurrent];
                NSLog(@"button index tag2");
            }
            NSLog(@"button index tag2");
        }
	}
    if( [alertView tag] == 3 ){
        if(buttonIndex == 1) {
            if (objSelectedCatalog != nil) {
                objSelectedCatalog.strConsentValue = @"Y";
            }
            [self showSupervisorPrompt];
		}
        if(buttonIndex == 0) {
            if (objSelectedCatalog != nil) {
                objSelectedCatalog.strConsentValue = @"N";
            }
            [self showSupervisorPrompt];
		}
	}
    if( [alertView tag] == 4 ){
        if(buttonIndex == 1) {
            /*ObjUser * objUser = [self.db getUserObj];
            objUser.strParentPassword = txtParentPassword.text;
            [self syncUnFreezeWith:objUser];*/
            NSString * strSPass = txtParentPassword.text;
            objSelectedCatalog.strSPass = strSPass;
            [self syncSPassWith:strSPass];
		}
	}
}

- (void)syncSPassWith:(NSString *)strPass{
    [self showLoadingScreen];
    if (checkSPassRequest == nil ) {
        checkSPassRequest = [[SOAPRequest alloc] initWithOwner:self];
    }
    checkSPassRequest.processId = 17;
    [checkSPassRequest syncSupervieorPassWith:strPass];
}

- (void)syncCataRegistrationWithCata:(ObjCatalog *)objCata{
    [self showLoadingScreen];
    if (cataRegisterRequest == nil ) {
        cataRegisterRequest = [[SOAPRequest alloc] initWithOwner:self];
    }
    cataRegisterRequest.processId = 19;
    [cataRegisterRequest syncRegisterWithCata:objSelectedCatalog];
}

- (void)dialogForFreezeAccount{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Supervisor's Password" message:@"   " delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok",nil];
    if (txtParentPassword == nil) {
        txtParentPassword = [[UITextField alloc]initWithFrame:CGRectMake(40, 45, 215, 30)];
    }
    txtParentPassword.text = @"";
    txtParentPassword.textAlignment = UITextAlignmentCenter;
    alert.tag = 1;
    [txtParentPassword becomeFirstResponder];

    txtParentPassword.placeholder = @"Password";
    txtParentPassword.secureTextEntry = TRUE;
    txtParentPassword.backgroundColor = [UIColor whiteColor];
    [txtParentPassword setBorderStyle:UITextBorderStyleBezel];

    [alert addSubview:txtParentPassword];
    alert.frame =  CGRectMake(10, 100, 310, 320);
    alert.delegate = self;
    [alert show];
    [alert release];
}

- (void)dialogForUnFreezeAccount{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Supervisor's Password" message:@"   " delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok",nil];
    if (txtParentPassword == nil) {
        txtParentPassword = [[UITextField alloc]initWithFrame:CGRectMake(40, 45, 215, 30)];
    }
    txtParentPassword.text = @"";
    txtParentPassword.textAlignment = UITextAlignmentCenter;
    alert.tag = 2;

    txtParentPassword.placeholder = @"Password";
    txtParentPassword.secureTextEntry = TRUE;
    txtParentPassword.backgroundColor = [UIColor whiteColor];
    [txtParentPassword setBorderStyle:UITextBorderStyleBezel];

    [alert addSubview:txtParentPassword];
    alert.frame =  CGRectMake(10, 100, 310, 320);
    alert.delegate = self;
    [alert show];
    [alert release];
}

- (void)dealloc
{
    [checkSPassRequest release];
    [txtParentPassword release];
    [objSelectedCatalog release];
    [trackRequest release];
    [adsViewController release];
    [strUdid release];
    [strGenKey release];
    [checkSessionRequest release];
    [generateKeyRequest release];
    [loginViewController release];
    [db release];
    [databasePath release];
    [_window release];
    [_viewController release];
    [super dealloc];
}

@end
