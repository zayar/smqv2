//
//  ObjUser.h
//  zBox
//
//  Created by Zayar Cn on 6/10/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjUser : NSObject
{
    int idx;
    NSString * strName;
    NSString * strPassword;
    NSString * strSession;
    NSString * strNewPassword;
    NSString * strParentPassword;
    NSString * strFreezeStatus;
    NSString * strAccountImageUrl;
    int intActive;
}
@property int idx;
@property (nonatomic,retain) NSString * strName;
@property (nonatomic,retain) NSString * strPassword;
@property (nonatomic,retain) NSString * strSession;
@property (nonatomic,retain) NSString * strNewPassword;
@property (nonatomic,retain) NSString * strParentPassword;
@property (nonatomic,retain) NSString * strFreezeStatus;
@property (nonatomic,retain) NSString * strAccountImageUrl;
@property int intActive;

@end
