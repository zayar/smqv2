//
//  ObjAnswer.h
//  zBox
//
//  Created by Zayar Cn on 6/13/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InternetImage.h"
@protocol ObjAnswerDelegate 
- (void) onImageDownloadFinish:(int)proccessId imageType:(int) itemType;
- (void) onImageDownloadFinish:(int)proccessId imageType:(int) itemType tblCell:(NSIndexPath *)rowCellIndex;
@end
@interface ObjAnswer : NSObject
{
    int idx;
    int question_id;
    int subject_id;
    int category_id;
    int rate_point;
    NSString * strQuestionStatus;
    NSString * strQuestionTitle;
    NSString * strQuestionText;
    NSString * strQuestionImageFileLink;
    NSString * strQuestionImageThumbFileLink;
    NSString * strQuestionMovieFileLink;
    NSString * strQuestAnsType;
    NSString * strQuestAns;
    
    NSString * strThumbnailPath;
	NSMutableArray * thumbnailCallBacks;
	InternetImage * thumbnailDownloader;
    
    int obj_proccessID;
    id <ObjAnswerDelegate> owner;
    int itemType;
}
@property int question_id;
@property int subject_id;
@property int category_id;
@property int idx;
@property int rate_point;
@property (nonatomic, retain) NSString * strQuestionStatus;
@property (nonatomic, retain) NSString * strQuestionTitle;
@property (nonatomic, retain) NSString * strQuestionText;
@property (nonatomic, retain) NSString * strQuestionImageFileLink;
@property (nonatomic, retain) NSString * strQuestionMovieFileLink;
@property (nonatomic, retain) NSString * strQuestAnsType;
@property (nonatomic, retain) NSString * strQuestAns;
@property (nonatomic, retain) NSString * strQuestionImageThumbFileLink;
@property (nonatomic, retain) NSMutableArray * thumbnailCallBacks;
- (void) downloadImage:(int) processId;
@end
