//
//  ObjQuizz.h
//  zBox
//
//  Created by Zayar on 5/8/13.
//
//

#import <Foundation/Foundation.h>
@interface ObjQuizz : NSObject
{
    int idx;
    NSString * strQuizName;
    NSString * strQuizSource;
    int class_id;
    NSString * strClassName;
    int subject_id;
    NSString * strSubjectName;
    int quiz_qest_count;
    NSString * strConsultType;
    int rate_point;
}
@property int idx;
@property (nonatomic, retain) NSString * strQuizName;
@property (nonatomic, retain) NSString * strQuizSource;
@property int class_id;
@property (nonatomic, retain) NSString * strClassName;
@property int subject_id;
@property (nonatomic, retain) NSString * strSubjectName;
@property (nonatomic, retain) NSString * strConsultType;
@property int quiz_qest_count;
@property int rate_point;
@end
