//
//  ObjAnswer.m
//  zBox
//
//  Created by Zayar Cn on 6/13/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "ObjAnswer.h"
#import "InternetImage.h"
#import "zBoxAppDelegate.h"
#import "Utility.h"

@implementation ObjAnswer
@synthesize question_id,subject_id,category_id,strQuestionStatus,strQuestionTitle,strQuestionText,strQuestionImageFileLink,strQuestionMovieFileLink,strQuestAnsType,strQuestAns,idx,thumbnailCallBacks,strQuestionImageThumbFileLink,rate_point;

- (id) init{
	if( self = [super init] ){
		self.thumbnailCallBacks = [[NSMutableArray alloc] init];
	}
	
	return self;
}

- (void) downloadImage:(int) processId{
    if (processId == 0) {
        if( thumbnailDownloader != nil ){
            [thumbnailDownloader abortDownload];
            [thumbnailDownloader release];
        }
        
        if( strQuestionImageThumbFileLink != nil && (NSNull *) strQuestionImageThumbFileLink != [NSNull null] ){
            thumbnailDownloader = [[InternetImage alloc] initWithUrl: strQuestionImageThumbFileLink];
            thumbnailDownloader.processId = 0;
            NSLog(@"Ads download link: %@", strQuestionImageThumbFileLink);
        }
        [thumbnailDownloader downloadImage: self];
        thumbnailDownloader.processId = processId;
    }
    obj_proccessID = processId;
}

- (void)cancelAllDownloads{
	if( thumbnailDownloader != nil ){
		[thumbnailDownloader abortDownload];
	}
}

-(void) internetImageReady:(InternetImage*)downloadedImage{	
    NSLog(@"download id %d",downloadedImage.processId);
    if( downloadedImage.Image != nil ){
        zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *tempPath = [paths objectAtIndex:0];
        
        if( downloadedImage.processId == 0 ){
            NSString * imgPath = [tempPath stringByAppendingPathComponent: [NSString stringWithFormat: @"%.0f_%d_news_ads.png", [NSDate timeIntervalSinceReferenceDate] * 1000.0]];
            strThumbnailPath = imgPath;
            [strThumbnailPath retain];
            
            //UIImage * imgDetail = [Utility imageByScalingProportionallyToSize: downloadedImage.Image newsize:CGSizeMake(78,63) resizeFrame:FALSE];
            //UIImage * imgDetail = [Utility forceImageResize:downloadedImage.Image newsize:CGSizeMake(320,60)];
            
            NSData *imageDetailData = [NSData dataWithData:UIImagePNGRepresentation(downloadedImage.Image)];
            [imageDetailData writeToFile:imgPath atomically:YES];
            
            NSLog(@"img thumb Path %@",imgPath);
            
            [delegate.db insertCachedImage:strQuestionImageThumbFileLink  path:imgPath];
            
            if( self.thumbnailCallBacks != nil ){
                NSLog(@"Ads thumbnail call backs count %d",[self.thumbnailCallBacks count]);
                for(int i=0; i<[self.thumbnailCallBacks count]; i++){
                    NSLog(@"Ads image create........");
                    UIImageView * imgView = [self.thumbnailCallBacks objectAtIndex: i];
                    
                    if( imgView != nil && (NSNull *) imgView != [NSNull null] ){
                        if( imgView.image != nil && (NSNull *) imgView.image != [NSNull null] ){
                            [imgView.image release];
                            imgView.image = nil;
                        }
                        imgView.hidden = FALSE;
                        [imgView setImage: [[UIImage alloc] initWithContentsOfFile: imgPath]];
                    }
                }
            }
            [thumbnailDownloader release];
            thumbnailDownloader = nil;
            //[owner onImageDownloadFinish:obj_CellIndex imageType:itemType];
            [owner onImageDownloadFinish:obj_proccessID imageType:itemType];
        }
    }
}

- (void)dealloc{
    [strQuestionImageThumbFileLink release];
    [strThumbnailPath release];
	[thumbnailCallBacks release];
	[thumbnailDownloader release];
    [strQuestionStatus release];
    [strQuestionTitle release];
    [strQuestionText release];
    [strQuestionImageFileLink release];
    [strQuestionMovieFileLink release];
    [strQuestAnsType release];
    [strQuestAns release];
    [super dealloc];
}

@end
