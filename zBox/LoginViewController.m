//
//  LoginViewController.m
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "LoginViewController.h"
#import "StringTable.h"
#import "zBoxAppDelegate.h"
#import "ObjUser.h"
#import "ResetView.h"

@interface LoginViewController ()

@end

@implementation LoginViewController
@synthesize entryFields;

//---size of keyboard--- 
CGRect keyboardBounds;
//---size of application screen--- 
CGRect applicationFrame;
//---original size of ScrollView--- 
CGSize scrollViewOriginalSize;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSString * strPath = [NSString stringWithFormat:@"%@/img_login_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];
    [imgBgView release];
    UIFont * cfont = [UIFont fontWithName:@"BradyBunchRemastered" size:18.0];
    
    txtPassword.font = cfont;
    txtLoginName.font = cfont;
    UIFont * fFont=[UIFont fontWithName:@"BradyBunchRemastered" size:20.0];
    lblForget.font = fFont;
    lblForget.textColor = [UIColor brownColor];
    scrollViewOriginalSize = scrollView.contentSize;
    applicationFrame = [[UIScreen mainScreen] applicationFrame];
    //scrollViewOriginalSize = scrollView.contentSize;
    NSLog(@"scroll width %f and scroll hight %f",scrollView.contentSize.width,scrollView.contentSize.height);
}

-(void)viewWillAppear:(BOOL)animated{
    //---registers the notifications for keyboard--- 
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    txtPassword.text = @"";
    txtLoginName.text = @"";
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) moveScrollView:(UIView *) theView {
    //---get the y-coordinate of the view--- 
    CGFloat viewCenterY = theView.center.y + 20;
    
    //---calculate how much visible space is left---
    CGFloat freeSpaceHeight = applicationFrame.size.height - keyboardBounds.size.height;
    
    //---calculate how much the scrollview must scroll--- 
    CGFloat scrollAmount = viewCenterY - freeSpaceHeight / 2.0;
    if (scrollAmount < 0) scrollAmount = 0;
    
    //---set the new scrollView contentSize--- 
    scrollView.contentSize = CGSizeMake(applicationFrame.size.width, applicationFrame.size.height +keyboardBounds.size.height);
    
    //---scroll the ScrollView---
    [scrollView setContentOffset:CGPointMake(0, scrollAmount) animated:YES];
}

-(void) textFieldDidBeginEditing:(UITextField *)textFieldView {
        [self moveScrollView:textFieldView];
}

-(void) textFieldDidEndEditing:(UITextField *) textFieldView {
    
    [UIView beginAnimations:@"back to original size" context:nil]; 
    scrollView.contentSize = scrollViewOriginalSize;
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField.tag == 1) {
        UIView * nextView = [self.view viewWithTag:2];
        [nextView becomeFirstResponder];
        return NO;
    }
    [textField resignFirstResponder];
	return YES;
}

//keyboard appear
-(void) keyboardWillShow:(NSNotification *) notification {
    //---gets the size of the keyboard---
    NSDictionary *userInfo = [notification userInfo];
    NSValue *keyboardValue = [userInfo objectForKey:UIKeyboardBoundsUserInfoKey]; 
    [keyboardValue getValue:&keyboardBounds];
    
}

-(void) keyboardWillHide:(NSNotification *) notification {
}

//////Sync Login//////
- (void)syncLgoinWith:(ObjUser *)objUser{
    loginRequest = [[SOAPRequest alloc] initWithOwner:self];
    loginRequest.processId = 3;
     NSLog(@"user :%@ and pass : %@",objUser.strName,objUser.strPassword);
    [loginRequest syncLoginWithUser:objUser];
}

- (void) onErrorLoad: (int) processId{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate hideLoadingScreen];
    NSLog(@"Error loaded %d",processId);
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{
    
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    if (processId == 3) {
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        if (status == STATUS_ACTION_SUCCESS) {
            ObjUser * objUser = [[ObjUser alloc]init];
            objUser.strSession = [dics objectForKey:@"session_id"];
            
            NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
            NSString * strFStatus = [dics objectForKey:@"student_status"];
            [prefs setObject:objUser.strSession forKey:LOGIN_LINK];
            [prefs setObject:strFStatus forKey:FREEZE_TYPE];
            objUser.strName = txtLoginName.text;
            objUser.strPassword = txtPassword.text;
            objUser.strAccountImageUrl = [dics objectForKey:@"account_logo"];
            NSLog(@"Account image url : %@", objUser.strAccountImageUrl);
            [delegate.db updateUser:objUser];
            [objUser release];
            NSString * strHasAdv = [dics objectForKey:@"has_advertisement"];
            if ([strHasAdv isEqualToString:@"Y"]) {
                delegate.isShowAds = TRUE;
                NSLog(@"to show ads");
            }
            else{
                delegate.isShowAds = FALSE;
                NSLog(@"to hide show ads");
            }
            
            [delegate showMainFromLogin];
            //[delegate showAds];
        }
        else if(status == STATUS_ACTION_FAILED){
            NSString * strErrorMsg = [dics objectForKey:@"message"];
            [self textValidateAlertShow:strErrorMsg];
        }
        [delegate hideLoadingScreen];
    }
}

- (IBAction)showReset:(id)sender{
    [self showResetView];
}

//**** RESETView ****//
- (void)showResetView{
    NSLog(@"show reset!!");
    
    if( resetView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ResetView" owner:nil options:nil];
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[ResetView class]]){
				resetView = (ResetView *) currentObject;
				[resetView settingViewViewDidLoad];
				[self.view addSubview: resetView];
				break;
			}
		}
	}
    [resetView settingViewWillAppear];
    resetView.hidden = FALSE;
    resetView.owner = self;
    resetView.frame = CGRectMake(320, 0, resetView.frame.size.width, resetView.frame.size.height);
    [self.view bringSubviewToFront: resetView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    resetView.frame = CGRectMake(0, 0, resetView.frame.size.width, resetView.frame.size.height);
    [UIView commitAnimations];
}

-(void) onResetViewCancel{
    resetView.frame = CGRectMake(0, 0, resetView.frame.size.width, resetView.frame.size.height);
    [self.view bringSubviewToFront: resetView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    resetView.frame = CGRectMake(320, 0, resetView.frame.size.width, resetView.frame.size.height);
    [UIView commitAnimations];
    [self performSelector:(@selector(closeResetView)) withObject:nil afterDelay:0.4];
    //smqView.hidden = TRUE;
}

-(void) closeResetView{
    resetView.hidden = TRUE;
}
//**** END RESETView ****//

//////END Sync Login//////
-(IBAction)onClick:(id)sender{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    if (delegate.getConnection) {
        if ([[txtLoginName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
            [self textValidateAlertShow:@"Username cannot be blank."];
            //[self textValidateAlertShow:@"Retry your name and password!"];
            [self textFieldDidEndEditing:txtLoginName];
            return;
        }
        if ([[txtPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
            [self textValidateAlertShow:@"Password cannot be blank."];
            [self textFieldDidEndEditing:txtPassword];
            return;
        }
        
        ObjUser * objUser = [[ObjUser alloc]init];
        objUser.strName = txtLoginName.text;
        objUser.strPassword = txtPassword.text;
        [delegate showLoadingScreen];
        [self syncLgoinWith:objUser];
        //NSLog(@"user :%@ and pass : %@",objUser.strName,objUser.strPassword);
        [objUser release];
    }
    [txtLoginName resignFirstResponder];
    [txtPassword resignFirstResponder];
    //[delegate showMainFromLogin];
}

- (void) textValidateAlertShow:(NSString *) strError{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: APP_TITLE
                          message: strError
                          delegate: nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
    [alert release];
    
}

- (void)dealloc{
    [resetView release];
    [lblForget release];
    [loginRequest release];
    [scrollView release];
    [txtLoginName release];
    [txtPassword release];
    [imgBgView release];
    [super dealloc];
}

@end
