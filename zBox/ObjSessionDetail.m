//
//  ObjSessionDetail.m
//  zBox
//
//  Created by Zayar on 5/25/13.
//
//

#import "ObjSessionDetail.h"

@implementation ObjSessionDetail
@synthesize objDashBoard,objLesson,objSessionOther,objThreePartPacing,objClassFocus,strTodos;
- (id) init{
	if( self = [super init] ){
		//self.arrCategories = [[NSMutableArray alloc] init];
        //self.arrLesson = [[NSMutableArray alloc]init];
        //self.arrOthersSession = [[NSMutableArray alloc]init];
        //self.arrClassFocus = [[NSMutableArray alloc]init];
        //self.arrExam = [[NSMutableArray alloc]init];
        //self.arrPacing = [[NSMutableArray alloc]init];
        self.objDashBoard = [[ObjDashboard alloc]init];
        self.objLesson = [[ObjLesson alloc]init];
        self.objSessionOther = [[ObjSessionOther alloc]init];
        self.objThreePartPacing = [[ObjThreePartPacing alloc]init];
        self.objClassFocus = [[ObjClassFocus alloc]init];
	}
	return self;
}

- (void)dealloc{
    [objDashBoard release];
    [objLesson release];
    [objSessionOther release];
    [objThreePartPacing release];
    [objClassFocus release];
    [strTodos release];
    [super dealloc];
}
@end
