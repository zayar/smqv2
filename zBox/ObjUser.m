//
//  ObjUser.m
//  zBox
//
//  Created by Zayar Cn on 6/10/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "ObjUser.h"

@implementation ObjUser
@synthesize idx,strName,strPassword,strSession,intActive,strNewPassword,strParentPassword,strFreezeStatus;
- (void)dealloc{
    [strFreezeStatus release];
    [strNewPassword release];
    [strName release];
    [strPassword release];
    [strSession release];
    [super dealloc];
}
@end
