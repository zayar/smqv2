//
//  ObjDashboard.m
//  zBox
//
//  Created by Zayar on 5/12/13.
//
//

#import "ObjDashboard.h"

@implementation ObjDashboard
@synthesize strLesson,strAttitude,strForgetfulness,strStrategy,strMilestone;
- (void)dealloc{
    [strLesson release];
    [strAttitude release];
    [strForgetfulness release];
    [strStrategy release];
    [strMilestone release];
    [super dealloc];
}
@end
