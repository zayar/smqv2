//
//  ObjClassFocus.m
//  zBox
//
//  Created by Zayar on 5/12/13.
//
//

#import "ObjClassFocus.h"
/*
 "class_focus": [
 {
 "session_name": "Session 1",
 "topic_name": "Topic A"
 },
 {
 "session_name": "Session 2",
 "topic_name": "Topic B"
 },
 {
 "session_name": "Session 3",
 "topic_name": "Topic C"
 },
 {
 "session_name": "Session 4",
 "topic_name": "Topic D"
 }
 */
@implementation ObjClassFocus
@synthesize strSessionName,strTopicName;
- (void)dealloc{
    [strSessionName release];
    [strTopicName release];
    [super dealloc];
}
@end
