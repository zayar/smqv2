//
//  zBoxAppDelegate.h
//  zBox
//
//  Created by Winner Computer Group on 1/31/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "Reachability.h"
#import "SOAPRequest.h"
#import "LoadingScreenView.h"
#import "AdsViewController.h"
#import "ObjCatalog.h"

@class zBoxViewController;
@class LoginViewController;

@interface zBoxAppDelegate : NSObject <UIApplicationDelegate> {
    NSString * databasePath;
    DBManager * db;
    
    NetworkStatus remoteHostStatus;
    NetworkStatus internetConnectionStatus;
    NetworkStatus localWiFiConnectionStatus;
    
    SOAPRequest * generateKeyRequest;
    SOAPRequest * checkSessionRequest;
    SOAPRequest * userPointRequest;
    SOAPRequest * trackRequest;
    SOAPRequest * freezeRequest;
    SOAPRequest * unfreezeRequest;
    SOAPRequest * checkSPassRequest;
    SOAPRequest * cataRegisterRequest;
    LoadingScreenView * loadingView;
    
    BOOL getConnection;
    BOOL isNowInLogin;
    BOOL isFromMain;
    BOOL isShowAds;
    NSString * strGenKey;
    NSString * strUdid;
    AdsViewController * adsViewController;
    ObjCatalog * objSelectedCatalog;
    UITextField * txtParentPassword;
    
}
@property (nonatomic, retain) NSString * databasePath;
@property (nonatomic, retain) NSString * strUdid;
@property (nonatomic, retain) DBManager * db;
@property BOOL getConnection;
@property BOOL isFromMain;
@property BOOL isShowAds;
@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet zBoxViewController *viewController;
@property (nonatomic, retain) IBOutlet LoginViewController *loginViewController;
@property (nonatomic, retain) NSString * strGenKey;
@property (nonatomic, retain) ObjCatalog * objSelectedCatalog;

@property NetworkStatus remoteHostStatus;
@property NetworkStatus internetConnectionStatus;
@property NetworkStatus localWiFiConnectionStatus;

- (void)showLoginFromMain;
- (void)showMainFromLogin;
- (void)showLoadingScreen;
- (void)hideLoadingScreen;
- (void)updateStatus;
- (void)updateCarrierDataNetworkWarning;
- (void)syncAndCheckTheSession;
- (void) clearTheBudgeIcon;
- (void)dialogForCatalogRegister;
- (void)dialogForFreezeAccount;
- (void)dialogForOpenCatalog;
- (void)showAds;
- (void)hideAds;
- (void)dialogForUnFreezeAccount;
- (void) syncTrack:(int)cataId andString:(NSString *)strType;
@end
