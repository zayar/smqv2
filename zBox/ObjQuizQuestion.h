//
//  ObjQuizQuestion.h
//  zBox
//
//  Created by Zayar on 5/9/13.
//
//

#import <Foundation/Foundation.h>
#import "InternetImage.h"
@protocol ObjAnswerDelegate
- (void) onImageDownloadFinish:(int)proccessId imageType:(int) itemType;
- (void) onImageDownloadFinish:(int)proccessId imageType:(int) itemType tblCell:(NSIndexPath *)rowCellIndex;
@end
@interface ObjQuizQuestion : NSObject
{
    int idx;
    NSString * strQuestTitle;
    NSString * strQuestType;
    NSString * strQuestText;
    NSString * strQuestStatus;
    NSString * strAnsType;
    NSString * strAnsText;
    int rate_point;
    
    NSString * strThumbnailPath;
	NSMutableArray * thumbnailCallBacks;
	InternetImage * thumbnailDownloader;
    
    NSString * strDetailPath;
	NSMutableArray * detailCallBacks;
	InternetImage * detailDownloader;
    
    int obj_proccessID;
    id <ObjAnswerDelegate> owner;
    int itemType;
}
@property int idx;
@property int rate_point;
@property (nonatomic, retain) NSString * strQuestTitle;
@property (nonatomic, retain) NSString * strQuestType;
@property (nonatomic, retain) NSString * strQuestText;
@property (nonatomic, retain) NSString * strQuestStatus;
@property (nonatomic, retain) NSString * strAnsType;
@property (nonatomic, retain) NSString * strAnsText;
@property (nonatomic, retain) NSMutableArray * thumbnailCallBacks;
@property (nonatomic, retain) NSMutableArray * detailCallBacks;
- (void) downloadImage:(int) processId;
@property id <ObjAnswerDelegate> owner;
@end
