//
//  ObjPacing.h
//  zBox
//
//  Created by Zayar on 5/6/13.
//
//

#import <Foundation/Foundation.h>

@interface ObjPacing : NSObject
{
    int idx;
    NSString * strTopicName;
    NSString * strPacingValue;
}
@property int idx;
@property (nonatomic, retain) NSString * strTopicName;
@property (nonatomic, retain) NSString * strPacingValue;
@end
