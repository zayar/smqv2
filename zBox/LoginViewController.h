//
//  LoginViewController.h
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOAPRequest.h"
#import "ResetView.h"
@interface LoginViewController : UIViewController <UITextFieldDelegate>
{
    IBOutlet UIImageView * imgBgView;
    IBOutlet UITextField * txtLoginName;
    IBOutlet UITextField * txtPassword;
    IBOutlet UIScrollView * scrollView;
    IBOutlet UIButton * btnSignin;
    NSMutableArray * entryFields;
    SOAPRequest * loginRequest;
    IBOutlet UILabel * lblForget;
    ResetView * resetView;
}
@property (nonatomic, retain) NSMutableArray *entryFields;
-(IBAction)onClick:(id)sender;
- (IBAction)showReset:(id)sender;
@end
