//
//  ObjQuizz.m
//  zBox
//
//  Created by Zayar on 5/8/13.
//
//

#import "ObjQuizz.h"

@implementation ObjQuizz
@synthesize idx,strQuizName,strQuizSource,class_id,strClassName,subject_id,strSubjectName,quiz_qest_count,strConsultType,rate_point;

- (void)dealloc{
    [strQuizName release];
    [strQuizSource release];
    [strClassName release];
    [strSubjectName release];
    [super dealloc];
}

@end
