//
//  ObjPacing.m
//  zBox
//
//  Created by Zayar on 5/6/13.
//
//

#import "ObjPacing.h"

@implementation ObjPacing
@synthesize idx,strTopicName,strPacingValue;

- (void)dealloc{
    [strTopicName release];
    [strPacingValue release];
    [super dealloc];
}

@end
