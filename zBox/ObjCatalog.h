//
//  ObjCatalog.h
//  zBox
//
//  Created by Zayar on 5/8/13.
//
//

#import <Foundation/Foundation.h>
#import "InternetImage.h"
@protocol ObjAnswerDelegate
- (void) onImageDownloadFinish:(int)proccessId imageType:(int) itemType;
- (void) onImageDownloadFinish:(int)proccessId imageType:(int) itemType tblCell:(NSIndexPath *)rowCellIndex;
@end
@interface ObjCatalog : NSObject
{
    int idx;
    NSString * strCompanyName;
    NSString * strCataTitle;
    NSString * strCataMediaType;
    NSString * strCataMedia;
    NSString * strCataMediaLink;
    NSString * strReponseType;
    NSString * strConsentValue;
    NSString * strSPass;
    
    NSString * strThumbnailPath;
	NSMutableArray * thumbnailCallBacks;
	InternetImage * thumbnailDownloader;
    
    NSString * strDetailPath;
	NSMutableArray * detailCallBacks;
	InternetImage * detailDownloader;
    
    int obj_proccessID;
    id <ObjAnswerDelegate> owner;
    int itemType;
}
@property int idx;
@property(nonatomic,retain) NSString * strCompanyName;
@property(nonatomic,retain) NSString * strCataTitle;
@property(nonatomic,retain) NSString * strCataMediaType;
@property(nonatomic,retain) NSString * strCataMedia;
@property(nonatomic,retain) NSString * strCataMediaLink;
@property(nonatomic,retain) NSString * strReponseType;
@property(nonatomic,retain) NSString * strConsentValue;
@property(nonatomic,retain) NSString * strSPass;
@property (nonatomic, retain) NSMutableArray * thumbnailCallBacks;
@property (nonatomic, retain) NSMutableArray * detailCallBacks;

- (void) downloadImage:(int) processId;
@property id <ObjAnswerDelegate> owner;
@end
