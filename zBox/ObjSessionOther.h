//
//  ObjSessionOther.h
//  zBox
//
//  Created by Zayar on 5/12/13.
//
//

#import <Foundation/Foundation.h>

@interface ObjSessionOther : NSObject
{
    NSString * strOtherName;
    NSString * strOtherDes;
    NSString * strNextLesson;
    NSString * strNextMilestone;
    NSString * strNextTopic;
    NSString * strAdmin;
}
@property (nonatomic, retain) NSString * strOtherName;
@property (nonatomic, retain) NSString * strOtherDes;
@property (nonatomic, retain) NSString * strNextLesson;
@property (nonatomic, retain) NSString * strNextMilestone;
@property (nonatomic, retain) NSString * strNextTopic;
@property (nonatomic, retain) NSString * strAdmin;
@end
