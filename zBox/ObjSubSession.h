//
//  ObjSubSession.h
//  zBox
//
//  Created by Zayar on 3/17/13.
//
//

#import <Foundation/Foundation.h>

/* 
 "progress_report_id": 1,
 "progress_report_session": "Session 21 (Topic volutpat)",
 "progress_report_date": "2000-11-14",
 "actual_score": 78,
 "careless_score": 16,
 "couldve_score": 83,
 "progress_report_details": [
 {
 */
#import "ObjDashboard.h"
#import "ObjLesson.h"

@interface ObjSubSession : NSObject
{
    int idx;
    int progressId;
    int classSessionId;
    int paper_1_due_date;
    int paper_2_due_date;
    int paper_3_due_date;
    float estimate_potential;
    NSString * strTopicName;
    NSString * strClassFocus;
    NSString * strTopicDesc;
    NSString * strTodoDesc;
    NSString * strNextLessonDesc;
    NSString * strMilestoneDesc;
    NSString * strAdminDesc;
    NSString * strProgressActualScores;
    NSString * strProgressCarelessScores;
    NSString * strProgressLesson;
    NSString * strProgressCouldveScores;
    NSString * strProgressAttitude;
    NSString * strProgressForgetFulness;
    NSString * strProgressSpeed;
    NSString * strProgressNextMilestone;
    NSString * strName;
    NSString * strDueDate;
    NSString * strReportDate;
    float estimatePotential;
    NSMutableArray * arrLesson;
    NSMutableArray * arrOthersSession;
    NSMutableArray * arrClassFocus;
    NSMutableArray * arrSessionDetail;
    ObjDashboard * objDashboard;
    ObjLesson * objLesson;
    NSDate * reportDate;
}
@property int idx;
@property int progressId;
@property int classSessionId;
@property int paper_1_due_date;
@property int paper_2_due_date;
@property int paper_3_due_date;
@property float estimate_potential;
@property (nonatomic, retain) NSString * strTopicName;
@property (nonatomic, retain) NSString * strClassFocus;
@property (nonatomic, retain) NSString * strTopicDesc;
@property (nonatomic, retain) NSString * strTodoDesc;
@property (nonatomic, retain) NSString * strNextLessonDesc;
@property (nonatomic, retain) NSString * strMilestoneDesc;
@property (nonatomic, retain) NSString * strAdminDesc;
@property (nonatomic, retain) NSString * strProgressActualScores;
@property (nonatomic, retain) NSString * strProgressCarelessScores;
@property (nonatomic, retain) NSString * strProgressLesson;
@property (nonatomic, retain) NSString * strProgressAttitude;
@property (nonatomic, retain) NSString * strProgressForgetFulness;
@property (nonatomic, retain) NSString * strProgressSpeed;
@property (nonatomic, retain) NSString * strProgressNextMilestone;
@property (nonatomic, retain) NSString * strProgressCouldveScores;
@property (nonatomic, retain) NSString * strName;
@property (nonatomic, retain) NSString * strDueDate;
@property (nonatomic, retain) NSString * strReportDate;
@property (nonatomic, retain) NSMutableArray * arrLesson;
@property (nonatomic, retain) NSMutableArray * arrOthersSession;
@property (nonatomic, retain) NSMutableArray * arrClassFocus;
@property (nonatomic, retain) NSMutableArray * arrSessionDetail;
@property (nonatomic, retain) ObjDashboard * objDashboard;
@property (nonatomic, retain) ObjLesson * objLesson;
@property (nonatomic, retain) NSDate * reportDate;
@property float estimatePotential;
@end
