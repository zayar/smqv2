//
//  AdsViewController.m
//  zBox
//
//  Created by Zayar on 5/10/13.
//
//

#import "AdsViewController.h"
#import "zBoxAppDelegate.h"
#import "StringTable.h"
@interface AdsViewController ()

@end

@implementation AdsViewController
@synthesize scrollView,pageControl,myTimer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)onClose:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self hardCodeAdsObject];
    adv_duration = 5;
}

- (void)syncAds{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate showLoadingScreen];
    if (adsRequest == nil) {
        adsRequest = [[SOAPRequest alloc]initWithOwner:self];
    }
    adsRequest.processId = 14;
    [adsRequest syncAds];
}

- (void)viewWillAppear:(BOOL)animated{
    [self syncAds];
    //[self loadTheAds];
}

- (IBAction)onOpen:(id)sender{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    ObjCatalog * objCata = [objAds.arrCatalog objectAtIndex:currentPagingIndex];
    [delegate syncTrack:objCata.idx andString:@"c"];
    NSString* launchUrl = objCata.strCataMedia;
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: launchUrl]];
}

- (void)loadTheAds{
    
    /*NSString * adsImgPath = [delegate.db getFilePath: objAds.strLink];
    if( adsImgPath != nil && (NSNull *) adsImgPath != [NSNull null] && ![adsImgPath isEqualToString:@""] ){
        [imgView setImage: [[UIImage alloc] initWithContentsOfFile: adsImgPath]];
    }
    else{
        [activity startAnimating];
        [objAds.thumbnailCallBacks addObject:imgView];
        [objAds downloadImage:0];
    }*/
    if ([objAds.arrCatalog count]>0){
        
    }
}

- (void) refeshTheSlider{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    NSLog(@"refeshTheSlider: slider count %d",[objAds.arrCatalog count]);
    int count=[objAds.arrCatalog count];
    
    for (UIView * v in scrollView.subviews) {
        if ([v isKindOfClass:[UIImageView class]]) {
            [v removeFromSuperview];
            //[v release];
        }
    }
    
    scrollView.pagingEnabled=TRUE;
    pageControl.numberOfPages=count;
    pageControl.currentPage=0;
    currentPagingIndex=0;
    pageControlUsed=TRUE;
    scrollViewMode2=ScrollViewModePaging2;
    scrollView.delegate=self;
    scrollView.showsHorizontalScrollIndicator=FALSE;
    //[scrollView scrollRectToVisible:CGRectMake(0, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height) animated:YES];
    [scrollView setContentSize:CGSizeMake(count* scrollView.frame.size.width, scrollView.frame.size.height)];
    
    if ([objAds.arrCatalog count]>0){
        ObjCatalog * objCata = [objAds.arrCatalog objectAtIndex:0];
        [delegate syncTrack:objCata.idx andString:@"o"];
    }
    
    for(int i=0;i<[objAds.arrCatalog count];i++){
        
        ObjCatalog * objCata = [objAds.arrCatalog objectAtIndex:i];
        objCata.owner = self;
        
        UIButton * btnMovieDetail=[[UIButton alloc]init];
        [btnMovieDetail setFrame:CGRectMake(i*scrollView.frame.size.width, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
        //[btnMovieDetail setTag:page];
        [btnMovieDetail setTag:i];
        [btnMovieDetail addTarget:self action:@selector(clickPromo:) forControlEvents:UIControlEventTouchUpInside];
        [scrollView addSubview:btnMovieDetail];
        
        pageControl.currentPage=i;
        
        UIImageView * detailImage=[[UIImageView alloc]init];
        [detailImage setFrame:CGRectMake(i*scrollView.frame.size.width, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
        
        NSLog(@"download detail image link %d %@",i,objCata.strCataMediaLink);
        NSString * adsImgPath = [delegate.db getFilePath: objCata.strCataMediaLink];
        if( adsImgPath != nil && (NSNull *) adsImgPath != [NSNull null] && ![adsImgPath isEqualToString:@""] ){
            [detailImage setImage: [[UIImage alloc] initWithContentsOfFile: adsImgPath]];
        }
        else{
            NSLog(@"here we are!!!!!");
            //[cell.activity startAnimating];
            [objCata.detailCallBacks addObject:detailImage];
            [objCata downloadImage:1];
        }
        [scrollView addSubview:detailImage];
    }
    
    if(myTimer ==nil){
       // myTimer=[NSTimer scheduledTimerWithTimeInterval:adv_duration target:self selector:@selector(slideTimely) userInfo:nil repeats:YES];
        slidingIndex=currentPagingIndex;
        [self startCountdown];
        //[scrollView scrollRectToVisible:CGRectMake(slidingIndex*scrollView.frame.size.width, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height) animated:YES];
    }
    
}

- (void) scrollViewWillBeginDragging:(UIScrollView *)scroll {
    
    /*if (myTimer != nil) {
        [myTimer invalidate];
        myTimer = nil;
    }*/
    [self killAllTimer];
    
    //[myTimer invalidate];
    pageControlUsed = FALSE;
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scroll {
    NSLog(@"scrollViewDidEndDecelerating");
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    /*
     if( scrollView.tag == 0 ){
     
     if( pageControlUsed ) return;
     if( scrollViewMode == ScrollViewModePaging ){
     
     CGFloat pageWidth = scroll.frame.size.width;
     int page = floor((scroll.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
     
     if( page != currentPagingIndex ){
     
     objSelectedMovie=[moviesList objectAtIndex:page];
     NSLog(@"Selected Movie title %@",objSelectedMovie.movieName);
     
     pageControl.currentPage = page;
     currentPagingIndex = page;
     
     //[self loadContentsAtPage: (currentPagingIndex-1) forScroll: scroll];
     [self loadContentsAtPage: currentPagingIndex forScroll: scroll];
     //[self loadContentsAtPage: (currentPagingIndex+1) forScroll: scroll];
     
     //[self removeContentsAtPage: (currentPagingIndex-2) forScroll: scroll];
     //[self removeContentsAtPage: (currentPagingIndex+2) forScroll: scroll];
     }
     }
     
     pageControlUsed = FALSE;
     }
     */
    
    CGFloat pageWidth = scroll.frame.size.width;
    int page = floor((scroll.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage=page;
    currentPagingIndex=page;
    //[self slideTimely];
    [self loadContentsAtPage:page forScroll:scroll];
    
    /*if(myTimer==nil){
        myTimer=[NSTimer scheduledTimerWithTimeInterval:adv_duration target:self selector:@selector(slideTimely) userInfo:nil repeats:YES];
        slidingIndex=currentPagingIndex;
        [self startCountdown];
        //[scroll scrollRectToVisible:CGRectMake(slidingIndex*scroll.frame.size.width, scroll.frame.origin.y, scroll.frame.size.width, scroll.frame.size.height) animated:YES];
    }*/
}

- (void) slideTimely{
    slidingIndex++;
    NSLog(@"slideTimely index::: %d",slidingIndex);
    if(slidingIndex>[objAds.arrCatalog count]-1)
        slidingIndex=0;
    //[self loadContentsAtPage:slidingIndex forScroll:scrollView];
    [scrollView scrollRectToVisible:CGRectMake(slidingIndex*scrollView.frame.size.width, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height) animated:YES];
    pageControl.currentPage=slidingIndex;
}

- (void)countdownUpdateMethod:(NSTimer*)theTimer {
    // code is written so one can see everything that is happening
    // I am sure, some people would combine a few of the lines together
    NSDate *currentDate = [NSDate date];
    NSTimeInterval elaspedTime = [currentDate timeIntervalSinceDate:startTime];
    
    NSTimeInterval difference = countdownSeconds - elaspedTime;
    int diffSec = difference;
    if (difference <= 0) {
        [theTimer invalidate];  // kill the timer
        //[startTime release];    // release the start time we don't need it anymore
        difference = 0;
        countdownSeconds = adv_duration;
        [self killAllTimer];
        [self dismissModalViewControllerAnimated:YES];
        //[self startCountdown];
        // set to zero just in case iOS fired the timer late
        // play a sound asynchronously if you like
    }
    // update the label with the remainding seconds
    lblCountDown.text = [NSString stringWithFormat:@"%d", diffSec+1];
}

- (void)startCountdown {
    countdownSeconds = adv_duration;  // Set this to whatever you want
    startTime = [[NSDate date] retain];
    // update the label
    lblCountDown.text = [NSString stringWithFormat:@"%d", countdownSeconds+1];
    
    // create the timer, hold a reference to the timer if we want to cancel ahead of countdown
    // in this example, I don't need it
    countDownTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector     (countdownUpdateMethod:) userInfo:nil repeats:YES];
    // couple of points:
    // 1. we have to invalidate the timer if we the view unloads before the end
    // 2. also release the NSDate if don't reach the end
}

- (void) killAllTimer{
    if (countDownTimer != nil) {
        [countDownTimer invalidate];
        countDownTimer = nil;
    }
    
    if (myTimer != nil) {
        [myTimer invalidate];
        myTimer = nil;
    }
    
    lblCountDown.hidden = TRUE;
}

- (void) scrollViewDidScroll:(UIScrollView *)scroll {
    // NSLog(@"scrollViewDidScroll");
}

- (void) loadContentsAtPage:(int) page forScroll:(UIScrollView *)scroll{
    
    BOOL load = TRUE;
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    
    if( page < 0 || ([objAds.arrCatalog count]<=0)){
        NSLog(@"invalid page index: %d", page);
        return;
    }
    if (page > [objAds.arrCatalog count] - 1){
        NSLog(@"invalid page index: %d", page);
        return;
    }
    
    
    //        for(UIView * v in scroll.subviews){
    //            if( [v isKindOfClass: [MovieView class]] && v.tag == page ){
    //                load = FALSE;
    //                break;
    //            }
    //        }
    
    
    /*
     if( load){
     NSLog(@"load content");
     
     objSelectedMovie = [moviesList objectAtIndex:page];
     float x = (page * scroll.frame.size.width);
     
     MovieView * customView;
     NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MovieView" owner:nil options:nil];
     for(id currentObject in topLevelObjects){
     if([currentObject isKindOfClass:[MovieView class]]){
     customView = (MovieView *) currentObject;
     customView.frame = CGRectMake(x,0,scroll.frame.size.width, scroll.frame.size.height);
     customView.tag = page;
     [objSelectedMovie retain];
     NSLog(@"Loaded movie is %@",objSelectedMovie.movieName);
     customView.objMovie=objSelectedMovie;
     customView.owner=self;
     [customView loadMovieView];
     //customView.owner = self;
     
     NSLog(@"thumb image link: %@ ",objSelectedMovie.thumbImage);
     NSString * imagePath = [delegate.db getFilePath: objSelectedMovie.thumbImage];
     if( imagePath != nil && (NSNull *) imagePath != [NSNull null] && ![imagePath isEqualToString:@""] ){
     [customView.imgThumb setImage: [[UIImage alloc] initWithContentsOfFile: imagePath]];
     }
     else{
     [objSelectedMovie.thumbnailCallBacks addObject:customView.imgThumb];
     [objSelectedMovie downloadImage:0];
     }
     
     //break;
     }
     }
     
     [scroll addSubview: customView];
     NSLog(@"image for page %d added, retainCount: %d", page, [customView retainCount]);
     
     }
     */
     ObjCatalog * objCata = [objAds.arrCatalog objectAtIndex:page];
    [delegate syncTrack:objCata.idx andString:@"o"];
    
    if( load){
        NSLog(@"load content");
        
        objSelectedCatalog = [objAds.arrCatalog objectAtIndex:page];
        
        CGFloat pageWidth = scroll.frame.size.width;
        int page = floor((scroll.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        NSLog(@"current page %d",page);
        pageControl.currentPage=page;
        
        UIButton * btnMovieDetail=[[UIButton alloc]init];
        [btnMovieDetail setFrame:CGRectMake(page*scrollView.frame.size.width, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
        //[btnMovieDetail setTag:page];
        [btnMovieDetail setTag:page];
        [btnMovieDetail addTarget:self action:@selector(clickPromo:) forControlEvents:UIControlEventTouchUpInside];
        [scroll addSubview:btnMovieDetail];
        
        UIImageView * detailImage=[[UIImageView alloc]init];
        [detailImage setFrame:CGRectMake(page*scrollView.frame.size.width, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
        
        //objSelectedSlide.strImgLink=obj.imageLink;
        NSLog(@"download detail image link %d %@",page,objSelectedCatalog.strCataMediaLink);
        NSString * adsImgPath = [delegate.db getFilePath: objSelectedCatalog.strCataMediaLink];
        if( adsImgPath != nil && (NSNull *) adsImgPath != [NSNull null] && ![adsImgPath isEqualToString:@""] ){
            [detailImage setImage: [[UIImage alloc] initWithContentsOfFile: adsImgPath]];
        }
        else{
            //[cell.activity startAnimating];
            NSLog(@"here we are!!!!!");
            [objSelectedCatalog.detailCallBacks addObject:detailImage];
            [objSelectedCatalog downloadImage:1];
        }
        [scrollView addSubview:detailImage];
        [scroll addSubview:detailImage];
        //NSLog(@"image for page %d added", page);
    }
    
    NSLog(@"loadContentsAtPage ends here....");
}

- (void) removeContentsAtPage:(int) pIndex forScroll:(UIScrollView *)scroll{
    
	if( pIndex < [objAds.arrCatalog count]-1 ) return;
    if( pIndex > 0) return;
    
    NSLog(@"trying to remove image %d, from scroll %d", pIndex, scroll.tag);
    
    //UIView * v = nil;
    
    /*for( v in scroll.subviews){
     NSLog(@"...removeContentsAtPage... %d",[scroll.subviews count]);
     if([v isKindOfClass: [MovieView class]] && v.tag == pIndex){
     NSLog(@"before removal: v retainCount: %d", [v retainCount]);
     [v removeFromSuperview];
     [v release];
     v = nil;
     NSLog(@"image %d, from scroll %d is removed", pIndex, scroll.tag);
     }
     }*/
}

- (void) clickPromo:(UIButton *)sender{
/*
    NSLog(@" here is click!!");
    ObjCatalog * objCata = [objAds.arrCatalog objectAtIndex:sender.tag];
    if (![self stringIsEmpty:objCata.strCataMedia shouldCleanWhiteSpace:YES]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: objCata.strCataMedia]];
    }*/
}

- (BOOL) stringIsEmpty:(NSString *) aString shouldCleanWhiteSpace:(BOOL)cleanWhileSpace {
    
    if ((NSNull *) aString == [NSNull null]) {
        return YES;
    }
    
    if (aString == nil) {
        return YES;
    } else if ([aString length] == 0) {
        return YES;
    }
    
    if (cleanWhileSpace) {
        aString = [aString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([aString length] == 0) {
            return YES;
        }
    }
    
    return NO;
}

- (void)hardCodeAdsObject{
    /*if (objAds == nil) {
        objAds = [[ObjAds alloc]init];
    }
    objAds.idx = 1;
    objAds.strLink = @"http://4.bp.blogspot.com/-u03sCGZrjHA/UGCRLhYSZHI/AAAAAAAACmg/6kqLQodJYAU/s1600/Math-pic.png";
    objAds.strName = @"Advertisment";*/
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    //Dispose of any resources that can be recreated.
}

- (void) onJsonLoaded:(NSMutableDictionary *)dics withProcessId:(int)processId{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    if (processId == 14) {
        [delegate hideLoadingScreen];
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        
        NSString * strMessage;
        if (status == STATUS_RETURN_RECORD) {
            objAds = [[ObjAds alloc]init];
            NSMutableArray * arrRawCata=[dics objectForKey:@"catalogs"];
            objAds.adv_duration = [[dics objectForKey:@"advert_duration"]floatValue];
            adv_duration = objAds.adv_duration;
            objAds.arrCatalog = [[NSMutableArray alloc]initWithCapacity:[arrRawCata count]];
            for ( int i =0; i < [arrRawCata count]; i++) {
                NSDictionary * dicCata = [arrRawCata objectAtIndex:i];
                ObjCatalog * objCata = [[ObjCatalog alloc]init];
                objCata.idx = [[dicCata objectForKey:@"catalog_id"] intValue];
                objCata.strCataTitle = [dicCata objectForKey:@"catalog_id"];
                objCata.strCataMediaLink = [dicCata objectForKey:@"catalog_media"];
                //objCata.strCataMediaLink = @"http://4.bp.blogspot.com/-u03sCGZrjHA/UGCRLhYSZHI/AAAAAAAACmg/6kqLQodJYAU/s1600/Math-pic.png";
                objCata.strCataMedia = [dicCata objectForKey:@"catalog_link"];
                objCata.strCataMediaType = [dicCata objectForKey:@"catalog_media_type"];
                [objAds.arrCatalog addObject:objCata];
            }
            // parse ads here
            [self refeshTheSlider];
            //NSLog(@"Ads Object Dic %@",dics);
        }
        else if(status == STATUS_ACTION_FAILED){
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
        [delegate hideLoadingScreen];
    }
}

- (void) onImageDownloadFinish:(int)proccessId imageType:(int) itemType{
    //[activity stopAnimating];
}

- (void) viewWillDisappear:(BOOL)animated{
    if (myTimer != nil) {
        [myTimer invalidate];
        myTimer = nil;
        
        [countDownTimer invalidate];
        countDownTimer = nil;
    }
    
    //[myTimer invalidate];
    //pageControlUsed = FALSE;
}

- (void) dealloc{
    [lblCountDown release];
    [scrollView release];
    [pageControl release];
    [objAds release];
    [activity release];
    [imgView release];
    [adsRequest release];
    [super dealloc];
}

@end
