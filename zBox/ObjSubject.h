//
//  ObjSubject.h
//  zBox
//
//  Created by Zayar Cn on 6/13/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <Foundation/Foundation.h>
/*
 "subject_id": 1,
 "subject_name": "Odio molestie wisi ipsum vel sed, aliquip vel sit nibh sed, vel facilisis sit blandit. Laoreet, sed et.",
 "subject_current_actual_score": 72,
 "subject_difference": 9,
 "exam_p1": 132,
 "exam_p2": 166,
 "exam_p3": 210,
 "objective": 77,
 "estimated_potential": "42.32%",
 "array_progress_report":[
 
 ]
 }
 */

@interface ObjSubject : NSObject
{
    int subject_id;
    NSString * strName;
    NSMutableArray * arrCategories;
    float previousMark;
    float currentMark;
    float couldveMark;
    float differentMark;
    float examp_p1;
    float examp_p2;
    float examp_p3;
    float objectiveMark;
    float estimated_potential;
    NSMutableArray * arrSession;
    NSMutableArray * arrExam;
    NSMutableArray * arrPacing;
    NSString * strTopUpStatus;
}
@property int subject_id;
@property float previousMark;
@property float currentMark;
@property float couldveMark;
@property float differentMark;
@property float examp_p1;
@property float examp_p2;
@property float examp_p3;
@property float objectiveMark;
@property float estimated_potential;
@property (nonatomic, retain)NSString * strName;
@property (nonatomic, retain)NSString * strTopUpStatus;
@property (nonatomic, retain)NSMutableArray * arrCategories;
@property (nonatomic, retain)NSMutableArray * arrSession;
@property (nonatomic, retain)NSMutableArray * arrExam;
@property (nonatomic, retain)NSMutableArray * arrPacing;
@end
