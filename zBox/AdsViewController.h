//
//  AdsViewController.h
//  zBox
//
//  Created by Zayar on 5/10/13.
//
//

#import <UIKit/UIKit.h>
#import "SOAPRequest.h"
#import "ObjAds.h"
#import "ObjCatalog.h"
typedef enum {
	ScrollViewModeNotInitialized2,           // view has just been loaded
	ScrollViewModePaging2,                   // fully zoomed out, swiping
	ScrollViewModeZooming2,                  // zoomed in, panning enabled
} ScrollViewMode2;
@interface AdsViewController : UIViewController
{
    IBOutlet UIImageView * imgView;
    IBOutlet UIActivityIndicatorView * activity;
    SOAPRequest * adsRequest;
    ObjAds * objAds;
    UIScrollView * scrollView;
    UIPageControl * pageControl;
    ScrollViewMode2 scrollViewMode2;
    BOOL pageControlUsed;
    int currentPagingIndex;
    float adv_duration;
    
    NSTimer * myTimer;
    int slidingIndex;
    
    ObjCatalog * objSelectedCatalog;
    IBOutlet UILabel * lblCountDown;
    NSDate * startTime;
    int countdownSeconds;
    
    NSTimer * countDownTimer;
    //IBOutlet UILabel * lblCountDown;
}
- (IBAction)onClose:(id)sender;
@property (nonatomic, retain) NSTimer * myTimer;
@property (nonatomic, retain) IBOutlet UIScrollView * scrollView;
@property (nonatomic, retain) IBOutlet UIPageControl * pageControl;
@end
