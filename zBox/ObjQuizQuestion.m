//
//  ObjQuizQuestion.m
//  zBox
//
//  Created by Zayar on 5/9/13.
//
//

#import "ObjQuizQuestion.h"
#import "zBoxAppDelegate.h"

@implementation ObjQuizQuestion
@synthesize idx,strQuestTitle,strQuestType,strQuestText,strQuestStatus,strAnsType,strAnsText,thumbnailCallBacks,rate_point,detailCallBacks,owner;

- (id) init{
	if( self = [super init] ){
		self.thumbnailCallBacks = [[NSMutableArray alloc] init];
        self.detailCallBacks = [[NSMutableArray alloc]init];
	}
	return self;
}

- (void) downloadImage:(int) processId{
    if (processId == 0) {
        if( thumbnailDownloader != nil ){
            [thumbnailDownloader abortDownload];
            [thumbnailDownloader release];
        }
        
        if( strAnsText != nil && (NSNull *) strAnsText != [NSNull null] ){
            thumbnailDownloader = [[InternetImage alloc] initWithUrl: strAnsText];
            thumbnailDownloader.processId = 0;
            NSLog(@"Ads download link: %@", strAnsText);
        }
        [thumbnailDownloader downloadImage: self];
        thumbnailDownloader.processId = processId;
    }
    if (processId == 1) {
        if( detailDownloader != nil ){
            [detailDownloader abortDownload];
            [detailDownloader release];
        }
        
        if( strQuestText != nil && (NSNull *) strQuestText != [NSNull null] ){
            detailDownloader = [[InternetImage alloc] initWithUrl: strQuestText];
            detailDownloader.processId = 1;
            NSLog(@"Ads download link: %@", strQuestText);
        }
        [detailDownloader downloadImage: self];
        detailDownloader.processId = processId;
    }
    obj_proccessID = processId;
}

- (void)cancelAllDownloads{
	if( thumbnailDownloader != nil ){
		[thumbnailDownloader abortDownload];
	}
}

-(void) internetImageReady:(InternetImage*)downloadedImage{
    NSLog(@"download id %d",downloadedImage.processId);
    if( downloadedImage.Image != nil ){
        zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *tempPath = [paths objectAtIndex:0];
        
        if( downloadedImage.processId == 0 ){
            NSString * imgPath = [tempPath stringByAppendingPathComponent: [NSString stringWithFormat: @"%.0f_%d_news_ads.png", [NSDate timeIntervalSinceReferenceDate] * 1000.0]];
            strThumbnailPath = imgPath;
            [strThumbnailPath retain];
            
            //UIImage * imgDetail = [Utility imageByScalingProportionallyToSize: downloadedImage.Image newsize:CGSizeMake(78,63) resizeFrame:FALSE];
            //UIImage * imgDetail = [Utility forceImageResize:downloadedImage.Image newsize:CGSizeMake(320,60)];
            
            NSData *imageDetailData = [NSData dataWithData:UIImagePNGRepresentation(downloadedImage.Image)];
            [imageDetailData writeToFile:imgPath atomically:YES];
            
            NSLog(@"img thumb Path %@",imgPath);
            
            [delegate.db insertCachedImage:strAnsText  path:imgPath];
            
            if( self.thumbnailCallBacks != nil ){
                NSLog(@"Ads thumbnail call backs count %d",[self.thumbnailCallBacks count]);
                for(int i=0; i<[self.thumbnailCallBacks count]; i++){
                    NSLog(@"Ads image create........");
                    UIImageView * imgView = [self.thumbnailCallBacks objectAtIndex: i];
                    
                    if( imgView != nil && (NSNull *) imgView != [NSNull null] ){
                        if( imgView.image != nil && (NSNull *) imgView.image != [NSNull null] ){
                            [imgView.image release];
                            imgView.image = nil;
                        }
                        imgView.hidden = FALSE;
                        [imgView setImage: [[UIImage alloc] initWithContentsOfFile: imgPath]];
                    }
                }
            }
            [thumbnailDownloader release];
            thumbnailDownloader = nil;
            //[owner onImageDownloadFinish:obj_CellIndex imageType:itemType];
            [owner onImageDownloadFinish:obj_proccessID imageType:itemType];
        }
        if( downloadedImage.processId == 1 ){
            NSString * imgPath = [tempPath stringByAppendingPathComponent: [NSString stringWithFormat: @"%.0f_%d_detail.png", [NSDate timeIntervalSinceReferenceDate] * 1000.0]];
            strDetailPath = imgPath;
            [strDetailPath retain];
            
            //UIImage * imgDetail = [Utility imageByScalingProportionallyToSize: downloadedImage.Image newsize:CGSizeMake(78,63) resizeFrame:FALSE];
            //UIImage * imgDetail = [Utility forceImageResize:downloadedImage.Image newsize:CGSizeMake(320,60)];
            
            NSData *imageDetailData = [NSData dataWithData:UIImagePNGRepresentation(downloadedImage.Image)];
            [imageDetailData writeToFile:imgPath atomically:YES];
            
            NSLog(@"img thumb Path %@",imgPath);
            
            [delegate.db insertCachedImage:strQuestText  path:imgPath];
            
            if( self.detailCallBacks != nil ){
                NSLog(@"Ads thumbnail call backs count %d",[self.detailCallBacks count]);
                for(int i=0; i<[self.detailCallBacks count]; i++){
                    NSLog(@"Ads image create........");
                    UIImageView * imgView = [self.detailCallBacks objectAtIndex: i];
                    
                    if( imgView != nil && (NSNull *) imgView != [NSNull null] ){
                        if( imgView.image != nil && (NSNull *) imgView.image != [NSNull null] ){
                            [imgView.image release];
                            imgView.image = nil;
                        }
                        imgView.hidden = FALSE;
                        [imgView setImage: [[UIImage alloc] initWithContentsOfFile: imgPath]];
                    }
                }
            }
            [detailCallBacks release];
            detailCallBacks = nil;
            //[owner onImageDownloadFinish:obj_CellIndex imageType:itemType];
            [owner onImageDownloadFinish:obj_proccessID imageType:itemType];
        }
    }
}

- (void)dealloc{
    [strThumbnailPath release];
	[thumbnailCallBacks release];
	[thumbnailDownloader release];
    [strQuestTitle release];
    [strQuestType release];
    [strQuestText release];
    [strQuestStatus release];
    [strAnsType release];
    [strAnsText release];
    [super dealloc];
}
@end
