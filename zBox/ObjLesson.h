//
//  ObjLesson.h
//  zBox
//
//  Created by Zayar on 5/12/13.
//
//

#import <Foundation/Foundation.h>
/* 
 {
 "actual_score": "73",
 "careless_score": "13",
 "couldve_score": "86",
 "topic": [
 {
 "topic_name": "Topic A"
 },
 {
 "topic_name": "Topic B"
 },
 {
 "topic_name": "Topic C"
 }
 */
@interface ObjLesson : NSObject
{
    int actual_score;
    int careless_score;
    int couldve_score;
    NSString * strArrayTopic;
}
@property int actual_score;
@property int careless_score;
@property int couldve_score;
@property (nonatomic, retain) NSString * strArrayTopic;
@end
