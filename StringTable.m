//
//  StringTable.m
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "StringTable.h"


@implementation StringTable

NSString * const DBNAME	= @"smath.sqlite";
NSString * const APP_TITLE = @"SMATHS";
NSString * const APP_ID = @"---";
NSString * const BASE_LINK= @"http://www.iwh.com.sg/tsmathv2/";
NSString * const GENR_KEY_LINK = @"modules/client/_ext_generate_key.php";
NSString * const LOGIN_LINK=@"modules/client/_ext_login.php";
NSString * const SUBJECT_CRITERIA_LINK= @"modules/subject/_ext_read_subject.php";
NSString * const CHECK_LOGIN_SESSION_LINK = @"modules/client/_ext_check_user_session.php";
NSString * const CHECK_USER_POINT_LINK=@"modules/point/_ext_read_point.php";
NSString * const SUBMIT_QUESTION_LINK= @"modules/question/_ext_create_question.php";
NSString * const USER_QUESTION_DETAIL_LINK= @"modules/question/_ext_read_question.php";
NSString * const CHANGE_PASSWORD_LINK= @"modules/client/_ext_change_password.php";
NSString * const RATING_LINK= @"modules/question/_ext_rate_question.php";
NSString * const UPDATE_OBJECTIVE_SCORE_LINK = @"modules/progress/_ext_write_objective.php";

NSString * const CATALOG_LINK= @"modules/catalog/_ext_read_catalog.php";
NSString * const CATALOG_TRACK_LINK= @"modules/catalog/_ext_open_catalog.php";
NSString * const QUIZZ_LINK= @"modules/quiz/_ext_read_quiz.php";
NSString * const QUIZZ_QUESTION_LINK =@"modules/quiz_question/_ext_read_quiz_question.php";

NSString * const PROGRESS_LOCAL_REPORT_LINK= @"http://localhost/smath/progressreport_2.json";
NSString * const PROGRESS_REPORT_LINK= @"modules/progress/_ext_read_progress.php";
NSString * const ADS_LINK= @"modules/catalog/_ext_read_advert.php";
NSString * const FREEZE_LINK= @"modules/client/_ext_freeze_account.php";
NSString * const CHECK_SUPERVISOR_LINK= @"modules/client/_ext_check_supervisor_password.php";
NSString * const UNFREEZE_LINK= @"modules/client/_ext_unfreeze_account.php";
NSString * const CATALOG_REGISTER_LINK=@"modules/catalog_response/_ext_create_catalog_response.php";
NSString * const RESET_PASS_LINK=@"modules/client/_ext_reset_password.php";
NSString * const QUIZ_CONSULT_LINK=@"modules/quiz_consultation/_ext_create_quiz_consultation.php";
NSString * const QUIZ_RATE_LINK=@"modules/three_part_pacing/_ext_rate_quiz_question.php";

NSString * const ANSWER_TYPE_IMAGE = @"I";
NSString * const ANSWER_TYPE_VIDEO = @"V";
NSString * const ANSWER_TYPE_TEXT = @"T";
NSString * const ANSWER_TYPE_NULL = @"null";

NSString * const FREEZE_TYPE = @"F";
NSString * const UNFREEZE_TYPE = @"U";

NSString * const APN_SERVER_PATH  = @"test.balancedconsultancy.com.sg/tsmaths/apn";

int const STATUS_ACTION_SUCCESS = 1;
int const STATUS_RETURN_RECORD = 2;
int const STATUS_ACTION_FAILED = 3;
int const STATUS_SESSION_EXPIRED = 5;
int const STATUS_NO_RECORD_FOUND = 6;

NSString * const ANSWER_STATUS_PENDING = @"P";
NSString * const ANSWER_STATUS_ANSWERED = @"A";
NSString * const ANSWER_STATUS_REJECTED = @"R";

NSString * const PROGRESS_STATUS_GOOD = @"1";
NSString * const PROGRESS_STATUS_ALERT = @"3";
NSString * const PROGRESS_STATUS_MEDIOCRE = @"2";

NSString * const STATUS_TOPUP= @"Y";
NSString * const STATUS_NOTTOPUP=@"N";

double const CACHE_DURATION		= 86400 * 5.0;
@end
