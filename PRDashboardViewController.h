//
//  PRDashboardViewController.h
//  zBox
//
//  Created by Zayar on 5/9/13.
//
//

#import <UIKit/UIKit.h>
#import "ObjSubSession.h"
#import "ObjDashboard.h"

@interface PRDashboardViewController : UIViewController
{
    IBOutlet UIImageView * imgBgView;
    IBOutlet UILabel * lblTitle;
    ObjSubSession * objSession;
    ObjDashboard * objDashboard;
    
    IBOutlet UILabel * lblLesson1;
    IBOutlet UILabel * lblLesson2;
    IBOutlet UILabel * lblAttitude1;
    IBOutlet UILabel * lblAttitude2;
    IBOutlet UILabel * lblCarelessness1;
    IBOutlet UILabel * lblCarelessness2;
    IBOutlet UILabel * lblSpeed1;
    IBOutlet UILabel * lblSpeed2;
    IBOutlet UILabel * lblMilestone1;
    IBOutlet UILabel * lblMilestone2;
}
- (IBAction)onBack:(id)sender;
@property (nonatomic, retain) ObjSubSession * objSession;
@end
