//
//  SessionMenuView.h
//  zBox
//
//  Created by Zayar on 3/18/13.
//
//

#import <UIKit/UIKit.h>
#import "ObjSubSession.h"
#import "ObjSubject.h"
@protocol SessionMenuViewDelegate
- (void) onSessionMenuViewCancel;
- (void) showDashboardWith:(ObjSubSession *)objSession;
- (void) showLessonWith:(ObjSubSession *)objSession;
- (void) showTodoWith:(ObjSubSession *)objSession;
- (void) showOthersWith:(ObjSubSession *)objSession;
- (void) show3PartPacingWith:(ObjSubSession *)objSession andSubject:(ObjSubject *)objSub;
- (void) showClassFocusWith:(ObjSubSession *)objSession andSubject:(ObjSubject *)objSub;
@end
@interface SessionMenuView : UIView
{
    id<SessionMenuViewDelegate> owner;
    IBOutlet UIImageView * imgBgView;
    IBOutlet UILabel * lblTitle;
    IBOutlet UILabel * lblExamDate1;
    IBOutlet UILabel * lblExamDate2;
    IBOutlet UILabel * lblExamDate3;
    IBOutlet UILabel * lblExamPotential;
    NSMutableArray * arrMenu;
    NSMutableArray * arrAnnex;
    IBOutlet UITableView * tbl;
    ObjSubSession * objSubSession;
    ObjSubject * objSubject;
}
@property id<SessionMenuViewDelegate> owner;
- (void) sessionMenuViewWillAppear:(ObjSubSession *)obj andSubject:(ObjSubject *)objSubj;

@end
