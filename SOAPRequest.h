//
//  SOAPRequest.h
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ObjUser.h"
#import "ObjAnswer.h"
#import "ObjCatalog.h"
#import "ObjQuizz.h"
#import "ObjQuizQuestion.h"
@protocol SOAPRequestDelegate
- (void) onDataLoad: (int) processId withStatus:(int) status;
- (void) onErrorLoad: (int) processId;
- (void) onJsonLoaded:(NSMutableDictionary *) dics;
- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId;
- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId andResponsestatus:(int)status;
@end

typedef enum {
	REQUEST_TYPE_GENR_KEY = 1,
    REQUEST_TYPE_CHECK_SESSION = 2,
    REQUEST_TYPE_LOGIN = 3,
    REQUEST_TYPE_GET_POINT = 4,
    REQUEST_TYPE_SUBJECT_CRITEA = 5,
    REQUeST_TYPE_SUBMIT_Q = 6,
    REQUEST_TYPE_GET_QUESTION = 7,
    REQUEST_TYPE_PASSWORD_SUBMIT = 8,
    REQUEST_TYPE_RATE_SUBMIT = 9,
    REQUEST_TYPE_PROGRESS_REPORT = 10,
    REQUEST_TYPE_CATALOG = 11,
    REQUEST_TYPE_CATALOG_TRACK=12,
    REQUEST_TYPE_QUIZZ = 13,
    REQUEST_TYPE_QUIZZ_QUESTION = 14,
    REQUEST_TYPE_ADS = 15,
    REQUEST_TYPE_FREEZE = 16,
    REQUeST_TYPE_CHECK_SUPERVISOR = 17,
    REQUEST_TYPE_UNFREEZE = 18,
    REQUEST_TYPE_CATALOG_REGISTER = 19,
    REQUEST_TYPE_RESET_PASS = 20,
    REQUEST_TYPE_QUIZZ_CREAT_CONSULT = 21,
    REQUEST_TYPE_QUIZZ_RATE = 22
} RequestType;

@interface SOAPRequest : NSObject<SOAPRequestDelegate> {
    NSMutableData * responseData;
    
    int processId;
    id <SOAPRequestDelegate> owner;
    
    RequestType rType;
    int intResponseStatusCode;
}

@property RequestType rType;
@property (nonatomic, retain) NSMutableData * responseData;
@property (assign) id<SOAPRequestDelegate> owner;
@property int processId;

- (id)initWithOwner:(id)del;
- (NSString *) urlencode: (NSString *) url;
- (void) syncGenerateKey;
- (void) syncGenerateKeyWithToken:(NSString*) token;
- (void) syncAndCheckTheSessionKey;
- (void) syncLoginWithUser:(ObjUser *)objUser;
- (void) syncFreezeWithUser:(ObjUser *)objUser;
- (void) syncUserPoint;
- (void) syncSubjectAndCritea;
- (void) syncSubmitQuestion:(UIImage *)img andSubjectId:(int)subj_id andCategoryId:(int)cate_id andSubjectName:(NSString *)subName andCategoryName: (NSString *) categoryName;
- (void) syncGetUserQA;
- (NSString *)encodePasswordWithSHA1:(NSString *)str;
- (void) syncNewPasswordWithUser:(ObjUser *)objUser;
- (NSString*)base64forData:(NSData*)theData;
- (void) syncRateWithAnswer:(ObjAnswer *)objAns;
- (void) syncProgressReport;
- (void) syncCatalog;
- (void) syncCatalogTrackWith:(int)cataId;
- (void) syncQuizz;
- (void) syncQuizzQuestionWith:(int)quizId;
- (void) syncAds;
- (void) syncSubmitObjectiveScore:(NSString *) score withProgressReportId:(NSString *) progressReportId;
- (void) syncCatalogTrackWith:(int)cataId andResponseType:(NSString *)strType;
- (void) syncSupervieorPassWith:(NSString *) strSPass;
- (void) syncUnFreezeWithUser:(ObjUser *)objUser;
- (void) syncRegisterWithCata:(ObjCatalog *)objCata;
- (void) syncResetPassWith:(NSString *) strUName;
- (void) syncCreateQuizzConsultWithUser:(ObjQuizz *)objQuizz;
- (void) syncRateWithQuiz:(ObjQuizQuestion *)objAns;
@end
