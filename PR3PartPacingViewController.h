//
//  PR3PartPacingViewController.h
//  zBox
//
//  Created by Zayar on 5/9/13.
//
//

#import <UIKit/UIKit.h>
#import "ObjSubSession.h"
#import "ObjSubject.h"
@interface PR3PartPacingViewController : UIViewController <UITextFieldDelegate>
{
    IBOutlet UIImageView * imgBgView;
    IBOutlet UILabel * lblTitle;
    ObjSubSession * objSession;
    ObjSubject * objSubject;
    
    IBOutlet UIScrollView * scrollAlert;
    IBOutlet UIScrollView * scrollMediocre;
    IBOutlet UIScrollView * scrollGood;
    IBOutlet UILabel * lblObjective;
    IBOutlet UILabel * lblEastimateScore;
    IBOutlet UILabel * lblGood;
    IBOutlet UILabel * lblMedicro;
    IBOutlet UITextField * lblObjectiveScore;
    IBOutlet UILabel * lblAlert;
    IBOutlet UIButton * updateButton;
    IBOutlet UIScrollView * scrollView;
}
@property (nonatomic,retain) ObjSubSession * objSession;
@property (nonatomic,retain) ObjSubject * objSubject;

- (IBAction)updateScore:(id)sender;

@end
