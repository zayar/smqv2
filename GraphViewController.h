//
//  GraphViewController.h
//  zBox
//
//  Created by Zayar on 5/12/13.
//
//

#import <UIKit/UIKit.h>
#import "ObjSubject.h"
#import "CorePlot-CocoaTouch.h"
#import "EskBarPlot.h"
#import "EskLinePlot.h"
#import "DemoTableController.h"
#import "EskLine2TouchPlot.h"

//#import "GraphViewController.h"
@class GraphViewController;
@protocol GraphViewDelegate
- (void) onGraphViewCancel:(GraphViewController *)viewController;
@end
@interface GraphViewController : UIViewController <EskBarPlotDelegate, EskLinePlotDelegate>
{
    id<GraphViewDelegate> owner;
    ObjSubject * objSubject;
    IBOutlet UIButton * btnClose;
    IBOutlet UIButton * btnSet;
    IBOutlet UIButton * btnSelector;
    CPTScatterPlot *aaplPlot;
    
    IBOutlet UILabel * capObjective;
    IBOutlet UILabel * capCouldve;
    IBOutlet UILabel * capActual;
    IBOutlet UILabel * lblObjective;
    IBOutlet UILabel * lblCouldve;
    IBOutlet UILabel * lblActual;
    IBOutlet UILabel * lblDiff;
    
@private
    EskBarPlot *barPlot;
    EskLinePlot *linePlot;
    EskLine2TouchPlot * linePlot2;
    
}
@property id<GraphViewDelegate> owner;
@property (nonatomic, retain) ObjSubject * objSubject;
@property (nonatomic, retain) IBOutlet CPTGraphHostingView *lineHostingView;
@property (nonatomic, retain) IBOutlet CPTGraphHostingView *barHostingView;
@property (nonatomic, retain) IBOutlet DemoTableController *demoTableViewController;
- (void) showTheLabels:(BOOL) show;
- (void)showResultLabel:(NSString *)str andHide:(BOOL)show;
-(IBAction)showActionSheet:(id)sender forEvent:(UIEvent*)event;
@end
