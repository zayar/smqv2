//
//  CustomPhotoAlbumTableCell.m
//  zBox
//
//  Created by Zayar Cn on 6/11/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "CustomPhotoAlbumTableCell.h"

@implementation CustomPhotoAlbumTableCell
@synthesize imgThumbView,lblName;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc{
    [imgThumbView release];
    [lblName release];
    [super dealloc];
}

@end
