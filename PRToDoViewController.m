//
//  PRToDoViewController.m
//  zBox
//
//  Created by Zayar on 5/9/13.
//
//

#import "PRToDoViewController.h"
#import "ObjSubSession.h"
#import "ObjSessionDetail.h"

@interface PRToDoViewController ()

@end

@implementation PRToDoViewController
@synthesize objSession;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSString * strPath = [NSString stringWithFormat:@"%@/img_vs_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];
    [imgBgView release];
}

- (void)viewWillAppear:(BOOL)animated{
    [self loadTodo:objSession];
}

- (void) loadTodo:(ObjSubSession *)obj{
    lblTopicName.text = objSession.strTodoDesc;
    ObjSessionDetail * objSecDetail = [obj.arrSessionDetail objectAtIndex:0];
    NSArray * commands = nil;
    NSMutableArray * arrTopic = nil;
    if( [ objSecDetail.strTodos rangeOfString:@","].location != NSNotFound ){
        
        commands = [objSecDetail.strTodos componentsSeparatedByString:@","];
        arrTopic = [[NSMutableArray alloc]initWithCapacity:[commands count]];
        //int count = -1;
        for(NSString * strCateName in commands){
            [arrTopic addObject:strCateName];
        }
    }
    
    if([arrTopic count]>0){
        int y=0;
        int i=0;
        for(NSString * str in arrTopic){
            UILabel * lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, y, scrollTodo.frame.size.width, 30)];
            if(i%2 == 0){
                lbl.backgroundColor = [UIColor lightGrayColor];
            }
            else{
                lbl.backgroundColor = [UIColor whiteColor];
            }
            lbl.text = str;
            lbl.font = [UIFont systemFontOfSize:13];
            [scrollTodo addSubview:lbl];
            y += lbl.frame.size.height;
            i++;
            [lbl release];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBack:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)dealloc{
    [imgBgView release];
    [lblTitle release];
    [lblTopicName release];
    [objSession release];
    [super dealloc];
}

@end
