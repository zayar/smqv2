//
//  SettingView.h
//  zBox
//
//  Created by Zayar Cn on 6/5/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOAPRequest.h"
@protocol ResetViewDelegate 
-(void) onResetViewCancel;
-(void) onResetViewSave;
@end

@interface ResetView : UIView<SOAPRequestDelegate>
{
    IBOutlet UIImageView * imgBgView;
    IBOutlet UITextField * txtLoginName;
    
    IBOutlet UIScrollView * scrollView;
    id<ResetViewDelegate> owner;
    SOAPRequest * submitResetPassRequest;
}
@property (assign) id<ResetViewDelegate> owner;
-(void)settingViewViewDidLoad;
-(IBAction)onBack:(id)sender;
-(IBAction)onSave:(id)sender;
- (void)settingViewWillAppear;
@end
