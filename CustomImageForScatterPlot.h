//
//  CustomImageForScatterPlot.h
//  zBox
//
//  Created by Zayar on 5/26/13.
//
//

#import <Foundation/Foundation.h>
 #import "CPTLayer.h"

@interface CustomImageForScatterPlot : CPTLayer
{
    UIImage *_image;
    
}
-(id)initWithImage:(UIImage *)image;

@end
