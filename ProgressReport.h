//
//  ProgressReport.h
//  zBox
//
//  Created by Zayar on 3/17/13.
//
//

#import <UIKit/UIKit.h>
#import "SOAPRequest.h"
#import "PopUpOverlay.h"
#import "ObjSubSession.h"
#import "ObjSubject.h"

#import "CorePlot-CocoaTouch.h"
#import "EskBarPlot.h"
#import "EskLinePlot.h"
#import "DemoTableController.h"
#import "EskLine2TouchPlot.h"
#import "ExpandButtonView.h"
#import "TestView.h"
#import "StockTextButtonBarView.h"

@protocol ProgressReportDelegate
- (void) onProgressReportViewCancel;
- (void) onSelectSession:(ObjSubSession *)objSe withSubject:(ObjSubject *)objSubject;
- (void) onGraph:(ObjSubject *)objSubject;
@end
@interface ProgressReport : UIView{
    id<ProgressReportDelegate> owner;
    IBOutlet UIImageView * imgBgView;
    SOAPRequest * getProgRepoRequest;
    NSMutableArray * arrSolution;
    NSMutableArray * arrSession;
    IBOutlet UITableView * tbl;
    IBOutlet UITableView * tblSession;
    IBOutlet UIButton * btnBack;
    ObjSubject * objSelectedSubject;
    
    ///for landscape view
    IBOutlet UIButton * btnSelector;
    IBOutlet UILabel * lblBg;
    IBOutlet UILabel * capObjective;
    IBOutlet UILabel * capCouldve;
    IBOutlet UILabel * capActual;
    IBOutlet UILabel * lblObjective;
    IBOutlet UILabel * lblCouldve;
    IBOutlet UILabel * lblActual;
    IBOutlet UILabel * lblDiff;
    IBOutlet UILabel * lblCurrentScore;
    IBOutlet UILabel * lblCurrentSubject;
    IBOutlet UILabel * lblCurrentDiff;
    
    ExpandButtonView * expandButtonView;
    BOOL isExpandButtonShow;
    UIInterfaceOrientation oreintationChanges;
    StockTextButtonBarView * stockBtnBarView;
@private
    EskBarPlot *barPlot;
    EskLinePlot *linePlot;
    EskLine2TouchPlot * linePlot2;
}
@property (nonatomic, retain) IBOutlet CPTGraphHostingView *lineHostingView;
@property (nonatomic, retain) IBOutlet TestView * touchLayerView;
@property id<ProgressReportDelegate> owner;
- (void)progRepoViewWillAppear;
- (void)progRepoViewDidLoad;
- (IBAction)onCancel:(id)sender;
- (void)progRepoViewWillDisAppear;
@end
