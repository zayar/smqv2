//
//  SessionMenuView.m
//  zBox
//
//  Created by Zayar on 3/18/13.
//
//

#import "SessionMenuView.h"
#import "ObjSubSession.h"
#import "zBoxAppDelegate.h"
#import "ObjSubject.h"
#import "PRDashboardViewController.h"
#import "ObjExam.h"

@implementation SessionMenuView
@synthesize owner;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)sessionMenuViewDidLoad{
    NSString * strPath = [NSString stringWithFormat:@"%@/img_vs_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];
    [imgBgView release];

    [self hardCodeMenu];
}

- (void)hardCodeMenu{
    arrMenu = [[NSMutableArray alloc]initWithCapacity:4];
    arrAnnex = [[NSMutableArray alloc] initWithCapacity:2];
    [arrMenu addObject:@"Dashboard"];
    [arrMenu addObject:@"Lesson"];
    [arrMenu addObject:@"To Do"];
    [arrMenu addObject:@"Others"];
    [arrAnnex addObject:@"Next Major Paper"];
    [arrAnnex addObject:@"Class Focus"];
}

- (void) sessionMenuViewWillAppear:(ObjSubSession *)obj andSubject:(ObjSubject *)objSubj{
    objSubSession = obj;
    objSubject = objSubj;
    lblTitle.text = obj.strName;

    /*int i=0;
    for (ObjExam * objExam in objSubject.arrExam) {
        if (i == 0) {
            int intDate = [self numberOfDaysUntil:objExam.strDate];
            lblExamDate1.text = [NSString stringWithFormat:@"%d",intDate];
        }
        if (i == 1) {
            int intDate = [self numberOfDaysUntil:objExam.strDate];
            lblExamDate2.text = [NSString stringWithFormat:@"%d",intDate];
        }
        if (i == 2) {
            int intDate = [self numberOfDaysUntil:objExam.strDate];
            lblExamDate3.text = [NSString stringWithFormat:@"%d",intDate];
        }
        i++;
    }*/
    lblExamDate1.text = [NSString stringWithFormat:@"%0.0f", objSubject.examp_p1];
    lblExamDate2.text = [NSString stringWithFormat:@"%0.0f", objSubject.examp_p2];
    lblExamDate3.text = [NSString stringWithFormat:@"%0.0f", objSubject.examp_p3];
    lblExamPotential.text = [NSString stringWithFormat:@"%0.2f",objSubject.estimated_potential];

}

- (NSInteger)numberOfDaysUntil:(NSString *)strEndDate{
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    //NSDate *startDate = [f dateFromString:[NSDate date]];
    //NSDate *endDate = [f stringFromDate:endDate];
    NSString * strStartDate = [f stringFromDate:[NSDate date]];
    NSDate * startDate = [f dateFromString:strStartDate];
    NSDate * endDate = [f dateFromString:strEndDate];
    [f release];
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];

    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit fromDate:startDate toDate:endDate options:0];

    return [components day];
}

- (IBAction)onCancel:(id)sender{
    //[self progRepoViewWillAppear];
    [owner onSessionMenuViewCancel];
}

////TableView Datasource & Delegate////
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return [arrMenu count];
    }
    else if (section == 1) {
        return [arrAnnex count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        //zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
        NSString * strName = [arrMenu objectAtIndex:indexPath.row];
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cellindent = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cellindent == nil) {
            cellindent = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }

        UIFont * cfont = [UIFont fontWithName:@"BradyBunchRemastered" size:18.0];
        //UIFont * sfont = [UIFont fontWithName:@"BradyBunchRemastered" size:15.0];
        cellindent.textLabel.font = cfont;
        cellindent.textLabel.text = strName;
        //cell.lblName.shadowColor = [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.5];

        //cell.lblTitle.t
        cellindent.textLabel.shadowOffset = CGSizeMake(2, 2);
        cellindent.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

        //cell.lblActualScore.text = [NSString stringWithFormat:@"%0.2f",objS.str];
        //cell.lblPreviousScore.text = [NSString stringWithFormat:@"%0.2f",objS.previousMark];

        return cellindent;
    }
    else if (indexPath.section == 1) {
        //zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
        NSString * strName = [arrAnnex objectAtIndex:indexPath.row];
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cellindent = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cellindent == nil) {
            cellindent = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }

        UIFont * cfont = [UIFont fontWithName:@"BradyBunchRemastered" size:18.0];
        //UIFont * sfont = [UIFont fontWithName:@"BradyBunchRemastered" size:15.0];
        cellindent.textLabel.font = cfont;
        cellindent.textLabel.text = strName;
        //cell.lblName.shadowColor = [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.5];

        //cell.lblTitle.t
        cellindent.textLabel.shadowOffset = CGSizeMake(2, 2);
        cellindent.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

        //cell.lblActualScore.text = [NSString stringWithFormat:@"%0.2f",objS.str];
        //cell.lblPreviousScore.text = [NSString stringWithFormat:@"%0.2f",objS.previousMark];

        return cellindent;
    }
    return nil;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 37;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //ObjAnswer * obj = [arrSolution objectAtIndex:[indexPath row]];
    /*if (![obj.strQuestionStatus isEqualToString:ANSWER_STATUS_PENDING]) {
     if ([obj.strQuestAnsType isEqualToString: ANSWER_TYPE_IMAGE]) {
     [owner showQuizPicPhotoBrowser:obj.strQuestAns andTitle:(NSString *)obj.strQuestionTitle];
     }
     else{
     [self onImageClick:obj andIsImage:FALSE];
     }
     }*/
    //[owner showQuizSecondView];
    /*if (tableView.tag == 0) {
        ObjSubject * obj = [arrSolution objectAtIndex:[indexPath row]];
        arrSession = obj.arrSession;
        [tblSession reloadData];
    }
    else if (tableView.tag ==1){
        ObjSubSession * objSe = [arrSession objectAtIndex:[indexPath row]];
        [owner onSelectSession:objSe];
    }*/

    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [owner showDashboardWith:objSubSession];
        }
        if (indexPath.row == 1) {
            [owner showLessonWith:objSubSession];
        }
        if (indexPath.row == 2) {
            [owner showTodoWith:objSubSession];
        }
        if (indexPath.row == 3) {
            [owner showOthersWith:objSubSession];
        }
    }
    else if(indexPath.section ==1){
        if (indexPath.row == 0) {
            [owner show3PartPacingWith:objSubSession andSubject:objSubject];
        }
        if (indexPath.row == 1) {
            [owner showClassFocusWith:objSubSession andSubject:objSubject];
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        UILabel * lblHeader = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 100, 30)];
        lblHeader.text = @"Annex";
        lblHeader.backgroundColor = [UIColor clearColor];
        lblHeader.textColor = [UIColor whiteColor];
        lblHeader.textColor = [UIColor blackColor];
        return lblHeader;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30.0;
}

- (void)dealloc{
    [lblExamPotential release];
    [lblExamDate1 release];
    [lblExamDate2 release];
    [lblExamDate3 release];
    [arrMenu release];
    [arrAnnex release];
    [tbl release];
    [super dealloc];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
