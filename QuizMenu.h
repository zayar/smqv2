//
//  SMQMenu.h
//  zBox
//
//  Created by Zayar Cn on 6/1/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol QuizDelegate
-(void) onQuizMenuCancel;
-(void) onQuizMenuConsult;
-(void) onQuizMenuMaterial;
@end

@interface QuizMenu : UIView
{
    IBOutlet UIImageView * imgMenuBgView;
    id<QuizDelegate> owner;
}
@property(assign) id<QuizDelegate> owner;
- (void)SMQViewDidLoad;
@end
