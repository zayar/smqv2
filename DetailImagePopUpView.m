//
//  DetailImagePopUpView.m
//  NewsHub
//
//  Created by Zayar Cn on 5/9/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "DetailImagePopUpView.h"
#import <QuartzCore/QuartzCore.h>

@implementation DetailImagePopUpView
@synthesize imgDetailView,lblImgDes,activity,imgBgView,btnClose,owner;
@synthesize imageList,imgScrollView,webView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        imageList =[[NSMutableArray alloc]init];
    }
    return self;
}

- (void) preLoadView{
    /*NSString * strNavImagePath = [NSString stringWithFormat:@"%@/bgImg_message.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imageNavBg = [[UIImage alloc] initWithContentsOfFile:strNavImagePath];
    [imgDesBgView setImage:imageNavBg];
    [imageNavBg release];*/
    
    NSString * strBgPath = [NSString stringWithFormat:@"%@/popupbg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imageBg = [[UIImage alloc] initWithContentsOfFile:strBgPath];
    [imgBgView setImage:imageBg];
    [imageBg release];
    
    [imgDetailView.layer setCornerRadius:5.0f];
    [imgDetailView.layer setMasksToBounds:YES];
    
    [webView.layer setCornerRadius:5.0f];
    [webView.layer setMasksToBounds:YES];
    
    [imgScrollView.layer setCornerRadius:5.0f];
    [imgScrollView.layer setMasksToBounds:YES];
    UIFont * cfont = [UIFont fontWithName:@"BradyBunchRemastered" size:20.0];
    lblImgDes.textColor = [UIColor whiteColor];
    lblImgDes.font = cfont;
}

- (void) loadWebView:(NSString *) strURL{
    NSLog(@"strURL %@",strURL);
    NSString *embedHTML = @"\
    <html><head>\
    <style type=\"text/css\">\
    body {\
    background-color: transparent;\
    color: white;\
    }\
    </style>\
    </head><body style=\"margin:0\">\
    <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
    width=\"%0.0f\" height=\"%0.0f\"></embed>\
    </body></html>";
    
    NSString *html = [NSString stringWithFormat:embedHTML, strURL, webView.frame.size.width, webView.frame.size.height];
    [webView loadHTMLString:html baseURL:nil];
}

- (IBAction)onClose:(id)sender{
    [owner onBtnCloseClick];
}

- (void) dealloc{
    [webView release];
    [imgScrollView release];
    [imageList release];
    [btnClose release];
    [imgBgView release];
    [activity release];
    [lblImgDes release];
    //[imgDesBgView release];
    [imgDetailView release];
    [super dealloc];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
