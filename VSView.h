//
//  VSView.h
//  zBox
//
//  Created by Zayar Cn on 5/11/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOAPRequest.h"
#import "DetailImagePopUpView.h"
@protocol VSViewDelegate 
- (void) showPicPhotoBrowser:(NSString *)picURL andTitle:(NSString *)picTitle;
- (void) onVSViewCancel;
@end
@interface VSView : UIView<DetailImagePopUpViewDelegate,UIWebViewDelegate>
{
    //IBOutlet UIWebView * webView;
    id<VSViewDelegate> owner;
    IBOutlet UIImageView * imgBgView;
    IBOutlet UITableView * tbl;
    DetailImagePopUpView * detailImgPopUpView;
    NSMutableArray * arrSolution;
    SOAPRequest * getQuestionRequest;
    SOAPRequest * rateRequest;
    NSArray *_photos;
}
@property (nonatomic, retain) NSArray *photos;
@property (nonatomic, retain) UITableView *tbl;
@property (assign) id<VSViewDelegate> owner;
- (void) loadWebView:(NSString *) strURL;
- (IBAction)onCancel:(id)sender;
- (void)vsViewDidLoad;
- (void)vsViewWillAppear;
- (void) vsViewWillDisAppear;
@end
