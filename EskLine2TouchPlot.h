//
//  EskLinePlot.h
//  Display the line chart that allow user select the plot symbol on the line
//  and slide left or right and the chart will display the value while the user
//  is moving the line along the chart.
//
//  Created by Ken Wong on 8/9/11.
//  Copyright 2011 Essence Work LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CorePlot-CocoaTouch.h"
#import "ObjSubject.h"

@protocol EskLinePlotDelegate;

@interface EskLine2TouchPlot : NSObject <CPTPlotSpaceDelegate, CPTPlotDataSource, CPTScatterPlotDelegate>
{
  @private
    CPTGraph *graph;
    CPTXYPlotSpace *plotSpace;
    CPTScatterPlot *linePlot;
    CPTScatterPlot *highPlot;
    CPTScatterPlot *touchPlot;
    CPTScatterPlot *touchPlot2;
    CPTScatterPlot *firstTouchedPlot;
    CPTScatterPlot *secondTouchedPlot;
    CPTScatterPlot *objectivePlot;
    CPTScatterPlot *selectedPlot;
    NSUInteger selectedCoordination;
    NSUInteger selected2Coordination;
    BOOL touchPlotSelected;
    BOOL touchPlot2Selected;
    BOOL touchedInPlot;
    BOOL touched2InPlot;
    BOOL isActualPlot;
    NSUInteger firstIndex;
    NSUInteger secondIndex;
    NSMutableArray * arrTouchPoint;
    NSArray * arrSelectedArr;
    CGPoint firstPoint;
    CGPoint secondPoint;
}

@property (nonatomic, retain) id<EskLinePlotDelegate> delegate;
@property (nonatomic, retain) NSMutableArray *sampleData;
@property (nonatomic, retain) NSMutableArray *sampleYears;
@property (nonatomic, retain) NSMutableArray *objectiveDatas;

// Render the chart on the hosting view from the view controller with the default theme.
- (void)renderInLayer:(CPTGraphHostingView *)layerHostingView withTheme:(CPTTheme *)theme;

// Change the select line plot color.
- (void)applyTouchPlotColor;

- (void)loadTheDataWith:(ObjSubject *)objSubject andIsActual:(BOOL)isActual;

- (void)moveFromTouchLayerEvent:(id)event
                        atPoint:(CGPoint)point;

- (void)moveDraggFromTouchLayerEvent:(id)event
                             atPoint:(CGPoint)point;

- (void)moveDraggForSecondFromTouchLayerEvent:(id)event
                             atPoint:(CGPoint)point;
- (void)moveUpFromTouchLayerEvent;
@end


// Delegate to notify the view controller that the location of the line has changed.
@protocol EskLinePlotDelegate <NSObject> 

- (void)eskLine2TouchPlot:(EskLine2TouchPlot *)plot indexLocation:(NSUInteger)index withSecondIndex:(NSUInteger)secIndex andFirstPoint:(CGPoint)fPoint andSecondPoint:(CGPoint)sPoint;

- (void)eskLine2TouchPlot2:(EskLine2TouchPlot *)plot indexLocation:(NSUInteger)index withFirstIndex:(NSUInteger)fristIndex;

@end
