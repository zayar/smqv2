//
//  VSView.h
//  zBox
//
//  Created by Zayar Cn on 5/11/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOAPRequest.h"
#import "DetailImagePopUpView.h"
#import "QuizMenu.h"
#import "PopUpOverlay.h"
#import "ObjQuizz.h"
@protocol QuizViewDelegate
- (void) showQuizPicPhotoBrowser:(NSString *)picURL andTitle:(NSString *)picTitle;
- (void) showQuizSecondView;
- (void) showQuizSecondViewWith:(int)quiz_id;
- (void) onQuizViewCancel;
@end
@interface QuizView : UIView<DetailImagePopUpViewDelegate,UIWebViewDelegate>
{
    //IBOutlet UIWebView * webView;
    id<QuizViewDelegate> owner;
    IBOutlet UIImageView * imgBgView;
    IBOutlet UITableView * tbl;
    DetailImagePopUpView * detailImgPopUpView;
    NSMutableArray * arrSolution;
    SOAPRequest * getQuestionRequest;
    SOAPRequest * rateRequest;
    SOAPRequest * createQuizRequest;
    NSArray *_photos;
    QuizMenu * smqMenuView;
    PopUpOverlay * popUpOverlayView;
    ObjQuizz * objCurrentQuizz;
    UIButton * btnSelectConsult;
}
@property (nonatomic, retain) NSArray *photos;
@property (nonatomic, retain) UITableView *tbl;
@property (assign) id<QuizViewDelegate> owner;
- (void) loadWebView:(NSString *) strURL;
- (IBAction)onCancel:(id)sender;
- (void)vsViewDidLoad;
- (void)vsViewWillAppear;
- (void) vsViewWillDisAppear;
@end
