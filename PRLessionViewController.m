//
//  PRLessionViewController.m
//  zBox
//
//  Created by Zayar on 5/9/13.
//
//

#import "PRLessionViewController.h"
#import "ObjSessionDetail.h"
@interface PRLessionViewController ()

@end

@implementation PRLessionViewController
@synthesize objSession;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSString * strPath = [NSString stringWithFormat:@"%@/img_vs_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];
    [imgBgView release];
}

- (void)viewWillAppear:(BOOL)animated{
    
    [self loadLesson:objSession];
}
- (void)loadLesson:(ObjSubSession *)objSubSe{
    //objLesson = objSubSe.objLesson;
    //lblTopicName.text = objLesson.strArrayTopic;
    ObjSessionDetail * objSecDetail = [objSubSe.arrSessionDetail objectAtIndex:0];
    objLesson =  objSecDetail.objLesson;
    
    lblMarks.text = [NSString stringWithFormat:@"%d",objLesson.actual_score];
    lblCareless.text = [NSString stringWithFormat:@"%d",objLesson.careless_score];
    //float actualScores = [objSubSe.strProgressActualScores floatValue];
    //float carelessScores = [objSubSe.strProgressCarelessScores floatValue];
    //float couldhaveScores = (actualScores + carelessScores);
    lblCouldhave.text = [NSString stringWithFormat:@"%d",objLesson.couldve_score];
    NSLog(@"obj lesson topic %@",objLesson.strArrayTopic);
    
    NSArray * commands = nil;
    NSMutableArray * arrTopic = nil; 
    if( [objLesson.strArrayTopic rangeOfString:@","].location != NSNotFound ){
        
        commands = [objLesson.strArrayTopic componentsSeparatedByString:@","];
        arrTopic = [[NSMutableArray alloc]initWithCapacity:[commands count]];
        //int count = -1;
        for(NSString * strCateName in commands){
            [arrTopic addObject:strCateName];
        }
    }
    
    if([arrTopic count]>0){
        int y=0;
        int i=0;
        for(NSString * str in arrTopic){
            UILabel * lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, y, scrollTopic.frame.size.width, 30)];
            if(i%2 == 0){
                lbl.backgroundColor = [UIColor lightGrayColor];
            }
            else{
                lbl.backgroundColor = [UIColor whiteColor];
            }
            lbl.text = str;
            lbl.font = [UIFont systemFontOfSize:13];
            [scrollTopic addSubview:lbl];
            y += lbl.frame.size.height;
            i++;
            [lbl release];
        }
    }
    
}

- (IBAction)onBack:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)dealloc{
    [objLesson release];
    [imgBgView release];
    [lblTitle release];
    [objSession release];
    [lblTopicName release];
    [lblMarks release];
    [lblCareless release];
    [lblCouldhave release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
