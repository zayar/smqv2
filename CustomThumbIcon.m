//
//  CustomThumbIcon.m
//  zBox
//
//  Created by Zayar Cn on 6/6/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "CustomThumbIcon.h"
#import <QuartzCore/QuartzCore.h>
@implementation CustomThumbIcon
@synthesize owner,imgThumbView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        imgShowdowView = [[UIImageView alloc] initWithFrame:CGRectMake(1,79,89,21)];
		imgShowdowView.hidden = FALSE;
		[imgShowdowView setImage: [UIImage imageNamed:@"img_smq_imageoverlay_showdow.png"]];
		[self addSubview: imgShowdowView];
        
        imgThumbView = [[UIImageView alloc] initWithFrame:CGRectMake(1,9,87,87)];
		imgThumbView.hidden = TRUE;
		//[imgThumbView setImage: [UIImage imageNamed:@"img_smq_imageoverlay_showdow.png"]];
		[self addSubview: imgThumbView];
        imgThumbView.layer.borderColor = [UIColor whiteColor].CGColor;
        imgThumbView.layer.borderWidth = 4.0;
        
        imgOverlayTapeView = [[UIImageView alloc] initWithFrame:CGRectMake(15,0,58,21)];
		imgOverlayTapeView.hidden = FALSE;
		[imgOverlayTapeView setImage: [UIImage imageNamed:@"img_smq_imageoverlay_tape.png"]];
		[self addSubview: imgOverlayTapeView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)customeSetImage:(UIImage *)img{
    imgThumbView.hidden = FALSE;
    [imgThumbView setImage:img];
}

- (void) attachEventHandlers{
	UITapGestureRecognizer* doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
	[doubleTap setNumberOfTapsRequired:2];
	[self addGestureRecognizer:doubleTap];													  
    
	UITapGestureRecognizer *tapping = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
	tapping.delegate = self;
	tapping.numberOfTapsRequired = 1;
	tapping.numberOfTouchesRequired = 1;
	[tapping requireGestureRecognizerToFail:doubleTap];
	[self addGestureRecognizer:tapping];
	[tapping release];
	[doubleTap release];
}

- (void) onTap:(UIGestureRecognizer *)gestureRecognizer{
    [owner onThumbIcon_Click:self];
}

- (void)selectedImageStyle{
    imgThumbView.layer.borderColor = [UIColor brownColor].CGColor;
    imgThumbView.layer.borderWidth = 4.0;
}

- (void)deSelectedImageStyle{
    imgThumbView.layer.borderColor = [UIColor whiteColor].CGColor;
    imgThumbView.layer.borderWidth = 4.0;
}

-(void)dealloc{
    [imgThumbView release];
    [imgOverlayTapeView release];
    [imgShowdowView release];
    [super dealloc];
}

@end
