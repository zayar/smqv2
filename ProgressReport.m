//
//  ProgressReport.m
//  zBox
//
//  Created by Zayar on 3/17/13.
//
//

#import "ProgressReport.h"
#import "zBoxAppDelegate.h"
#import "StringTable.h"
#import "ObjSubject.h"
#import "ObjSubSession.h"
#import "SOAPRequest.h"
#import "quizTableViewCell.h"
#import "ProgressSubjectTableCell.h"
#import <QuartzCore/QuartzCore.h>
#import "ObjExam.h"
#import "ObjPacing.h"
#import "JSONKit.h"
#import "ObjSessionOther.h"
#import "ObjClassFocus.h"
#import "ObjSessionDetail.h"

#import "CPTScatterPlot.h"
#import "CorePlot-CocoaTouch.h"
#import "EskPlotTheme.h"
#import "DemoTableController.h"
#import "EskLine2TouchPlot.h"
#import "ExpandButtonView.h"
#import "StockTextButtonBarView.h"

@implementation ProgressReport
@synthesize owner,lineHostingView,touchLayerView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)progRepoViewDidLoad{
    NSString * strPath = [NSString stringWithFormat:@"%@/img_vs_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];
    [imgBgView release];

    arrSolution = [[NSMutableArray alloc]init];
    arrSession = [[NSMutableArray alloc] init];

    [tbl.layer setCornerRadius:5.0f];
    [tbl.layer setMasksToBounds:YES];
    
    [tblSession.layer setCornerRadius:5.0f];
    [tblSession.layer setMasksToBounds:YES];
    
    // Change gradient color here
    lblBg.backgroundColor = [UIColor colorWithRed:0.21f green:0.27f blue:0.46f alpha:1.00f];
    self.backgroundColor = [UIColor colorWithRed:0.21f green:0.27f blue:0.46f alpha:1.00f];
    
    touchLayerView.delegate = self;
    [self prepareDeltaColor];
    
    [self bringSubviewToFront:tblSession];
    
}

- (void)progRepoViewWillAppear{
    zBoxAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    isExpandButtonShow = FALSE;
    //[delegate clearTheBudgeIcon];
    [self syncProgressReport];
    tbl.tableFooterView = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 10.0f)] autorelease];
    ///hardcode
    /*NSString * strPath = [[NSBundle mainBundle]pathForResource:@"progressreport_jeff" ofType:@"json"];
    NSData * jsonData= [NSData dataWithContentsOfFile:strPath];
    NSDictionary *dics;
    JSONDecoder* decoder = [[JSONDecoder alloc]
                            initWithParseOptions:JKParseOptionNone];
    NSMutableArray * arrTemp = [[NSMutableArray alloc]init];
    dics = [decoder objectWithData:jsonData];
    [delegate hideLoadingScreen];
    NSLog(@"arr count %d",[dics count]);
    int status = [[dics objectForKey:@"status"]intValue];
    NSLog(@"%i count", status);*/
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)  name:UIDeviceOrientationDidChangeNotification  object:nil];
    
    //[self changeToOrientation:[[UIDevice currentDevice] orientation]];
    [self showProtraitThings:YES];
}

- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIDevice currentDevice] orientation]];
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        //load the portrait view
        [self changeToOrientation:orientation];
        
    }
    else if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
    {
        //load the landscape view
        [self changeToOrientation:orientation];
    }
}

- (void)changeToOrientation:(UIInterfaceOrientation) orientation {
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        //load the portrait view
        NSLog(@"load the portrait view");
        
        self.frame = CGRectMake(0, 0, 320, 480);
        [[UIApplication sharedApplication] setStatusBarOrientation:orientation];
        
        if (orientation == UIInterfaceOrientationPortrait) {
            
            [self setTransform:CGAffineTransformMakeRotation(-2*M_PI)];
        }
        else if (orientation == UIInterfaceOrientationPortraitUpsideDown){
            [self setTransform:CGAffineTransformMakeRotation(2*M_PI)];
        }
        self.frame = CGRectMake(0, 0, 480, 320);
        [UIView animateWithDuration:0.5 animations:^(void) {
            imgBgView.alpha = 1;
            btnBack.alpha = 1;
            tbl.alpha = 1;
            tblSession.alpha = 1;
            
            //for landscape
            btnSelector.alpha = 0;
            lineHostingView.alpha = 0;
            lblDiff.alpha = 0;
            touchLayerView.alpha = 0;
            lblCurrentScore.alpha = 0;
            stockBtnBarView.alpha = 0;
            lblCurrentDiff.alpha = 0;
        }];
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
        self.frame = CGRectMake(0, 0, 320, 480);
        tbl.frame = CGRectMake(20, 66, 280, 210);
        tblSession.frame = CGRectMake(20, 275, 280, 174);
        [self showProtraitThings:YES];
        [self bringSubviewToFront:tbl];
        [self bringSubviewToFront:tblSession];
        oreintationChanges = orientation;
        if (isExpandButtonShow) [self onExpandButtonCancel:btnSelector];
    }
    else if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
    {
        //load the landscape view
        NSLog(@"load the landscape view");
        
        /*CGRect frame = self.frame;
        frame.origin.y -= 20;
        frame.size.height += 20;
        self.frame = frame;*/
        
        [self showProtraitThings:NO];
    
        self.frame = CGRectMake(0, 0, 480, 320);
        
        [[UIApplication sharedApplication] setStatusBarOrientation:orientation];
        
        if (orientation == UIInterfaceOrientationLandscapeLeft) {
            [self setTransform:CGAffineTransformMakeRotation(M_PI / -2.0)];
            [touchLayerView setTransform:CGAffineTransformMakeRotation(M_PI / -2.0)];
        }
        else if (orientation == UIInterfaceOrientationLandscapeRight){
            [self setTransform:CGAffineTransformMakeRotation(M_PI / 2.0)];
            [touchLayerView setTransform:CGAffineTransformMakeRotation(M_PI / 2.0)];
        }
        
        self.frame = CGRectMake(0, 0, 320, 480);
        [UIView animateWithDuration:1.0 animations:^(void) {
            imgBgView.alpha = 0;
            btnBack.alpha = 0;
            tbl.alpha = 0;
            tblSession.alpha = 0;
            
            //for landscape
            lineHostingView.alpha = 1;
            btnSelector.alpha = 1;
            lblDiff.alpha = 1;
            touchLayerView.alpha = 1;
            lblCurrentScore.alpha = 1;
            stockBtnBarView.alpha = 1;
            lblCurrentDiff.alpha = 1;
        
            NSLog(@"lineHostingViewCGRect Frame x %f and y %f and self view frame x %f and y: %f",lineHostingView.frame.origin.x,lineHostingView.frame.origin.y,self.frame.origin.x,self.frame.origin.y);
            lineHostingView.frame = CGRectMake(0, 40, 480, 280);
            touchLayerView.frame = CGRectMake(0, 40, 480, 280);
            lblBg.frame = CGRectMake(0, 0, 480, 320);
            [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];
            
            //[self onSelectActual];
        }];
        [self bringSubviewToFront:btnSelector];
        if (orientation == UIInterfaceOrientationLandscapeLeft) {
            lineHostingView.frame = CGRectMake(30, 50, 480, 280);
            touchLayerView.frame = CGRectMake(30, 50, 480, 280);
            lblBg.frame = CGRectMake(20, 0, 480, 320);
            btnSelector.frame = CGRectMake(25 , 5 ,btnSelector.frame.size.width,btnSelector.frame.size.height);
            lblCurrentScore.frame = CGRectMake(310, btnSelector.frame.origin.y, 200, 30);
            lblCurrentDiff.frame = CGRectMake(320, btnSelector.frame.origin.y+lblCurrentScore.frame.size.height, 200, 30);
            lblDiff.frame = CGRectMake(200, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height-5, 120, 30);
            lblActual.frame = CGRectMake(900, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 40, 15);
            lblObjective.frame = CGRectMake(lblActual.frame.origin.x + lblActual.frame.size.width, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
            lblCouldve.frame = CGRectMake(lblObjective.frame.origin.x + lblObjective.frame.size.width, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
            [self showStockBtnBar:orientation];
            [self setCurrentSubjectFrameWith:orientation];
        }
        else if (orientation == UIInterfaceOrientationLandscapeRight){
            lineHostingView.frame = CGRectMake(-10, 50, 480, 280);
            touchLayerView.frame = CGRectMake(-10, 50, 480, 280);
            lblBg.frame = CGRectMake(-20, 0, 480, 320);
            btnSelector.frame = CGRectMake(-10 , 5 ,btnSelector.frame.size.width,btnSelector.frame.size.height);
            lblCurrentScore.frame = CGRectMake(270, btnSelector.frame.origin.y, 200, 30);
            lblCurrentDiff.frame = CGRectMake(280, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 200, 30);
            lblDiff.frame = CGRectMake(160, btnSelector.frame.origin.y+lblCurrentScore.frame.size.height-5, 120, 30);
            lblActual.frame = CGRectMake(900, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
            lblObjective.frame = CGRectMake(20-lblActual.frame.origin.x + lblActual.frame.size.width, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
            lblCouldve.frame = CGRectMake(20-lblObjective.frame.origin.x + lblObjective.frame.size.width, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
            [self showStockBtnBar:orientation];
            [self setCurrentSubjectFrameWith:orientation];
        }
        
        oreintationChanges = orientation;
    }
}


- (void)showProtraitThings:(BOOL)isShow{
    if (isShow) {
        imgBgView.hidden = FALSE;
        btnBack.hidden = FALSE;
        tbl.hidden = FALSE;
        tblSession.hidden = FALSE;
        
        btnSelector.hidden = TRUE;
        capObjective.hidden = TRUE;
        capCouldve.hidden = TRUE;
        capActual.hidden = TRUE;
        lblObjective.hidden = TRUE;
        lblCouldve.hidden = TRUE;
        lblActual.hidden = TRUE;
        lblDiff.hidden = TRUE;
        lineHostingView.hidden = TRUE;
        touchLayerView.hidden = TRUE;
        lblBg.hidden = TRUE;
        lblCurrentScore.hidden = TRUE;
        lblCurrentSubject.hidden = TRUE;
        lblCurrentDiff.hidden = TRUE;
    }
    else{
        imgBgView.hidden = TRUE;
        btnBack.hidden = TRUE;
        tbl.hidden = TRUE;
        tblSession.hidden = TRUE;
        
        btnSelector.hidden = FALSE;
        capObjective.hidden = FALSE;
        capCouldve.hidden = FALSE;
        capActual.hidden = FALSE;
        lblObjective.hidden = FALSE;
        lblCouldve.hidden = FALSE;
        lblActual.hidden = FALSE;
        lblDiff.hidden = FALSE;
        lineHostingView.hidden = FALSE;
        touchLayerView.hidden = FALSE;
        lblBg.hidden = FALSE;
        lblCurrentScore.hidden = FALSE;
        lblCurrentSubject.hidden = FALSE;
        lblCurrentDiff.hidden = FALSE;
    }
}

- (float)loadActualScore:(ObjSubject *)objSub{
    float sessionScore=0.0;
    int totalSession=0;
    float actualScore = 0.0;
    if (objSub !=nil) {
        for (ObjSubSession * objSe in objSub.arrSession) {
            sessionScore += [objSe.strProgressActualScores floatValue];
            totalSession++;
        }
        actualScore= sessionScore/totalSession;
        return actualScore;
    }
    return 0.0;
}

- (void)syncProgressReport{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    [delegate showLoadingScreen];
    delegate.isFromMain = FALSE;
    getProgRepoRequest = [[SOAPRequest alloc] initWithOwner:self];
    getProgRepoRequest.processId = 10;
    [getProgRepoRequest syncProgressReport];
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
    NSMutableArray * arrTemp = [[NSMutableArray alloc]init];
    if (processId == 10) {

        [delegate hideLoadingScreen];
        NSLog(@"arr count %d",[dics count]);
        NSString * strMessage;
        int status = [[dics objectForKey:@"status"]intValue];
        NSLog(@"%i count", status);
        if (status == STATUS_RETURN_RECORD) {
            NSMutableArray * arrSubject = [dics objectForKey:@"subjects"];
            if([arrSubject count] > 0 ) {
                NSLog(@"count : %d", [arrSubject count]);
                for ( int i =0; i < [arrSubject count]; i++) {
                    NSLog(@"%i", i);
                    /*
                     "subject_id": 1,
                     "subject_name": "Odio molestie wisi ipsum vel sed, aliquip vel sit nibh sed, vel facilisis sit blandit. Laoreet, sed et.",
                     "subject_current_actual_score": 72,
                     "subject_difference": 9,
                     "exam_p1": 132,
                     "exam_p2": 166,
                     "exam_p3": 210,
                     "objective": 77,
                     "estimated_potential": "42.32%",
                     */
                    NSDictionary * dicQ = [arrSubject objectAtIndex:i];
                    ObjSubject * objS = [[ObjSubject alloc]init];
                    objS.subject_id = [[dicQ objectForKey:@"subject_id"]intValue];
                    objS.strName = [dicQ objectForKey:@"subject_name"];
                    objS.previousMark = [[dicQ objectForKey:@"subject_previous_score"] floatValue];
                    objS.currentMark = [[dicQ objectForKey:@"subject_current_actual_score"] floatValue];
                    objS.couldveMark = [[dicQ objectForKey:@"subject_couldve_score"] floatValue];
                    objS.differentMark = [[dicQ objectForKey:@"subject_difference"] floatValue];
                    objS.examp_p1 = [[dicQ objectForKey:@"exam_p1"] floatValue];
                    objS.examp_p2 = [[dicQ objectForKey:@"exam_p2"] floatValue];
                    objS.examp_p3 = [[dicQ objectForKey:@"exam_p3"] floatValue];
                    objS.objectiveMark = [[dicQ objectForKey:@"objective"] floatValue];
                    objS.estimated_potential = [[dicQ objectForKey:@"estimated_potential"] floatValue];
                    
                    arrTemp = [dicQ objectForKey:@"array_progress_report"];
                    for ( int y =0; y < [arrTemp count]; y++) {
                        /*
                         {
                         "progress_report_id": 1,
                         "progress_report_session": "Session 21 (Topic volutpat)",
                         "progress_report_date": "2000-11-14",
                         "actual_score": 78,
                         "careless_score": 16,
                         "couldve_score": 83,
                         "progress_report_details": [
                         ]}
                         */
                        
                        NSDictionary * dicSe = [arrTemp objectAtIndex: y];
                        ObjSubSession * objSe = [[ObjSubSession alloc]init];
                        objSe.idx = [[dicSe objectForKey:@"progress_report_id"]intValue];
                        objSe.strName = [dicSe objectForKey:@"progress_report_session"];
                        objSe.strReportDate = [dicSe objectForKey:@"progress_report_date"];
                        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
                        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                        objSe.reportDate = [dateFormatter dateFromString:objSe.strReportDate];
                        [dateFormatter release];
                        NSLog(@"arr temp count %d and objSe id %d and session name %@",[arrTemp count],objSe.idx,objSe.strName);
                        
                        objSe.strProgressActualScores = [dicSe objectForKey:@"actual_score"];
                        objSe.strProgressCarelessScores = [dicSe objectForKey:@"careless_score"];
                        objSe.strProgressCouldveScores = [dicSe objectForKey:@"couldve_score"];
                        
                        NSMutableArray * arrSessionDetail = [dicSe objectForKey:@"progress_report_details"];
                        objSe.arrSessionDetail = [[NSMutableArray alloc]initWithCapacity:[arrSessionDetail count]];
                        for ( int d =0; d < [arrSessionDetail count]; d++) {
                            ObjSessionDetail * objSessionDetail = [[ObjSessionDetail alloc]init];
                            NSDictionary * dicSessionDetail = [arrSessionDetail objectAtIndex: d];
                            
                            NSDictionary * dicDashboard = [dicSessionDetail objectForKey:@"dashboard"];
                            objSessionDetail.objDashBoard.strLesson = [NSString stringWithFormat:@"%d",[[dicDashboard objectForKey:@"lesson"] intValue]];
                            objSessionDetail.objDashBoard.strForgetfulness = [NSString stringWithFormat:@"%d",[[dicDashboard objectForKey:@"attitude"] intValue]];
                            objSessionDetail.objDashBoard.strAttitude = [NSString stringWithFormat:@"%d",[[dicDashboard objectForKey:@"forgetfulness"] intValue]];
                            objSessionDetail.objDashBoard.strMilestone = [NSString stringWithFormat:@"%d",[[dicDashboard objectForKey:@"strategy"] intValue]];
                            objSessionDetail.objDashBoard.strStrategy = [NSString stringWithFormat:@"%d",[[dicDashboard objectForKey:@"milestone"] intValue]];
                            
                            NSDictionary * dicLesson = [dicSessionDetail objectForKey:@"lesson"];
                            objSessionDetail.objLesson.actual_score = [[dicLesson objectForKey:@"actual_score"] intValue];
                            objSessionDetail.objLesson.careless_score = [[dicLesson objectForKey:@"careless_score"] intValue];
                            objSessionDetail.objLesson.couldve_score = [[dicLesson objectForKey:@"couldve_score"] intValue];
                            objSessionDetail.objLesson.strArrayTopic = [dicLesson objectForKey:@"topic"];
                            
                            objSessionDetail.strTodos = [dicSessionDetail objectForKey:@"todo"];
                            
                            NSDictionary * dicOther = [dicSessionDetail objectForKey:@"others"];
                            objSessionDetail.objSessionOther.strNextTopic = [dicOther objectForKey:@"class_focus_next_topic"] ;
                            objSessionDetail.objSessionOther.strNextLesson = [dicOther objectForKey:@"class_focus_next_lesson"] ;
                            objSessionDetail.objSessionOther.strNextMilestone = [dicOther objectForKey:@"class_focus_next_milestone"];
                            objSessionDetail.objSessionOther.strAdmin = [dicOther objectForKey:@"class_focus_admin"];
                            
                            NSDictionary * dicThreePacing = [dicSessionDetail objectForKey:@"three_part_pacing"];
                            objSessionDetail.objThreePartPacing.strAlert = [dicThreePacing objectForKey:@"alert"];
                            objSessionDetail.objThreePartPacing.strMediocre = [dicThreePacing objectForKey:@"mediocre"];
                            objSessionDetail.objThreePartPacing.strGood = [dicThreePacing objectForKey:@"good"];
                            objSessionDetail.objThreePartPacing.strEstimatedScore = [dicThreePacing objectForKey:@"estimated score"];
                            objSessionDetail.objThreePartPacing.strObjective = [dicThreePacing objectForKey:@"objective"];
                            
                            NSDictionary * dicClassFocus = [dicSessionDetail objectForKey:@"class_focus"];
                            objSessionDetail.objClassFocus.strSessionName = [dicClassFocus objectForKey:@"Session {{lorem(1,1)}}"];
                            
                            [objSe.arrSessionDetail addObject:objSessionDetail];
                        }
                        [objS.arrSession addObject:objSe];
                        [objSe release];
                       
                    }
                    
                    [arrSolution addObject:objS];
                    
                    [objS release];
                }
                if ([arrSolution count]>0) {
                    objSelectedSubject = [arrSolution objectAtIndex:0];
                    arrSession = objSelectedSubject.arrSession;
                    NSLog(@"arr session count %d",[arrSession count]);
                    lblCurrentSubject.text = objSelectedSubject.strName;
                    NSLog(@"obj current sub name %@",objSelectedSubject.strName);
                    [tbl reloadData];
                    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                    [self tableView:tbl didSelectRowAtIndexPath:indexPath];
                    [tblSession reloadData];
                }
            } else {
                strMessage = @"No data";
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                [alert release];
            }
        }
        else if(status == STATUS_NO_RECORD_FOUND){
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            strMessage = [dics objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }

    }
}

- (IBAction)onCancel:(id)sender{
    NSLog(@"OnCancel");
    //[self progRepoViewWillAppear];
    [owner onProgressReportViewCancel];
}

- (IBAction)onGraphReport:(id)sender{
    NSLog(@"OnGraph");
    [owner onGraph:objSelectedSubject];
}

- (void)progRepoViewWillDisAppear{
    if ([arrSolution count]>0) {
        [arrSession removeAllObjects];
        [arrSolution removeAllObjects];
    }
    NSLog(@"OnDisappear");
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

////TableView Datasource & Delegate////
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag == 1) {
        return [arrSession count];
    }
    else if (tableView.tag == 0) {
        return [arrSolution count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag == 0) {
        zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
        ObjSubject * objS = [arrSolution objectAtIndex:[indexPath row]];
        static NSString *CellIdentifier = @"ProgressSubjectTableCell";
        ProgressSubjectTableCell *cell = nil;
        UITableViewCell *cellindent = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cellindent == nil) {
            NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ProgressSubjectTableCell" owner:nil options:nil];
            for(id currentObject in topLevelObjects){
                if([currentObject isKindOfClass:[UITableViewCell class]]){
                    cell = (ProgressSubjectTableCell *) currentObject;
                    cell.accessoryView = nil;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;

                    break;
                }
            }
        }

        cell.imgBgView.image = [UIImage imageNamed:@"imb_vs_tablecell_bg"];

        UIFont * cfont = [UIFont fontWithName:@"BradyBunchRemastered" size:24.0];
        UIFont * sfont = [UIFont fontWithName:@"BradyBunchRemastered" size:15.0];
        cell.lblName.font = cfont;
        cell.lblName.textColor = [UIColor whiteColor];
        cell.lblActualScore.font = sfont;
        cell.lblPreviousScore.font = sfont;

        if (objS.strName != (id)[NSNull null] && objS.strName != NULL) {
            cell.lblName.text = objS.strName;
        }
        //cell.lblActualScore.text = [NSString stringWithFormat:@"%0.2f",[self loadActualScore:objS]];

        cell.lblName.shadowColor = [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.5];
        //cell.lblTitle.t
        cell.lblName.shadowOffset = CGSizeMake(2, 2);

        cell.lblActualScore.text = [NSString stringWithFormat:@"%0.2f",objS.currentMark];
        cell.lblActualScore.hidden = FALSE;
        if (objS.differentMark >= 0) {
            cell.lblPreviousScore.backgroundColor = [UIColor greenColor];
        }
        else{
            cell.lblPreviousScore.backgroundColor = [UIColor redColor];
        }
        cell.lblPreviousScore.text = [NSString stringWithFormat:@"%0.2f",objS.differentMark];
        //cell.selectionStyle = UITableViewCellS
        cell.layer.shouldRasterize = YES;
        cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
        cell.lblName.frame = CGRectMake(6, 8, 173, 21);
        return cell;
    }
    else if (tableView.tag == 1) {
        zBoxAppDelegate * delegate =[[UIApplication sharedApplication]delegate];
        ObjSubSession * objS = [arrSession objectAtIndex:[indexPath row]];
        static NSString *CellIdentifier = @"ProgressSubjectTableCell";
        ProgressSubjectTableCell *cell = nil;
        UITableViewCell *cellindent = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cellindent == nil) {
            NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ProgressSubjectTableCell" owner:nil options:nil];
            for(id currentObject in topLevelObjects){
                if([currentObject isKindOfClass:[UITableViewCell class]]){
                    cell = (ProgressSubjectTableCell *) currentObject;
                    cell.accessoryView = nil;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;

                    break;
                }
            }
        }

        UIFont * cfont = [UIFont fontWithName:@"BradyBunchRemastered" size:18.0];
        //UIFont * sfont = [UIFont fontWithName:@"BradyBunchRemastered" size:15.0];
        cell.lblName.font = cfont;
        cell.lblName.textColor = [UIColor blackColor];
        //cell.lblActualScore.font = sfont;
        //cell.lblPreviousScore.font = sfont;

        if (objS.strName != (id)[NSNull null] && objS.strName != NULL) {
            cell.lblName.text = objS.strName;
        }
        NSLog(@"objS name %@",objS.strName);

        //cell.lblName.shadowColor = [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.5];
        //cell.lblTitle.t
        //cell.lblName.shadowOffset = CGSizeMake(2, 2);

       //cell.lblActualScore.text = [NSString stringWithFormat:@"%0.2f",objS.currentMark];
        //cell.lblPreviousScore.text = [NSString stringWithFormat:@"%0.2f",objS.previousMark];
        cell.lblActualScore.hidden = TRUE;
        cell.lblPreviousScore.hidden = TRUE;
        cell.layer.shouldRasterize = YES;
        cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
        cell.lblName.frame = CGRectMake(6, 8, 173+40, 21);
        return cell;
    }
    return nil;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 37;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //ObjAnswer * obj = [arrSolution objectAtIndex:[indexPath row]];
    /*if (![obj.strQuestionStatus isEqualToString:ANSWER_STATUS_PENDING]) {
     if ([obj.strQuestAnsType isEqualToString: ANSWER_TYPE_IMAGE]) {
     [owner showQuizPicPhotoBrowser:obj.strQuestAns andTitle:(NSString *)obj.strQuestionTitle];
     }
     else{
     [self onImageClick:obj andIsImage:FALSE];
     }
     }*/
    //[owner showQuizSecondView];
    if (tableView.tag == 0) {
        ObjSubject * obj = [arrSolution objectAtIndex:[indexPath row]];
        arrSession = obj.arrSession;
        objSelectedSubject = obj;
        [tblSession reloadData];
        
        if (indexPath.row > 0) {
            NSIndexPath * unSelectedIndexPath =[NSIndexPath indexPathForRow:0 inSection:0];
            ProgressSubjectTableCell * cell = (ProgressSubjectTableCell *)[tableView cellForRowAtIndexPath:unSelectedIndexPath];
            [cell.imgBgView setImage:[UIImage imageNamed:@"imb_vs_tablecell_bg"]];
        }
        
        ProgressSubjectTableCell * cell = (ProgressSubjectTableCell *)[tableView cellForRowAtIndexPath:indexPath];
        [cell.imgBgView setImage:[UIImage imageNamed:@"imb_vs_tablecell_bg_highlighted"]];
    }
    else if (tableView.tag ==1){
        ObjSubSession * objSe = [arrSession objectAtIndex:[indexPath row]];
        [owner onSelectSession:objSe withSubject:objSelectedSubject];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    ProgressSubjectTableCell * cell = (ProgressSubjectTableCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell.imgBgView setImage:[UIImage imageNamed:@"imb_vs_tablecell_bg"]];
}

- (void) onSelectActual{
    //[self closeFPPopover];
    if (lineHostingView != nil) {
        for (UIView * v in lineHostingView.subviews) {
            [v removeFromSuperview];
            [v release];
        }
    }
    //barHostingView.hidden = TRUE;
    lineHostingView.hidden =FALSE;
    
    EskPlotTheme *defaultTheme = [[EskPlotTheme alloc] init];
    //CPStocksTheme *defaultTheme = [[CPStocksTheme alloc] init];
    //[self bringSubviewToFront:lineHostingView];
    if (oreintationChanges == UIInterfaceOrientationLandscapeLeft) {
        lineHostingView.frame = CGRectMake(30, 50, 480, 280);
        touchLayerView.frame = CGRectMake(30, 50, 480, 280);
        lblBg.frame = CGRectMake(20, 0, 480, 320);
        btnSelector.frame = CGRectMake(25 , 5 ,btnSelector.frame.size.width,btnSelector.frame.size.height);
        lblCurrentScore.frame = CGRectMake(310, btnSelector.frame.origin.y, 200, 30);
        lblCurrentDiff.frame = CGRectMake(320, btnSelector.frame.origin.y+20, 200, 30);
        lblActual.frame = CGRectMake(900, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 40, 15);
        lblObjective.frame = CGRectMake(lblActual.frame.origin.x + lblActual.frame.size.width, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
        lblCouldve.frame = CGRectMake(lblObjective.frame.origin.x + lblObjective.frame.size.width, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
    }
    else if (oreintationChanges == UIInterfaceOrientationLandscapeRight){
        lineHostingView.frame = CGRectMake(-10, 50, 480, 280);
        touchLayerView.frame = CGRectMake(-10, 50, 480, 280);
        lblBg.frame = CGRectMake(-20, 0, 480, 320);
        btnSelector.frame = CGRectMake(-10 , 5 ,btnSelector.frame.size.width,btnSelector.frame.size.height);
        lblCurrentScore.frame = CGRectMake(270, btnSelector.frame.origin.y, 200, 30);
        lblCurrentDiff.frame = CGRectMake(280, btnSelector.frame.origin.y+20, 200, 30);
        lblActual.frame = CGRectMake(900, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
        lblObjective.frame = CGRectMake(20-lblActual.frame.origin.x + lblActual.frame.size.width, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
        lblCouldve.frame = CGRectMake(20-lblObjective.frame.origin.x + lblObjective.frame.size.width, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
    }
    if (linePlot2 == nil) {
        linePlot2 = [[EskLine2TouchPlot alloc] init];
    }
    [linePlot2 loadTheDataWith:objSelectedSubject andIsActual:YES];
    linePlot2.delegate = self;
    [linePlot2 renderInLayer:lineHostingView withTheme:defaultTheme];
    lblCurrentScore.hidden = FALSE;
    [self setStringToCurrentScore:[NSString stringWithFormat:@"Current Score: %0.2f (%0.2f)",objSelectedSubject.currentMark,objSelectedSubject.differentMark]];
     //s[self setStringToCurrentDiff:[NSString stringWithFormat:@"Current Diff: %0.2f",objSelectedSubject.differentMark]];
    [defaultTheme release];
    [self bringToFrontView:lineHostingView];
    [self showTheLabels:NO];
    lblDiff.text = @"";
    //lblDiff.hidden = FALSE;
    lblActual.hidden = FALSE;
    lblObjective.hidden = FALSE;
    lblCouldve.hidden = TRUE;
    lblActual.text = @"Actual";
    lblDiff.hidden = TRUE;
}

- (void)bringToFrontView:(UIView *)v{
    [self bringSubviewToFront:v];
    [self bringSubviewToFront:capActual];
    [self bringSubviewToFront:capCouldve];
    [self bringSubviewToFront:capObjective];
    [self bringSubviewToFront:lblActual];
    [self bringSubviewToFront:lblObjective];
    [self bringSubviewToFront:lblCouldve];
    [self bringSubviewToFront:lblDiff];
    [self bringSubviewToFront:btnSelector];
    [self bringSubviewToFront:lblCurrentScore];
    [self bringSubviewToFront:lblCurrentSubject];
    [self bringSubviewToFront:lblCurrentDiff];
    [self bringSubviewToFront:touchLayerView];
    
}

- (void) onSelectCouldve{
    //[self closeFPPopover];
    if (lineHostingView != nil) {
        for (UIView * v in lineHostingView.subviews) {
            [v removeFromSuperview];
            [v release];
        }
    }
    //barHostingView.hidden = TRUE;
    lineHostingView.hidden =FALSE;
    touchLayerView.hidden = FALSE;
    
    EskPlotTheme *defaultTheme = [[EskPlotTheme alloc] init];
    //CPStocksTheme *defaultTheme = [[CPStocksTheme alloc] init];
    
    if (oreintationChanges == UIInterfaceOrientationLandscapeLeft) {
        lineHostingView.frame = CGRectMake(30, 50, 480, 280);
        touchLayerView.frame = CGRectMake(30, 50, 480, 280);
        lblBg.frame = CGRectMake(20, 0, 480, 320);
        btnSelector.frame = CGRectMake(25 , 5 ,btnSelector.frame.size.width,btnSelector.frame.size.height);
        lblCurrentScore.frame = CGRectMake(310, btnSelector.frame.origin.y, 200, 30);
        lblActual.frame = CGRectMake(900, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 40, 15);
        lblObjective.frame = CGRectMake(lblActual.frame.origin.x + lblActual.frame.size.width, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
        lblCouldve.frame = CGRectMake(lblObjective.frame.origin.x + lblObjective.frame.size.width, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
    }
    else if (oreintationChanges == UIInterfaceOrientationLandscapeRight){
        lineHostingView.frame = CGRectMake(-10, 50, 480, 280);
        touchLayerView.frame = CGRectMake(-10, 50, 480, 280);
        lblBg.frame = CGRectMake(-20, 0, 480, 320);
        btnSelector.frame = CGRectMake(-10 , 5 ,btnSelector.frame.size.width,btnSelector.frame.size.height);
        lblCurrentScore.frame = CGRectMake(270, btnSelector.frame.origin.y, 200, 30);
        lblActual.frame = CGRectMake(900, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
        lblObjective.frame = CGRectMake(20-lblActual.frame.origin.x + lblActual.frame.size.width, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
        lblCouldve.frame = CGRectMake(20-lblObjective.frame.origin.x + lblObjective.frame.size.width, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
    }
    if (linePlot2 == nil) {
        linePlot2 = [[EskLine2TouchPlot alloc] init];
    }
    //[self bringToFrontView:lineHostingView];
    [linePlot2 loadTheDataWith:objSelectedSubject andIsActual:NO];
    linePlot2.delegate = self;
    [linePlot2 renderInLayer:lineHostingView withTheme:defaultTheme];
    lblCurrentScore.hidden = FALSE;
    [self setStringToCurrentScore:[NSString stringWithFormat:@"Current Score: %0.2f (%0.2f)",objSelectedSubject.currentMark,objSelectedSubject.differentMark]];
    //[self setStringToCurrentDiff:[NSString stringWithFormat:@"Current Score: %0.2f",objSelectedSubject.previousMark]];
    [defaultTheme release];
    [self showTheLabels:NO];
    lblDiff.text = @"";
    [self bringToFrontView:lineHostingView];
    lblObjective.hidden = FALSE;
    lblCouldve.hidden = TRUE;
    lblActual.hidden = FALSE;
    lblActual.text = @"Couldve";

    lblDiff.hidden = TRUE;
}

- (void) onSelectActualAndCouldve{
    //[self closeFPPopover];
    if (lineHostingView != nil) {
        for (UIView * v in lineHostingView.subviews) {
            [v removeFromSuperview];
            [v release];
        }
    }
    lineHostingView.hidden =FALSE;
    //barHostingView.hidden = FALSE;
    EskPlotTheme *defaultTheme = [[EskPlotTheme alloc] init];
    //CPStocksTheme *defaultTheme = [[CPStocksTheme alloc] init];
    
    if (oreintationChanges == UIInterfaceOrientationLandscapeLeft) {
        lineHostingView.frame = CGRectMake(30, 50, 480, 280);
        touchLayerView.frame = CGRectMake(30, 50, 480, 280);
        lblBg.frame = CGRectMake(20, 0, 480, 320);
        btnSelector.frame = CGRectMake(25 , 5 ,btnSelector.frame.size.width,btnSelector.frame.size.height);
        lblCurrentScore.frame = CGRectMake(310, btnSelector.frame.origin.y, 200, 30);
        lblActual.frame = CGRectMake(900, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 40, 15);
        lblObjective.frame = CGRectMake(lblActual.frame.origin.x + lblActual.frame.size.width, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
        lblCouldve.frame = CGRectMake(lblObjective.frame.origin.x + lblObjective.frame.size.width, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
    }
    else if (oreintationChanges == UIInterfaceOrientationLandscapeRight){
        lineHostingView.frame = CGRectMake(-10, 50, 480, 280);
        touchLayerView.frame = CGRectMake(-10, 50, 480, 280);
        lblBg.frame = CGRectMake(-20, 0, 480, 320);
        btnSelector.frame = CGRectMake(-10 , 5 ,btnSelector.frame.size.width,btnSelector.frame.size.height);
        lblCurrentScore.frame = CGRectMake(270, btnSelector.frame.origin.y, 200, 30);
        lblActual.frame = CGRectMake(900, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
        lblObjective.frame = CGRectMake(20-lblActual.frame.origin.x + lblActual.frame.size.width, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
        lblCouldve.frame = CGRectMake(20-lblObjective.frame.origin.x + lblObjective.frame.size.width, lblCurrentScore.frame.origin.y+lblCurrentScore.frame.size.height, 50, 15);
    }
    if (linePlot == nil) {
        linePlot = [[EskLinePlot alloc] init];
    }
    
    [linePlot loadTheDataWith:objSelectedSubject];
    linePlot.delegate = self;
    [linePlot renderInLayer:lineHostingView withTheme:defaultTheme];
    /*barPlot = [[EskBarPlot alloc] init];
     barPlot.delegate = self;
     [barPlot renderInLayer:barHostingView withTheme:defaultTheme];*/
    [self bringToFrontView:lineHostingView];
    [defaultTheme release];
    [self showTheLabels:YES];
    lblDiff.hidden = TRUE;
    lblCurrentScore.hidden = FALSE;
    lblActual.hidden = FALSE;
    lblCouldve.hidden =FALSE;
    lblObjective.hidden= FALSE;
    lblActual.text = @"Actual";
    [self bringSubviewToFront:lineHostingView];
    lblDiff.hidden = TRUE;
}

- (void)prepareDeltaColor{
    lblActual.text = @"Actual";
    lblActual.backgroundColor = [UIColor whiteColor];
    lblActual.textColor = [UIColor blackColor];
    lblActual.font = [UIFont systemFontOfSize:11];
    lblActual.textAlignment = UITextAlignmentCenter;
    
    lblCouldve.text = @"Couldve";
    lblCouldve.backgroundColor = [UIColor yellowColor];
    lblCouldve.textColor = [UIColor blackColor];
    lblCouldve.font = [UIFont systemFontOfSize:11];
    lblCouldve.textAlignment = UITextAlignmentCenter;
    
    lblObjective.text = @"Objective";
    lblObjective.backgroundColor = [UIColor greenColor];
    lblObjective.textColor = [UIColor blackColor];
    lblObjective.font = [UIFont systemFontOfSize:11];
    lblObjective.textAlignment = UITextAlignmentCenter;
}

- (void) setStringToCurrentScore:(NSString *)str{
    lblCurrentScore.text = str;
}

- (void) setStringToCurrentDiff:(NSString *)str{
    lblCurrentDiff.text = str;
}

- (void) setCurrentSubjectFrameWith:(UIInterfaceOrientation)orientation{
     if (orientation == UIInterfaceOrientationLandscapeLeft) {
         lblCurrentSubject.frame = CGRectMake(30, 0, 280, 30);
     }
    if (orientation == UIInterfaceOrientationLandscapeRight) {
        lblCurrentSubject.frame = CGRectMake(10, 0, 280, 30);
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

// view for graph
#pragma mark - LinePlot delegate
- (void)linePlot:(EskLinePlot *)plot indexLocation:(NSUInteger)index
{
    /*selectedYearLabel.text = [NSString stringWithFormat:@"Selected Year: %@", [plot.sampleYears objectAtIndex:index]];
     spendingPerYearLabel.text = [NSString stringWithFormat:@"Spending Per Year: $%@", [plot.sampleData objectAtIndex:index]];*/
    [self showResultLabel:[plot.sampleData objectAtIndex:index] andHide:YES];
    
}

- (void)eskLine2TouchPlot2:(EskLine2TouchPlot *)plot indexLocation:(NSUInteger)index withFirstIndex:(NSUInteger)fristIndex{
    NSLog(@"here call back 2 !!and sec %@ and fir %@",[plot.sampleData objectAtIndex:index],[plot.sampleData objectAtIndex:fristIndex]);
    
    //int fristScore=[[plot.sampleData objectAtIndex:fristIndex] intValue];
    //int secScore = [[plot.sampleData objectAtIndex:index] intValue];
    //int result = fristScore - secScore;
    //[self showResultLabel:[NSString stringWithFormat:@"%d",result] andHide:YES];
    
}

- (void)eskLine2TouchPlot:(EskLine2TouchPlot *)plot indexLocation:(NSUInteger)index withSecondIndex:(NSUInteger)secIndex andFirstPoint:(CGPoint)fPoint andSecondPoint:(CGPoint)sPoint{
    //NSLog(@"here call back 1 !!and sec %@ and fir %@",[plot.sampleData objectAtIndex:secIndex],[plot.sampleData objectAtIndex:index]);
    NSLog(@"here is first point: %f and sec point: %f",fPoint.x,sPoint.x);
    int fristScore;
    int secScore;
    if (fPoint.x>sPoint.x) {
        fristScore =[[plot.sampleData objectAtIndex:secIndex] intValue];
        secScore = [[plot.sampleData objectAtIndex:index]intValue];

    }
    else {
        fristScore =[[plot.sampleData objectAtIndex:index] intValue];
        secScore = [[plot.sampleData objectAtIndex:secIndex]intValue];
    }
    
    int result = fristScore - secScore;
    [self showResultLabel:[NSString stringWithFormat:@"%d",result] andHide:YES];
}

- (void)showResultLabel:(NSString *)str andHide:(BOOL)show{
    if (show) {
        lblDiff.hidden = FALSE;
        lblDiff.text = [NSString stringWithFormat:@"Diff is %@",str];
        int resultInt = [str intValue];
        if (resultInt == 0) {
            [self changeDiffPositiveColor:1];
        }
        else if(resultInt>0){
            [self changeDiffPositiveColor:2];
        }
        else [self changeDiffPositiveColor:3];
    }
    else{
        lblDiff.hidden = TRUE;
    }
}

- (void)changeDiffPositiveColor:(int) i{
    if (i == 1) {
        lblDiff.backgroundColor = [UIColor whiteColor];
        lblDiff.textColor = [UIColor blackColor];
    }
    else if (i == 2){
        lblDiff.backgroundColor = [UIColor whiteColor];
        lblDiff.textColor = [UIColor greenColor];
    }
    else if (i == 3){
        lblDiff.backgroundColor = [UIColor whiteColor];
        lblDiff.textColor = [UIColor redColor];
    }
}


- (void) showTheLabels:(BOOL) show{
    if (show) {
        capActual.hidden = FALSE;
        capCouldve.hidden = FALSE;
        capObjective.hidden = FALSE;
        lblActual.hidden = FALSE;
        lblCouldve.hidden = FALSE;
        lblObjective.hidden = FALSE;
        lblDiff.hidden = TRUE;
    }
    else{
        capActual.hidden = TRUE;
        capCouldve.hidden = TRUE;
        capObjective.hidden = TRUE;
        lblActual.hidden = TRUE;
        lblCouldve.hidden = TRUE;
        lblObjective.hidden = TRUE;
        lblDiff.hidden = FALSE;
    }
}

//Expand button
- (void)showExpandButtonView:(UIButton *)btn{
    //NSString * strURL = @"http://www.youtube.com/watch?v=Vetg7vWitTU&feature=related";
    //http://youtu.be/lgT1AidzRWM
    if( expandButtonView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ExpandButtonView" owner:nil options:nil];
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[ExpandButtonView class]]){
				expandButtonView = (ExpandButtonView *) currentObject;
				//[expandButtonView vsViewDidLoad];
				[self addSubview:expandButtonView];
				break;
			}
		}
	}
    //[qView vsViewWillAppear];
    expandButtonView.hidden = FALSE;
    expandButtonView.owner = self;
    expandButtonView.frame = CGRectMake(-expandButtonView.frame.size.width , btn.frame.origin.y, expandButtonView.frame.size.width, expandButtonView.frame.size.height);
    [self bringSubviewToFront: expandButtonView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    expandButtonView.frame = CGRectMake(btn.frame.origin.x+btn.frame.size.width, btn.frame.origin.y, expandButtonView.frame.size.width, expandButtonView.frame.size.height);
    [UIView commitAnimations];
    isExpandButtonShow = TRUE;
}

-(void) onExpandButtonCancel:(UIButton *)btn{
    expandButtonView.frame = CGRectMake(btn.frame.origin.x+btn.frame.size.width, btn.frame.origin.y, expandButtonView.frame.size.width, expandButtonView.frame.size.height);
    [self bringSubviewToFront: expandButtonView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    expandButtonView.frame = CGRectMake(-expandButtonView.frame.size.width , btn.frame.origin.y, expandButtonView.frame.size.width, expandButtonView.frame.size.height);
    [UIView commitAnimations];
    [self performSelector:(@selector(closeExpandButtonView)) withObject:nil afterDelay:0.4];
    isExpandButtonShow = FALSE;
}

-(void) closeExpandButtonView{
    expandButtonView.hidden = TRUE;
}

- (IBAction)onSelector:(UIButton *)sender{
    if (isExpandButtonShow)
        [self onExpandButtonCancel:sender];
    else [self showExpandButtonView:sender];
}

- (void) onCouldveSelected:(NSString *)strImage{
    [btnSelector setTitle:@"Couldve" forState:normal];
    if (isExpandButtonShow)[self onExpandButtonCancel:btnSelector];
    [self onSelectCouldve];
}

- (void) onActualSelected:(NSString *)strImage{
    [btnSelector setTitle:@"Actual" forState:normal];
    if (isExpandButtonShow)[self onExpandButtonCancel:btnSelector];
    [self onSelectActual];
}

- (void) onActualCouldveSelected:(NSString *)strImage{
    [btnSelector setTitle:@"Both" forState:normal];
    if (isExpandButtonShow)[self onExpandButtonCancel:btnSelector];
    [self onSelectActualAndCouldve];
}

- (void) showStockBtnBar:(UIInterfaceOrientation)orientation{
    if( stockBtnBarView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"StockTextButtonBarView" owner:nil options:nil];
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[StockTextButtonBarView class]]){
				stockBtnBarView = (StockTextButtonBarView *) currentObject;
				//[expandButtonView vsViewDidLoad];
				[self addSubview:stockBtnBarView];
				break;
			}
		}
    
	}
    
    if (orientation == UIInterfaceOrientationLandscapeLeft) {
        stockBtnBarView.frame = CGRectMake(145, 15, stockBtnBarView.frame.size.width, stockBtnBarView.frame.size.height);
    }
    else if(orientation == UIInterfaceOrientationLandscapeRight){
        stockBtnBarView.frame = CGRectMake(125, 15, stockBtnBarView.frame.size.width, stockBtnBarView.frame.size.height);
    }
    stockBtnBarView.owner = self;
    [stockBtnBarView setDefaultSelector];
    [stockBtnBarView setHidden:FALSE];
}

//////// Touch Layer Delegate ///////
- (void)myTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    lblDiff.hidden = FALSE;
    stockBtnBarView.hidden = TRUE;
    lblCurrentSubject.hidden = TRUE;
    lblCurrentScore.hidden = TRUE;
    lblCurrentDiff.hidden = TRUE;
    if (touches.count == 1) {
        UITouch *touch = (UITouch *)[[touches allObjects] objectAtIndex:0];
        CGPoint point = [touch locationInView:lineHostingView];
        
        //NSLog(@"Touch View::: x:%.1f y:%.1f w:%.1f h:%.1f",touchLayerView.frame.origin.x,touchLayerView.frame.origin.y,touchLayerView.frame.size.width,touchLayerView.frame.size.height);
        //[self plotSpace:self.plotSpace shouldHandlePointingDeviceDraggedEvent:event atPoint:point];
        //[self bringSubviewToFront:lineHostingView];
        [linePlot2 moveFromTouchLayerEvent:event atPoint:point];
    }
    if (touches.count == 2) {
        //UITouch *touch = (UITouch *)[[touches allObjects] objectAtIndex:1];
        //CGPoint point = [touch locationInView:nil];
        //NSLog(@"dragging touch 2!!!!!");
        //[self plotSpace:self.plotSpace shouldHandlePointingDeviceDraggedEvent:event atPoint:point];
        //[self bringSubviewToFront:touchLayerView];
    }
}

- (void)myTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    
    if (touches.count == 1) {
        //UITouch *touch = (UITouch *)[[touches allObjects] objectAtIndex:0];
        //CGPoint point = [touch locationInView:lineHostingView];
        //[linePlot2 moveDraggFromTouchLayerEvent:event atPoint:point];
        //NSLog(@"dragging touch 1!!!!!");
        
        //[self plotSpace:self.plotSpace shouldHandlePointingDeviceDraggedEvent:event atPoint:point];
    }
    if (touches.count == 2) {
        lblDiff.hidden = FALSE;
        UITouch *touch1 = (UITouch *)[[touches allObjects] objectAtIndex:0];
        UITouch *touch2 = (UITouch *)[[touches allObjects] objectAtIndex:1];
        CGPoint point1 = [touch1 locationInView:lineHostingView];
        CGPoint point2 = [touch2 locationInView:lineHostingView];
        
        [linePlot2 moveDraggFromTouchLayerEvent:event atPoint:point1];
        [linePlot2 moveDraggForSecondFromTouchLayerEvent:event atPoint:point2];
        //NSLog(@"dragging touch 2!!!!! x:%.1f y:%.1f",point.x,point.y);
        //[self plotSpace:self.plotSpace shouldHandlePointingDeviceDraggedEvent:event atPoint:point];
    }
}

- (void)myTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    //UITouch *touch1 = (UITouch *)[[touches allObjects] objectAtIndex:0];
    //UITouch *touch2 = (UITouch *)[[touches allObjects] objectAtIndex:1];
    //CGPoint point1 = [touch1 locationInView:lineHostingView];
    //CGPoint point2 = [touch2 locationInView:lineHostingView];
    
    [linePlot2 moveUpFromTouchLayerEvent];
    lblDiff.hidden = TRUE;
    stockBtnBarView.hidden = FALSE;
    lblCurrentSubject.hidden = FALSE;
    lblCurrentScore.hidden = FALSE;
    lblCurrentDiff.hidden = FALSE;
}
//////// End Touch Layer Delegate ///////

- (void)dealloc{
    [lblCurrentScore release];
    [lblCurrentDiff release];
    [stockBtnBarView release];
    [touchLayerView release];
    [capObjective release];
    [capCouldve release];
    [capActual release];
    [lblObjective release];
    [lblCouldve release];
    [lblActual release];
    [lineHostingView release];
    [linePlot release];
    [super dealloc];
}

@end
